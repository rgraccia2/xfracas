﻿using Compass.XFracas.WebGerenciadorDAL;
using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorBL
{
    public class RulesBL
    {
        public   List<RuleModel> GetRules()
        {
            RuleDAO d = new RuleDAO();

            return d.GetRules();
        }

    }
}
