﻿using Compass.XFracas.WebGerenciadorDAL;
using Compass.XFracas.WebGerenciadorInterface.Interfaces;
using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorBL
{
    public class UsuarioBL : UsuarioInterface
    {
        public void AtribuirPerfilUsuario(string CodUsuario, int CodPerfil)
        {
            UsuarioDAL obj = new UsuarioDAL();
            obj.AtribuirPerfilUsuario(CodUsuario, CodPerfil);
        }

        public bool Authenticar(string CodUsuario, string Tipo, string Senha)
        {
            UsuarioDAL obj = new UsuarioDAL();
            return obj.Authenticar(CodUsuario, Tipo, Senha);
        }

        public void ExcluirPerfilUsuario(int Id_Usuario_Perfil)
        {
            UsuarioDAL obj = new UsuarioDAL();
            obj.ExcluirPerfilUsuario(Id_Usuario_Perfil);
        }

        public List<Usuario> ListarUsuariosComPerfil()
        {
            UsuarioDAL obj = new UsuarioDAL();
            return obj.ListarUsuariosComPerfil();
        }

        public void UpdateUsuarioAtivo(int Id_Usuario_Perfil, string ativo)
        {
            UsuarioDAL obj = new UsuarioDAL();
            obj.UpdateUsuarioAtivo(Id_Usuario_Perfil, ativo);
        }

        public bool UsuarioExiste(string CodUsuario)
        {
            UsuarioDAL obj = new UsuarioDAL();
            return obj.UsuarioExiste(CodUsuario);
        }

       

      
    

    }
}
