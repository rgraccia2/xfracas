﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class FailureItemModel
    {
        public int CODE { get; set; }
        public string NAME { get; set; }
        public int TYPE { get; set; }
        public int ANCESTOR { get; set; }

        public FailureItemModel(int code, string name, int type, int ancestor)
        {
            CODE = code;
            NAME = name;
            TYPE = type;
            ANCESTOR = ancestor;
        }

        public FailureItemModel()
        {
        }

        public Dictionary<string, object> dic { get; set; } = new Dictionary<string, object>();
    }
}
