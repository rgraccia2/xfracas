﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class MaintenanceNoteModel
    {
        public decimal action_id { get; set; }
        public string maintenanceNoteCode { get; set; }
        public string maintenanceNoteStatus { get; set; }
    }
}
