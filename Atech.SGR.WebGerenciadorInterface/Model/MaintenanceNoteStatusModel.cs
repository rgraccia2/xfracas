﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class MaintenanceNoteStatusModel
    {
        public decimal detail_id_status_nota { get; set; }
        public string status_nota { get; set; }
        public string numero_nota { get; set; }
    }
}
