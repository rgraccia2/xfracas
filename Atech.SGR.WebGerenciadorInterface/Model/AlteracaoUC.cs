﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atech.SGR.WebGerenciadorInterface.Model
{
    public class AlteracaoUC
    {
        public string num_uc { get; set; }
        public string nome_completo { get; set; }
        public string num_medidor { get; set; }
        public string uc_atual { get; set; }
        public string nome_atual { get; set; }
        public string medidor_atual { get; set; }
    }
}
