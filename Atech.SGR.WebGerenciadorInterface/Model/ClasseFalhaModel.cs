﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class ClasseFalhaModel
    {
        public string DESC_PERFILCATALOGO { get; set; } = "";
        public string DESC_SISTEMA { get; set; } = "";
        public string DESC_CONJUNTO { get; set; } = "";
        public string DESC_ITEM { get; set; } = "";
        public string DESC_PROBLEMA { get; set; } = "";
        public string DESC_SOLUCAO { get; set; } = "";
        public DateTime DATA_ULTIMAATUALIZACAO { get; set; }
        public DateTime DATA_CRIACAO { get; set; }
        public string IDIOMA { get; set; } = "";

        public override string ToString()
        {
            return string.Join("\t", new[] { DESC_PERFILCATALOGO, DESC_SISTEMA, DESC_CONJUNTO, DESC_ITEM, DESC_PROBLEMA, DESC_SOLUCAO, DATA_ULTIMAATUALIZACAO, (object)DATA_CRIACAO, IDIOMA });
        }
    }
}
