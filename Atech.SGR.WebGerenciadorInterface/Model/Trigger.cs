﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class RuleModel
    {
        public string hierarchy { get; set; }
        public bool enabled { get; set; }
        public string expression { get; set; }
        public decimal recidivism { get; set; }
    }

    public class Trigger
    {
        public string Id { get; set; }

        public string hierarquia { get; set; }

        public bool ativo { get; set; }

        public string expressao { get; set; }

        public int recorrencia_dias  { get; set; }



    }

   

}
