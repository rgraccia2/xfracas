﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class IncidentModel
    {
        public string COD_ENTIDADE { get; set; } = "";

        public decimal INDICADOR_PARADA { get; set; } = 0;
        public string NUMERO_ORDEM { get; set; } = "";
        public string DESC_LOCALINSTALACAO { get; set; } = "";
        public string TIPO_ORDEM { get; set; } = "";
        public string COD_PLANTA { get; set; } = "";
        public string COD_LOCALINSTALACAO { get; set; } = "";
        public string COD_NOTA { get; set; } = "";
        public string COD_PERFILCATALOGO { get; set; } = "";
        public string TIPO_NOTA { get; set; } = "";
        public string COD_EQUIPAMENTO { get; set; } = "";
        public string COD_PLANTACENTROTRABALHO { get; set; } = "";
        public string COD_CENTROTRABALHO { get; set; } = "";
        public string COD_PLANTAGRUPOPLANEJAMENTO { get; set; } = "";
        public string COD_GRUPOPLANEJAMENTO { get; set; } = "";
        public string DESC_PRIORIDADE { get; set; } = "";
        public double TEMPO_DURACAOPARADA { get; set; } = 0;
        public string DESC_SISTEMA { get; set; } = "";
        public string DESC_CONJUNTO { get; set; } = "";
        public string DESC_ITEM { get; set; } = "";
        public string DESC_PROBLEMA { get; set; } = "";
        public string DESC_SOLUCAO { get; set; } = "";
        public int KM_INICIO { get; set; } = 0;
        public int KM_FIM { get; set; } = 0;
        public string DESC_EQUIPAMENTO { get; set; } = "";
        public string DESC_CURTA { get; set; } = "";
        public string DESC_LONGA { get; set; } = "";
        // Public Property DATA_CRIACAO As Date = Nothing
        public string COD_INDICADORABC { get; set; } = "";
        public double VALOR_CUSTOPLANEJADO { get; set; } = 0;
        public double VALOR_CUSTOREAL { get; set; } = 0;
        public string COD_IDENTIFLOCALEQUIP { get; set; } = "";
        public string COD_UNIDADEPARADA { get; set; } = "";
        public string COD_STATUSORDEM { get; set; } = "";

        private DateTime _DATA_ABERTURANOTA = DateTime.MinValue;
        private DateTime _DATA_FECHAMENTONOTA = DateTime.MinValue;
        private DateTime _DATA_INICIOPARADA = DateTime.MinValue;
        private DateTime _DATA_FIMPARADA = DateTime.MinValue;
        private DateTime _DATA_ULTIMAATUALIZACAO = DateTime.MinValue;
        private DateTime _DATA_INICIOGARANTIAFABRICANTE = DateTime.MinValue;
        private DateTime _DATA_FIMGARANTIAFABRICANTE = DateTime.MinValue;
        private static DateTime DaylightIni;
        private static DateTime DaylightEnd;

        public static void setBraziliamDayLightSavingDates(DateTime dataini, DateTime datafim)
        {
            DaylightIni = dataini;
            DaylightEnd = datafim;
        }

        private int checkTimeZoneActionValue(DateTime dataRef)
        {
            if (dataRef > DaylightIni & dataRef < DaylightEnd)
            {
                return 2;
            }

            return 3;
        }

        public DateTime DATA_ABERTURANOTA
        {
            get
            {
                return _DATA_ABERTURANOTA;
            }

            set
            {
                _DATA_ABERTURANOTA = value.AddHours(checkTimeZoneActionValue(value));
            }
        }

        public DateTime DATA_FECHAMENTONOTA
        {
            get
            {
                return _DATA_FECHAMENTONOTA;
            }

            set
            {
                _DATA_FECHAMENTONOTA = value.AddHours(checkTimeZoneActionValue(value));
            }
        }

        public DateTime DATA_INICIOPARADA
        {
            get
            {
                return _DATA_INICIOPARADA;
            }

            set
            {
                _DATA_INICIOPARADA = value.AddHours(checkTimeZoneActionValue(value));
            }
        }

        public DateTime DATA_FIMPARADA
        {
            get
            {
                return _DATA_FIMPARADA;
            }

            set
            {
                _DATA_FIMPARADA = value.AddHours(checkTimeZoneActionValue(value));
            }
        }

        public DateTime DATA_ULTIMAATUALIZACAO
        {
            get
            {
                return _DATA_ULTIMAATUALIZACAO;
            }

            set
            {
                _DATA_ULTIMAATUALIZACAO = value.AddHours(checkTimeZoneActionValue(value));
            }
        }

        public DateTime DATA_INICIOGARANTIAFABRICANTE
        {
            get
            {
                return _DATA_INICIOGARANTIAFABRICANTE;
            }

            set
            {
                _DATA_INICIOGARANTIAFABRICANTE = value.AddHours(checkTimeZoneActionValue(value));
            }
        }

        public DateTime DATA_FIMGARANTIAFABRICANTE
        {
            get
            {
                return _DATA_FIMGARANTIAFABRICANTE;
            }

            set
            {
                _DATA_FIMGARANTIAFABRICANTE = value.AddHours(checkTimeZoneActionValue(value));
            }
        }
    }
}
