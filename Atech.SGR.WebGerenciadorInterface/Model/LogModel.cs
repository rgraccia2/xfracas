﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class LogModel
    {
        public DateTime Date { get; set; }
        public string GuiID { get; set; }
        public string Application_Code { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public string Method { get; set; }
        public int Check_Point { get; set; }
    }
}
