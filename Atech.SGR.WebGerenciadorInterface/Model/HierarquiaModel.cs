﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class HierarquiaModel
    {
        public string COD_ENTIDADE { get; set; }

        public string COD_LOCALINSTALACAO { get; set; }

        public int DESC_LOCALINSTALACAO { get; set; }

        public string COD_EQUIPAMENTO { get; set; }

        public string DESC_EQUIPAMENTO { get; set; }

        public string STATUS_SISTEMA { get; set; }
    }
}
