﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class Usuario
    {
        public string Nome { get; set; }

        public string CodigoUser { get; set; }

        public int Id_Usuario_Perfil { get; set; }

        public string DescricaoPerfil { get; set; }

        public string DataCadastro { get; set; }

        public string Ativo { get; set; }


    }

   

}
