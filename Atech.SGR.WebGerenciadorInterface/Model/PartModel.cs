﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class PartModel
    {
        public Decimal part_id { get; set; }

        public string part_name { get; set; }

        public string part_version { get; set; }

        public string part_number { get; set; }

    }
}
