﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorInterface.Model
{
    public class PartTreeModel
    {
        public string part_number { get; set; }
        public string path { get; set; }
        public decimal id { get; set; }
        public decimal parent_id { get; set; }
        public string part_version { get; set; }
    }
}
