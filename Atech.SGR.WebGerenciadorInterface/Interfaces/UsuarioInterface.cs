﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System.Collections.Generic;

namespace Compass.XFracas.WebGerenciadorInterface.Interfaces
{
    public interface UsuarioInterface
    {
        void AtribuirPerfilUsuario(string CodUsuario, int CodPerfil);

        void ExcluirPerfilUsuario(int ID);

        void UpdateUsuarioAtivo(int Id_Usuario_Perfil, string ativo);

        bool Authenticar(string CodUsuario, string Tipo, string Senha);

        List<Usuario> ListarUsuariosComPerfil();

        bool UsuarioExiste(string CodUsuario);
    }
}
