﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compass.XFracas.WebGerenciador.Models
{
    public class BreadcrumbViewModel
    {
    
            public String RouteIdentifier { get; set; }
            public String Title { get; set; }
            public String Caminho { get; set; }
            public Dictionary<String, List<BreadcrumbViewModel>> Children { get; set; }
 
    }


}