﻿using System;
using System.Configuration;
using System.IO;
using Compass.XFracas.WebGerenciadorBL;

namespace Compass.XFracas.WebGerenciador.Utils
{
    public class Log
    {

        public static void Gravar_Log(string logMessage, string TipoLog)
        {
            StreamWriter Arq;

            string vArquivo = ConfigurationManager.AppSettings["CaminhoLog"].ToString();

            Arq = File.AppendText(vArquivo.Insert(vArquivo.IndexOf("."), "_" + string.Format("{0:yyyy_MM_dd}", DateTime.Now)));

            Arq.Write("\r\n" + TipoLog);

            Arq.Write("\r\n" + new string('-', 200) + "\r\nData: " + DateTime.Now.ToString() + ": " + logMessage);

            Arq.Close();

            Arq.Dispose();
        }

    }
}