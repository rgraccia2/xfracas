﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compass.XFracas.WebGerenciador.Enum
{
    public enum EnumPage
    {
        Monitoramento = 1,
        Dashboard = 2,
        Devices = 5,
        Analise = 6,
        Perfil = 7,
        CriarDevice = 8,
        DevicesPendentes = 9,
        AssociarUsuario = 11,
        EditarDevice = 12,
        ConsultarEditar = 13,
        AssociarDevices = 14,
        Grupos = 15,
        CriarGrupo = 16,
        Otimizacoes= 17,
        CriarOtimizador= 18,
        ConsultarEditarO=19, 
        Relatorios = 20, 
        Grafico = 22,
        Marcadores = 23,
        CriarMarcador = 24,
        ConsultarEditarM= 25
    }
}