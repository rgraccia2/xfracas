﻿

function hideFiltro() {
    var $filtro = jQuery('.filtro');
    $filtro.attr('hidden', true);
    //hideBackgroundBlur();
    $("body").css("position", "");
}

function showFiltro() {
    var $filtro = jQuery('.filtro');
    $filtro.attr('hidden', false);
    //showBackgroundBlur();
    $("body").css("position", "fixed");
}

jQuery('.filtro>main>fieldset').find('.btn.btn--azul.escuro').click(function () {


    var $this = jQuery(this);
    var type = jQuery(this).data('type');
    var val = jQuery(this).data('value');
    var $input = jQuery('#' + type + '-' + val);

    $this.toggleClass('active escuro');

    if ($this.find('.fa').hasClass('fa-check-square-o')) {

        $this.find('.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
    } else {

        $this.find('.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
    }

    if ($this.hasClass('active')) {
        $input.prop('checked', true).attr('value', val);
    } else {
        $input.prop('checked', false).attr('value', '');
    }
    setFiltroButtons(this);
    //countFilters();
});

function setFiltroButtons(el) {
    var $this = jQuery(el);
    var type = $this.data('type');
    var text = $this.data('text');
    var arr = [];
    var tmpl = '<div id="' + type + '" class="filtro--buttons mt-4">\n                <div class"d-flex">\n                ' + text + '\n                <button type="button" class="ml-3" data-id="' + type + '" onclick="removeFormButton(this);">\n                  <i class="fa fa-times-circle"></i>\n                </button>\n                </div>\n              </div>';
}

function removeFormButton(el) {
    var id = jQuery(el).data('id');
    jQuery('#' + id).remove();
}

function BuscarMarcadores() {
    BuscarMarcadoresDisponiveis();
    BuscarMarcadoresAtribuidos();
}

function BuscarMarcadoresDisponiveis() {

    $("#SelectList").empty();
    StartProgress();
    $.ajax({
        url: urlMarcadoresDisp,
        dataType: 'json',
        data: {
            "device_address": deviceRota
        },
        type: 'POST'
    }).success(function (d) {
        if (d === "Expirou") {
            window.top.location.href = '@Url.Action("Login", "Home", new { ses = "S" }, Request.Url.Scheme)';
        }
        else {
            EndProgress();
            $("#SelectList").empty();

            $.each(d.data, function (i, item) {
                $('#SelectList').append($('<option>', {
                    value: item.idtag,
                    text: item.nome
                }));
            });
        }

    }).fail(function () {
        EndProgress();
        showMessage(msgErroInterno, 'exclamation');
    });
}

/* ********************************** */
/* JavaScript não tem função sleep(). */
/* Então eu criei uma...              */
/* ********************************** */
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}


function SalvarTags() {
    DeletaTags();
    var pickList = document.getElementById("PickList");

	/* ********************************************************************* */
	/* Está dando uma pausa de 1 segundo aqui porque aparentemente as        */
	/* chamadas são assíncronas e o sistema estava executando o DeletarTag() */
	/* no meio ou até no fim das chamadas para SalvarTag()                   */
	/* ********************************************************************* */

	sleep(1000);
    if (pickList.length === 0)
        showMessage('Marcador salvo com sucesso.', 'check');

    for (var j = 0; j < pickList.length; j++) {
        tag = pickList[j].text;
        SalvarTag();
    }
}

function SalvarTag() {

    StartProgress();
    $.ajax({
        url: urlSalvarTag,
        dataType: 'json',
        data: {
            "device_address": deviceRota,
            "tag_device": tag
        },
        type: 'POST'
    }).success(function (d) {
        if (d === "Expirou") {
            window.top.location.href = '@Url.Action("Login", "Home", new { ses = "S" }, Request.Url.Scheme)';
        }
        else if (d === "OK") {
            showMessage('Marcador salvo com sucesso.', 'check');
        }
        EndProgress();
    }).fail(function () {
        EndProgress();
        showMessage(msgErroInterno, 'exclamation');
    });
}

function DeletaTags() {

    StartProgress();
    $.ajax({
        url: urlDeletaTag,
        dataType: 'json',
        data: {
            "device_address": deviceRota
        },
        type: 'POST'
    }).success(function (d) {
        EndProgress();
        if (d === "Expirou") {
            window.top.location.href = '@Url.Action("Login", "Home", new { ses = "S" }, Request.Url.Scheme)';
        }
    }).fail(function () {
        EndProgress();
        showMessage(msgErroInterno, 'exclamation');
    });
}

function BuscarMarcadoresAtribuidos(device) {
    $("#listMardadores p").html("");
    if (device) {
        deviceRota = device;
    }

    $("#SelectList").empty();
    StartProgress();
    $.ajax({
        url: urlMarcadoresAtribuidos,
        dataType: 'json',
        data: {
            "device_address": deviceRota
        },
        type: 'POST'
    }).success(function (d) {
        if (d === "Expirou") {
            window.top.location.href = '@Url.Action("Login", "Home", new { ses = "S" }, Request.Url.Scheme)';
        }
        else {
            EndProgress();
            $("#PickList").empty();

            $.each(d.data, function (i, item) {
                $('#PickList').append($('<option>', {
                    value: item.id_tag,
                    text: item.nome
                }));
                setTimeout(function () {
                    $("#listMardadores p").append(item.nome + " ");
                }, 1000);

            });
        }
    }).fail(function () {
        EndProgress();
        showMessage(msgErroInterno, 'exclamation');
    });
}

jQuery('form').submit(function (event) {
    var $form = jQuery(this);
    event.preventDefault();
});

if ($(".nav__item.tab-item.active[data-type='mapa']")) {
    $("body").css("position", "fixed");
}
$("#nav-principal li[data-type='lista']").on("click", function () {
    $("body").css("position", "");
});
$("#nav-principal li[data-type='mapa']").on("click", function () {
    $("body").css("position", "fixed");
});