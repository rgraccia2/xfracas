var checkin = $('#dataInicial').datepicker({
	language: "pt-BR",
	format: "dd/mm/yyyy",
	autoclose: true
}).on('changeDate', function (ev) {
	checkout.setStartDate(ev.date);
	$('#dataFinal').focus();
}).data('datepicker');

var checkout = $('#dataFinal').datepicker({
	language: "pt-BR",
	format: "dd/mm/yyyy",
	autoclose: true
}).on('changeDate', function (ev) {
	checkin.setEndDate(ev.date);
}).data('datepicker');