// Menu functions
$("#menu-button").on("click", function(){
  toggMenu();
})
$("#background-blur").on("click", function(){
  minimizeMenu();
  hideFilter();
  // hideFiltro();
  // $(".dialog-message").dialog("close");
})
$(".sidebar li").on("click", function(){
  expandMenu();
})
$("#side-menu").on("click", function(){
  // $("#side-menu").addClass("active");
  expandMenu();
  // alert();
})
function expandMenu(){
  $("#side-menu").addClass("active");
  showBackgroundBlur();
  $("body").addClass("fixed");
  $("#side-menu").removeClass("compact");
}
function minimizeMenu(){
  $("#side-menu").removeClass("active");
  hideBackgroundBlur();
  $("body").removeClass("fixed");
  $("#side-menu").addClass("compact");
}
function hideFilter(){
  var $filtro = jQuery('.filtro');
  $filtro.attr('hidden', true);
  hideBackgroundBlur();
  $("body").css("position", "");
}
function toggMenu(){
  $("#side-menu").toggleClass("active");
  toggBackgroundBlur();
  $("body").toggleClass("fixed");
  $("#side-menu").toggleClass("compact");
}
function toggBackgroundBlur(){
  $("#background-blur").toggleClass("active");
}
function showBackgroundBlur(){
  $("#background-blur").addClass("active");
}
function hideBackgroundBlur(){
  $("#background-blur").removeClass("active");
}

var parts = window.location.href.split('/');
var lastSegment = parts.pop() || parts.pop();  // handle potential trailing slash
lastSegment = lastSegment.replace('#','');

$( document ).ready(function(){

  switch(lastSegment){
    case "Index":
      focusMenu("analise");
      $("#dashboard, #dashboard a").addClass("focus");
      break;

    case "DashBoard":
      focusMenu("analise");
      $("#dashboard, #dashboard a").addClass("focus");
      break;

    case "ListarDevices":
      focusMenu("analise");
      $("#listarDev, #listarDev a").addClass("focus");
      break;

    case "GerenciarGraficos":
      focusMenu("analise");
      $("#graficos, #graficos a").addClass("focus");
      break;

    case "GerenciarDevices":
      focusMenu("devices");
      $("#gerencDev, #gerencDev a").addClass("focus");
      break;
      
    case "GerenciarRegraEventos":
      focusMenu("devices");
      $("#eventos, #eventos a").addClass("focus");
      break;

    case "DevicesPendentes":
      focusMenu("devices");
      $("#devPendentes, #devPendentes a").addClass("focus");
      break;

    case "GerenciarGrupos":
      focusMenu("devices");
      $("#manageGroup, #manageGroup a").addClass("focus");
      break;

    case "AssociarDevices":
      focusMenu("devices");
      $("#associateDevices, #associateDevices a").addClass("focus");
      break;

    case "CriarEditarGrupo":
      focusMenu("devices");
      $("#createGroup, #createGroup a").addClass("focus");
      break;

    case "AtribuirUsuario":
      focusMenu("perfil");
      $("#assUsuario, #assUsuario a").addClass("focus");
      break;

    default:
  }
})
function focusMenu(atual){
  $(".sidebar.compact ."+atual+"").addClass("active");
}
$(".side-login input").focus(function(){
  $(".side-login input").keypress(function(event){
    if ( event.which === 13 ) {
      $("#login").click();
    }
  })
})

function toggleMenuDrop(area){
  if($('.'+area+' ul').css('display') !== "none"){
    $('.'+area+' ul').css('display', "none");
  }
  else if($('.'+area+' ul').css('display') === "none"){
    $('.'+area+' ul').css('display', "");
  }
}

function toggleSubMenuDrop(area){
  if($('li.'+area+'').css('display') !== "none"){
    $('li.'+area+'').css('display', "none");
  }
  else if($('li.'+area+'').css('display') === "none"){
    $('li.'+area+'').css('display', "");
  }

}

// Menu functions END


// ABA DE MARCADORES

$('.add').click(function(){
  $('.all').prop("checked",false);
  var items = $("#list1 input:checked:not('.all')");
  var n = items.length;
  if (n > 0) {
    items.each(function(idx,item){
      var choice = $(item);
      choice.prop("checked",false);
      choice.parent().appendTo("#list2");
    });
  }
  else {
    alert("Choose an item from list 1");
  }
});

$('.remove').click(function(){
  $('.all').prop("checked",false);
  var items = $("#list2 input:checked:not('.all')");
items.each(function(idx,item){
    var choice = $(item);
    choice.prop("checked",false);
    choice.parent().appendTo("#list1");
  });
});

/* toggle all checkboxes in group */
$('.all').click(function(e){
e.stopPropagation();
var $this = $(this);
  if($this.is(":checked")) {
    $this.parents('.list-group').find("[type=checkbox]").prop("checked",true);
  }
  else {
    $this.parents('.list-group').find("[type=checkbox]").prop("checked",false);
      $this.prop("checked",false);
  }
});

$('[type=checkbox]').click(function(e){
e.stopPropagation();
});

/* toggle checkbox when list group item is clicked */
$('.list-group a').click(function(e){

  e.stopPropagation();

  var $this = $(this).find("[type=checkbox]");
  if($this.is(":checked")) {
    $this.prop("checked",false);
  }
  else {
    $this.prop("checked",true);
  }

  if ($this.hasClass("all")) {
    $this.trigger('click');
  }
});


// ABA DE MARCADORES END