//$("#deviceStatus").click(function(){
//    updateReadyData();
//});

function insertListData(data) {
    var deviceStatusBody = $('#deviceStatusBody')

             //Captura os campos da tabela para serem inseridos os dados de 'data'
             var lineDevice = deviceStatusBody[0].children;
             var colFail = lineDevice[0].cells;
             var colSinal = lineDevice[1].cells;
             var colThoughput = lineDevice[2].cells;
             var colErrors = lineDevice[3].cells;
             var colLatency = lineDevice[4].cells;
             var colRank = lineDevice[5].cells;
             var colNeighbors = lineDevice[6].cells;


             colFail[1].textContent = data[0].falha_normal;
             colFail[2].textContent = data[0].falha_atencao;
             colFail[3].textContent = data[0].falha_critico;
             colFail[4].textContent = data[0].falha_indefinido;

             colSinal[1].textContent = data[0].sinal_normal;
             colSinal[2].textContent = data[0].sinal_atencao;
             colSinal[3].textContent = data[0].sinal_critico;
             colSinal[4].textContent = data[0].sinal_indefinido;

             colThoughput[1].textContent = data[0].throughput_normal;
             colThoughput[2].textContent = data[0].throughput_atencao;
             colThoughput[3].textContent = data[0].throughput_critico;
             colThoughput[4].textContent = data[0].throughput_indefinido;

             colErrors[1].textContent = data[0].erros_normal;
             colErrors[2].textContent = data[0].erros_atencao;
             colErrors[3].textContent = data[0].erros_critico;
             colErrors[4].textContent = data[0].erros_indefinido;

             colLatency[1].textContent = data[0].latencia_normal;
             colLatency[2].textContent = data[0].latencia_atencao;
             colLatency[3].textContent = data[0].latencia_critico;
             colLatency[4].textContent = data[0].latencia_indefinido;

             colRank[1].textContent = data[0].rank_normal;
             colRank[2].textContent = data[0].rank_atencao;
             colRank[3].textContent = data[0].rank_critico;
             colRank[4].textContent = data[0].rank_indefinido;

             colNeighbors[1].textContent = data[0].vizinhos_normal;
             colNeighbors[2].textContent = data[0].vizinhos_atencao;
             colNeighbors[3].textContent = data[0].vizinhos_critico;
             colNeighbors[4].textContent = data[0].vizinhos_indefinido;
}

//function updateReadyData() {    
//    $.ajax({
//        url: '/MESHAPP/Device/GetAlerta',
//        dataType: 'json',
//        type: 'GET'
//    }).success(function (data) {
//        insertListData(data);
//    }).fail(function () {
//        EndProgress();
//        showMessage('@Compass.XFracas.WebGerenciador.Resources.ResourceSGR.Mensagem_ErroInterno', 'exclamation');
//    }).done(function () {
//        EndProgress();
//        hideFiltro();
//    });
//}


