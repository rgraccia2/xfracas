'use strict';

;
(function ($) {
    $.fn.tabs = function (options) {

        var settings = $.extend({
            target: '.tab-target'
        }, options);

        var $self = jQuery(this);
        var $target = jQuery(settings.target);

        $self.find('.tab>.tab-item').on('click', function () {
            var $clicked = jQuery(this);
            var $type = jQuery('#' + $clicked.data('type'));
            $self.find('.tab>.tab-item').each(function () {
                $self.find(this).removeClass('active');
            });
            $clicked.addClass('active');
            $target.find('.tab-item').each(function () {
                jQuery(this).attr('hidden', true);
            });
            $type.attr('hidden', false);
        });
        return $self;
    };
})(jQuery);