var tmplNovoRepetidor = jQuery('#tmpl-novo-repetidor').html();
var tmplNovoConcentrador = jQuery('#tmpl-novo-concentrador').html();

//var novoRepetidor = Mustache.render(tmplNovoRepetidor);
//var novoConcentrador = Mustache.render(tmplNovoConcentrador);

function displayNovoDevice(el) {
  var $el = jQuery(el);
  var data_type = $el.data('type');
  var $frm = jQuery('#' + data_type);
  var $header = jQuery('#form-novo-device header');

  jQuery('.frm-device').each(function () {
    jQuery(this).attr('hidden', true);
  });

  $header.find('.btn').each(function () {

    var $btn = jQuery(this);
    $btn.removeClass('btn--azul active').addClass('btn--cinza');
    $btn.find('.fa').removeClass('fa-check-square').addClass('fa-square');
    $el.removeClass('btn--cinza').addClass('btn--azul active');
    $el.find('.fa').removeClass('fa-square').addClass('fa-check-square');
    $('#device_type').val(data_type);

  });

  if (data_type === 'repetidor') {
      jQuery('#frm-novo-device__inputs').empty().append(tmplNovoRepetidor);
  } else if (data_type === 'concentrador') {
      jQuery('#frm-novo-device__inputs').empty().append(tmplNovoConcentrador);
  } else {
    jQuery('#frm-novo-device__inputs').empty();
  }
}
