'use strict';

var graficoEquipamentoFalha = jQuery('#grafico-equipamentos-falha');
var graficoLatencia = jQuery('#grafico-latencia');
var graficoPacoteFalha = jQuery('#grafico-pacote-falha');


function random() {
  return Math.random(99999) * 500;
}

//status check || exclamation
function showMessage(msg, status) {
  var render = Mustache.render('<div class="message {{status}} card">' + '<div class="d-flex align-items-center">' + '<i class="fa fa-{{status}}-circle" style="font-size:2.2rem;"></i>' + '<p>{{msg}}</p>' + '</div>' + '</div>', {
    msg: msg,
    status: status
  });

  jQuery('.header--secundario .row .message').remove();
  jQuery('.header--secundario .row').append(render);
  jQuery('.message').fadeIn();
  setTimeout(function () {
    jQuery('.message').fadeOut(function () {
      jQuery(this).remove();
    });
  }, 5000);
}

function toggleMenu() {
  jQuery('.menu').toggleClass('active');
}

jQuery('.menu__button').on('click', function () {
    if (jQuery('.menu').hasClass('active')) {
        jQuery('.menu').removeClass('active');
    } else {
        jQuery('.menu').addClass('active');
    }
});


function showMessageLogin(msg, status) {
    var render = Mustache.render('<div class="message {{status}} card">' + '<div class="d-flex align-items-center">' + '<i class="fa fa-{{status}}-circle" style="font-size:2.2rem;"></i>' + '<p>{{msg}}</p>' + '</div>' + '</div>', {
        msg: msg,
        status: status
    });

    jQuery('.side-login .message').remove();
    jQuery('.side-login').append(render);
    jQuery('.message').fadeIn();
    setTimeout(function () {
        jQuery('.message').fadeOut(function () {
            jQuery(this).remove();
        });
    }, 5000);

}


//jQuery('body').not('.menu__button').on('click', function () {
//    jQuery('.menu__button').trigger('click');
//});


function toggleUsuario() {
  jQuery('.box-usuario').stop().fadeToggle('fast');
}

function toggleAcoes() {
  jQuery('.acoes').stop().slideToggle('fast');
}

try {
  jQuery('[title!=""]').tooltip();
} catch(ex) {
} finally {
}

function StartProgress() {
    $("body").append('<main class="modal"><div class="d-flex w-100 h-100"><div class="spinner m-auto"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div></main>');
    $('.modal').fadeIn();
}

function EndProgress() {
    $(".modal").fadeOut().remove();
}


jQuery(window).scroll(function () {
    if (jQuery(window).scrollTop() >= window.outerHeight / 10) {
        jQuery('.header').addClass('fixed');
    } else if (jQuery(window).scrollTop() === 0) {
        jQuery('.header').removeClass('fixed');
    }
});



// check || exclamation
// exemplo
 //showMessage('Novo device foi associado com sucesso, e não está mais em sua lista de pendências.', 'exclamation');
//# sourceMappingURL=utils.js.map
