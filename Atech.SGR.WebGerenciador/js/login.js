jQuery('form').validate({
  errorElement: 'div',
  errorClass: 'form-control-feedback',
  focusInvalid: false,
  rules: {
    txtUsuario: {
      required: true
    },
    txtSenha: {
      required: true,
    },
  },
  messages: {
    txtUsuario: 'Campo obrigatório',
    txtSenha: 'Campo obrigatório'
  },
  highlight: function (e) {
    jQuery(e).closest('.form-group').addClass('has-danger');
    jQuery(e).closest('.form-group').focus();
  },
  unhighlight: function (e) {
    jQuery(e).closest('.form-group').removeClass('has-danger');
  },
  success: function (e) {
    jQuery(e).closest('.form-group').removeClass('has-danger');
  },
  submitHandler: function (form) {
    event.preventDefault();
    var $form = jQuery(form);
    var formSerialize = $form.serialize();
    jQuery.ajax({
      url: 'scripts/api/login',
      method: 'GET', //POST
      // dataType: 'json',
      // data: formSerialize,
      success: function (data) {
        console.log(data)
        var parse = JSON.parse(data);
        if (true) location.href = '/dashboard.html';
      }
    })
  },
  invalidHandler: function (form) {}
});
