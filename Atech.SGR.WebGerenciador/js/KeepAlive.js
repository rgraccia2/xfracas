﻿// KEEP ALIVE SCRIPTS
var listaKeepAlive = [];

// keepAlive dataTable
function format(d) {
    // `d` is the original data object for the row
    return `
        <div class="detalhesKeep">
        <div class="form-group">
            <label> Vizinhos </label>
            <p>${d.num_vizinhos} </p>
        </div>
        <div class="form-group">
            <label>Latência </label>
            <p>${d.latencia} </p>
        </div>
        <div class="form-group">
            <label>Bytes enviados </label>
            <p>${d.bytes_env} </p>
        </div>
        <div class="form-group">
            <label>Pacotes enviados </label>
            <p>${d.pack_env} </p>
        </div>
        <div class="form-group">
            <label> Pacotes sem ACK</label>
            <p>${d.pack_no_ack} </p>
        </div>
        <div class="form-group">
            <label>IP do concentrador </label>
            <p>${d.ip_concentrador} </p>
        </div>
        <div class="form-group">
            <label>Sinal mínimo </label>
            <p>${d.sinal_min} </p>
        </div>
        <div class="form-group">
            <label> Sinal máximo </label>
            <p>${d.sinal_max} </p>
        </div>
    </div>
    `;
}

function createKeepAliveDataGrid() {
//console.log('LIST ON DATATABLE',listaKeepAlive);
    var table = $('#KeepAliveDataTable').DataTable({
        data: listaKeepAlive,
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "dt_device_data" },
            { "data": "device_parent_address" },
            { "data": "sinal_med" },
            { "data": "num_rank" }
        ],
        bAutoWidth: false,
        multiselect: true,
        multiboxonly: true,
        aaSorting: false,
        bFilter: false,
        bLengthChange: false,
        bInfo: true,
        iDisplayLength: 20,
        bPaginate: false,
        oLanguage: {
            sProcessing: "Processando...",
            sLengthMenu: "Mostrar _MENU_ registros",
            sZeroRecords: "Não foram encontrados resultados",
            sInfo: "Exibindo _START_ de _END_ total _TOTAL_ registros",
            sInfoEmpty: "Exibindo 0 de 0 total de 0 registros",
            sInfoFiltered: "",
            sInfoPostFix: "",
            sSearch: "Buscar:",
            sUrl: "",
            oPaginate: {
                sPrevious: "<",
                sNext: ">"
            }
        }
    });



    // Add event listener for opening and closing details
    $('#KeepAliveDataTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var td = $(this).first();


        console.log('antes 1', td);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            td.html('<i class="fa fa-plus-circle" aria-hidden="true"></i>');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
            td.html('<i class="fa fa-minus-circle" aria-hidden="true"></i>');

        }
        console.log('depois', td);
    });

    $("td.details-control").html('<i class="fa fa-plus-circle" aria-hidden="true"></i>');
}

function getkeepAliveList() {
    var table = $('#KeepAliveDataTable').DataTable();
   // console.log('DESTRUIR ', table);
    var deviceAddressId = $("#deviceAddressId").text();
  //  console.log(deviceAddressId);
    listaKeepAlive = [];
    if (table) {
        table.destroy();
    }

    StartProgress();
    $.ajax({
        url: BuscarKeepAlive,
        dataType: 'json',
        type: 'POST',
        data: {
            "device_address": deviceAddressId
        }
    }).success(function (data) {
        if (data == "Expirou") {
            window.top.location.href = '@Url.Action("Login", "Home", new { ses = "S" }, Request.Url.Scheme)';
        }
        else {
            EndProgress();
          //  console.log('lista on endpoint',data.data );
            listaKeepAlive = data.data;
            createKeepAliveDataGrid();
        }
    }).fail(function () {
        EndProgress();
        showMessage("Não foi possível carregar a lista", 'exclamation');
    });
}