﻿using Compass.XFracas.WebGerenciador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Compass.XFracas.WebGerenciador.Helpers
{   
    public class BreadcrumbHelper
    {
        //"" OU "/SGRAPP"

        public static string local = System.Configuration.ConfigurationManager.AppSettings["pasta_app"].ToString();


        private static readonly Dictionary<String, List<BreadcrumbViewModel>> Breadcrumbs =
            new Dictionary<string, List<BreadcrumbViewModel>>
            {                

                  { ""+local+"/Device/ListarDevices",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Monitoramento de Devices", Caminho = ".."+local+"/Device/ListarDevices"}
                    }
                },

                { ""+local+"/Device/DevicesPendentes",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Devices Pendentes",Caminho = ".."+local+"/Device/DevicesPendentes" }
                    }
                },
                { ""+local+"/Device/GerenciarDevices",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Criar Device", Caminho = ".."+local+"/Device/GerenciarDevices" }
                    }
                },
                { ""+local+"/Device/EditarDevice",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Editar Device", Caminho = ".."+local+"/Device/EditarDevice" }
                    }
                },
                  { ""+local+"/Device/MapaDevices",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Mapa Device", Caminho = ".."+local+"/Device/MapaDevices" }
                    }
                },
                { ""+local+"/Usuario/AtribuirUsuario",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Associar Usuário", Caminho= ".."+local+"/Usuario/AtribuirUsuario"}
                    }
                },
                 { ""+local+"/DashBoard/Index",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "DashBoard", Caminho= ".."+local+"/DashBoard/Index"}
                    }
                },
                  { ""+local+"/DashBoard",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "DashBoard", Caminho= ".."+local+"/DashBoard/Index"}
                    }
                 },
                   { ""+local+"/Grupo/GerenciarGrupos",
                   new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Gerenciar Grupos", Caminho= ".."+local+"/Grupo/GerenciarGrupos"}
                }
                },

                   { ""+local+"/Grupo/CriarEditarGrupo",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Criar Editar Grupos", Caminho= ".."+local+"/Grupo/CriarEditarGrupo"}
                    }
                },

                   { ""+local+"/Grupo/AssociarDevices",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Associar Devices", Caminho= ".."+local+"/Grupo/AssociarDevices"}
                    }
                },
                   { ""+local+"/Otimizador/GerenciarOtimizador",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Gerenciar Otimizador", Caminho= ".."+local+"/Otimizador/GerenciarOtimizador"}
                    }
                },
                   { ""+local+"/Otimizador/CriarEditarOtimizador",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Criar Editar Otimizador", Caminho= ".."+local+"/Otimizador/CriarEditarOtimizador"}
                    }
                },
                   { ""+local+"/Relatorio/GerenciarRelatorios",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Relatórios", Caminho= ".."+local+"/Relatorio/GerenciarRelatorios"}
                    }
                 },
                   { ""+local+"/Grafico/GerenciarGraficos",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Gráficos", Caminho= ".."+local+"/Grafico/GerenciarGraficos"}
                    }
                 },
                    { ""+local+"/Tag/CriarEditarTag",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Criar Marcador", Caminho = ".."+local+"/Tag/CriarEditarTag" }
                    }
                },
                   { ""+local+"/Tag/GerenciarTags",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Marcadores", Caminho = ".."+local+"/Tag/GerenciarTags" }
                    }
                },
                 
                    { ""+local+"/Device/ImportarUC",
                    new List<BreadcrumbViewModel> {
                        new BreadcrumbViewModel { Title = "Importar Unidades Consumidoras", Caminho= ".."+local+"/Device/ImportarUC"}
                    }
                 }
            };


        public static IEnumerable<BreadcrumbViewModel> GetBreadcrumbs(String url)
        {

            if (url == "" + local + " / Grupo/CriarEditarGrupo")
            {
                List<BreadcrumbViewModel> item = Breadcrumbs[url];

                if (HttpContext.Current.Session["OPER"].ToString() == "edit")
                {
                    item[0].Title = "Editar Grupo";
                }
                else if (HttpContext.Current.Session["OPER"].ToString() == "view")
                {
                    item[0].Title = "Visualizar Grupo";
                }
                else
                {
                    item[0].Title = "Criar Grupo";
                }

                return (IEnumerable<BreadcrumbViewModel>)item;
            }
            else if (url == "" + local + "/Otimizador/CriarEditarOtimizador")
            {
                List<BreadcrumbViewModel> item = Breadcrumbs[url];

                if (HttpContext.Current.Session["OPER_OTMZ"].ToString() == "edit")
                {
                    item[0].Title = "Editar Otimizador";
                }
                else if (HttpContext.Current.Session["OPER_OTMZ"].ToString() == "view")
                {
                    item[0].Title = "Visualizar Otimizador";
                }
                else
                {
                    item[0].Title = "Criar Otimizador";
                }
            }


            return Breadcrumbs[url];
        }

    
    }

}