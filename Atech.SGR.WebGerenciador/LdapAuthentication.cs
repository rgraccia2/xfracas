﻿using System;
using System.Text;
using System.Collections;
using System.DirectoryServices;

namespace Compass.XFracas.WebGerenciador
{

    public class LdapAuthentication
    {
        private String _path;
        private String _filterAttribute;

        public LdapAuthentication(String path)
        {
            _path = path;
        }

        public bool IsAuthenticated(String domain, String username, String pwd)
        {

            string propSearch = System.Configuration.ConfigurationManager.AppSettings["LDAP_Campo_Login"].ToString();

            String userAuthentication = username;
            DirectoryEntry entry = new DirectoryEntry(_path, userAuthentication, pwd , AuthenticationTypes.Secure);
            try
            {//Bind to the native AdsObject to force authentication.

                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(" + propSearch + "=" + username + ")";
                search.PropertiesToLoad.Add(propSearch);
                SearchResult searchResult = search.FindOne();

                if (searchResult == null)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log("Erro na authenticacao" + ex.Message, "Falha Authenticação.");
                return false;
            }
            return true;
        }

        public static string GetProperty(SearchResult searchResult,string PropertyName)
        {
            if (searchResult.Properties.Contains(PropertyName))
            {
                return searchResult.Properties[PropertyName][0].ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public String GetGroups()
        {
            DirectorySearcher search = new DirectorySearcher(_path);
            search.Filter = "(cn=" + _filterAttribute + ")";
            search.PropertiesToLoad.Add("memberOf");
            StringBuilder groupNames = new StringBuilder();

            try
            {
                SearchResult result = search.FindOne();

                int propertyCount = result.Properties["memberOf"].Count;

                String dn;
                int equalsIndex, commaIndex;

                for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
                {
                    dn = (String)result.Properties["memberOf"][propertyCounter];

                    equalsIndex = dn.IndexOf("=", 1);
                    commaIndex = dn.IndexOf(",", 1);
                    if (-1 == equalsIndex)
                    {
                        return null;
                    }

                    groupNames.Append(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1));
                    groupNames.Append("|");

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error obtaining group names. " + ex.Message);
            }
            return groupNames.ToString();
        }
    }
}
