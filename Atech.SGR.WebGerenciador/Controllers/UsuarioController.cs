﻿using Compass.XFracas.WebGerenciadorBL;
using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.XFracas.WebGerenciador.Controllers
{

    [Authorize]
    public class UsuarioController : CommonController
    {
        // Codigo menu página atribuir perfil
        public const int CodPagina = 11;


        public JsonResult BuscarUsuario()
        {
            if (Session["User"] != null)
            {
                string CodigoUsuario = Request["usarioSearch"].ToString();
                List<string> lstUsuarios = new List<string>();

                String userAuthentication = System.Configuration.ConfigurationManager.AppSettings["LDAP_DN_ADM"].ToString();
                String pwd = System.Configuration.ConfigurationManager.AppSettings["LDAP_DN_ADM_PASSWORD"].ToString();
                string servidor = System.Configuration.ConfigurationManager.AppSettings["LDAP_Servidor"].ToString();

                DirectoryEntry entry = new DirectoryEntry(servidor, userAuthentication, pwd, AuthenticationTypes.Secure);

                try
                {//Bind to the native AdsObject to force authentication.
                    Object objNative = entry.NativeObject;
                    DirectorySearcher search = new DirectorySearcher(entry);

                    string propSearch = System.Configuration.ConfigurationManager.AppSettings["LDAP_Campo_Login"].ToString();
                    search.Filter = "(" + propSearch + "=" + CodigoUsuario + "*)";
                    search.PropertiesToLoad.Add(propSearch);
                    SearchResultCollection lstUsuariosSearch = search.FindAll();
                    foreach (SearchResult item in lstUsuariosSearch)
                    {
                        lstUsuarios.Add(item.Properties[propSearch][0].ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error authenticating user. " + ex.Message);
                }
                return Json(lstUsuarios, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult AtribuirPerfilUsuario()
        {
            if (Session["User"] != null)
            {
                string retorno = "OK";
                    string CodUsuario = "";
                    int CodPerfil = 0;
                    if (Request["CodUsuario"] != null && Request["CodPerfil"] != null)
                    {
                        CodUsuario = Request["CodUsuario"].ToString();
                        CodPerfil = Convert.ToInt32(Request["CodPerfil"].ToString());
                        try
                        {
                            UsuarioBL obj = new UsuarioBL();

                            if (!obj.UsuarioExiste(CodUsuario))
                            {
                                obj.AtribuirPerfilUsuario(CodUsuario, CodPerfil);
                            }
                            else
                            {
                                retorno = "Usuário já associado a um perfil.";
                            }
                        }
                        catch (Exception ex)
                        {
                            retorno = "Error ao atribuir usuário ao perfil, tente mais tarde.";
                            Utils.Log.Gravar_Log(ex.ToString(), "Error");
                        }
                    }
                    else
                    {
                        retorno = "Preencha todos os campos corretamente";
                    }
                    if (retorno == "OK")
                    {
                        string user = "Não Identificado";
    
                    }
                    return Json(retorno);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult ExcluirPerfilUsuario()
        {
            if (Session["User"] != null)
            {
                string retorno = "OK";
                Int32 Id_Usuario_Perfil = 0;
                if (Request["Id_Usuario_Perfil"] != null)
                {
                    Id_Usuario_Perfil = Convert.ToInt32(Request["Id_Usuario_Perfil"].ToString());
                    try
                    {
                        UsuarioBL obj = new UsuarioBL();
                        obj.ExcluirPerfilUsuario(Id_Usuario_Perfil);
                    }
                    catch (Exception ex)
                    {
                        retorno = "Error ao excluir perfil do usuário";
                        Utils.Log.Gravar_Log(ex.ToString(), "Error");
                    }
                }
                else
                {
                    retorno = "Usuário não encontrado.";
                }
                if (retorno == "OK")
                {
                    string user = "Não Identificado";
                }
                return Json(retorno);

            }
            else
            {
                return Json("Expirou");
            }
        }

    }
}