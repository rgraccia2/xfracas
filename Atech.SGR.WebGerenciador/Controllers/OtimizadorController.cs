﻿using Atech.SGR.WebGerenciadorBL;
using Atech.SGR.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Mvc;
using System.Linq;


namespace Atech.SGR.WebGerenciador.Controllers
{
    [Authorize]
    public class OtimizadorController : CommonController
    {
        public const Int32 Cod_Param_Timeout = 10;
       
        // GET: Otimizador
        public ActionResult GerenciarOtimizador()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.ConsultarEditarO))
                {
                    // Obtém parametro timeout para refresh página de monitóramento.
                    ParametroBL objParam = new ParametroBL();
                    Parametro timeoutParam = objParam.ObterParametro(Cod_Param_Timeout);
                    ViewBag.TimeOut = Convert.ToInt64(timeoutParam.vl_param) / 1000;

                    ViewBag.lstDadosCalc = new List<ResultCalcOtimizar>();
                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }

        public JsonResult ListarOtimizacoes()
        {
            List<Otimizacao> retorno = new List<Otimizacao>();
            try
            {
                OtimizadorBL obj = new OtimizadorBL();
                retorno = obj.ListarOtimizacoes();
                foreach (Otimizacao item in retorno)
                {
                    try { item.DT_CRIACAO = item.DT_CRIACAO != null ? Convert.ToDateTime(item.DT_CRIACAO).ToString("dd/MM/yyyy HH:mm:ss") : ""; } catch { item.DT_CRIACAO = "-"; };
                    try { item.DT_OTIMIZACAO = item.DT_OTIMIZACAO != null ? Convert.ToDateTime(item.DT_OTIMIZACAO).ToString("dd/MM/yyyy HH:mm:ss") : ""; } catch { item.DT_OTIMIZACAO = "-"; }
                }

            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar otimizações.");
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }


        public ActionResult CriarEditarOtimizador()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.CriarOtimizador))
                {
                    int id_regra = 0;
                    if (Request["oper"] != null)
                    {
                        id_regra = Convert.ToInt32(Request["id_regra"]);
                        ViewBag.id_regra = id_regra;
                        RegraOtimizador regra = BuscarRegra(id_regra);
                        ViewBag.nome_regra = regra.NOME_REGRA;
                        ViewBag.descricao = regra.DESCRICAO_REGRA;
                        ViewBag.nome_otimizador = Request["nm"].ToString();

                        ViewBag.id_otimizador = Convert.ToInt32(Request["id_otimizador"]);
                        ViewBag.lstDevicesOtimizador = ListarOtimizacoesDevices();

                        ViewBag.Operacao = Request["oper"].ToString();
                        if (Session["OPER_OTMZ"] != null)
                        {
                            Session.Add("OPER_OTMZ", ViewBag.Operacao);
                        }
                        else
                        {
                            Session["OPER_OTMZ"] = ViewBag.Operacao;
                        }
                    }

                    if (Request["id_otimizador"] != null)
                    {
                        ViewBag.Operacao = Request["oper"].ToString();
                        if (Session["ID_OTIMIZACAO"] == null)
                        {
                            Session.Add("ID_OTIMIZACAO", Convert.ToInt32(Request["id_otimizador"]));
                        }
                        else
                        {
                            Session["ID_OTIMIZACAO"] = Convert.ToInt32(Request["id_otimizador"]);
                        }
                    }
                    else
                    {
                        ViewBag.Operacao = "add";
                        ViewBag.lstDevicesOtimizador = ListarOtimizacoesDevices();
                        if (Session["OPER_OTMZ"] != null)
                        {
                            Session.Add("OPER_OTMZ", "add");
                        }
                        else
                        {
                            Session["OPER_OTMZ"] = "add";
                        }
                    }

                    List<RegraOtimizador> listaRegras = new List<RegraOtimizador>();
                    try
                    {
                        OtimizadorBL obj = new OtimizadorBL();
                        listaRegras.AddRange(obj.ListarRegras());
                        ViewBag.ListaRegras = new SelectList(listaRegras, "ID_OTIMIZ_REGRA", "NOME_REGRA", id_regra);

                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar regras.");
                    }

                    List<Municipio> lstMunicipio = new List<Municipio>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Municipio item = new Municipio();
                        item.municipio = "-TODOS-";
                        lstMunicipio.Add(item);
                        lstMunicipio.AddRange(obj.ListarMunicipio());

                        ViewBag.ListaMunicipio = new SelectList(lstMunicipio, "municipio", "municipio");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar municipio.");
                    }

                    List<GrupoSubGrupo> lstGrupoSubGrupo = new List<GrupoSubGrupo>();
                    try
                    {
                        UcBL obj = new UcBL();
                        GrupoSubGrupo item = new GrupoSubGrupo();
                        item.tipo_subgrupo = "-TODOS-";
                        lstGrupoSubGrupo.Add(item);
                        lstGrupoSubGrupo.AddRange(obj.ListarGrupoSubGrupo());

                        ViewBag.ListaGrupoSubGrupo = new SelectList(lstGrupoSubGrupo, "tipo_subgrupo", "tipo_subgrupo");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupo sub grupo.");
                    }

                    List<Grupo> lstGrupoDevice = new List<Grupo>();
                    try
                    {
                        GrupoBL obj = new GrupoBL();
                        Grupo item = new Grupo();
                        item.id_grupo = null;
                        item.nome = "-TODOS-";
                        lstGrupoDevice.Add(item);
                        lstGrupoDevice.AddRange(obj.ListarGrupos());
                        ViewBag.ListaGrupoDevice = new SelectList(lstGrupoDevice, "id_grupo", "nome");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupos.");
                    }

                    ViewBag.LstDevices = new List<DeviceListaGrupo>();
                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }

        public JsonResult BuscarRegraDescricao()
        {
            RegraOtimizador retorno = new RegraOtimizador();
            try
            {
                if (Request["id_regra"] != null)
                {
                    int id_regra = Convert.ToInt32(Request["id_regra"]);
                    OtimizadorBL obj = new OtimizadorBL();
                    retorno = obj.ObterRegra(id_regra);
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao obter regra.");
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public List<DeviceListaGrupo> ListarOtimizacoesDevices()
        {
            List<DeviceListaGrupo> retorno = new List<DeviceListaGrupo>();
            try
            {
                if (Request["id_otimizador"] != null)
                {
                    int id_otimizador = Convert.ToInt32(Request["id_otimizador"]);
                    OtimizadorBL obj = new OtimizadorBL();
                    retorno = obj.ListarOtimizacoesDevices(id_otimizador);
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar otimizações devices.");
            }
            return retorno;
        }
        

        public RegraOtimizador BuscarRegra(int id_regra)
        {

            RegraOtimizador retorno = new RegraOtimizador();
            try
            {
                OtimizadorBL obj = new OtimizadorBL();
                retorno = obj.ObterRegra(id_regra);
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao obter regra.");
            }
            return retorno;
        }

        public JsonResult RemoverOtimizacao()
        {
            if (Session["User"] != null)
            {
                string retorno = "OK";
                try
                {
                    if (Request["id_otimizador"] != null)
                    {
                        Int32 id_otimizador = Convert.ToInt32(Request["id_otimizador"]);
                        OtimizadorBL obj = new OtimizadorBL();
                        obj.RemoverOtimizacao(id_otimizador);
                    }
                    else
                    {
                        retorno = "Informe o identificador do otimizador.";
                    }
                }
                catch (Exception ex)
                {
                    retorno = "Erro ao remover device otimização, contate o administrador do sistema.";
                    Utils.Log.Gravar_Log(ex.ToString(), "Error ao remover device otimização.");
                }
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }


        public JsonResult ListarResultCalcOtimizacao()
        {
            List<ResultCalcOtimizar> retorno = new List<ResultCalcOtimizar>();

            if (Session["User"] != null)
            {
                try
                {
                    if (Request["id_otimizador"] != null && !string.IsNullOrEmpty(Request["id_otimizador"].ToString()))
                    {
                        int id_otimizador = Convert.ToInt32(Request["id_otimizador"]);
                        if (ExecutarOtimizacao(id_otimizador))
                        {
                            OtimizadorBL obj = new OtimizadorBL();
                            retorno = obj.ListarResultCalcOtilizacao(id_otimizador);
                        }
                        else
                        {
                            retorno = new List<ResultCalcOtimizar>();
                            return Json(retorno, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch (Exception ex)
                {
                    retorno = new List<ResultCalcOtimizar>();
                    Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar otimizações.");
                }
            }
            else
            {
                return Json("Expirou", JsonRequestBehavior.AllowGet);
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public bool ExecutarOtimizacao(int id_otimizador)
        {
            bool retorno = true;
            try
            {
                OtimizadorBL obj = new OtimizadorBL();
                obj.ExecutarOtimizacao(id_otimizador);
            }
            catch (Exception ex)
            {
                retorno = false;
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao executar Otimização.");
            }
            return retorno;
        }
        
        public JsonResult CancelarOtimizacao()
        {
            string retorno = "OK";
            try
            {
                if (Request["id_otimizador"] != null)
                {
                    Int32 id_otimizador = Convert.ToInt32(Request["id_otimizador"]);
                    OtimizadorBL obj = new OtimizadorBL();
                    obj.CancelarOtimizacao(id_otimizador);
                }
                else
                {
                    retorno = "Informe o identificador do otimizador.";
                }
            }
            catch (Exception ex)
            {
                retorno = "Ocorreu um erro ao cancelar a operação, tente mais tarde.";
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao cancelar otimização.");
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EnviarOtimizacao()
        {
            string retorno = "OK";
            try
            {
                if (Session["User"] != null)
                {
                    if (Request["id_otimizador"] != null)
                    {
                        int id_otimizador = Convert.ToInt32(Request["id_otimizador"]);
                        SendXKC obj = new SendXKC();
                        if (obj.VerificarStatusOperacao(id_otimizador))
                        {
                            //retorno = obj.ExecutarEnvioOtimizacao(id_otimizador);

                            ////Cria uma nova thread, indicando qual método essa thread deverá executar
                            var thread = new Thread(() => obj.ExecutarEnvioOtimizacao(id_otimizador));
                            thread.Start();
                        }
                        else
                        {
                            retorno = "Processo de envio está em execução, aguarde....";
                        }
                    }
                    else
                    {
                        retorno = "Informe o identificador do otimizador.";
                    }
                }
                else
                {
                    return Json("Expirou");
                }
            }
            catch (Exception ex)
            {
                retorno = "Ocorreu um erro durante execução, contate o administrador do sistema";
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao enviar otimização.");
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }
               
        public JsonResult GetDadosDevice()
        {
            List<DeviceListaGrupo> lista = new List<DeviceListaGrupo>();

            if (Session["User"] != null)
            {
                OtimizadorBL obj = new OtimizadorBL();
                FiltroListaGrupo filtro = new FiltroListaGrupo();

                string tp_device = "T,R";
                if (Request["tp_device"] != null && !string.IsNullOrEmpty(Request["tp_device"].ToString()))
                {
                    tp_device = Request["tp_device"].ToString();
                }
                filtro.p_tp_device = tp_device;

                string p_deviceAddress = string.Empty;
                if (Request["p_deviceAddress"] != null)
                {
                    p_deviceAddress = Request["p_deviceAddress"].ToString();
                }
                filtro.p_deviceAddress = p_deviceAddress;

                string p_uc = string.Empty;
                if (Request["p_uc"] != null)
                {
                    p_uc = Request["p_uc"].ToString();
                }
                filtro.p_uc = p_uc;

                string p_municipio = string.Empty;
                if (Request["p_municipio"] != null)
                {
                    p_municipio = Request["p_municipio"].ToString();
                }
                filtro.p_municipio = p_municipio;

                string p_bairro = string.Empty;
                if (Request["p_bairro"] != null)
                {
                    p_bairro = Request["p_bairro"].ToString();
                }
                filtro.p_bairro = p_bairro;

                string p_ip = string.Empty;
                if (Request["p_ipconcentrador"] != null)
                {
                    p_ip = Request["p_ipconcentrador"].ToString();
                }
                filtro.p_ip = p_ip;

                string p_cmbgruposubg = string.Empty;
                if (Request["p_gruposub"] != null)
                {
                    p_cmbgruposubg = Request["p_gruposub"].ToString();
                }
                filtro.p_cmbgruposubg = p_cmbgruposubg;

                string p_cmbgrupodevice = string.Empty;
                if (Request["p_grupo_device"] != null)
                {
                    p_cmbgrupodevice = Request["p_grupo_device"].ToString();
                }
                filtro.p_cmbgrupodevice = p_cmbgrupodevice;

              
                lista = obj.BuscarDeviceGrupo(filtro);

                foreach (DeviceListaGrupo itemLista in lista)
                {
                    itemLista.vl_sinal = itemLista.vl_sinal = (itemLista.vl_sinal / 2) - 130;
                    itemLista.vl_rank = itemLista.vl_rank = Convert.ToInt16(Math.Truncate(Convert.ToDecimal(itemLista.vl_rank / 128)));
                }

            }
            else
            {
                return Json("Expirou", JsonRequestBehavior.AllowGet);
            }

            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult AlterarOtimizacao()
        {
            string retorno = "OK";
            try
            {
                if (Session["User"] != null)
                {
                    if (Request["id_otimizacao"] != null && Request["id_regra"] != null && Request["descricao"] != null)
                    {
                        int id_regra = Convert.ToInt32(Request["id_regra"]);
                        int id_otimizacao = Convert.ToInt32(Request["id_otimizacao"]);
                        string descricao = Convert.ToString(Request["descricao"]);
                        OtimizadorBL obj = new OtimizadorBL();
                        obj.AlterarOtimizacao(id_otimizacao, id_regra, descricao);
                    }
                    else
                    {
                        retorno = "Informe os dados para alterar otimização";
                    }
                }
                else
                {
                    return Json("Expirou");
                }
            }
            catch(Exception ex)
            {
                retorno = "Erro ao alterar otimização contate o administrador do sistema";
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao alterar Otimização.");
            }
            return Json(retorno,JsonRequestBehavior.AllowGet);
        }

        public JsonResult CriarOtimizacao()
        {
            string retorno = "OK";
            try
            {
                if (Request["id_regra"] != null && Request["descricao"] != null)
                {

                    int id_regra = Convert.ToInt32(Request["id_regra"]);
                    string descricao = Convert.ToString(Request["descricao"]);
                    OtimizadorBL obj = new OtimizadorBL();
                    obj.CriarOtimizacao(id_regra, descricao);
                }
                else
                {
                    retorno = "Informe os dados para criar otimização";
                }
                
            }
            catch (Exception ex)
            {
                retorno = "Erro ao criar otimização contate o administrador do sistema";
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao criar otimização.");
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

     
        public JsonResult InserirDeviceOtimizacao()
        {
            string retorno = "OK";
            try
            {

                if (Session["User"] != null)
                {
                    if (Request["lstDevices"] != null && Request["descricao"] != null && Request["id_regra"] != null && !string.IsNullOrEmpty(Request["lstDevices"].ToString()) && !string.IsNullOrEmpty(Request["descricao"].ToString()) && !string.IsNullOrEmpty(Request["id_regra"].ToString()))
                    {
                        string descricao = Convert.ToString(Request["descricao"]);
                        int id_regra = Convert.ToInt32(Request["id_regra"]);
                        Array lstIdsDevices = Request["lstDevices"].ToString().Split(',');
                        List<Device> lst = new List<Device>();
                        foreach (string item in lstIdsDevices)
                        {
                            Device novo = new Device();
                            novo.device_address = item;
                            lst.Add(novo);
                        }

                        OtimizadorBL obj = new OtimizadorBL();
                        obj.CriaOtimizacaoAssociando(id_regra, descricao, lst);
                    }
                    else
                    {
                        retorno = "Informe os dados para criar otimização.";
                    }
                }
                else
                {
                    return Json("Expirou");
                }
            }
            catch (Exception ex)
            {
                retorno = "Erro ao inserir device otimizacao, contate o administrador do sistema.";
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao inserir device otimizacao.");
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }
                  
        public JsonResult ListarRegras()
        {
            List<RegraOtimizador> retorno = new List<RegraOtimizador>();
            try
            {
                OtimizadorBL obj = new OtimizadorBL();
                retorno = obj.ListarRegras();
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar regras para otimizadores.");
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

       

    
    }
}