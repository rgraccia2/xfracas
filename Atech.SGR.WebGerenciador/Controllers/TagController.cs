﻿using Atech.SGR.WebGerenciadorBL;
using Atech.SGR.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;


namespace Atech.SGR.WebGerenciador.Controllers
{
    [Authorize]
    public class TagController : CommonController
    {
        // GET: Tag
        public ActionResult GerenciarTags()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.ConsultarEditarM))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
                   
            }
        }

        public JsonResult ValidaCriarTag()
        {
            string retorno = "OK";
            if (!ValidarAcesso(Enum.EnumPage.CriarMarcador))
            {
                Response.Redirect("/Home/Login");
                retorno = "NO";
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CriarEditarTag()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.CriarMarcador))
                {
                    if (Session["OPER_TAG"] != null)
                    {
                        Session.Add("OPER_TAG", "add");
                    }
                    else
                    {
                        Session["OPER_TAG"] = "add";
                    }

                    ViewBag.Operacao = "add";

                    if (Request["id_tag"] != null)
                    {

                        if (Session["OPER_TAG"] != null)
                        {
                            Session.Add("OPER_TAG", "edit");
                        }
                        else
                        {
                            Session["OPER_REG"] = "edit";
                        }

                        ViewBag.ID = Request["id_tag"].ToString();
                        ViewBag.Nome = Request["nome"].ToString();
                        ViewBag.Descricao = Request["descricao"].ToString();
                        ViewBag.Operacao = "edit";
                        return View();
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }                
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });

            }
        }
               
        public JsonResult ListarTags()
        {
            TagBL obj = new TagBL();
            List<Tag> lista = new List<Tag>();
            lista = obj.ListarTag();            
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalvarTag()
        {
            string retorno = "OK";

            if (Session["User"] != null)
            {
                try
                {
                    Tag tag = new Tag();

                    bool validaNome;

                    int caractEsp = Request["nome"].ToString().Count(p => !char.IsLetterOrDigit(p));

                    if (caractEsp == 0)
                    {
                        if (!string.IsNullOrEmpty(Request["nome"].ToString())
                            && !string.IsNullOrEmpty(Request["descricao"].ToString())
                            )
                        {
                            tag.nome = Request["nome"].ToString();
                            tag.descricao = Request["descricao"].ToString();
                        }
                        else
                        {
                            return Json("Informe os dados da tag.");
                        }

                        if (tag.nome.IndexOf(" ") != -1)
                        {
                            return Json("Marcador não permite espaço.");
                        }

                        else
                        {
                            TagBL obj = new TagBL();

                            validaNome = obj.ValidaNomeTag(tag.nome.ToLower());

                            if (validaNome)
                            {
                                obj.SalvarTag(tag);
                            }
                            else
                                retorno = "O nome da tag ja existe.";
                        }
                        if (retorno == "OK")
                        {
                            Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = tag.nome, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Incluida a tag " + tag.nome, operacao = "Inclusão de Tag" });
                        }
                    }
                    else
                    {
                        retorno = "Não é permitido caracteres especiais no nome da Tag";
                    }
                }
                catch (Exception ex)
                {

                    retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                    Utils.Log.Gravar_Log(ex.ToString(), "Error");
                }
            }
            else
            {
                retorno = "Expirou";
            }
            return Json(retorno);
        }

        public JsonResult EditarTag()
        {
            string retorno = "OK";
            if (Session["User"] != null)
            {
                try
                {
                    Tag tag = new Tag();
                    bool validaNome;

                    int caractEsp = Request["nome"].ToString().Count(p => !char.IsLetterOrDigit(p));

                    if (caractEsp == 0)
                    {


                        if (!string.IsNullOrEmpty(Request["nome"].ToString())
                        && !string.IsNullOrEmpty(Request["descricao"].ToString()))
                        {
                            tag.id_tag = Convert.ToInt32(Request["id"].ToString());
                            tag.nome = Request["nome"].ToString();
                            tag.descricao = Request["descricao"].ToString();
                        }
                        else
                        {
                            return Json("Informe os dados da Tag.");
                        }

                        if (tag.nome.IndexOf(" ") != -1)
                        {
                            return Json("Marcador não permite espaço.");
                        }
                        else
                        {

                            TagBL obj = new TagBL();

                            validaNome = obj.ValidaNomeTag(tag.nome.ToLower(), tag.id_tag);

                            if (validaNome)
                            {
                                obj.EditarTag(tag);
                            }
                            else
                                retorno = "O nome da tag ja existe.";
                        }
                        if (retorno == "OK")
                        {
                            Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = tag.nome, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Alterada a tag " + tag.nome, operacao = "Alterada a Tag" });
                        }
                    }
                    else
                    {
                        retorno = "Não é permitido caracteres especiais no nome da Tag";
                    }
                }
                catch (Exception ex)
                {

                    retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                    Utils.Log.Gravar_Log(ex.ToString(), "Error");
                }
            }
            else
            {
                retorno = "Expirou";
            }

            return Json(retorno);

        }

        public JsonResult ExcluirTag()
        {
            string retorno = "OK";
            if (Session["User"] != null)
            {
                try
                {
                    Tag tag = new Tag();
                    tag.id_tag = Convert.ToInt16(Request["id_tag"].ToString());

                    TagBL obj = new TagBL();

                    obj.DeletarTag(tag.id_tag);

                    if (retorno == "OK")
                    {
                        Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = tag.id_tag.ToString(), cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Excluido tag " + tag.id_tag.ToString(), operacao = "Exclusão de Tag" });
                    }
                }
                catch (Exception ex)
                {
                    retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                    Utils.Log.Gravar_Log(ex.ToString(), "Error");
                }
            }
            else
            {
                retorno = "Expirou";
            }
            return Json(retorno);
        }

    }
}