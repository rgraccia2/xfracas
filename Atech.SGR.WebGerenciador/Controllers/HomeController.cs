﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.XFracas.WebGerenciador.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<Trigger> lstTrigger = new List<Trigger>();

            Trigger t = new Trigger();
            t.hierarquia = "TESTE";
            t.ativo = true;
            t.expressao = "expressão";
            t.Id = "25";
            t.recorrencia_dias = 36;

            lstTrigger.Add(t);


            ViewBag.lstTrigger = lstTrigger;

            return View();
        }
    }
}