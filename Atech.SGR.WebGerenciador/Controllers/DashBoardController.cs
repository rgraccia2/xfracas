﻿using Atech.SGR.WebGerenciadorBL;
using Atech.SGR.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Atech.SGR.WebGerenciador.Controllers
{
    public class DashBoardController : CommonController
    {
        public class GraficoPizza
        {
            public Int64[] arrayValores { get; set; }
            public string[] arrayCores { get; set; }
            public string[] arrayLabels { get; set; }
        }

        // GET: DashBoard
        public ActionResult Index()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.Dashboard))
                {
                    Session.Add("p_filtrorapido", string.Empty);
                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });

            }
        }

        public JsonResult BuscarTotaisIndicadores()
        {

            DashBoardIndicadoresBL obj = new DashBoardIndicadoresBL();
            TotaisIndicadores retorno = obj.BuscarTotaisIndicadores();
            return Json(retorno, JsonRequestBehavior.AllowGet);

        }


        public JsonResult ListarIndicadorErros()
        {

            DateTime dataIni = DateTime.Now.AddDays(-7);
            DateTime dataFim = DateTime.Now;

            DashBoardIndicadoresBL obj = new DashBoardIndicadoresBL();
            List<PacoteErros> retorno = obj.ListarIndicadorErros(dataIni, dataFim);

            List<Int64> arrayValores = new List<Int64>();
            List<string> arrayCores = new List<string>();
            List<string> arrayLabels = new List<string>();

            foreach (PacoteErros item in retorno)
            {
                arrayValores.Add(item.total);
                if (item.indicador == 0)
                {
                    arrayLabels.Add("Indefinido");
                    arrayCores.Add("#AEAEAE");
                }
                else if (item.indicador == 1)
                {
                    arrayLabels.Add("Normal");
                    arrayCores.Add("#70BF54");
                }
                else if (item.indicador == 2)
                {
                    arrayCores.Add("#FF9900");
                    arrayLabels.Add("Atenção");
                }
                else if (item.indicador == 3)
                {
                    arrayLabels.Add("Falha");
                    arrayCores.Add("#ED1C24");
                }
            }

            GraficoPizza ret = new GraficoPizza();
            ret.arrayCores = arrayCores.ToArray();
            ret.arrayValores = arrayValores.ToArray();
            ret.arrayLabels = arrayLabels.ToArray();

            return Json(ret, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ListarIndicadorFalha()
        {
            DateTime dataIni = DateTime.Now.AddDays(-7);
            DateTime dataFim = DateTime.Now;

            DashBoardIndicadoresBL obj = new DashBoardIndicadoresBL();
            List<EquipamentoFalha> retorno = obj.ListarIndicadorFalha(dataIni, dataFim);

            List<Int64> arrayValores = new List<Int64>();
            List<string> arrayCores = new List<string>();
            List<string> arrayLabels = new List<string>();

            foreach (EquipamentoFalha item in retorno)
            {
                arrayValores.Add(item.total);
                if (item.indicador == 0)
                {
                    arrayLabels.Add("Indefinido");
                    arrayCores.Add("#AEAEAE");
                }
                else if (item.indicador == 1)
                {
                    arrayLabels.Add("Normal");
                    arrayCores.Add("#70BF54");
                }
                else if (item.indicador == 2)
                {
                    arrayCores.Add("#FF9900");
                    arrayLabels.Add("Atenção");
                }
                else if (item.indicador == 3)
                {
                    arrayLabels.Add("Falha");
                    arrayCores.Add("#ED1C24");
                }
            }

            GraficoPizza ret = new GraficoPizza();
            ret.arrayCores = arrayCores.ToArray();
            ret.arrayValores = arrayValores.ToArray();
            ret.arrayLabels = arrayLabels.ToArray();

            return Json(ret, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ListarIndicadorLatencia()
        {

            DateTime dataIni = DateTime.Now.AddDays(-7);
            DateTime dataFim = DateTime.Now;

            DashBoardIndicadoresBL obj = new DashBoardIndicadoresBL();
            List<HistoricoLatencia> retorno = obj.ListarIndicadorLatencia(dataIni, dataFim);

            List<Int64> arrayValores = new List<Int64>();
            List<string> arrayCores = new List<string>();
            List<string> arrayLabels = new List<string>();

            foreach (HistoricoLatencia item in retorno)
            {
                arrayValores.Add(item.total);
                if (item.indicador == 0)
                {
                    arrayLabels.Add("Indefinido");
                    arrayCores.Add("#AEAEAE");
                }
                else if (item.indicador == 1)
                {
                    arrayLabels.Add("Normal");
                    arrayCores.Add("#70BF54");
                }
                else if (item.indicador == 2)
                {
                    arrayCores.Add("#FF9900");
                    arrayLabels.Add("Atenção");
                }
                else if (item.indicador == 3)
                {
                    arrayLabels.Add("Falha");
                    arrayCores.Add("#ED1C24");
                }
            }

            GraficoPizza ret = new GraficoPizza();
            ret.arrayCores = arrayCores.ToArray();
            ret.arrayValores = arrayValores.ToArray();
            ret.arrayLabels = arrayLabels.ToArray();

            return Json(ret, JsonRequestBehavior.AllowGet);
        }

    }
}