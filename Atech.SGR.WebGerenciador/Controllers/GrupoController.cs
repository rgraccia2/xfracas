﻿using Atech.SGR.WebGerenciadorBL;
using Atech.SGR.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Atech.SGR.WebGerenciador.Controllers
{
    [Authorize]
    public class GrupoController : CommonController
    {
        public const int IndicadorFalha = 1;
        public const int IndicadorErros = 2;
        public const int IndicadorThroughput = 3;
        public const int IndicadorRank = 4;
        public const int IndicadorVizinhos = 5;
        public const int IndicadorSinal = 6;
        public const int IndicadorLatencia = 7;
        public const int IndicadorNaoSelecionado = 9;


        //tela de criar grupo        
        public ActionResult CriarEditarGrupo()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.CriarGrupo))
                {
                    if (Request["oper"] != null)
                    {
                        ViewBag.Operacao = Request["oper"].ToString();
                        if (Session["OPER"] != null)
                        {
                            Session.Add("OPER", ViewBag.Operacao);
                        }
                        else
                        {
                            Session["OPER"] = ViewBag.Operacao;
                        }
                    }
                    if (Request["id_grupo"] != null)
                    {
                        ViewBag.Operacao = Request["oper"].ToString();
                        ViewBag.nome_grupo = Request["nomeGrupo"].ToString();
                        ViewBag.descricao = Request["descricao"].ToString();
                        ViewBag.tempoBase = Request["tb"].ToString();
                        if (Session["IDGRUPO"] == null)
                        {
                            Session.Add("IDGRUPO", Convert.ToInt32(Request["id_grupo"]));
                        }
                        else
                        {
                            Session["IDGRUPO"] = Convert.ToInt32(Request["id_grupo"]);
                        }
                        GrupoBL obj = new GrupoBL();
                        List<Regra> lista = new List<Regra>();

                        lista = obj.ListarRegras(Convert.ToInt32(Request["id_grupo"]));
                        Session.Add("LISTAREGRA", lista);

                        ViewBag.LstErro = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorErros);
                        ViewBag.LstThroughput = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorThroughput);

                        List<Regra> listaRank = new List<Regra>();
                        listaRank = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorRank);
                        foreach (Regra itemRank in listaRank)
                        {
                            itemRank.lim_inferior = Convert.ToInt16(Math.Truncate(Convert.ToDecimal(itemRank.lim_inferior / 128)));
                        }
                        ViewBag.LstRank = listaRank;


                        ViewBag.LstVizinhos = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorVizinhos);

                        List<Regra> listaSinal = new List<Regra>();
                        listaSinal = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorSinal);
                        foreach (Regra itemSinal in listaSinal)
                        {
                            itemSinal.lim_inferior = (itemSinal.lim_inferior / 2) - 130;
                        }
                        ViewBag.LstSinal = listaSinal;

                        ViewBag.LstLatencia = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorLatencia);

                        ViewBag.vlInicial = (from cust in lista where cust.tipo_indicador == 1 orderby cust.lim_inferior ascending select cust.lim_inferior).FirstOrDefault();
                        ViewBag.vlFinal = (from cust in lista where cust.tipo_indicador == 1 orderby cust.lim_inferior descending select cust.lim_inferior).FirstOrDefault();

                        List<Grupo> lstGrupo = new List<Grupo>();
                        Grupo item = new Grupo();
                        item.nome = "Selecione um Grupo";
                        lstGrupo.Add(item);
                        lstGrupo.AddRange(obj.ListarGrupos());
                        ViewBag.ListaGrupos = new SelectList(lstGrupo, "id_grupo", "nome");

                        return View();
                    }
                    else
                    {
                        ViewBag.LstErro = new List<Regra>();
                        ViewBag.LstThroughput = new List<Regra>();
                        ViewBag.LstRank = new List<Regra>();
                        ViewBag.LstVizinhos = new List<Regra>();
                        ViewBag.LstSinal = new List<Regra>();
                        ViewBag.LstLatencia = new List<Regra>();
                        Session.Add("LISTAREGRA", new List<Regra>());
                        ViewBag.Operacao = "add";

                        if (Session["OPER"] != null)
                        {
                            Session.Add("OPER", "add");
                        }
                        else
                        {
                            Session["OPER"] = "add";
                        }

                        List<Grupo> lstGrupo = new List<Grupo>();
                        GrupoBL obj = new GrupoBL();
                        Grupo item = new Grupo();
                        item.nome = "Selecione um Grupo";
                        lstGrupo.Add(item);
                        lstGrupo.AddRange(obj.ListarGrupos());
                        ViewBag.ListaGrupos = new SelectList(lstGrupo, "id_grupo", "nome");
                        return View();
                    }

                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }
        
        public JsonResult RetornaRegras()
        {
            if (Session["User"] != null)
            {
                if (Request["id_grupo"] != null && !String.IsNullOrEmpty(Request["id_grupo"].ToString()))
                {
                    string retorno = "OK";
                    GrupoBL obj = new GrupoBL();
                    List<Regra> lista = new List<Regra>();

                    lista = obj.ListarRegras(Convert.ToInt32(Request["id_grupo"]));

                    foreach (Regra listaitem in lista)
                    {
                        if (listaitem.tipo_indicador == IndicadorRank)
                            listaitem.lim_inferior = Convert.ToInt16(Math.Truncate(Convert.ToDecimal(listaitem.lim_inferior / 128)));
                        if (listaitem.tipo_indicador == IndicadorSinal)
                            listaitem.lim_inferior = (listaitem.lim_inferior / 2) - 130;
                    }

                    Session.Add("LISTAREGRA", lista);
                    return Json(retorno);
                }
                else
                {
                    return Json("NAOSELECIONADO");
                }
            }
            else
            {
                return Json("Expirou");
            }
        }
       
        public JsonResult AtualizarGrupo()
        {
            var lst = new List<Regra>();
            if (Session["LISTAREGRA"] != null)
            {
                if (Request["oper"] == "del")
                {
                    string[] ids = Request["id"].ToString().Split(',');
                    foreach (string id in ids)
                    {
                        lst = ((List<Regra>)Session["LISTAREGRA"]);
                        lst.Remove(lst.Find(t => t.id_regra == Convert.ToInt32(id)));
                    }
                    Session.Add("LISTAREGRA", lst);
                }

                else if (Request["oper"] == "edit")
                {
                    lst = ((List<Regra>)Session["LISTAREGRA"]);
                    Regra RegraEditar = lst.Find(t => t.id_regra == Convert.ToInt32(Request["id"]));
                    if (Session["IDGRUPO"] != null && !string.IsNullOrEmpty(Session["IDGRUPO"].ToString()))
                    {
                        RegraEditar.id_grupo = Convert.ToInt32(Session["IDGRUPO"].ToString());
                    }
                    RegraEditar.nivel_alerta = Convert.ToInt32(Request["nivel_alerta"]);
                    RegraEditar.lim_inferior = Convert.ToInt32(Request["lim_inferior"]);
                    Session.Add("LISTAREGRA", lst);
                }
                else if (Request["oper"] == "add")
                {
                    lst = ((List<Regra>)Session["LISTAREGRA"]);
                    int index = 0;
                    Regra novaRegra = new Regra();
                    if (lst != null)
                    {
                        index = lst.Count;
                    }
                    if (Session["IDGRUPO"] != null && string.IsNullOrEmpty(Session["IDGRUPO"].ToString()))
                    {
                        novaRegra.id_grupo = Convert.ToInt32(Session["IDGRUPO"].ToString());
                    }

                    novaRegra.id_regra = index++;
                    novaRegra.tipo_indicador = Convert.ToInt32(Request["tp_indicador"]);
                    novaRegra.lim_inferior = Convert.ToInt32(Request["lim_inferior"]);
                    novaRegra.nivel_alerta = Convert.ToInt32(Request["nivel_alerta"]);
                    lst.Add(novaRegra);
                    Session.Add("LISTAREGRA", lst);
                }
            }

            return Json("OK");
        }
       
        public JsonResult AtualizarGrid()
        {
            Int32 indicador = Convert.ToInt32(Request["tp_indicador"]);
            var lista = new List<Regra>();
            if (indicador == IndicadorErros)
            {
                lista = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorErros);
            }
            else if (indicador == IndicadorThroughput)
            {
                lista = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorThroughput);
            }
            else if (indicador == IndicadorSinal)
            {
                lista = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorSinal);
            }
            else if (indicador == IndicadorRank)
            {
                lista = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorRank);
            }
            else if (indicador == IndicadorVizinhos)
            {
                lista = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorVizinhos);
            }
            else if (indicador == IndicadorLatencia)
            {
                lista = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorLatencia);
            }
            else if (indicador == IndicadorFalha)
            {
                lista = ((List<Regra>)Session["LISTAREGRA"]).FindAll(x => x.tipo_indicador == IndicadorFalha);
                lista.OrderBy(p => p.lim_inferior);
            }
            else if (indicador == IndicadorNaoSelecionado)
            {
                lista = new List<Regra>();
            }

            return Json(lista, JsonRequestBehavior.AllowGet);
        }
     
        public JsonResult SalvarGrupoRegra()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.CriarGrupo))
                {
                    string retorno = "OK";
                    int id = 0;

                    try
                    {
                        Grupo grupo = new Grupo();
                        bool validaNome;
                        string validaRegra;
                        if (Request["nome"] != null
                            && Request["tb"] != null
                            && !string.IsNullOrEmpty(Request["nome"].ToString())
                            && !string.IsNullOrEmpty(Request["tb"].ToString()))
                        {
                            grupo.nome = Request["nome"].ToString();
                            grupo.descricao = Request["descricao"].ToString();
                            grupo.tb = Convert.ToInt32(Request["tb"].ToString());
                        }
                        else
                        {
                            return Json("Informe os dados grupo.");
                        }

                        grupo.ListaRegra = new List<Regra>();
                        grupo.ListaRegra = (List<Regra>)Session["LISTAREGRA"];

                        if (Request["valor1"] != null && Request["valor2"] != null && !string.IsNullOrEmpty(Request["valor1"].ToString()) && !string.IsNullOrEmpty(Request["valor2"].ToString()))
                        {
                            int vli = Convert.ToInt32(Request["valor1"].ToString());
                            int vlf = Convert.ToInt32(Request["valor2"].ToString());

                            if (vli < vlf)
                            {
                                if (grupo.ListaRegra.Find(p => p.tipo_indicador == 1 && p.nivel_alerta == 1) == null)
                                {
                                    Regra regraFalhaIni = new Regra();
                                    regraFalhaIni.id_regra = grupo.ListaRegra.Count() + 1;
                                    regraFalhaIni.lim_inferior = Convert.ToInt32(Request["valor1"].ToString());
                                    regraFalhaIni.nivel_alerta = 1; // Faixa Inicial
                                    regraFalhaIni.tipo_indicador = 1; // Indicador Falha
                                    grupo.ListaRegra.Add(regraFalhaIni);
                                }
                                if (grupo.ListaRegra.Find(p => p.tipo_indicador == 1 && p.nivel_alerta == 2) == null)
                                {
                                    Regra regraFalhaFim = new Regra();
                                    regraFalhaFim.id_regra = grupo.ListaRegra.Count() + 1;
                                    regraFalhaFim.lim_inferior = Convert.ToInt32(Request["valor2"].ToString());
                                    regraFalhaFim.nivel_alerta = 2; // Faixa Inicial
                                    regraFalhaFim.tipo_indicador = 1; // Indicador Falha
                                    grupo.ListaRegra.Add(regraFalhaFim);
                                }
                            }
                            else
                            {
                                return Json("O indicador de falha deve conter a quantidade de minutos Atenção menor que Falha");
                            }
                        }
                        else
                        {
                            return Json("Informe os valores para a regra de falha.");
                        }

                        GrupoBL obj = new GrupoBL();
                        validaNome = obj.ValidaNomeGrupo(grupo.nome.ToLower());

                        if (validaNome)
                        {
                            validaRegra = validaRegras(grupo.ListaRegra);

                            if (validaRegra == "OK")
                            {
                                id = obj.SalvarGrupo(grupo);

                                foreach (var item in grupo.ListaRegra)
                                {
                                    item.id_grupo = id;

                                    if (item.tipo_indicador == IndicadorSinal)
                                    {
                                        item.lim_inferior = (item.lim_inferior + 130) * 2;
                                    };

                                    if (item.tipo_indicador == IndicadorRank)
                                    {
                                        item.lim_inferior = item.lim_inferior * 128;
                                    }

                                }

                                obj.SalvarRegraGrupo(grupo.ListaRegra, id);
                            }
                            else
                                retorno = validaRegra;
                        }
                        else
                            retorno = "O nome do grupo ja existe.";

                        if (retorno == "OK")
                        {
                            Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = grupo.nome, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Incluido grupo " + grupo.nome, operacao = "Inclusão de Grupo" });
                        }
                    }
                    catch (Exception ex)
                    {
                        GrupoBL obj = new GrupoBL();
                        obj.DeletarGrupo(id);
                        retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                        Utils.Log.Gravar_Log(ex.ToString(), "Error");
                    }
                    return Json(retorno);
                }
                else
                    return Json("Acesso Negado");
            }
            else
            {
                return Json("Expirou");
            }
        }
       
        public JsonResult AlterarGrupoRegra()
        {
            Grupo grupo = new Grupo();
            if (Session["IDGRUPO"] != null && !string.IsNullOrEmpty(Session["IDGRUPO"].ToString()))
            {
                grupo.id_grupo = Convert.ToInt32(Session["IDGRUPO"].ToString());
            }
            else
            {
                return Json("Expirou");
            }

            if (ValidarAcesso(Enum.EnumPage.CriarGrupo))
            {
                string retorno = "OK";
                try
                {
                    bool validaNome = false;
                    string validaRegra;
                    if (Request["nome"] != null
                        && Request["tb"] != null
                        && !string.IsNullOrEmpty(Request["nome"].ToString())
                        && !string.IsNullOrEmpty(Request["tb"].ToString()))
                    {
                        grupo.nome = Request["nome"].ToString();
                        grupo.descricao = Request["descricao"].ToString();
                        grupo.tb = Convert.ToInt32(Request["tb"].ToString());
                    }
                    else
                    {
                        return Json("Informe os dados grupo.");
                    }

                    grupo.ListaRegra = new List<Regra>();
                    grupo.ListaRegra = (List<Regra>)Session["LISTAREGRA"];

                    if (Request["valor1"] != null && Request["valor2"] != null && !string.IsNullOrEmpty(Request["valor1"].ToString()) && !string.IsNullOrEmpty(Request["valor2"].ToString()))
                    {
                        Regra regra1 = grupo.ListaRegra.Find(p => p.tipo_indicador == 1 && p.nivel_alerta == 1);
                        if (regra1 == null)
                        {
                            Regra regraFalhaIni = new Regra();
                            regraFalhaIni.id_regra = grupo.ListaRegra.Count() + 1;
                            regraFalhaIni.lim_inferior = Convert.ToInt32(Request["valor1"].ToString());
                            regraFalhaIni.nivel_alerta = 1; // Faixa Inicial
                            regraFalhaIni.tipo_indicador = 1; // Indicador Falha
                            grupo.ListaRegra.Add(regraFalhaIni);
                        }
                        else
                        {
                            regra1.lim_inferior = Convert.ToInt32(Request["valor1"].ToString());
                        }
                        Regra regra2 = grupo.ListaRegra.Find(p => p.tipo_indicador == 1 && p.nivel_alerta == 2);
                        if (regra2 == null)
                        {
                            Regra regraFalhaFim = new Regra();
                            regraFalhaFim.id_regra = grupo.ListaRegra.Count() + 1;
                            regraFalhaFim.lim_inferior = Convert.ToInt32(Request["valor2"].ToString());
                            regraFalhaFim.nivel_alerta = 2; // Faixa Inicial
                            regraFalhaFim.tipo_indicador = 1; // Indicador Falha
                            grupo.ListaRegra.Add(regraFalhaFim);
                        }
                        else
                        {
                            regra2.lim_inferior = Convert.ToInt32(Request["valor2"].ToString());
                        }

                    }
                    else
                    {
                        return Json("Informe os valores para a regra de falha.");
                    }

                    GrupoBL obj = new GrupoBL();
                    validaNome = obj.ValidaNomeGrupo(grupo.nome.ToLower(), grupo.id_grupo);

                    if (validaNome)
                    {
                        validaRegra = validaRegras(grupo.ListaRegra);

                        if (validaRegra == "OK")
                        {
                            foreach (var item in grupo.ListaRegra)
                            {
                                if (item.tipo_indicador == IndicadorSinal)
                                {
                                    item.lim_inferior = (item.lim_inferior + 130) * 2;
                                };

                                if (item.tipo_indicador == IndicadorRank)
                                {
                                    item.lim_inferior = item.lim_inferior * 128;
                                }

                            }

                            obj.AlterarGrupo(grupo);

                            obj.SalvarRegraGrupo(grupo.ListaRegra, Convert.ToInt32(grupo.id_grupo));
                        }
                        else
                            retorno = validaRegra;
                    }
                    else
                        retorno = "O nome do grupo ja existe.";

                    if (retorno == "OK")
                    {
                        Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = grupo.nome, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Alterado os dados do grupo " + grupo.nome, operacao = "Alteração de Grupo" });
                    }

                }
                catch (Exception ex)
                {

                    retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                    Utils.Log.Gravar_Log(ex.ToString(), "Error");
                }

                return Json(retorno);
            }
            else
                return Json("Acesso Negado");


        }

        public string validaRegras(List<Regra> item)
        {
            string retorno;
            string msgFalha;
            List<Regra> regraFalha = item.FindAll(p => p.tipo_indicador == IndicadorFalha);
            if (regraFalha.Count == 2 && (regraFalha[0].lim_inferior != regraFalha[1].lim_inferior))
                msgFalha = "OK";
            else if (regraFalha.Count == 2 && (regraFalha[0].lim_inferior < regraFalha[1].lim_inferior))
                msgFalha = "O indicador de atenção deve ser menor que o indicador de falha.";
            else
                msgFalha = "O indicador de falha deve conter 2 parametros de valores distintos.";

            string msgErro;
            List<Regra> regraErro = item.FindAll(p => p.tipo_indicador == IndicadorErros);
            if (regraErro.Count == 0)
                msgErro = "O indicador de erros deve conter pelo menos uma regra.";
            else
            {
                if (regraErro.GroupBy(a => a.lim_inferior).Any(a => a.Count() > 1))
                    msgErro = "O indicador de erros deve conter parametros de valores distintos.";
                else
                    msgErro = "OK";
            }

            string msgThroughput;
            List<Regra> regraThroughput = item.FindAll(p => p.tipo_indicador == IndicadorThroughput);
            if (regraThroughput.Count == 0)
                msgThroughput = "O indicador de Throughput deve conter pelo menos uma regra.";
            else
            {
                if (regraThroughput.GroupBy(a => a.lim_inferior).Any(a => a.Count() > 1))
                    msgThroughput = "O indicador de Throughput deve conter parametros de valores distintos.";
                else
                    msgThroughput = "OK";
            }

            string msgRank;
            List<Regra> regraRank = item.FindAll(p => p.tipo_indicador == IndicadorRank);
            if (regraRank.Count == 0)
                msgRank = "O indicador de Rank deve conter pelo menos uma regra.";
            else
            {
                if (regraRank.GroupBy(a => a.lim_inferior).Any(a => a.Count() > 1))
                    msgRank = "O indicador de Rank deve conter parametros de valores distintos.";
                else
                    msgRank = "OK";
            }

            string msgVizinhos;
            List<Regra> regraVizinhos = item.FindAll(p => p.tipo_indicador == IndicadorVizinhos);
            if (regraVizinhos.Count == 0)
                msgVizinhos = "O indicador de Vizinhos deve conter pelo menos uma regra.";
            else
            {
                if (regraVizinhos.GroupBy(a => a.lim_inferior).Any(a => a.Count() > 1))
                    msgVizinhos = "O indicador de Vizinhos deve conter parametros de valores distintos.";
                else
                    msgVizinhos = "OK";
            }

            string msgSinal;
            List<Regra> regraSinal = item.FindAll(p => p.tipo_indicador == IndicadorSinal);
            if (regraSinal.Count == 0)
                msgSinal = "O indicador de Sinal deve conter pelo menos uma regra.";
            else
            {
                if (regraSinal.GroupBy(a => a.lim_inferior).Any(a => a.Count() > 1))
                    msgSinal = "O indicador de Sinal deve conter parametros de valores distintos.";
                else
                    msgSinal = "OK";
            }

            string msgLatencia;
            List<Regra> regraLatencia = item.FindAll(p => p.tipo_indicador == IndicadorLatencia);
            if (regraLatencia.Count == 0)
                msgLatencia = "O indicador de Latência deve conter pelo menos uma regra.";
            else
            {
                if (regraVizinhos.GroupBy(a => a.lim_inferior).Any(a => a.Count() > 1))
                    msgLatencia = "O indicador de Latência deve conter parametros de valores distintos.";
                else
                    msgLatencia = "OK";
            }

            if (msgFalha == "OK" && msgErro == "OK" && msgLatencia == "OK" && msgRank == "OK" && msgSinal == "OK" && msgThroughput == "OK" && msgVizinhos == "OK")
            {
                retorno = "OK";
            }
            else
            {
                retorno = "";
                if (msgFalha != "OK") retorno = msgFalha;
                if (msgErro != "OK") retorno = retorno + " " + msgErro;
                if (msgThroughput != "OK") retorno = retorno + " " + msgThroughput;
                if (msgRank != "OK") retorno = retorno + " " + msgRank;
                if (msgVizinhos != "OK") retorno = retorno + " " + msgVizinhos;
                if (msgSinal != "OK") retorno = retorno + " " + msgSinal;
                if (msgLatencia != "OK") retorno = retorno + " " + msgLatencia;
            }
            return retorno;
        }


        //tela gerenciar grupos

        public ActionResult GerenciarGrupos()
        {           
            try
            {
                List<Grupo> listaGrupo = new List<Grupo>();
                if (Session["User"] != null)
                {
                    if (ValidarAcesso(Enum.EnumPage.ConsultarEditar))
                    {
                        GrupoBL obj = new GrupoBL();
                        listaGrupo.AddRange(obj.ListarGrupos());
                        ViewBag.ListaGrupo = new SelectList(listaGrupo, "id_grupo", "nome");
                    }
                    else
                    {
                        return RedirectToAction("AcessoNegado", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Home", new { ses = "S" });
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupos.");
            }
            return View();
        }

        public JsonResult ListarGrupos()
        {
            GrupoBL obj = new GrupoBL();
            List<Grupo> lista = new List<Grupo>();
            lista = obj.ListarGrupos();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExcluirGrupoRegra()
        {
            string retorno = "OK";

            if (Session["User"] != null)
            {
                try
                {
                    Grupo grupo = new Grupo();
                    grupo.id_grupo = Convert.ToInt16(Request["id_grupo"].ToString());

                    if (grupo.id_grupo == 0)
                        retorno = "Default";
                    else
                    {
                        GrupoBL obj = new GrupoBL();
                        int? devices = obj.VerificaDeviceGrupo(grupo.id_grupo);

                        if (devices == 0)
                        {
                            obj.DeletarGrupo(grupo.id_grupo);
                        }
                        else
                        {
                            retorno = "Devices";
                        }
                    }

                    if (retorno == "OK")
                    {
                        Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = grupo.id_grupo.ToString(), cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Excluido grupo " + grupo.id_grupo.ToString(), operacao = "Exclusão de Grupo" });
                    }
                }
                catch (Exception ex)
                {
                    retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                    Utils.Log.Gravar_Log(ex.ToString(), "Error");
                }
            }
            else
            {
                retorno = "Expirou";
            }

            return Json(retorno);
        }
        
        public JsonResult TransferenciaExcluirGrupoRegra()
        {
            string retorno = "OK";
            if (Session["User"] != null)
            {
                try
                {
                    GrupoBL obj = new GrupoBL();

                    if (Convert.ToInt16(Request["id_grupo"].ToString()) == Convert.ToInt16(Request["id_transferencia"].ToString()))
                        retorno = "O grupo de transferência não pode ser o mesmo a ser excluido, escolha outro grupo.";

                    else
                        obj.TransferirDeletarGrupo(Convert.ToInt16(Request["id_grupo"].ToString()), Convert.ToInt16(Request["id_transferencia"].ToString()));

                    if (retorno == "OK")
                    {
                        Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = Request["id_grupo"].ToString(), cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Excluido grupo " + Request["id_grupo"].ToString() + " e devices transferidos para grupo " + Request["id_transferencia"].ToString(), operacao = "Exclusão Grupo" });
                    }
                }
                catch (Exception ex)
                {
                    retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                    Utils.Log.Gravar_Log(ex.ToString(), "Error");
                }
                return Json(retorno);
            }
            else
            {
                retorno = "Expirou";
                return Json(retorno);
            }
        }




        //Associar device

        public ActionResult AssociarDevices()
        {
            List<Grupo> listaGrupo = new List<Grupo>();
            try
            {
                if (Session["User"] != null)
                {
                    if (ValidarAcesso(Enum.EnumPage.AssociarDevices))
                    {
                        GrupoBL obj = new GrupoBL();
                        listaGrupo.AddRange(obj.ListarGrupos());
                        ViewBag.ListaGrupo = new SelectList(listaGrupo, "id_grupo", "nome");
                    }
                    else
                    {
                        return RedirectToAction("AcessoNegado", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Home", new { ses = "S" });
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupos.");
            }

            List<Municipio> lstMunicipio = new List<Municipio>();
            try
            {
                UcBL obj = new UcBL();
                Municipio item = new Municipio();
                item.municipio = "-TODOS-";
                lstMunicipio.Add(item);
                lstMunicipio.AddRange(obj.ListarMunicipio());

                ViewBag.ListaMunicipio = new SelectList(lstMunicipio, "municipio", "municipio");
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar municipio.");
            }

            List<GrupoSubGrupo> lstGrupoSubGrupo = new List<GrupoSubGrupo>();
            try
            {
                UcBL obj = new UcBL();
                GrupoSubGrupo item = new GrupoSubGrupo();
                item.tipo_subgrupo = "-TODOS-";
                lstGrupoSubGrupo.Add(item);
                lstGrupoSubGrupo.AddRange(obj.ListarGrupoSubGrupo());

                ViewBag.ListaGrupoSubGrupo = new SelectList(lstGrupoSubGrupo, "tipo_subgrupo", "tipo_subgrupo");
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupo sub grupo.");
            }

            List<Grupo> lstGrupoDevice = new List<Grupo>();
            try
            {
                GrupoBL obj = new GrupoBL();
                Grupo item = new Grupo();
                item.id_grupo = null;
                item.nome = "-TODOS-";
                lstGrupoDevice.Add(item);
                lstGrupoDevice.AddRange(obj.ListarGrupos());
                ViewBag.ListaGrupoDevice = new SelectList(lstGrupoDevice, "id_grupo", "nome");
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupos.");
            }

            ViewBag.LstDevices = new List<DeviceListaGrupo>();

            return View();
        }
        
        public JsonResult AssociarDeviceGrupo()
        {
            if (Session["User"] != null)
            {              
                    string retorno = "OK";

                    try
                    {
                        int id_grupo = Convert.ToInt16(Request["id_grupo"].ToString());

                        var devices = Request["lista_device"].ToString();
                        List<string> lstarray = devices.Split(',').ToList();

                        Device device = new Device();
                        List<Device> list = new List<Device>();

                        foreach (string item in lstarray)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                Device deviceitem = new Device();
                                deviceitem.device_address = item;
                                list.Add(deviceitem);
                            }
                        }


                        GrupoBL obj = new GrupoBL();
                        obj.AssociarDeviceGrupo(list, id_grupo);

                        if (retorno == "OK")
                        {
                            Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = id_grupo.ToString(), cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Transferido " + list.Count + " device(s) para o grupo " + id_grupo.ToString(), operacao = "Transferencia de Devices" });
                        }

                    }
                    catch (Exception ex)
                    {
                        retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                        Utils.Log.Gravar_Log(ex.ToString(), "Error");
                    }

                    return Json(retorno);                
              
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult GetDadosDeviceGrupo()
        {
            GrupoBL obj = new GrupoBL();
            FiltroListaGrupo filtro = new FiltroListaGrupo();

            string tp_device = "T,C,R";
            if (Request["tp_device"] != null && !string.IsNullOrEmpty(Request["tp_device"].ToString()))
            {
                tp_device = Request["tp_device"].ToString();
            }
            filtro.p_tp_device = tp_device;

            string p_deviceAddress = string.Empty;
            if (Request["p_deviceAddress"] != null)
            {
                p_deviceAddress = Request["p_deviceAddress"].ToString();
            }
            filtro.p_deviceAddress = p_deviceAddress;

            string p_uc = string.Empty;
            if (Request["p_uc"] != null)
            {
                p_uc = Request["p_uc"].ToString();
            }
            filtro.p_uc = p_uc;

            string p_municipio = string.Empty;
            if (Request["p_municipio"] != null)
            {
                p_municipio = Request["p_municipio"].ToString();
            }
            filtro.p_municipio = p_municipio;

            string p_bairro = string.Empty;
            if (Request["p_bairro"] != null)
            {
                p_bairro = Request["p_bairro"].ToString();
            }
            filtro.p_bairro = p_bairro;

            string p_ip = string.Empty;
            if (Request["p_ipconcentrador"] != null)
            {
                p_ip = Request["p_ipconcentrador"].ToString();
            }
            filtro.p_ip = p_ip;

            string p_cmbgruposubg = string.Empty;
            if (Request["p_gruposub"] != null)
            {
                p_cmbgruposubg = Request["p_gruposub"].ToString();
            }
            filtro.p_cmbgruposubg = p_cmbgruposubg;

            string p_cmbgrupodevice = string.Empty;
            if (Request["p_grupo_device"] != null)
            {
                p_cmbgrupodevice = Request["p_grupo_device"].ToString();
            }
            filtro.p_cmbgrupodevice = p_cmbgrupodevice;

            List<DeviceListaGrupo> lista = new List<DeviceListaGrupo>();
            lista = obj.BuscarDeviceGrupo(filtro);

            foreach (DeviceListaGrupo itemLista in lista)
            {
                itemLista.vl_sinal = itemLista.vl_sinal = (itemLista.vl_sinal / 2) - 130;
                itemLista.vl_rank = itemLista.vl_rank = Convert.ToInt16(Math.Truncate(Convert.ToDecimal(itemLista.vl_rank / 128)));
            }

            return Json(lista, JsonRequestBehavior.AllowGet);
        }


        //Acho que nao usados

        public JsonResult RetornaListaDevice()
        {

            string retorno = "OK";

            try
            {
                List<Device> list = new List<Device>();
                int id_grupo;
                string device_address;
                string num_medidor;
                string tp_device;

                #region Dados para teste
                //*****************************  Dados para teste
                id_grupo = 23;

                //*****************************
                #endregion

                GrupoBL obj = new GrupoBL();
                obj.AssociarDeviceGrupo(list, id_grupo);
            }
            catch (Exception ex)
            {
                retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                Utils.Log.Gravar_Log(ex.ToString(), "Error");
            }

            return Json(retorno);
        }
        
        public JsonResult BuscaGrupo()
        {
            string nome = "";

            GrupoBL obj = new GrupoBL();
            List<Grupo> lista = new List<Grupo>();
            lista = obj.BuscarGrupo(nome.ToUpper());
            return Json(lista, JsonRequestBehavior.AllowGet);

        }        

    }

}