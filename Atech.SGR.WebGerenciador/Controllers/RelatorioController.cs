﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atech.SGR.WebGerenciadorBL;
using Atech.SGR.WebGerenciadorInterface.Model;
using System.Web.Mvc;
using System.Text;
using System.Reflection;

namespace Atech.SGR.WebGerenciador.Controllers
{
    [Authorize]
    public class RelatorioController : CommonController
    {
        public ActionResult GerenciarRelatorios()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.Relatorios))
                {
                    List<Municipio> lstMunicipio = new List<Municipio>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Municipio item = new Municipio();
                        item.municipio = "-TODOS-";
                        lstMunicipio.Add(item);
                        lstMunicipio.AddRange(obj.ListarMunicipio());

                        ViewBag.ListaMunicipio = new SelectList(lstMunicipio, "municipio", "municipio");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar municipio.");
                    }

                    List<Bairro> lstBairro = new List<Bairro>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Bairro item = new Bairro();
                        item.bairro = "";
                        lstBairro.Add(item);
                        lstBairro.AddRange(obj.ListarBairro());

                        ViewBag.ListaBairro = new SelectList(lstBairro, "bairro", "bairro");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar bairro.");
                    }

                    List<GrupoSubGrupo> lstGrupoSubGrupo = new List<GrupoSubGrupo>();
                    try
                    {
                        UcBL obj = new UcBL();
                        GrupoSubGrupo item = new GrupoSubGrupo();
                        item.tipo_subgrupo = "-TODOS-";
                        lstGrupoSubGrupo.Add(item);
                        lstGrupoSubGrupo.AddRange(obj.ListarGrupoSubGrupo());

                        ViewBag.ListaGrupoSubGrupo = new SelectList(lstGrupoSubGrupo, "tipo_subgrupo", "tipo_subgrupo");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupo/sub grupo.");
                    }

                    List<Tag> lstTag = new List<Tag>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Tag item = new Tag();
                        item.nome = "-TODOS-";
                        lstTag.Add(item);
                        lstTag.AddRange(obj.ListarTag());

                        ViewBag.ListaMarcadores = new SelectList(lstTag, "id_tag", "nome");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupo/sub grupo.");
                    }

                    List<Grupo> listaGrupo = new List<Grupo>();
                    try
                    {
                        GrupoBL obj = new GrupoBL();
                        Grupo item = new Grupo();
                        item.nome = "-TODOS-";
                        listaGrupo.Add(item);
                        listaGrupo.AddRange(obj.ListarGrupos());
                        ViewBag.ListaGrupo = new SelectList(listaGrupo, "id_grupo", "nome");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupos.");
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }

        public JsonResult GetDevice()
        {
            if (Session["User"] != null)
            {  
                RelatorioBL obj = new RelatorioBL();
                FiltroRelatorioDevice filtro = new FiltroRelatorioDevice();

                string tp_device = "T,C,R";
                if (Request["tp_device"] != null && !string.IsNullOrEmpty(Request["tp_device"].ToString()))
                {
                    tp_device = Request["tp_device"].ToString();
                }
                filtro.p_tp_device = tp_device;

                string p_municipio = string.Empty;
                if (Request["p_municipio"] != null && !string.IsNullOrEmpty(Request["p_municipio"].ToString()))
                {
                    p_municipio = Request["p_municipio"].ToString();
                }
                filtro.p_municipio = p_municipio;

                string p_bairro = string.Empty;
                if (Request["p_bairro"] != null && !string.IsNullOrEmpty(Request["p_bairro"].ToString()))
                {
                    p_bairro = Request["p_bairro"].ToString();
                }
                filtro.p_bairro = p_bairro;

                string p_ip = string.Empty;
                if (Request["p_ipconcentrador"] != null && !string.IsNullOrEmpty(Request["p_ipconcentrador"].ToString()))
                {
                    p_ip = Request["p_ipconcentrador"].ToString();
                }
                filtro.p_ip = p_ip;

                int p_marcador = 0;
                if (Request["p_marcador"] != null && !string.IsNullOrEmpty(Request["p_marcador"].ToString()))
                {
                    p_marcador = Convert.ToInt32(Request["p_marcador"].ToString());
                }
                filtro.p_marcador = p_marcador;

                string p_grupo_device = string.Empty;
                if (Request["p_grupo_device"] != null && !string.IsNullOrEmpty(Request["p_grupo_device"].ToString()))
                {
                    p_grupo_device = Request["p_grupo_device"].ToString();
                }
                filtro.p_cmbgrupodevice = p_grupo_device;

                List<RelatorioDevice> lista = new List<RelatorioDevice>();
                lista = obj.BuscarDevice(filtro);

                return GetYourFile(lista, "Devices");
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult GetUltimosDevicesInstalados()
        {
            if (Session["User"] != null)
            {
                RelatorioBL obj = new RelatorioBL();
                FiltroRelatorioDevice filtro = new FiltroRelatorioDevice();

                string tp_device = "T,C,R";
                if (Request["tp_device"] != null && !string.IsNullOrEmpty(Request["tp_device"].ToString()))
                {
                    tp_device = Request["tp_device"].ToString();
                }
                filtro.p_tp_device = tp_device;

                string p_municipio = string.Empty;
                if (Request["p_municipio"] != null && !string.IsNullOrEmpty(Request["p_municipio"].ToString()))
                {
                    p_municipio = Request["p_municipio"].ToString();
                }
                filtro.p_municipio = p_municipio;

                string p_bairro = string.Empty;
                if (Request["p_bairro"] != null && !string.IsNullOrEmpty(Request["p_bairro"].ToString()))
                {
                    p_bairro = Request["p_bairro"].ToString();
                }
                filtro.p_bairro = p_bairro;

                string p_ip = string.Empty;
                if (Request["p_ipconcentrador"] != null && !string.IsNullOrEmpty(Request["p_ipconcentrador"].ToString()))
                {
                    p_ip = Request["p_ipconcentrador"].ToString();
                }
                filtro.p_ip = p_ip;

                int p_marcador = 0;
                if (Request["p_marcador"] != null && !string.IsNullOrEmpty(Request["p_marcador"].ToString()))
                {
                    p_marcador = Convert.ToInt32(Request["p_marcador"].ToString());
                }
                filtro.p_marcador = p_marcador;

                string p_grupo_device = string.Empty;
                if (Request["p_grupo_device"] != null && !string.IsNullOrEmpty(Request["p_grupo_device"].ToString()))
                {
                    p_grupo_device = Request["p_grupo_device"].ToString();
                }
                filtro.p_cmbgrupodevice = p_grupo_device;

                List<RelatorioUltimosDevices> lista = new List<RelatorioUltimosDevices>();
                lista = obj.BuscarUltimosDevicesInstalados(filtro);

                return GetYourFile(lista, "Ultimos_Devices_Instalados");
            }
            else
            {
                return Json("Expirou");
            }
        }
        


        public JsonResult GetDadosGrupo()
        {
            if (Session["User"] != null)
            {
                RelatorioBL obj = new RelatorioBL();
                List<RelatorioGrupo> lista = new List<RelatorioGrupo>();
                lista = obj.BuscarGrupo();
                return GetYourFile(lista, "Dados_Grupo");
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult GetDadosDevice()
        {
            if (Session["User"] != null)
            {
                RelatorioBL obj = new RelatorioBL();
                FiltroRelatorioDadosDevice filtro = new FiltroRelatorioDadosDevice();

                if (!string.IsNullOrEmpty(Request["p_dt_inicio"]))
                {
                    filtro.p_dt_inicio = Request["p_dt_inicio"].ToString();
                }

                string p_dt_final = string.Empty;
                if (!string.IsNullOrEmpty(Request["p_dt_final"]))
                {
                    filtro.p_dt_final = Request["p_dt_final"].ToString();
                }

                if (string.IsNullOrEmpty(filtro.p_dt_inicio) || string.IsNullOrEmpty(filtro.p_dt_final))
                    return Json("dataVazia", JsonRequestBehavior.AllowGet);
                else
                {
                    DateTime dtini = DateTime.ParseExact(filtro.p_dt_inicio, "dd/MM/yyyy", null);
                    DateTime dtfim = DateTime.ParseExact(filtro.p_dt_final, "dd/MM/yyyy", null);

                    int totalDias = (dtfim).Subtract(dtini).Days;

                    if (totalDias < 0)
                        return Json("dataSuperior", JsonRequestBehavior.AllowGet);
                }

                string p_device_address = string.Empty;
                if (Request["p_device_address"] != null)
                {
                    p_device_address = Request["p_device_address"].ToString();
                }
                filtro.p_device_address = p_device_address;

                string p_uc = string.Empty;
                if (Request["p_uc"] != null)
                {
                    p_uc = Request["p_uc"].ToString();
                }
                filtro.p_uc = p_uc;

                string p_medidor = string.Empty;
                if (Request["p_medidor"] != null)
                {
                    p_medidor = Request["p_medidor"].ToString();
                }
                filtro.p_medidor = p_medidor;

                string p_ip = string.Empty;
                if (Request["p_ip"] != null)
                {
                    p_ip = Request["p_ip"].ToString();
                }
                filtro.p_ip = p_ip;

                string p_falha = "0,1,2,3";
                if (Request["p_falha"] != null && !string.IsNullOrEmpty(Request["p_falha"].ToString()))
                {
                    p_falha = Request["p_falha"].ToString();
                }
                filtro.p_falha = p_falha;

                string p_sinal = "0,1,2,3";
                if (Request["p_sinal"] != null && !string.IsNullOrEmpty(Request["p_sinal"].ToString()))
                {
                    p_sinal = Request["p_sinal"].ToString();
                }
                filtro.p_sinal = p_sinal;

                string p_throughput = "0,1,2,3";
                if (Request["p_throughput"] != null && !string.IsNullOrEmpty(Request["p_throughput"].ToString()))
                {
                    p_throughput = Request["p_throughput"].ToString();
                }
                filtro.p_throughput = p_throughput;

                string p_erro = "0,1,2,3";
                if (Request["p_erro"] != null && !string.IsNullOrEmpty(Request["p_erro"].ToString()))
                {
                    p_erro = Request["p_erro"].ToString();
                }
                filtro.p_erro = p_erro;

                string p_latencia = "0,1,2,3";
                if (Request["p_latencia"] != null && !string.IsNullOrEmpty(Request["p_latencia"].ToString()))
                {
                    p_latencia = Request["p_latencia"].ToString();
                }
                filtro.p_latencia = p_latencia;

                string p_rank = "0,1,2,3";
                if (Request["p_rank"] != null && !string.IsNullOrEmpty(Request["p_rank"].ToString()))
                {
                    p_rank = Request["p_rank"].ToString();
                }
                filtro.p_rank = p_rank;

                string p_vizinhos = "0,1,2,3";
                if (Request["p_vizinhos"] != null && !string.IsNullOrEmpty(Request["p_vizinhos"].ToString()))
                {
                    p_vizinhos = Request["p_vizinhos"].ToString();
                }
                filtro.p_vizinhos = p_vizinhos;

                string p_cmbgrupodevice = string.Empty;
                if (Request["p_grupo_device"] != null)
                {
                    p_cmbgrupodevice = Request["p_grupo_device"].ToString();
                }
                filtro.p_cmbgrupodevice = p_cmbgrupodevice;

                List<RelatorioDadosDevice> lista = new List<RelatorioDadosDevice>();
                lista = obj.BuscarDadosDevice(filtro);
                return GetYourFile(lista, "Dados_Device");
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult GetYourFile<T>(List<T> list, string nomerelatorio)
        {
            if (Session["User"] != null)
            {
                string nomerelcompleto = nomerelatorio + " " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");

                StringBuilder csv = new StringBuilder();

                if (list == null || list.Count == 0)
                    return Json("NAOENCONTRADO", JsonRequestBehavior.AllowGet);

                //get type from 0th member
                Type t = list[0].GetType();
                string newLine = Environment.NewLine;

                //make a new instance of the class name we figured out to get its props
                object o = Activator.CreateInstance(t);
                //gets all properties
                PropertyInfo[] props = o.GetType().GetProperties();

                //foreach of the properties in class above, write out properties
                //this is the header row
                csv.Append(string.Join(",", props.Select(d => d.Name).ToArray()) + " \n ");

                //this acts as datarow
                object curValue;
                foreach (T item in list)
                {
                    //Loop properties (columns)
                    foreach (PropertyInfo curProp in props)
                    {
                        //Get value, check, and write
                        curValue = item.GetType().GetProperty(curProp.Name).GetValue(item, null);
                        if (curValue != null)
                        {
                            csv.Append("" + curValue.ToString() + ",");
                        }
                        else
                        {
                            csv.Append('"' + "" + '"' + ",");
                        }

                    }
                    csv.Append(" \n ");
                }

                return Json(csv.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }

        }
    }

}
