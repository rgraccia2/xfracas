﻿
using Atech.SGR.WebGerenciadorBL;
using Atech.SGR.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Atech.SGR.WebGerenciador.Controllers
{

    [Authorize]
    public class PerfilController : CommonController
    {
        // GET: Perfil
        public ActionResult Index()
        {
            Session.Add("p_filtrorapido", string.Empty);
            return View();
        }

        public JsonResult ListarPerfil()
        {
            List<Perfil> lstPerfil = new List<Perfil>();
            try
            {
                PerfilBL obj = new PerfilBL();
                lstPerfil = obj.ListarPerfil();
            }
            catch (Exception ex)
            {
                Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar perfils.");
            }
            return Json(lstPerfil, JsonRequestBehavior.AllowGet);
        }

    }
}