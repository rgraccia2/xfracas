﻿using Atech.SGR.WebGerenciadorBL;
using Atech.SGR.WebGerenciadorInterface.Enum;
using Atech.SGR.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;


namespace Atech.SGR.WebGerenciador.Controllers
{

    [Authorize]
    public class DeviceController : CommonController
    {
        public const Int32 Cod_Error_Sucess = 0;
        public const Int32 Cod_Error_InvalidParameter = 1;
        public const Int32 Cod_Error_UCNoExist = 2;
        public const Int32 Cod_Param_Timeout = 10;
        public const Int32 Cod_Param_Cluster= 15;
        public const Int32 Cod_Param_UC = 16;
      


        public class RetornoRota
        {
            public List<RotaDevice> data { get; set; }
            public string Pendente { get; set; }
        }

        // GET: Device

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ImportarUC()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }



        public ActionResult ListarDevices()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.Monitoramento))
                {
                    // Obtém parametro timeout para refresh página de monitóramento.
                    ParametroBL objParam = new ParametroBL();
                    Parametro timeoutParam = objParam.ObterParametro(Cod_Param_Timeout);
                    ViewBag.TimeOut = timeoutParam.vl_param;

                    List<Municipio> lstMunicipio = new List<Municipio>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Municipio item = new Municipio();
                        item.municipio = "-TODOS-";
                        lstMunicipio.Add(item);
                        lstMunicipio.AddRange(obj.ListarMunicipio());

                        ViewBag.ListaMunicipio = new SelectList(lstMunicipio, "municipio", "municipio");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar municipio.");
                    }

                    List<Bairro> lstBairro = new List<Bairro>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Bairro item = new Bairro();
                        item.bairro = "";
                        lstBairro.Add(item);
                        lstBairro.AddRange(obj.ListarBairro());

                        ViewBag.ListaBairro = new SelectList(lstBairro, "bairro", "bairro");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar bairro.");
                    }

                    if (Request["p_filtrorapido"] != null)
                    {
                        ViewBag.p_filtrorapido = Request["p_filtrorapido"].ToString();
                    }

                    List<GrupoSubGrupo> lstGrupoSubGrupo = new List<GrupoSubGrupo>();
                    try
                    {
                        UcBL obj = new UcBL();
                        GrupoSubGrupo item = new GrupoSubGrupo();
                        item.tipo_subgrupo = "-TODOS-";
                        lstGrupoSubGrupo.Add(item);
                        lstGrupoSubGrupo.AddRange(obj.ListarGrupoSubGrupo());

                        ViewBag.ListaGrupoSubGrupo = new SelectList(lstGrupoSubGrupo, "tipo_subgrupo", "tipo_subgrupo");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupo sub grupo.");
                    }



                    List<Grupo> lstGrupo = new List<Grupo>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Grupo item = new Grupo();
                        item.nome = "-TODOS-";
                        lstGrupo.Add(item);
                        lstGrupo.AddRange(obj.ListarGrupo());

                        ViewBag.ListaGrupo = new SelectList(lstGrupo, "id_grupo", "nome");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupo sub grupo.");
                    }

                    List<Tag> lstTag = new List<Tag>();
                    try
                    {
                        TagBL obj = new TagBL();
                        Tag item = new Tag();
                        item.nome = "-TODOS-";
                        lstTag.Add(item);
                        lstTag.AddRange(obj.ListarTag());

                        ViewBag.ListaMarcador = new SelectList(lstTag, "id_tag", "nome");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Erro ao listar marcadores.");
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }

        public JsonResult GetDadosDevice()
        {
            DeviceLinkBL obj = new DeviceLinkBL();
            FiltroListaMapa filtro = new FiltroListaMapa();
            

            if (Session["User"] != null)
            {
                try
                {
                    string tp_device = "T,C,R";
                    if (Request["tp_device"] != null && !string.IsNullOrEmpty(Request["tp_device"].ToString()))
                    {
                        tp_device = Request["tp_device"].ToString();
                    }
                    filtro.tp_device = tp_device;

                    string p_falha = "0,1,2,3";
                    if (Request["p_falha"] != null && !string.IsNullOrEmpty(Request["p_falha"].ToString()))
                    {
                        p_falha = Request["p_falha"].ToString();
                    }
                    filtro.p_falha = p_falha;

                    string p_sinal = "0,1,2,3";
                    if (Request["p_sinal"] != null && !string.IsNullOrEmpty(Request["p_sinal"].ToString()))
                    {
                        p_sinal = Request["p_sinal"].ToString();
                    }
                    filtro.p_sinal = p_sinal;

                    string p_throughput = "0,1,2,3";
                    if (Request["p_throughput"] != null && !string.IsNullOrEmpty(Request["p_throughput"].ToString()))
                    {
                        p_throughput = Request["p_throughput"].ToString();
                    }
                    filtro.p_throughput = p_throughput;

                    string p_erros = "0,1,2,3";
                    if (Request["p_erros"] != null && !string.IsNullOrEmpty(Request["p_erros"].ToString()))
                    {
                        p_erros = Request["p_erros"].ToString();
                    }
                    filtro.p_erros = p_erros;

                    string p_st_medidor = "0,1,3";
                    if (Request["p_st_medidor"] != null && !string.IsNullOrEmpty(Request["p_st_medidor"].ToString()))
                    {
                        p_st_medidor = Request["p_st_medidor"].ToString();
                    }
                    filtro.p_st_medidor = p_st_medidor;

                    string p_latencia = "0,1,2,3";
                    if (Request["p_latencia"] != null && !string.IsNullOrEmpty(Request["p_latencia"].ToString()))
                    {
                        p_latencia = Request["p_latencia"].ToString();
                    }
                    filtro.p_latencia = p_latencia;

                    string p_rank = "0,1,2,3";
                    if (Request["p_rank"] != null && !string.IsNullOrEmpty(Request["p_rank"].ToString()))
                    {
                        p_rank = Request["p_rank"].ToString();
                    }
                    filtro.p_rank = p_rank;

                    string p_rotas = "0,1,2,3";
                    if (Request["p_rotas"] != null && !string.IsNullOrEmpty(Request["p_rotas"].ToString()))
                    {
                        p_rotas = Request["p_rotas"].ToString();
                    }
                    filtro.p_rotas = p_rotas;

                    string p_vizinhos = "0,1,2,3";
                    if (Request["p_vizinhos"] != null && !string.IsNullOrEmpty(Request["p_vizinhos"].ToString()))
                    {
                        p_vizinhos = Request["p_vizinhos"].ToString();
                    }
                    filtro.p_vizinhos = p_vizinhos;

                    string p_ip = string.Empty;
                    if (Request["p_ip"] != null)
                    {
                        p_ip = Request["p_ip"].ToString();
                    }
                    filtro.p_ip = p_ip;

                    string p_filtrorapido = string.Empty;
                    if (Session["p_filtrorapido"] != null)
                    {
                        p_filtrorapido = Session["p_filtrorapido"].ToString();
                    }
                    filtro.p_filtrorapido = p_filtrorapido;


                    string p_cmbgruposubg = string.Empty;
                    if (Request["p_cmbgruposubg"] != null)
                    {
                        p_cmbgruposubg = Request["p_cmbgruposubg"].ToString();
                    }
                    filtro.p_cmbgruposubg = p_cmbgruposubg;

                    string p_municipio = string.Empty;
                    if (Request["p_municipio"] != null)
                    {
                        p_municipio = Request["p_municipio"].ToString();
                    }
                    filtro.p_municipio = p_municipio;

                    string p_bairro = string.Empty;
                    if (Request["p_bairro"] != null)
                    {
                        p_bairro = Request["p_bairro"].ToString();
                    }
                    filtro.p_bairro = p_bairro;

                    string p_cmbgrupo = string.Empty;
                    if (Request["p_grupo"] != null)
                    {
                        p_cmbgrupo = Request["p_grupo"].ToString();
                    }
                    filtro.p_cmbgrupo = p_cmbgrupo;


                    string p_tag = string.Empty;
                    if (Request["p_tag"] != null)
                    {
                        p_tag = Request["p_tag"].ToString();
                    }
                    filtro.p_tag = p_tag;

                    List<DeviceLista> lista = new List<DeviceLista>();
                    lista = obj.BuscaDeviceData(filtro);
                    return Json(lista, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    Utils.Log.Gravar_Log(ex.ToString(), "Erro");
                    return Json(ex.ToString());
                }
            }
            else
            {
                return Json("Expirou");
            }
        }            

        public JsonResult ImportarMedidores()
        {
            DeviceLinkBL obj = new DeviceLinkBL();
            List<ImportarMedidor> listaretorno = new List<ImportarMedidor>();
            List<ImportarMedidor> listaenvio = new List<ImportarMedidor>();
            if (Session["User"] != null)
            {
                var arquivo = Convert.ToString(Request["caminho_arquivo"]);
                int posicao = arquivo.IndexOf("base64");
                string arqui = arquivo.Remove(0, posicao+7);
                //string arqui1 = arquivo.Replace("data:application/vnd.ms-excel;base64,", "");
                
                var bytes = Convert.FromBase64String(arqui);
                string idgrupo = "";

                List<ImportarMedidor> list = new List<ImportarMedidor>();
                if (Request["grupo"] != "") idgrupo = Request["grupo"].ToString();

                try
                {
                    using (StreamReader sr = new StreamReader(new MemoryStream(bytes)))
                    {
                        String linha;
                        // Lê linha por linha
                        while ((linha = sr.ReadLine()) != null)
                        {
                            ImportarMedidor novo = new ImportarMedidor();
                            var array = linha.Split(';');

                            bool sucessoUC = false;
                            bool sucessoMedidor = false;

                            if (array.Length > 0)
                            {
                                try
                                {
                                    novo.uc = Convert.ToInt32(array[0]);
                                    sucessoUC = true;
                                }
                                catch
                                {
                                    listaretorno.Add(new ImportarMedidor() { causa = "Erro ao converter número uc: " + array[0] + "", retorno = 0, medidor = array[1] });
                                }
                                try
                                {
                                    novo.medidor = Convert.ToString(array[1]);
                                    sucessoMedidor = true;
                                }
                                catch
                                {
                                    listaretorno.Add(new ImportarMedidor() { causa = "Erro ao converter número medidor: " + array[1] + "", retorno = 0, medidor = array[1] });
                                }
                                if (sucessoUC && sucessoMedidor)
                                {
                                    listaenvio.Add(novo);
                                }
                            }
                            else
                            {
                                listaretorno.Add(new ImportarMedidor() { causa = "Não foi possivel ler o registro", retorno = 0, medidor = "" });
                            }
                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = "Erro", cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = ex.Message, operacao = "Erro durante Importação de arquivo de medidores" });
                    return Json("Ocorreu um erro ao executar a operação, contate o administrador.");
                }
                try
                {
                    listaretorno.AddRange(obj.ImportarMedidor(listaenvio, idgrupo));
                }
                catch(Exception ex)
                {
                    Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = "Erro", cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = ex.Message, operacao = "Erro durante Importação de arquivo de medidores" });
                    return Json("Ocorreu um erro ao executar a operação, contate o administrador.");
                }

                foreach (var item in listaretorno)
                {
                    //if (item.retorno == 1)
                   // {
                        Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = item.uc.ToString(), cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = item.medidor + ' ' + item.device_address + ' ' + item.causa, operacao = "Importação de arquivo de medidores" });
                  //  }
                }

                return Json(listaretorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }        

        public JsonResult BuscaDetalhesDevice()
        {
            if (Session["User"] != null)
            {
                if (Request["device_address"] != null && !String.IsNullOrEmpty(Request["device_address"].ToString()))
                {
                    DeviceDetalhe item = new DeviceDetalhe();

                    item.device_address = Request["device_address"].ToString();
                    DeviceLinkBL obj = new DeviceLinkBL();
                    DeviceDetalhe lst = obj.BuscaDetalhesDevice(item);
                    return Json(obj.BuscaDetalhesDevice(item), JsonRequestBehavior.AllowGet);
                }
                return Json("");
            }
            else
            {
                return Json("Expirou");               
            }
        }

        public JsonResult ConsultarUC()
        {
            Int64 uc_digitada = 0;

            if (Request["num_uc"] != null)
            { uc_digitada = Convert.ToInt64(Request["num_uc"].ToString()); }

            DeviceLinkBL obj = new DeviceLinkBL();
            AtribuirUC dadosUC = obj.ConsultarUC(uc_digitada);
            return Json(dadosUC, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ListaRotasDevice()
        {
            if (Session["User"] != null)
            {
                DeviceLinkBL obj = new DeviceLinkBL();
                string device_address = Request["device_address"].ToString();


                RetornoRota retorno = new RetornoRota();
                retorno.data = new List<RotaDevice>();
                retorno.data = obj.ListaRotaDevice(device_address);

                List<RotaDevice> lstPendente = retorno.data.FindAll(t => t.pendente == "PENDENTE");
                if (lstPendente != null && lstPendente.Count > 0)
                {
                    retorno.Pendente = "OK";
                }
                else
                {
                    retorno.Pendente = "NAO";
                }

                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult ObtemParametroCluster()
        {
            if (Session["User"] != null)
            {
                ParametroBL objParam = new ParametroBL();               

                return Json(objParam.ObterParametro(Cod_Param_Cluster), JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json("Expirou");
            }
    }

    public JsonResult BuscarMarcadoresDisponiveis()
        {
            if (Session["User"] != null)
            {
                RetornoTag retorno = new RetornoTag();

                DeviceLinkBL obj = new DeviceLinkBL();
                string device_address = Request["device_address"].ToString();
                retorno.data = new List<Tag>();
                retorno.data = obj.ListaTagsDisponiveis(device_address);

                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult BuscarMarcadoresAtribuidos()
        {
            if (Session["User"] != null)
            {
                RetornoTag retorno = new RetornoTag();
                DeviceLinkBL obj = new DeviceLinkBL();
                string device_address = Request["device_address"].ToString();

                retorno.data = new List<Tag>();
                retorno.data = obj.ListaTagsAtribuidos(device_address);

                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public ActionResult DevicesPendentes()
        {
            if (Session["User"] != null && ValidarAcesso(Enum.EnumPage.DevicesPendentes))
            {
                if (ValidarAcesso(Enum.EnumPage.DevicesPendentes))
                {

                    List<Grupo> lstGrupo = new List<Grupo>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Grupo item = new Grupo();
                        item.nome = "-TODOS-";
                        lstGrupo.Add(item);
                        lstGrupo.AddRange(obj.ListarGrupo());

                        ViewBag.ListaGrupo = new SelectList(lstGrupo, "id_grupo", "nome");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupo sub grupo.");
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }
               
        public JsonResult GetDevicesPendentes()
        {
            string device = "";
            string device_address = "";

            if (Request["tp_device"] != null)
            {
                device = Request["tp_device"].ToString();
            }
            if (Session["p_filtrorapido"] != null)
            {
                device_address = Session["p_filtrorapido"].ToString();
            }
            DeviceLinkBL obj = new DeviceLinkBL();
            List<DevicePendente> lstpendentes = obj.BuscaDevicesPendentes(device, device_address);
            return Json(lstpendentes, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AtribuirConsumidor()
        {
            if (Session["User"] != null)
            {
                string retorno = "OK";
                MsgAtribuirUC msguc = new MsgAtribuirUC();
                try
                {
                    if (Request["num_uc"] != null && Request["num_medidor"] != null )
                    {
                        string num_uc = Request["num_uc"].ToString();
                        string num_medidor = Request["num_medidor"].ToString();
                        string grupo = Request["grupo"].ToString();
                        
                        UcBL obj = new UcBL();
                        msguc = obj.AtribuirConsumidor(num_medidor, num_uc, grupo, User.Identity.Name);

                        if (msguc.ret_error_code != Cod_Error_Sucess)
                        {
                            retorno = MontarMsg(msguc);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utils.Log.Gravar_Log(ex.ToString(), "Erro");
                    retorno = Atech.SGR.WebGerenciador.Resources.ResourceSGR.DevicesPendentes_MsgAtribuirConsumidorError;
                }
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }


        public JsonResult AlterarConsumidor()
        {
            if (Session["User"] != null)
            {
                string retorno = "OK";
                MsgAtribuirUC msguc = new MsgAtribuirUC();
                try
                {
                    if (Request["num_uc"] != null && Request["num_medidor"] != null)
                    {
                        string num_uc = Request["num_uc"].ToString();
                        string num_medidor = Request["num_medidor"].ToString();
                        string grupo = Request["grupo"].ToString();

                        UcBL obj = new UcBL();
                        msguc = obj.AlterarConsumidor(num_medidor, num_uc, grupo, User.Identity.Name);

                        if (msguc.ret_error_code != Cod_Error_Sucess)
                        {
                            retorno = MontarMsg(msguc);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utils.Log.Gravar_Log(ex.ToString(), "Erro");
                    retorno = Atech.SGR.WebGerenciador.Resources.ResourceSGR.DevicesPendentes_MsgAtribuirConsumidorError;
                }
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult DesativarDevice()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.CriarDevice))
                {
                    string retorno = "OK";
                    try
                    {
                        Device item = new Device();
                        DeviceLinkBL obj = new DeviceLinkBL();

                        item.device_address = Request["device_address"].ToString();
                        obj.DesativarDevice(item);

                        if (retorno == "OK")
                        {
                            Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = item.device_address, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Desativado device " + item.device_address, operacao = "Desativar Device" });
                        }
                    }
                    catch
                    {
                        retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                    }
                    return Json(retorno);

                }
                else
                {
                    return Json("Acesso Negado");
                }
            }
            else
            {
                return Json("Expirou");
            }
        }

        private string MontarMsg(MsgAtribuirUC msg)
        {
            string retorno = string.Empty;
            if (msg.ret_error_code == Cod_Error_InvalidParameter)
            {
                retorno = Atech.SGR.WebGerenciador.Resources.ResourceSGR.DevicesPendentes_MsgInvalidParameter_1 + " " + msg.num_medidor + " " + Atech.SGR.WebGerenciador.Resources.ResourceSGR.DevicesPendentes_MsgInvalidParameter_2 + " " + msg.ret_error_msg;
            }
            else if (msg.ret_error_code == Cod_Error_UCNoExist)
            {
                retorno = Atech.SGR.WebGerenciador.Resources.ResourceSGR.DevicesPendentes_MsgUCNoExist_1 + " " + msg.ret_error_msg + " " + Atech.SGR.WebGerenciador.Resources.ResourceSGR.DevicesPendentes_MsgUCNoExist_2;
            }
            return retorno;
        }
        
        public ActionResult GerenciarDevices()
        {
            Session.Add("p_filtrorapido", string.Empty);
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.CriarDevice))
                {
                    if (Request["deviceaddress"] != null && Request["tpdevice"] != null)
                    {
                        ViewBag.deviceaddress = Request["deviceaddress"].ToString();
                        ViewBag.tipodevice = Request["tpdevice"].ToString();
                    }

                    List<Municipio> lstMunicipio = new List<Municipio>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Municipio item = new Municipio();
                        item.municipio = "-TODOS-";
                        lstMunicipio.Add(item);
                        lstMunicipio.AddRange(obj.ListarMunicipio());

                        ViewBag.ListaMunicipio = new SelectList(lstMunicipio, "municipio", "municipio");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar municipio.");
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }

        public JsonResult SalvarDeviceConfiguration()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.CriarDevice))
                {
                    string retorno = "OK";
                    try
                    {
                        DeviceConfiguration item = new DeviceConfiguration();
                        if (Request["tipo_device"].ToString() != "")
                        {
                            item.tipo_device = Request["tipo_device"].ToString().Substring(0, 1).ToUpper();
                            item.device_address = Request["device_address"].ToString();
                            item.latitude = string.IsNullOrEmpty(Request["latitude"].ToString()) ? 0 : Convert.ToDecimal(Request["latitude"].Replace(",", "."));
                            item.longitude = string.IsNullOrEmpty(Request["longitude"].ToString()) ? 0 : Convert.ToDecimal(Request["longitude"].Replace(",", "."));
                            item.logradouro = Request["logradouro"].ToString();
                            item.num_imovel = Request["num_imovel"].ToString();
                            item.bairro = Request["bairro"].ToString();
                            item.complemento = Request["complemento"].ToString();
                            item.distrital = Request["distrital"].ToString();
                            item.municipio = Request["municipio"].ToString();
                            item.tipo_logr = Request["tipo_logr"].ToString();
                            item.ativo = Request["ativo"].ToString() == "sim" ? "S" : "N";
                            item.ip = item.tipo_device == "C" ? Request["ip"].ToString() : null;
                            item.porta = item.tipo_device == "C" ? Request["porta"].ToString() : null;
                            item.qtdMax = item.tipo_device == "C" ? Convert.ToInt16(Request["qtdMax"].ToString()) : 0;
                            item.url_get = item.tipo_device == "C" ? Request["url_get"].ToString() : null;
                            item.url_delete = item.tipo_device == "C" ? Request["url_delete"].ToString() : null;

                            DeviceConfigurationBL obj = new DeviceConfigurationBL();
                            if (item.tipo_device == "R" && (item.latitude == 0 || item.longitude == 0 || string.IsNullOrEmpty(item.device_address)))
                            {
                                retorno = "Preencha os campos obrigatórios.";
                            }

                            else if (item.tipo_device == "C" && (item.latitude == 0 || item.longitude == 0 || string.IsNullOrEmpty(item.device_address) || string.IsNullOrEmpty(item.ip) || string.IsNullOrEmpty(item.porta) || item.qtdMax == 0 || string.IsNullOrEmpty(item.url_get) || string.IsNullOrEmpty(item.url_delete)))
                            {
                                retorno = "Preencha os campos obrigatórios.";
                            }
                            else
                            {
                                obj.SalvarDeviceConfiguration(item);
                            }

                            if (retorno == "OK")
                            {
                                Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = item.device_address, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Inclusão do device tipo " + item.tipo_device + " - Device Address " + item.device_address, operacao = "Inclusão Device" });
                            }
                        }
                        else
                        {
                            retorno = "Selecione um tipo de device.";
                        }
                    }
                    catch
                    {
                        retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                    }
                    return Json(retorno);

                }
                else
                {
                    return Json("AcessoNegado");
                }
            }
            else
            {
                return Json("Expirou");
            }
        }
        
        public ActionResult EditarDevice()
        {
            if (Session["User"] != null)
            {
                Session.Add("p_filtrorapido", string.Empty);
                if (ValidarAcesso(Enum.EnumPage.EditarDevice))
                {
                    if (Request["deviceaddress"] != null && Request["tpdevice"] != null)
                    {
                        ViewBag.deviceaddress = Request["deviceaddress"].ToString();
                        ViewBag.tipodevice = Request["tpdevice"].ToString();

                    }

                    List<Municipio> lstMunicipio = new List<Municipio>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Municipio item = new Municipio();
                        item.municipio = "-TODOS-";
                        lstMunicipio.Add(item);
                        lstMunicipio.AddRange(obj.ListarMunicipio());

                        ViewBag.ListaMunicipio = new SelectList(lstMunicipio, "municipio", "municipio");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar municipio.");
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }

        public JsonResult BuscaDevice()
        {
            DeviceConfiguration item = new DeviceConfiguration();

            item.device_address = Request["device_address"].ToString();

            DeviceConfigurationBL obj = new DeviceConfigurationBL();
            return Json(obj.BuscaDevice(item), JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult AlterarDeviceConfiguration()
        {
            string retorno = "OK";
            try
            {
                if (Session["User"] != null)
                {
                    if (ValidarAcesso(Enum.EnumPage.EditarDevice))
                    {
                        DeviceConfiguration item = new DeviceConfiguration();
                        item.tipo_device = Request["tipo_device"].ToString().Substring(0, 1).ToUpper();
                        item.device_address = Request["device_address"].ToString();
                        item.latitude = string.IsNullOrEmpty(Request["latitude"].ToString()) ? 0 : Convert.ToDecimal(Request["latitude"].Replace(",", "."));
                        item.longitude = string.IsNullOrEmpty(Request["longitude"].ToString()) ? 0 : Convert.ToDecimal(Request["longitude"].Replace(",", "."));
                        item.logradouro = Request["logradouro"].ToString();
                        item.num_imovel = Request["num_imovel"].ToString();
                        item.bairro = Request["bairro"].ToString();
                        item.complemento = Request["complemento"].ToString();
                        item.distrital = Request["distrital"].ToString();
                        item.municipio = Request["municipio"].ToString();
                        item.tipo_logr = Request["tipo_logr"].ToString();
                        item.ativo = Request["ativo"].ToString() == "S" ? "S" : "N";
                        item.ip = item.tipo_device == "C" ? Request["ip"].ToString() : null;
                        item.porta = item.tipo_device == "C" ? Request["porta"].ToString() : null;
                        item.qtdMax = item.tipo_device == "C" ? Convert.ToInt16(Request["qtdMax"].ToString()) : 0;
                        item.url_get = item.tipo_device == "C" ? Request["url_get"].ToString() : null;
                        item.url_delete = item.tipo_device == "C" ? Request["url_delete"].ToString() : null;


                        DeviceConfigurationBL obj = new DeviceConfigurationBL();
                        if (item.tipo_device == "R" && (item.latitude == 0 || item.longitude == 0 || string.IsNullOrEmpty(item.device_address)))
                        {
                            retorno = "Preencha os campos obrigatórios.";
                        }

                        else if (item.tipo_device == "C" && (item.latitude == 0 || item.longitude == 0 || string.IsNullOrEmpty(item.device_address) || string.IsNullOrEmpty(item.ip) || string.IsNullOrEmpty(item.porta) || item.qtdMax == 0 || string.IsNullOrEmpty(item.url_get) || string.IsNullOrEmpty(item.url_delete)))
                        {
                            retorno = "Preencha os campos obrigatórios.";
                        }
                        else
                        {
                            obj.AlterarDeviceConfiguration(item);
                        }

                        if (retorno == "OK")
                        {
                            Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = item.device_address, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Alteração do device tipo " + item.tipo_device + " - Device Address " + item.device_address, operacao = "Alteração Device" });
                        }
                    }
                    else
                    {
                        return Json("Acesso Negado", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Expirou");
                }
            }
            catch
            {
                retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
            }
            return Json(retorno);
        }

        public JsonResult BuscarKeepAlive()
        {
            RetornoKeepAlive retorno = new RetornoKeepAlive();
            if (Session["User"] != null)
            {
                DeviceLinkBL obj = new DeviceLinkBL();
                string device_address = Request["device_address"].ToString();

                retorno.data = new List<DeviceData>();
                retorno.data = obj.ListaKeepAlive(device_address);


                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult SalvarTag()
        {
            string retorno = "OK";
            if (Session["User"] != null)
            {
                try
                {
                    DeviceLinkBL obj = new DeviceLinkBL();
                    string device_address = Request["device_address"].ToString();
                    string tag = Request["tag_device"].ToString();
                    obj.SalvarTag(device_address, tag);
                    if (retorno == "OK")
                    {
                        Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = device_address, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Deletando tags do device " + device_address, operacao = "Deletar tags" });
                    }
                }
                catch
                {
                    retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                }                
                return Json(retorno);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult DeletaTags()
        {
            if (Session["User"] != null)
            {
                string retorno = "OK";            
                try
                {
                    DeviceLinkBL obj = new DeviceLinkBL();
                    string device_address = Request["device_address"].ToString();
                    obj.DeletaTag(device_address);

                    if (retorno == "OK")
                    {
                        Utils.Log.Gravar_Log(new Atech.SGR.WebGerenciadorInterface.Model.Log() { cod_objeto = device_address, cod_tipo = "1", dt_log = DateTime.Now, cod_usuario = User.Identity.Name, descricao = "Deletando tags do device " + device_address, operacao = "Deletar tags" });
                    }
                }
                catch
                {
                    retorno = "Ocorreu um erro ao executar a operação, tente mais tarde.";
                }
                return Json(retorno);
            }
            else
            {
                return Json("Expirou");
            }
        }
       
        public JsonResult VerificaSeDevicePendente()
        {
            bool retorno = false;
            if (Request["device_address"] != null)
            {
                string device_address = Request["device_address"].ToString();
                DeviceLinkBL obj = new DeviceLinkBL();
                retorno = obj.VerificaSeDevicePendente(device_address);
                Session.Add("p_filtrorapido", device_address);
            }
            return Json(retorno);
        }
        
        public ActionResult MapaDevices()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home", new { ses = "S" });
            }

        }

        private string GetUrlParameter(HttpRequestBase request, string parName)
        {
            string result = string.Empty;

            var urlParameters = HttpUtility.ParseQueryString(request.Url.Query);
            if (urlParameters.AllKeys.Contains(parName))
            {
                result = urlParameters.Get(parName);
            }

            return result;
        }

        public JsonResult UpLoadArquivo()
        {
            string retorno = "OK";
            if (Session["User"] != null)
            {
                try
                {
                    HttpPostedFileBase arquivo = Request.Files["FileUpload"];
                    string guid = "";
                    DeviceLinkBL obj = new DeviceLinkBL();
                    ArquivoUC arq = new ArquivoUC();
                    arq.id_user = ((Usuario)Session["User"]).CodigoUser;
                    arq.nome_arquivo = Path.GetFileName(arquivo.FileName);
                    guid = Guid.NewGuid().ToString();
                    arq.guid = guid;
                    Int64 id_file = obj.SalvarArquivoUC(arq);
                    UnidadeConsumidoraBL bl = new UnidadeConsumidoraBL();

                    //Salva o arquivo
                    if (arquivo.ContentLength > 0)
                    {
                        ParametroBL paramBL = new ParametroBL();                        
                        string uploadPath = paramBL.ObterParametro(Cod_Param_UC).vl_param;   

                      //  var uploadPath = Server.MapPath("~/Content/Uploads");
                        string caminhoArquivo = Path.Combine(@uploadPath, guid);
                        arquivo.SaveAs(caminhoArquivo);

                        //bl.ImportarUnidadesConsumidoras(id_file, caminhoArquivo);

                        var thread = new Thread(() => bl.ImportarUnidadesConsumidoras(id_file, caminhoArquivo));
                        thread.Start();

                    }
                    else
                    {
                        bl.UpdateStatus(id_file);
                    }

                    return Json(retorno);
                }
                catch
                {
                    return Json("Erro no meio do processo");                  
                }
            }
            else
            {
                return Json("Expirou");
            }


        }


        public JsonResult ListarFilaArquivosUC()
        {
            if (Session["User"] != null)
            {
                DeviceLinkBL obj = new DeviceLinkBL();
                var lista = obj.ListarArquivoUC();

                List<ArquivoUCExibir> lst = new List<ArquivoUCExibir>();
                foreach (ArquivoUC item in lista)
                {
                    ArquivoUCExibir novo = new ArquivoUCExibir();
                    novo.dt_envio = item.dt_envio.ToString("dd/MM/yyyy HH:mm:ss");
                    novo.id_file = item.id_file;
                    novo.guid = item.guid;
                    novo.id_user = item.id_user;
                    novo.nome_arquivo = item.nome_arquivo;
                    novo.status = item.status;
                    lst.Add(novo);
                }

                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult TotaisUpLoadUC()
        {
            if (Session["User"] != null)
            {
                DeviceLinkBL obj = new DeviceLinkBL();
                int id_file = Convert.ToInt32(Request["id_file"]);
                var totais = obj.TotaisUploadUC(id_file);
                return Json(totais, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }
        }

        public JsonResult ListaErrosFile()
        {
            if (Session["User"] != null)
            {
                DeviceLinkBL obj = new DeviceLinkBL();
                int id_file = Convert.ToInt32(Request["id_file"]);
                List<FileErroUC> lista = new List<FileErroUC>();
                lista = obj.ListaErrosFile(id_file);
                return GetYourFile(lista, "ErrosImportarUC");
            }
            else
            {
                return Json("Expirou", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetYourFile<T>(List<T> list, string nomerelatorio)
        {
            if (Session["User"] != null)
            {
                string nomerelcompleto = nomerelatorio + " " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");

                StringBuilder csv = new StringBuilder();

                if (list == null || list.Count == 0)
                    return Json("NAOENCONTRADO", JsonRequestBehavior.AllowGet);

                //get type from 0th member
                Type t = list[0].GetType();
                string newLine = Environment.NewLine;

                //make a new instance of the class name we figured out to get its props
                object o = Activator.CreateInstance(t);
                //gets all properties
                PropertyInfo[] props = o.GetType().GetProperties();

                //foreach of the properties in class above, write out properties
                //this is the header row
                csv.Append(string.Join(";", props.Select(d => d.Name).ToArray()) + " \n ");

                //this acts as datarow
                object curValue;
                foreach (T item in list)
                {
                    //Loop properties (columns)
                    foreach (PropertyInfo curProp in props)
                    {
                        //Get value, check, and write
                        curValue = item.GetType().GetProperty(curProp.Name).GetValue(item, null);
                        if (curValue != null)
                        {
                            csv.Append("" + curValue.ToString() + ";");
                        }
                        else
                        {
                            csv.Append('"' + "" + '"' + ";");
                        }

                    }
                    csv.Append(" \n ");
                }

                return Json(csv.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Expirou");
            }

        }



    }
}