﻿using Compass.XFracas.WebGerenciador.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Compass.XFracas.WebGerenciadorInterface.Model;

namespace Compass.XFracas.WebGerenciador.Controllers
{
    public class CommonController : Controller
    {
        [ChildActionOnly]
        public ActionResult Breadcrumb()
        {           
            IEnumerable<Compass.XFracas.WebGerenciador.Models.BreadcrumbViewModel> teste = BreadcrumbHelper.GetBreadcrumbs(Request.Path);            
            return PartialView("~/Views/Shared/Breadcrumb.cshtml", teste);
        }

        [ChildActionOnly]
        public ActionResult HeaderSecundarioComFiltroRapido()
        {
            if (Session["User"] != null)
            {
                return PartialView("~/Views/Device/HeaderSecundarioComFiltroRapido.cshtml");
            }
            else
            {
                return RedirectToAction("AcessoNegado", "Home");
            }
        }

        [ChildActionOnly]
        public ActionResult HeaderSecundarioSemFiltroRapido()
        {
            if (Session["User"] != null)
            {
                return PartialView("~/Views/Device/HeaderSecundarioSemFiltroRapido.cshtml");
            }
            else
            {
                return RedirectToAction("AcessoNegado", "Home");
            }
        }


    }
}