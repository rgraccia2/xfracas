﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Atech.SGR.WebGerenciadorBL;
using Atech.SGR.WebGerenciadorInterface.Model;

namespace Atech.SGR.WebGerenciador.Controllers
{
    public class GraficoController : CommonController
    {
        public class GraficoLinha
        {
            public string title { get; set; }
            public string subtitle { get; set; }

            public string yAxis { get; set; }

            public Series[] series { get; set; }

            public int pointStart { get; set; }

            public string[] categorias { get; set; }
        }

        public class Series {
            public string name {get;set;}
            public List<List<float>> data { get; set; }
        }
        

        public ActionResult GerenciarGraficos()
        {
            if (Session["User"] != null)
            {
                if (ValidarAcesso(Enum.EnumPage.Grafico))
                {
                    List<TipoIndicador> lsttipoIndicador = new List<TipoIndicador>();
                    try
                    {
                        UcBL obj = new UcBL();
                        lsttipoIndicador.AddRange(obj.ListarTipoIndicador());

                        ViewBag.ListaIndicador = new SelectList(lsttipoIndicador, "tipo_indicador", "nome_indicador");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar tipo de indicador.");
                    }


                    List<Municipio> lstMunicipio = new List<Municipio>();
                    try
                    {
                        UcBL obj = new UcBL();
                        Municipio item = new Municipio();
                        item.municipio = "-TODOS-";
                        lstMunicipio.Add(item);
                        lstMunicipio.AddRange(obj.ListarMunicipioEstado());

                        ViewBag.ListaMunicipio = new SelectList(lstMunicipio, "municipio", "municipio");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar municipio.");
                    }

                    List<Grupo> listaGrupo = new List<Grupo>();
                    try
                    {
                        GrupoBL obj = new GrupoBL();
                        Grupo item = new Grupo();
                        item.nome = "-TODOS-";
                        listaGrupo.Add(item);
                        listaGrupo.AddRange(obj.ListarGrupos());
                        ViewBag.ListaGrupo = new SelectList(listaGrupo, "id_grupo", "nome");
                    }
                    catch (Exception ex)
                    {
                        Utils.Log.Gravar_Log(ex.ToString(), "Error ao listar grupos.");
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("AcessoNegado", "Home");
                }
            }
            else
            {
               return RedirectToAction("Login", "Home", new { ses = "S" });
            }
        }

        public JsonResult BuscarGrafico()
        {
            if (Session["User"] != null)
            {
                FiltroGrafico filtro = new FiltroGrafico();
                    filtro.p_dt_inicio = Request["p_data_inicio"].ToString();
                    filtro.p_dt_fim = Request["p_data_fim"].ToString();
                    filtro.p_id_agrupamento = Convert.ToInt32(Request["p_id_agrupamento"].ToString());
                    filtro.p_nome_agrupamento = Request["p_nome_agrupamento"].ToString();
                    filtro.p_tipo_indicador = Convert.ToInt32(Request["p_tipo_indicador"].ToString());

                    if (string.IsNullOrEmpty(filtro.p_dt_inicio) || string.IsNullOrEmpty(filtro.p_dt_fim))
                        return Json("dataVazia", JsonRequestBehavior.AllowGet);
                    else
                    {
                        DateTime dtini = DateTime.ParseExact(filtro.p_dt_inicio, "dd/MM/yyyy", null);
                        DateTime dtfim = DateTime.ParseExact(filtro.p_dt_fim, "dd/MM/yyyy", null);

                        int totalDias = (dtfim).Subtract(dtini).Days;

                        if (totalDias > 30)
                            return Json("limiteSuperior", JsonRequestBehavior.AllowGet);
                        else if (totalDias < 0)
                            return Json("dataSuperior", JsonRequestBehavior.AllowGet);
                        else
                        {
                            GraficoLinha ret = new GraficoLinha();
                            if (Request["p_tipoDados"].ToString() == "0")
                            {
                                ret.title = "Gráfico por Nível de Alerta de Indicador";
                                ret.yAxis = "Quantidade de Devices";
                            }
                            else
                            {
                                ret.title = "Gráfico por Valor de Indicador";
                                ret.yAxis = "Valor do Indicador";
                            }
                            if (Request["p_frequencia"].ToString() == "0")
                                ret.subtitle = "Horário - " + Request["p_nome_indicador"];
                            else
                                ret.subtitle = "Diário - " + Request["p_nome_indicador"];

                            //ret.pointStart = 2010;

                            GraficoBL obj = new GraficoBL();

                            if (Request["p_frequencia"].ToString() == "1")
                            {
                                List<GraficoDiario> lst = obj.BuscarGraficoDiario(filtro);

                                List<Series> ListaSeries = new List<Series>();
                                if (lst.Count > 0)
                                {
                                    if (Request["p_tipoDados"].ToString() == "0")
                                    {
                                        Series SerieNormal = new Series();
                                        List<List<float>> arrayNormal = new List<List<float>>();

                                        List<string> lstCategorias = new List<string>();

                                        SerieNormal.name = "Normal";
                                        List<float> valoresnormal;
                                        foreach (GraficoDiario item in lst)
                                        {
                                            valoresnormal = new List<float>();
                                            lstCategorias.Add(item.dia.ToString("dd/MM/yyyy"));
                                            valoresnormal.Add(item.qtd_normal);
                                            arrayNormal.Add(valoresnormal);
                                        }
                                        SerieNormal.data = arrayNormal;
                                        ListaSeries.Add(SerieNormal);
                                        ret.categorias = lstCategorias.ToArray();


                                        Series SerieAtencao = new Series();
                                        List<List<float>> arrayAtencao = new List<List<float>>();
                                        SerieAtencao.name = "Atencao";
                                        List<float> valoresatencao;
                                        foreach (GraficoDiario item in lst)
                                        {
                                            valoresatencao = new List<float>();
                                            valoresatencao.Add(item.qtd_atencao);
                                            arrayAtencao.Add(valoresatencao);
                                        }
                                        SerieAtencao.data = arrayAtencao;
                                        ListaSeries.Add(SerieAtencao);

                                        Series SerieCritico = new Series();
                                        List<List<float>> arrayCritico = new List<List<float>>();
                                        SerieCritico.name = "Critico";
                                        List<float> valorescritico;
                                        foreach (GraficoDiario item in lst)
                                        {
                                            valorescritico = new List<float>();
                                            valorescritico.Add(item.qtd_critico);
                                            arrayCritico.Add(valorescritico);
                                        }

                                        SerieCritico.data = arrayCritico;
                                        ListaSeries.Add(SerieCritico);

                                        ret.series = ListaSeries.ToArray();
                                    }
                                    else
                                    {
                                        List<string> lstCategorias = new List<string>();
                                        Series SerieValorMedio = new Series();
                                        List<List<float>> arrayValorMedio = new List<List<float>>();
                                        SerieValorMedio.name = "Valor Médio";
                                        List<float> valoresmedio;
                                        foreach (GraficoDiario item in lst)
                                        {
                                            valoresmedio = new List<float>();
                                            lstCategorias.Add(item.dia.ToString("dd/MM/yyyy"));
                                            valoresmedio.Add(item.valor);
                                            arrayValorMedio.Add(valoresmedio);
                                        }
                                        SerieValorMedio.data = arrayValorMedio;
                                        ListaSeries.Add(SerieValorMedio);
                                        ret.series = ListaSeries.ToArray();
                                        ret.categorias = lstCategorias.ToArray();
                                    }

                                    return Json(ret, JsonRequestBehavior.AllowGet);
                                }
                                else
                                { return Json("SemDados", JsonRequestBehavior.AllowGet); }
                            }
                            else
                            {
                                List<GraficoHorario> lst = obj.BuscarGraficoHorario(filtro);

                                List<Series> ListaSeries = new List<Series>();

                                if (lst.Count > 0)
                                {
                                    if (Request["p_tipoDados"].ToString() == "0")
                                    {
                                        List<string> lstCategorias = new List<string>();

                                        Series SerieNormal = new Series();
                                        List<List<float>> arrayNormal = new List<List<float>>();
                                        SerieNormal.name = "Normal";
                                        List<float> valoresnormal;
                                        for (int h = 0; h < 24; h++)
                                        {
                                            float CalcMedia = 0;
                                            int cont = 0;
                                            valoresnormal = new List<float>();
                                            string Eixohora = "";

                                            foreach (GraficoHorario item in lst)
                                            {
                                                //lstCategorias.Add(item.hora.ToString("HH:mm"));

                                                if (item.hora.Hour == h)
                                                {
                                                    Eixohora = item.hora.ToString("HH:mm");
                                                    CalcMedia = CalcMedia + item.qtd_normal;
                                                    cont = cont + 1;
                                                }
                                            }
                                            if (cont > 0)
                                            {
                                                lstCategorias.Add(Eixohora.ToString());
                                                valoresnormal.Add(CalcMedia / cont);
                                                arrayNormal.Add(valoresnormal);
                                            }
                                            else
                                            {

                                                DateTime conveHora = DateTime.ParseExact(h.ToString().Length == 1 ? string.Concat("0", h.ToString()) : h.ToString(), "HH", null);
                                                lstCategorias.Add(conveHora.ToString("HH:mm"));
                                                valoresnormal.Add(0);
                                                arrayNormal.Add(valoresnormal);
                                            }
                                        }
                                        SerieNormal.data = arrayNormal;
                                        ListaSeries.Add(SerieNormal);

                                        Series SerieAtencao = new Series();
                                        List<List<float>> arrayAtencao = new List<List<float>>();
                                        SerieAtencao.name = "Atencao";
                                        List<float> valoresatencao;
                                        for (int h = 0; h < 24; h++)
                                        {
                                            float CalcMedia = 0;
                                            int cont = 0;
                                            valoresatencao = new List<float>();
                                            foreach (GraficoHorario item in lst)
                                            {
                                                if (item.hora.Hour == h)
                                                {
                                                    CalcMedia = CalcMedia + item.qtd_atencao;
                                                    cont = cont + 1;
                                                }
                                            }
                                            if (cont > 0)
                                            {
                                                valoresatencao.Add(CalcMedia / cont);
                                                arrayAtencao.Add(valoresatencao);
                                            }
                                            else
                                            {
                                                valoresatencao.Add(0);
                                                arrayAtencao.Add(valoresatencao);
                                            }
                                        }
                                        SerieAtencao.data = arrayAtencao;
                                        ListaSeries.Add(SerieAtencao);

                                        Series SerieCritico = new Series();
                                        List<List<float>> arrayCritico = new List<List<float>>();

                                        SerieCritico.name = "Critico";
                                        List<float> valorescritico;
                                        for (int h = 0; h < 24; h++)
                                        {
                                            float CalcMedia = 0;
                                            int cont = 0;
                                            valorescritico = new List<float>();
                                            foreach (GraficoHorario item in lst)
                                            {
                                                if (item.hora.Hour == h)
                                                {
                                                    CalcMedia = CalcMedia + item.qtd_critico;
                                                    cont = cont + 1;
                                                }
                                            }
                                            if (cont > 0)
                                            {
                                                valorescritico.Add(CalcMedia / cont);
                                                arrayCritico.Add(valorescritico);
                                            }
                                            else
                                            {
                                                valorescritico.Add(0);
                                                arrayCritico.Add(valorescritico);
                                            }

                                        }

                                        SerieCritico.data = arrayCritico;
                                        ListaSeries.Add(SerieCritico);

                                        ret.series = ListaSeries.ToArray();
                                        ret.categorias = lstCategorias.ToArray();
                                    }
                                    else
                                    {
                                        List<string> lstCategorias = new List<string>();
                                        Series SerieValorMedio = new Series();
                                        List<List<float>> arrayValorMedio = new List<List<float>>();
                                        string Eixohora = "";

                                        SerieValorMedio.name = "Valor Médio";
                                        List<float> valoresmedio;
                                        for (int h = 0; h < 24; h++)
                                        {
                                            float CalcMedia = 0;
                                            int cont = 0;
                                            valoresmedio = new List<float>();
                                            foreach (GraficoHorario item in lst)
                                            {
                                                if (item.hora.Hour == h)
                                                {
                                                    Eixohora = item.hora.ToString("HH:mm");
                                                    CalcMedia = CalcMedia + item.valor;
                                                    cont = cont + 1;
                                                }
                                            }
                                            if (cont > 0)
                                            {
                                                lstCategorias.Add(Eixohora.ToString());
                                                valoresmedio.Add(CalcMedia / cont);
                                                arrayValorMedio.Add(valoresmedio);
                                            }
                                            else
                                            {
                                                DateTime conveHora = DateTime.ParseExact(h.ToString().Length == 1 ? string.Concat("0", h.ToString()) : h.ToString(), "HH", null);
                                                lstCategorias.Add(conveHora.ToString("HH:mm"));
                                                valoresmedio.Add(0);
                                                arrayValorMedio.Add(valoresmedio);
                                            }
                                        }

                                        SerieValorMedio.data = arrayValorMedio;
                                        ListaSeries.Add(SerieValorMedio);
                                        ret.series = ListaSeries.ToArray();
                                        ret.categorias = lstCategorias.ToArray();
                                    }

                                    return Json(ret, JsonRequestBehavior.AllowGet);
                                }
                                else
                                { return Json("SemDados", JsonRequestBehavior.AllowGet); }
                            }

                        }
                    }

                }
                else
                {
                    return Json("Expirou", JsonRequestBehavior.AllowGet);
                }           
        }

    }
}