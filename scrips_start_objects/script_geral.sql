create table TB_GRUPO
(
  id_grupo  NUMBER not null,
  nome      NVARCHAR2(30),
  descricao NVARCHAR2(50),
  tb        NUMBER
)
;

alter table TB_GRUPO
  add constraint PK_TB_GRUPO primary key (ID_GRUPO);


create table TB_DEVICE
(
  device_id        NUMBER(8),
  num_medidor      NVARCHAR2(20),
  device_address   NVARCHAR2(16) not null,
  firmware_version VARCHAR2(20),
  id_grupo         NUMBER default 0 not null,
  ativo            CHAR(1),
  tipo_device      CHAR(1)
)
;
comment on column TB_DEVICE.device_id
  is 'Identificador do device fornecido pelo concentrador.';
comment on column TB_DEVICE.num_medidor
  is 'Numero do medidor conectado a esse device. Esse valor e'' fornecido pelo proprio device.';
comment on column TB_DEVICE.device_address
  is 'Endereco do device. E'' obtido a partir dos 16 algarismos hexadecimais finais do endereco IPv6 do device.';
comment on column TB_DEVICE.firmware_version
  is 'Versoes dos firmwares instalados no device, incluindo o firmware ativo e o adicional.';
comment on column TB_DEVICE.id_grupo
  is 'Grupo ao qual esse device esta'' associado. Todo o device pertence a um grupo. O grupo zero e'' o default.';
create index IDX_TB_DEVICE01 on TB_DEVICE (ATIVO, TIPO_DEVICE);
create index IDX_TB_DEVICE02 on TB_DEVICE (NUM_MEDIDOR);
alter table TB_DEVICE
  add constraint PK_TB_DEVICE primary key (DEVICE_ADDRESS);
alter table TB_DEVICE
  add constraint FK_TB_DEVICE_TB_GRUPO foreign key (ID_GRUPO)
  references TB_GRUPO (ID_GRUPO);  


create table TB_DEVICE_DATA
(
  num_id                NUMBER(11) not null,
  device_address        NVARCHAR2(16) not null,
  device_parent_address NVARCHAR2(16),
  sinal_min             NUMBER(3),
  sinal_max             NUMBER(3),
  sinal_med             NUMBER(3),
  bytes_env             NUMBER(11),
  pack_env              NUMBER(11),
  pack_no_ack           NUMBER(11),
  num_rank              NUMBER(5),
  latencia              NUMBER(5),
  num_rotas             NUMBER(3),
  num_vizinhos          NUMBER(3),
  dt_device_data        DATE not null,
  ip_concentrador       VARCHAR2(16) not null,
  fl_mais_recente       CHAR(1)
)
;
comment on column TB_DEVICE_DATA.num_id
  is 'Identificador unico para esse Device_Data';
comment on column TB_DEVICE_DATA.device_address
  is 'Endereco do device. E'' obtido a partir dos 16 algarismos hexadecimais finais do endereco IPv6 do device.';
comment on column TB_DEVICE_DATA.device_parent_address
  is 'Endereço do device pai. E'' obtido a partir dos 16 algarismos hexadecimais finais do endereco IPv6 do device.';
comment on column TB_DEVICE_DATA.sinal_min
  is 'Nivel mínimo do sinal nas ultimas <NN> amostragens.';
comment on column TB_DEVICE_DATA.sinal_max
  is 'Nível maximo do sinal nas ultimas <NN> amostragens.';
comment on column TB_DEVICE_DATA.sinal_med
  is 'Nivel medio do sinal nas ultimas <NN> amostragens.';
comment on column TB_DEVICE_DATA.bytes_env
  is 'Quantidade de bytes enviados (acumulado).';
comment on column TB_DEVICE_DATA.pack_env
  is 'Quantidade de pacotes enviados (acumulado).';
comment on column TB_DEVICE_DATA.pack_no_ack
  is 'Quantidade de pacotes enviados sem resposta (acumulado).';
comment on column TB_DEVICE_DATA.num_rank
  is 'Rank';
comment on column TB_DEVICE_DATA.latencia
  is 'Latencia (em milissegundos).';
comment on column TB_DEVICE_DATA.num_rotas
  is 'Numero de rotas sobre esse device.';
comment on column TB_DEVICE_DATA.num_vizinhos
  is 'Numero de vizinhos desse device.';
comment on column TB_DEVICE_DATA.dt_device_data
  is 'Data de coleta dos dados desse Device_Data.';
comment on column TB_DEVICE_DATA.ip_concentrador
  is 'Endereco do concentrador que enviou esses dados.';
comment on column TB_DEVICE_DATA.fl_mais_recente
  is 'Indica que esse e o registro mais recente desse device.';
create index NDX_DEVICE_DATA_FL_MAIS_RECENTE on TB_DEVICE_DATA (FL_MAIS_RECENTE);
create index NDX_DT_DEVICE_DATA on TB_DEVICE_DATA (DT_DEVICE_DATA);
alter table TB_DEVICE_DATA
  add constraint PK_TB_DEVICE_DATA primary key (DEVICE_ADDRESS, DT_DEVICE_DATA);
alter table TB_DEVICE_DATA
  add constraint FK_TB_DEVICE_DATA_TB_DEVICE foreign key (DEVICE_ADDRESS)
  references TB_DEVICE (DEVICE_ADDRESS);

create table TB_CONCENTRADOR_CONFIG
(
  ip             NVARCHAR2(20) not null,
  porta          NVARCHAR2(6) not null,
  url_get        NVARCHAR2(150) not null,
  qtdmax         NUMBER not null,
  url_delete     NVARCHAR2(150) not null,
  device_address NVARCHAR2(16) not null
)
;
comment on column TB_CONCENTRADOR_CONFIG.ip
  is 'Endereco IPv4 do concentrador';
comment on column TB_CONCENTRADOR_CONFIG.porta
  is 'Porta de conexao do concentrador';
comment on column TB_CONCENTRADOR_CONFIG.url_get
  is 'Comando para obter dados dos devices do concentrador';
comment on column TB_CONCENTRADOR_CONFIG.qtdmax
  is 'Quantidade maxima de devices que devem ser retornados na resposta da solicitacao.';
comment on column TB_CONCENTRADOR_CONFIG.url_delete
  is 'Comando para remover dados de devices do concentrador.';
comment on column TB_CONCENTRADOR_CONFIG.device_address
  is 'Endereco do device do concentrador. E'' obtido a partir dos 16 algarismos hexadecimais finais do endereco IPv6 do device.';
alter table TB_CONCENTRADOR_CONFIG
  add constraint FK_TB_CONCENTRADOR_CONFIG_TB_DEVICE foreign key (DEVICE_ADDRESS)
  references TB_DEVICE (DEVICE_ADDRESS);


create table TB_DEVICE_CONFIGURATION
(
  num_medidor    VARCHAR2(20),
  latitude       FLOAT,
  longitude      FLOAT,
  bairro         VARCHAR2(100),
  distrital      VARCHAR2(100),
  municipio      VARCHAR2(100),
  localidade     VARCHAR2(100),
  cod_local      VARCHAR2(20),
  tipo_logr      VARCHAR2(20),
  logradouro     VARCHAR2(250),
  num_imovel     VARCHAR2(20),
  complemento    VARCHAR2(150),
  device_address NVARCHAR2(16),
  tipo_device    CHAR(1)
)
;
comment on column TB_DEVICE_CONFIGURATION.num_medidor
  is 'Numero do medidor (apenas para terminal)';
comment on column TB_DEVICE_CONFIGURATION.latitude
  is 'Coordenada geografica do device: Latitude SIRGAS2000';
comment on column TB_DEVICE_CONFIGURATION.longitude
  is 'Coordenada geografica do device: Longitude SIRGAS2000';
comment on column TB_DEVICE_CONFIGURATION.bairro
  is 'Nome do Bairro';
comment on column TB_DEVICE_CONFIGURATION.distrital
  is 'Nome do distrito';
comment on column TB_DEVICE_CONFIGURATION.municipio
  is 'Nome do municipio';
comment on column TB_DEVICE_CONFIGURATION.localidade
  is 'Codigo de localidade';
comment on column TB_DEVICE_CONFIGURATION.cod_local
  is 'Codigo do local';
comment on column TB_DEVICE_CONFIGURATION.tipo_logr
  is 'Tipo do logradouro';
comment on column TB_DEVICE_CONFIGURATION.logradouro
  is 'Nome do logradouro';
comment on column TB_DEVICE_CONFIGURATION.num_imovel
  is 'Número do imovel';
comment on column TB_DEVICE_CONFIGURATION.complemento
  is 'Complemento do endereco';
comment on column TB_DEVICE_CONFIGURATION.device_address
  is 'Endereco do device. E'' obtido a partir dos 16 algarismos hexadecimais finais do endereco IPv6 do device.';
comment on column TB_DEVICE_CONFIGURATION.tipo_device
  is 'C=Concentrador, T=Terminal, R=Repetidor';
create index IDX_TB_DEVICE_CONFIG01 on TB_DEVICE_CONFIGURATION (DEVICE_ADDRESS);
alter table TB_DEVICE_CONFIGURATION
  add constraint FK_TB_DEVICE_CONFIGURATION_TB_DEVICE foreign key (DEVICE_ADDRESS)
  references TB_DEVICE (DEVICE_ADDRESS)
  novalidate;


create table TB_DEVICE_UC
(
  id             NUMBER(20) not null,
  num_medidor    VARCHAR2(20) not null,
  device_address CHAR(8) not null,
  dc_cadastro    DATE not null,
  latitude       FLOAT(50) not null,
  longitude      FLOAT not null,
  status         CHAR(1) not null
)
;
alter table TB_DEVICE_UC
  add constraint PK_TB_REL_DEVICE_UC primary key (ID);


create table TB_INDICADORES
(
  device_address  NVARCHAR2(20) not null,
  falha           NUMBER,
  erros           NUMBER,
  throughput      NUMBER,
  rank            NUMBER,
  vizinhos        NUMBER,
  sinal           NUMBER,
  latencia        NUMBER,
  rotas           NUMBER,
  dt_keep_alive   DATE not null,
  vl_throughput   NUMBER,
  vl_erros        NUMBER,
  vl_rank         NUMBER,
  vl_vizinhos     NUMBER,
  vl_sinal        NUMBER,
  vl_latencia     NUMBER,
  vl_rotas        NUMBER,
  dt_calculo      DATE not null,
  fl_mais_recente CHAR(1)
)
;
create index NDX_TB_INDICADOR_FALHA on TB_INDICADORES (FALHA);
create bitmap index NDX_TB_INDICADOR_FL_MAIS_RECENTE on TB_INDICADORES (FL_MAIS_RECENTE, DEVICE_ADDRESS);
create index NDX_TB_INDICADOR_001 on TB_INDICADORES (DT_CALCULO);
alter table TB_INDICADORES
  add primary key (DEVICE_ADDRESS, DT_CALCULO);
alter table TB_INDICADORES
  add constraint FK_TB_INDICADORES_TB_DEVICE foreign key (DEVICE_ADDRESS)
  references TB_DEVICE (DEVICE_ADDRESS);
  

create table TB_LOG
(
  id          NUMBER(20) not null,
  cod_objeto  VARCHAR2(100),
  cod_tipo    CHAR(1) not null,
  dt_log      DATE not null,
  cod_usuario VARCHAR2(100),
  descricao   VARCHAR2(250),
  operacao    VARCHAR2(100)
)
;
alter table TB_LOG
  add constraint PK_TB_LOG primary key (ID);


create table TB_MENU
(
  cod_menu       NUMBER not null,
  nome           NVARCHAR2(40) not null,
  caminho_pagina NVARCHAR2(100) not null,
  cod_menu_pai   NUMBER,
  icone_pagina   VARCHAR2(100),
  ordem_menu     NUMBER,
  exibir_menu    CHAR(1)
)
;
alter table TB_MENU
  add constraint PK_TB_MENU primary key (COD_MENU);

create table TB_OPERACAO
(
  cod_operacao       NUMBER not null,
  nome_operacao      NVARCHAR2(20),
  descricao_operacao NVARCHAR2(50)
)
;
alter table TB_OPERACAO
  add constraint PK_TB_OPERACAO primary key (COD_OPERACAO);


create table TB_PERFIL
(
  cod_perfil NUMBER not null,
  nome       NVARCHAR2(100),
  descricao  NVARCHAR2(200)
)
;
alter table TB_PERFIL
  add constraint PK_TB_PERFIL primary key (COD_PERFIL);


create table TB_MENU_OPERACAO
(
  cod_menu     NUMBER not null,
  cod_perfil   NUMBER not null,
  cod_operacao NUMBER not null,
  permitido    CHAR(1) not null
)
;
alter table TB_MENU_OPERACAO
  add constraint FK_TB_MENU_OPERACAO_TB_MENU foreign key (COD_MENU)
  references TB_MENU (COD_MENU);
alter table TB_MENU_OPERACAO
  add constraint FK_TB_MENU_OPERACAO_TB_OPERACAO foreign key (COD_OPERACAO)
  references TB_OPERACAO (COD_OPERACAO);
alter table TB_MENU_OPERACAO
  add constraint FK_TB_MENU_OPERACAO_TB_PERFIL foreign key (COD_PERFIL)
  references TB_PERFIL (COD_PERFIL);


create table TB_NIVEL_ALERTA
(
  nivel_alerta       NUMBER not null,
  descr_nivel_alerta VARCHAR2(50) not null
)
;
alter table TB_NIVEL_ALERTA
  add constraint PK_NIVEL_ALERTA primary key (NIVEL_ALERTA);


create table TB_PARAMETRO
(
  id_param    NUMBER(4) not null,
  vl_param    VARCHAR2(50) not null,
  descr_param VARCHAR2(50)
)
;
comment on column TB_PARAMETRO.id_param
  is 'Identificador unico do parametro';
comment on column TB_PARAMETRO.vl_param
  is 'Valor base do parametro.';
comment on column TB_PARAMETRO.descr_param
  is 'Descricao do parametro';
alter table TB_PARAMETRO
  add constraint PK_PARAMETRO primary key (ID_PARAM);


create table TB_PERFIL_MENU
(
  cod_perfil NUMBER not null,
  cod_menu   NUMBER not null
)
;
alter table TB_PERFIL_MENU
  add constraint FK_TB_PERFIL_MENU_TB_MENU foreign key (COD_MENU)
  references TB_MENU (COD_MENU);
alter table TB_PERFIL_MENU
  add constraint FK_TB_PERFIL_MENU_TB_PERFIL foreign key (COD_PERFIL)
  references TB_PERFIL (COD_PERFIL);


create table TB_TIPO_INDICADOR
(
  tipo_indicador  NUMBER not null,
  nome_indicador  VARCHAR2(50) not null,
  descr_indicador VARCHAR2(200),
  unidade         VARCHAR2(50)
)
;
alter table TB_TIPO_INDICADOR
  add constraint PK_TIPO_INDICADOR primary key (TIPO_INDICADOR);


create table TB_REGRA
(
  id_regra       NUMBER not null,
  id_grupo       NUMBER not null,
  tipo_indicador NUMBER not null,
  lim_inferior   NUMBER,
  lim_superior   NUMBER,
  nivel_alerta   NUMBER not null
)
;
comment on column TB_REGRA.id_regra
  is 'ID unico para a regra';
comment on column TB_REGRA.id_grupo
  is 'A qual grupo essa regra pertence';
comment on column TB_REGRA.tipo_indicador
  is 'A qual indicador essa regra se refere. 1=FALHA, 2=ERROS, 3=THROUGHPUT, 4=RANK, 5=VIZINHOS, 6=SINAL, 7=LATENCIA, 8=ROTAS.';
comment on column TB_REGRA.lim_inferior
  is 'Valor que determina nivel de alerta normal';
comment on column TB_REGRA.lim_superior
  is 'Valor que determina nivel de alerta atencao';
comment on column TB_REGRA.nivel_alerta
  is 'Valor que determina nivel de alerta critico';
alter table TB_REGRA
  add constraint PK_TB_REGRA primary key (ID_REGRA);
alter table TB_REGRA
  add constraint UN_TB_REGRA unique (ID_GRUPO, TIPO_INDICADOR, NIVEL_ALERTA);
alter table TB_REGRA
  add constraint FK_TB_REGRA_TB_GRUPO foreign key (ID_GRUPO)
  references TB_GRUPO (ID_GRUPO);
alter table TB_REGRA
  add constraint FK_TB_REGRA_TB_NIVEL_ALERTA foreign key (NIVEL_ALERTA)
  references TB_NIVEL_ALERTA (NIVEL_ALERTA);
alter table TB_REGRA
  add constraint FK_TB_REGRA_TB_TIPO_INDICADOR foreign key (TIPO_INDICADOR)
  references TB_TIPO_INDICADOR (TIPO_INDICADOR);


create table TB_ROUTE
(
  device_address        NVARCHAR2(16) not null,
  device_parent_address NVARCHAR2(16) not null,
  fl_active             CHAR(1) not null,
  dt_route              DATE not null
)
;
comment on column TB_ROUTE.device_address
  is 'Endereco do device. E'' obtido a partir dos 16 algarismos hexadecimais finais do endereco IPv6 do device.';
comment on column TB_ROUTE.device_parent_address
  is 'Endereco do device pai. E'' obtido a partir dos 16 algarismos hexadecimais finais do endereco IPv6 do device.';
comment on column TB_ROUTE.fl_active
  is '1 = Rota ativa. 0 = Rota não ativa.';
comment on column TB_ROUTE.dt_route
  is 'Data de recebimento dessa informacao de rota.';
alter table TB_ROUTE
  add constraint FK_TB_ROUTE_TB_DEVICE foreign key (DEVICE_ADDRESS)
  references TB_DEVICE (DEVICE_ADDRESS);


create table TB_SUBGRUPO_FAT
(
  tipo_subgrupo  VARCHAR2(5) not null,
  descr_subgrupo VARCHAR2(50)
)
;
comment on table TB_SUBGRUPO_FAT
  is 'Subgrupos de faturamento';
comment on column TB_SUBGRUPO_FAT.tipo_subgrupo
  is 'Codigo do subgrupo, conforme definido pelo PRODIST';
comment on column TB_SUBGRUPO_FAT.descr_subgrupo
  is 'Descricao da subclasse de faturamento.';


create table TB_TIPO_LOGRADOURO
(
  tipo_logr       VARCHAR2(10) not null,
  descr_tipo_logr VARCHAR2(30)
)
;
comment on column TB_TIPO_LOGRADOURO.tipo_logr
  is 'Codigo do tipo do logradouro';
comment on column TB_TIPO_LOGRADOURO.descr_tipo_logr
  is 'Descricao do tipo do logradouro';


create table TB_UNIDADE_CONSUMIDORA
(
  num_uc             NUMBER(20) not null,
  nome_completo      VARCHAR2(200),
  cod_situ           VARCHAR2(20),
  dt_situ            DATE,
  dt_ligacao         DATE,
  grupo_subgrupo_fat VARCHAR2(20),
  cod_fase           VARCHAR2(100),
  classe_principal   VARCHAR2(200),
  cond_fat           VARCHAR2(20),
  etapa              VARCHAR2(4),
  livro              VARCHAR2(20),
  tipo_med           VARCHAR2(20),
  num_medidor        VARCHAR2(20),
  num_elementos      NUMBER(11),
  num_fios           NUMBER(11),
  imin               NUMBER(5,2),
  ano_fab_med        NUMBER(4),
  subtipo_equip      VARCHAR2(100),
  alimentador        VARCHAR2(100),
  transformador      VARCHAR2(100),
  cod_clas_cons      VARCHAR2(100),
  classe_consumo     VARCHAR2(100)
)
;
comment on column TB_UNIDADE_CONSUMIDORA.num_uc
  is 'Numero da unidade consumidora.';
comment on column TB_UNIDADE_CONSUMIDORA.nome_completo
  is 'Nome do cliente.';
comment on column TB_UNIDADE_CONSUMIDORA.cod_situ
  is 'Codigo da situacao do cliente.';
comment on column TB_UNIDADE_CONSUMIDORA.dt_situ
  is 'Data da situacao do cliente.';
comment on column TB_UNIDADE_CONSUMIDORA.dt_ligacao
  is 'Data de ligacao do cliente.';
comment on column TB_UNIDADE_CONSUMIDORA.grupo_subgrupo_fat
  is 'Grupo/Subgrupo de faturamento.';
comment on column TB_UNIDADE_CONSUMIDORA.cod_fase
  is 'Codigo das fases de alimentacao do cliente.';
comment on column TB_UNIDADE_CONSUMIDORA.classe_principal
  is 'Classe do cliente.';
comment on column TB_UNIDADE_CONSUMIDORA.cond_fat
  is 'Condicao de faturamento.';
comment on column TB_UNIDADE_CONSUMIDORA.etapa
  is 'Etapa de medicao.';
comment on column TB_UNIDADE_CONSUMIDORA.livro
  is 'Livro para medicao.';
comment on column TB_UNIDADE_CONSUMIDORA.tipo_med
  is 'Tipo do medidor.';
comment on column TB_UNIDADE_CONSUMIDORA.num_medidor
  is 'Numero do medidor.';
comment on column TB_UNIDADE_CONSUMIDORA.num_elementos
  is 'Quantidade de elementos do medidor.';
comment on column TB_UNIDADE_CONSUMIDORA.num_fios
  is 'Quantidade de fios do medidor.';
comment on column TB_UNIDADE_CONSUMIDORA.imin
  is 'Corrente mínima (?)';
comment on column TB_UNIDADE_CONSUMIDORA.ano_fab_med
  is 'Ano de fabricacao do medidor.';
comment on column TB_UNIDADE_CONSUMIDORA.subtipo_equip
  is 'Subtipo do medidor.';
comment on column TB_UNIDADE_CONSUMIDORA.alimentador
  is 'Identificacao do alimentador da rede.';
comment on column TB_UNIDADE_CONSUMIDORA.transformador
  is 'Identificacao da instalacao transformadora da rede.';
comment on column TB_UNIDADE_CONSUMIDORA.cod_clas_cons
  is 'Codigo de classe de consumo.';
comment on column TB_UNIDADE_CONSUMIDORA.classe_consumo
  is 'Descricao da classe de consumo.';
create index NDX_TB_UNIDADE_CONSUMIDORA01 on TB_UNIDADE_CONSUMIDORA (NUM_MEDIDOR);
alter table TB_UNIDADE_CONSUMIDORA
  add constraint PK_TB_UNIDADE_CONSUMIDORA primary key (NUM_UC);


create table TB_USUARIO
(
  cod_usuario VARCHAR2(100) not null,
  ativo       CHAR(1) not null,
  senha       NVARCHAR2(20)
)
;
alter table TB_USUARIO
  add constraint PK_TB_USUARIO primary key (COD_USUARIO);


create table TB_USUARIO_PERFIL
(
  cod_usuario VARCHAR2(100) not null,
  cod_perfil  NUMBER not null,
  data        DATE,
  id          NUMBER
)
;
alter table TB_USUARIO_PERFIL
  add constraint FK_TB_USUARIO_PERFIL_TB_PERFIL foreign key (COD_PERFIL)
  references TB_PERFIL (COD_PERFIL);
alter table TB_USUARIO_PERFIL
  add constraint FK_TB_USUARIO_PERFIL_TB_USUARIO foreign key (COD_USUARIO)
  references TB_USUARIO (COD_USUARIO);
  

insert into TB_GRUPO (id_grupo, nome, descricao, tb)
values (0, 'DEFAULT', 'Grupo padrão para devices', 30);
commit;


insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (5, 'Devices', ' ', null, '<i class=''fa fa-pencil icon devices mr-3'' style=''color:white;font-size:2.4rem;''></i>', 2, 'S');
insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (6, 'Análise', ' ', null, '<i class=''fa fa-list icon analise mr-3'' style=''color:white;font-size:2.4rem;''></i>', 1, 'S');
insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (7, 'Configurações', ' ', null, '<i class=''fa fa-users icon config mr-3'' style=''color:white;font-size:2.4rem;''></i>', 3, 'S');
insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (8, 'Criar Device', '/SGRAPP/Device/GerenciarDevices', 5, null, 4, 'S');
insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (9, 'DashBoard', '/SGRAPP/DashBoard/Index', 6, null, 5, 'S');
insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (11, 'Associar Perfis', '/SGRAPP/Usuario/AtribuirPerfil', 7, null, 6, 'S');
insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (1, 'Monitoramento de Devices', '/SGRAPP/Device/ListarDevices', 6, null, 7, 'S');
insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (2, 'Devices Pendentes', '/SGRAPP/Device/DevicesPendentes', 5, null, 8, 'S');
insert into TB_MENU (cod_menu, nome, caminho_pagina, cod_menu_pai, icone_pagina, ordem_menu, exibir_menu)
values (12, 'Editar Device', '/SGRAPP/Device/EditarDevice', null, null, null, 'N');
commit;



insert into TB_OPERACAO (cod_operacao, nome_operacao, descricao_operacao)
values (1, 'DELETE', 'DELETAR');
insert into TB_OPERACAO (cod_operacao, nome_operacao, descricao_operacao)
values (2, 'UPDATE', 'ALTERAR');
insert into TB_OPERACAO (cod_operacao, nome_operacao, descricao_operacao)
values (3, 'SALVAR', 'SALVAR');
insert into TB_OPERACAO (cod_operacao, nome_operacao, descricao_operacao)
values (4, 'VISUALIZAR', 'VISUALIZAR');
commit;



insert into TB_PERFIL (cod_perfil, nome, descricao)
values (1, 'COM', 'PERFIL COM TECNICO SUPORTE');
insert into TB_PERFIL (cod_perfil, nome, descricao)
values (2, 'TELECOM', 'PERFIL TECNICO ANALISE');
insert into TB_PERFIL (cod_perfil, nome, descricao)
values (3, 'GESTOR', 'PERFIL GESTÃO ANALISE');
insert into TB_PERFIL (cod_perfil, nome, descricao)
values (4, 'ADMINISTRADOR', 'PERFIL DE ACESSO A TODAS A AÇÕES');
commit;


insert into TB_NIVEL_ALERTA (nivel_alerta, descr_nivel_alerta)
values (0, 'INDEFINIDO');
insert into TB_NIVEL_ALERTA (nivel_alerta, descr_nivel_alerta)
values (1, 'NORMAL');
insert into TB_NIVEL_ALERTA (nivel_alerta, descr_nivel_alerta)
values (2, 'ATENÇÃO');
insert into TB_NIVEL_ALERTA (nivel_alerta, descr_nivel_alerta)
values (3, 'CRÍTICO');
commit;



insert into TB_PARAMETRO (id_param, vl_param, descr_param)
values (1, '15', 'Qtd. dias para manter o historico das leituras');
commit;



insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 11);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 12);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (1, 1);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (1, 8);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (1, 12);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (2, 2);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (2, 5);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (3, 1);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (3, 9);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (3, 8);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (3, 6);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (3, 5);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (3, 2);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (3, 12);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (1, 2);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (2, 8);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (1, 5);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (1, 6);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (2, 9);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (2, 12);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (1, 9);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (2, 6);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (2, 1);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 1);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 2);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 5);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 6);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 7);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 8);
insert into TB_PERFIL_MENU (cod_perfil, cod_menu)
values (4, 9);
commit;

insert into TB_TIPO_INDICADOR (tipo_indicador, nome_indicador, descr_indicador, unidade)
values (1, 'FALHA', 'Indica se o device está com falha.', 'minutos');
insert into TB_TIPO_INDICADOR (tipo_indicador, nome_indicador, descr_indicador, unidade)
values (2, 'ERROS', 'Taxa de erros de envio de pacotes.', 'pacotes/h');
insert into TB_TIPO_INDICADOR (tipo_indicador, nome_indicador, descr_indicador, unidade)
values (3, 'THROUGHPUT', 'Taxa de envio de bytes.', 'bytes/s');
insert into TB_TIPO_INDICADOR (tipo_indicador, nome_indicador, descr_indicador, unidade)
values (4, 'RANK', 'Rank desse device na rede MESH.', null);
insert into TB_TIPO_INDICADOR (tipo_indicador, nome_indicador, descr_indicador, unidade)
values (5, 'VIZINHOS', 'Quantidade de vizinhos identificados', 'vizinhos');
insert into TB_TIPO_INDICADOR (tipo_indicador, nome_indicador, descr_indicador, unidade)
values (6, 'SINAL', 'Nível médio do sinal de rádio.', '(dBm)');
insert into TB_TIPO_INDICADOR (tipo_indicador, nome_indicador, descr_indicador, unidade)
values (7, 'LATÊNCIA', 'Latência na comunicação em milissegundos.', 'ms');
insert into TB_TIPO_INDICADOR (tipo_indicador, nome_indicador, descr_indicador, unidade)
values (8, 'ROTAS', 'Quantidade de rotas sobre esse device', 'rotas');
commit;

insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (6, 0, 3, 10000, 0, 1);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (7, 0, 3, 50000, 0, 2);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (8, 0, 3, 100000, 0, 3);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (9, 0, 4, 300, 0, 1);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (11, 0, 4, 600, 0, 3);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (12, 0, 5, 2, 0, 3);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (13, 0, 5, 3, 0, 2);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (14, 0, 5, 5, 0, 1);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (15, 0, 6, 100, 0, 3);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (16, 0, 6, 180, 0, 2);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (17, 0, 6, 200, 0, 1);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (18, 0, 7, 1500, 0, 1);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (19, 0, 7, 3000, 0, 2);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (20, 0, 7, 5000, 0, 3);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (21, 0, 8, 0, 0, 1);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (10, 0, 4, 450, 0, 2);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (1, 0, 1, 2, 0, 1);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (2, 0, 1, 5, 0, 2);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (3, 0, 2, 10, 0, 1);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (4, 0, 2, 50, 0, 2);
insert into TB_REGRA (id_regra, id_grupo, tipo_indicador, lim_inferior, lim_superior, nivel_alerta)
values (5, 0, 2, 100, 0, 3);
commit;



insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('A1', '230kV ou mais');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('A2', '88kV a 138kV');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('A3', '69kV');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('A3a', '30kV a 44kV');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('A4', '2,3kV a 25kV');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('AS', 'até 2,3kV subterrâneo');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('B1', 'Residencial');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('B2', 'Rural');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('B3', 'Demais classes');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('B4a', 'Iluminação pública rede de distribuição');
insert into TB_SUBGRUPO_FAT (tipo_subgrupo, descr_subgrupo)
values ('B4b', 'Iluminação pública bulbo da lâmpada');
commit;


insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ALD', 'Aldeia');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ASS', 'Assentamento');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('AV', 'Avenida');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CHA', 'Chácara');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CRR', 'Córrego');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('EST', 'Estrada');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ETN', 'Estância');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('FAZ', 'Fazenda');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('INC', 'Incorporação');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('PSQ', 'Pesqueiro');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RCH', 'Riacho');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ROD', 'Rodovia');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RUA', 'Rua');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('SIT', 'Sítio');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VIL', 'Vila');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('AL', 'Alameda');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ACAMP', 'Acampamento');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('AC', 'Acesso');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('AD', 'Adro');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ERA', 'Aeroporto');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('AT', 'Alto');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('A', 'Área');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('AE', 'Área Especial');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ART', 'Artéria');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ATL', 'Atalho');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('AV-CONT', 'Avenida Contorno');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BX', 'Baixa');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BLO', 'Balão');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BAL', 'Balneário');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BC', 'Beco');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BELV', 'Belvedere');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BL', 'Bloco');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BSQ', 'Bosque');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BVD', 'Boulevard');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BCO', 'Buraco');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('C', 'Cais');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CALC', 'Calçada');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CAM', 'Caminho');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CPO', 'Campo');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CAN', 'Canal');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CHAP', 'Chapadão');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CIRC', 'Circular');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('COL', 'Colônia');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CMP-VR', 'Complexo Viário');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('COND', 'Condomínio');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CJ', 'Conjunto');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('COR', 'Corredor');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CRG', 'Córrego');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('DSC', 'Descida');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('DSV', 'Desvio');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('DT', 'Distrito');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('EVD', 'Elevada');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ENT-PART', 'Entrada Particular');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('EQ', 'Entre Quadra');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ESC', 'Escada');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ESP', 'Esplanada');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ETC', 'Estação');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ESTC', 'Estacionamento');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ETD', 'Estádio');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('EST-MUN', 'Estrada Municipal');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('FAV', 'Favela');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('FRA', 'Feira');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('FER', 'Ferrovia');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('FNT', 'Fonte');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('FTE', 'Forte');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('GAL', 'Galeria');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('GJA', 'Granja');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('HAB', 'Habitacional');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('IA', 'Ilha');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('JD', 'Jardim');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('JDE', 'Jardinete');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('LD', 'Ladeira');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('LG', 'Lago');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('LGA', 'Lagoa');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('LRG', 'Largo');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('LOT', 'Loteamento');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('PC', 'Praça');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('PC-ESP', 'Praça de Esportes');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('PR', 'Praia');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('PRL', 'Prolongamento');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('Q', 'Quadra');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('QTA', 'Quinta');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('QTAS', 'Quinta');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RAM', 'Ramal');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RMP', 'Rampa');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('REC', 'Recanto');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RES', 'Residencial');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RET', 'Reta');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RER', 'Retiro');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RTN', 'Retorno');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ROD-AN', 'Rodo Anel');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('RTT', 'Rotatória');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ROT', 'Rótula');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('R', 'Rua');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('R-LIG', 'Rua de Ligação');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('R-PED', 'Rua de Pedestre');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('SRV', 'Servidão');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ST', 'Setor');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('SUB', 'Subida');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('TER', 'Terminal');
commit;

insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('TV', 'Travessa');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('TV-PART', 'Travessa Particular');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('TRC', 'Trecho');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('TRV', 'Trevo');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('TCH', 'Trincheira');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('TUN', 'Túnel');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('UNID', 'Unidade');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VAL', 'Vala');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VLE', 'Vale');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VRTE', 'Variante');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VER', 'Vereda');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('V', 'Via');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('V-AC', 'Via de Acesso');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('V-PED', 'Via de Pedestre');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('V-EVD', 'Via Elevada');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('V-EXP', 'Via Expressa');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VD', 'Viaduto');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VLA', 'Viela');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VL', 'Vila');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ZIG-ZAG', 'Zigue Zague');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('AER', 'Aeroporto');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('COM', 'Comunidade');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('QD', 'Quadra');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('BEC', 'Beco');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CAS', 'Cais');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('CMP', 'Campo');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('FLT', 'Floresta');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ILH', 'Ilha');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('LUG', 'Lugar');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('MRR', 'Morro');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('PQE', 'Parque');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('PAS', 'Passeio');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('VIA', 'Via');
insert into TB_TIPO_LOGRADOURO (tipo_logr, descr_tipo_logr)
values ('ALT', 'Alto');
commit;

insert into TB_USUARIO (cod_usuario, ativo, senha)
values ('ADMATECH', 'S', 'ADMATECH');
commit;

insert into TB_USUARIO_PERFIL (cod_usuario, cod_perfil, data, id)
values ('ADMATECH', 4, to_date('23-02-2018', 'dd-mm-yyyy'), 1);

commit;


create sequence TB_LOG_SEQ
minvalue 1
maxvalue 100000
start with 3081
increment by 1
cache 20;

/

create sequence TB_USUARIO_PERFIL_SEQ
minvalue 1
maxvalue 100000
start with 321
increment by 1
cache 20;


/

create or replace package ATECH_SGR is

    -- DEFINIÇÕES DE CONSTANTES E PARÂMETROS DO SISTEMA:

    -- Estados de operação:
    C_ESTADO_INDEFINIDO constant NUMBER := 0;
    C_ESTADO_NORMAL     constant NUMBER := 1;
    C_ESTADO_ATENCAO    constant NUMBER := 2;
    C_ESTADO_CRITICO    constant NUMBER := 3;

    -- Estado de ativação do objeto:
    C_FL_ATIVO   constant VARCHAR2(1) := 'S';
    C_FL_INATIVO constant VARCHAR2(1) := 'N';

    C_FL_SIM     constant VARCHAR2(1) := 'S';
    C_FL_NAO     constant VARCHAR2(1) := 'N';

    -- Tipos de devices
    C_TERMINAL     constant VARCHAR2(1) := 'T';
    C_REPETIDOR    constant VARCHAR2(1) := 'R';
    C_CONCENTRADOR constant VARCHAR2(1) := 'C';

    -- Quantidade máxima de comandos até dar um COMMIT;
    C_CONTADOR_COMMIT constant NUMBER := 1024;  

    -- Grupo DEFAULT.
    C_GRUPO_DEFAULT constant NUMBER := 0;

    -- Códigos dos parâmetros de configuração do sistema
    C_PARAM_DIAS_HIST constant NUMBER := 1; -- Dias para manter o histórico

    function parametro(p_id_param TB_PARAMETRO.ID_PARAM%TYPE)
      return TB_PARAMETRO.VL_PARAM%TYPE;

    procedure limpar_historico;

end ATECH_SGR;

/

create or replace package ATECH_SGR_CONCENTRADOR_CONFIG is

  -- Author  : RGRACCIA
  -- Created : 02/02/2018 09:18:13
  -- Purpose : 

TYPE T_CURSOR IS REF CURSOR; 

PROCEDURE LISTAR_CONCENTRADORES (IO_CURSOR IN OUT T_CURSOR);  


end ATECH_SGR_CONCENTRADOR_CONFIG;
/


CREATE OR REPLACE PACKAGE ATECH_SGR_DASHBOARD IS

  -- Author  : RGRACCIA
  -- Created : 10/04/2018 14:55:09
  -- Purpose : 
  
TYPE T_CURSOR IS REF CURSOR;  
  
PROCEDURE LISTAR_TOTALIZADORES (IO_CURSOR IN OUT T_CURSOR);

PROCEDURE LISTAR_EQUIPAMENTO_FALHA (IO_CURSOR IN OUT T_CURSOR);

PROCEDURE LISTAR_PACOTES_COM_ERROS (IO_CURSOR IN OUT T_CURSOR);

PROCEDURE LISTAR_LATENCIA (IO_CURSOR IN OUT T_CURSOR);

end ATECH_SGR_DASHBOARD;
/


create or replace package ATECH_SGR_DEVICE is

  -- Author  : PANDRADE
  -- Created : 05/02/2018 10:51:11
  -- Purpose : 
  
TYPE T_CURSOR IS REF CURSOR; 

procedure listar_devices (io_cursor in out t_cursor,
                         p_tp_device varchar2,
                         p_falha varchar2,
                         p_sinal varchar2,
                         p_throughput varchar2,
                         p_latencia varchar2,
                         p_rank varchar2,
                         p_rotas varchar2,
                         p_vizinhos varchar2,
                         p_municipio varchar2,
                         p_bairro varchar2,
                         p_ipconcentrador varchar2,
                         p_gruposub varchar2,
                         p_filtrorapido varchar2
                         );

procedure listar_devices_pendentes (io_cursor in out t_cursor, P_TIPO_DEVICE tb_device.tipo_device%type, p_device_adrress varchar2);

PROCEDURE listar_detalhes_device (IO_CURSOR IN OUT T_CURSOR, P_DEVICE_ADDRESS tb_device.device_address%type); 

procedure listar_device_dataCalc (io_cursor in out t_cursor);

procedure listar_devices_calc (io_cursor in out t_cursor);

procedure listar_devices_concentradores (io_cursor in out t_cursor);

procedure listar_regras (io_cursor in out t_cursor);

procedure device_inserir_indicadores (xml_data clob);

procedure listar_ultimo_calculado (io_cursor in out t_cursor);

procedure listar_rotas (io_cursor in out t_cursor,p_device_address tb_device.device_address%type);

procedure listar_grupo_subgrupo (io_cursor in out t_cursor);

procedure listar_municipio (io_cursor in out t_cursor);

procedure listar_bairro (io_cursor in out t_cursor);

procedure busca_device_pendente (io_cursor in out t_cursor, p_device_adrees varchar2);


end ATECH_SGR_DEVICE;
/


create or replace package ATECH_SGR_DEVICE_CONFIGURATION is

  -- Author  : PANDRADE
  -- Created : 06/02/2018 13:47:36
  -- Purpose :   
  
TYPE T_CURSOR IS REF CURSOR; 
PROCEDURE listar_devices_configuration (IO_CURSOR IN OUT T_CURSOR);  
  
procedure insert_device_configuration(p_latitude decimal, p_longitude decimal, p_bairro nvarchar2, p_distrital nvarchar2, p_municipio nvarchar2, p_tipo_logr nvarchar2, p_logradouro nvarchar2, p_num_imovel nvarchar2, p_complemento nvarchar2, p_device_address nvarchar2, p_tipo_device nvarchar2, p_ip nvarchar2, p_porta nvarchar2, p_qtdMax number, p_ativo tb_device.ativo%type, p_Api_get nvarchar2, p_Api_delete nvarchar2);

procedure update_device_configuration(p_latitude decimal, p_longitude decimal, p_bairro nvarchar2, p_distrital nvarchar2, p_municipio nvarchar2, p_tipo_logr nvarchar2, p_logradouro nvarchar2, p_num_imovel nvarchar2, p_complemento nvarchar2, p_device_address nvarchar2, p_tipo_device nvarchar2, p_ip nvarchar2, p_porta nvarchar2, p_qtdMax number, p_ativo tb_device.ativo%type, p_Api_get nvarchar2, p_Api_delete nvarchar2);

PROCEDURE select_device_configuration(IO_CURSOR IN OUT T_CURSOR, P_DEVICE_ADDRESS tb_device.device_address%type); 

procedure excluir_device_configuration(p_device_address nvarchar2);

end ATECH_SGR_DEVICE_CONFIGURATION;
/


create or replace package ATECH_SGR_LOG is

  -- Author  : PANDRADE
  -- Created : 09/02/2018 08:49:33
  -- Purpose : 
  
procedure inserir_log(p_cod_objeto nvarchar2, p_cod_tipo nvarchar2, p_dt_log date, p_cod_usuario nvarchar2, p_descricao nvarchar2, p_operacao nvarchar2);

end ATECH_SGR_LOG;
/


create or replace package ATECH_SGR_MENU is

TYPE T_CURSOR IS REF CURSOR;

PROCEDURE LISTAR_MENU (IO_CURSOR IN OUT T_CURSOR, P_COD_PERFIL NUMBER);

end ATECH_SGR_MENU;
/


create or replace package ATECH_SGR_PERMISSAO is

  -- Author  : RGRACCIA
  -- Created : 23/02/2018 15:03:52
  -- Purpose : 

TYPE T_CURSOR IS REF CURSOR;

PROCEDURE LISTA_PERMISSAO_PAGINA (IO_CURSOR IN OUT T_CURSOR,P_CAMINHO_PAGINA TB_MENU.CAMINHO_PAGINA%type);


end ATECH_SGR_PERMISSAO;
/

create or replace package atech_sgr_route is

  -- Author  : RGRACCIA
  -- Created : 31/01/2018 16:34:06
  -- Purpose : 
  
procedure inserir_upd_route(p_device_address tb_route.device_address%type,p_device_parent_address tb_route.device_parent_address%type,p_fl_active tb_route.fl_active%type,p_dt_route nvarchar2);

procedure inserir_upd_device(p_device_id tb_device.device_id%type,p_num_medidor tb_device.num_medidor%type,p_device_address tb_device.device_address%type,p_firmware_version tb_device.firmware_version%type);

procedure inserir_devicedata(p_num_id number, p_device_address nvarchar2, p_device_parent_address nvarchar2, p_sinal_min number, p_sinal_max number, p_sinal_med number, p_bytes_env number, p_pack_env number, p_pack_no_ack number, p_num_rank number, p_latencia number, p_num_rotas number, p_num_vizinhos number, p_dt_device_data nvarchar2,p_ip_concentrador nvarchar2);

end atech_sgr_route;
/


create or replace package ATECH_SGR_USUARIO is

  -- Author  : RGRACCIA
  -- Created : 02/02/2018 09:18:13
  -- Purpose : 

TYPE T_CURSOR IS REF CURSOR; 



PROCEDURE AUTENTICAR_ADMINISTRADOR (IO_CURSOR IN OUT T_CURSOR, P_COD_USUARIO VARCHAR2,P_SENHA VARCHAR2,P_TIPO CHAR); 

PROCEDURE USUARIO_EXISTE (IO_CURSOR IN OUT T_CURSOR, P_COD_USUARIO VARCHAR2);

PROCEDURE LISTAR_USUARIOS_COM_PERFIL (IO_CURSOR IN OUT T_CURSOR);

PROCEDURE INSERIR_PERFIL_USUARIO (P_COD_USUARIO TB_USUARIO.COD_USUARIO%type,P_COD_PERFIL TB_USUARIO_PERFIL.COD_PERFIL%type);

PROCEDURE EXCLUIR_PERFIL_USUARIO (P_ID Number);

PROCEDURE UPDATE_USUARIO_ATIVO (P_ID Number,P_ATIVO CHAR);

PROCEDURE LISTAR_PERFIL (IO_CURSOR IN OUT T_CURSOR);

PROCEDURE BUSCA_PERFIL_USUARIO (IO_CURSOR IN OUT T_CURSOR,P_COD_USUARIO TB_USUARIO.COD_USUARIO%type);

end ATECH_SGR_USUARIO;
/

create or replace package body ATECH_SGR is

    --
    -- parametro()
    --
    -- Retorna um valor de um parâmetro a partir do seu identificador.
    --
    function parametro(p_id_param TB_PARAMETRO.ID_PARAM%TYPE)
      return TB_PARAMETRO.VL_PARAM%TYPE
    is
        v_vl_param TB_PARAMETRO.VL_PARAM%TYPE;
    begin
        SELECT P.VL_PARAM
        INTO   v_vl_param
        FROM   TB_PARAMETRO P
        WHERE  P.ID_PARAM = p_id_param;
        
        return v_vl_param;
    end parametro;

    --
    -- limpar_historico
    --
    -- Efetua a limpeza dos registros mais antigos da base de histórico
    -- de registros do sistema.
    --
    procedure limpar_historico
    IS
        v_data_limite DATE;
        v_conta_commit NUMBER;
        v_dias_limpeza NUMBER;
        v_contador NUMBER;

    BEGIN

        v_dias_limpeza := CAST(ATECH_SGR.PARAMETRO(ATECH_SGR.C_PARAM_DIAS_HIST) AS NUMBER);

        ATECH_SGR_LOG.INSERIR_LOG(p_cod_objeto => 'limpar_historico'
                                , p_cod_tipo => 1
                                , p_dt_log => CAST(SYSTIMESTAMP AT TIME ZONE 'UTC' AS DATE)
                                , p_cod_usuario => NULL
                                , p_descricao => 'Limpeza do histórico de ' || CAST(v_dias_limpeza AS VARCHAR2) || ' dias.'
                                , p_operacao => 'Limpa historico');

        -- Obtém a data limite para limpeza do histórico.
        v_data_limite := TRUNC(CAST(SYSTIMESTAMP AT TIME ZONE 'UTC' AS DATE))-v_dias_limpeza+1;
        v_contador := 0;

        -- Varre a tabela de indicadores para remover o histórico.
        v_conta_commit := 0;
        FOR reg IN
           (SELECT I.DEVICE_ADDRESS, I.DT_CALCULO
            FROM   TB_INDICADORES I
            WHERE  I.FL_MAIS_RECENTE = ATECH_SGR.C_FL_NAO
            AND    I.DT_CALCULO < v_data_limite
           )LOOP

            DELETE FROM TB_INDICADORES I
            WHERE  I.DEVICE_ADDRESS = reg.device_address
            AND    I.DT_CALCULO = reg.dt_calculo;
            
            v_contador := v_contador + 1;
            v_conta_commit := v_conta_commit + 1;
            IF v_conta_commit = ATECH_SGR.C_CONTADOR_COMMIT THEN
                COMMIT;
                v_conta_commit := 0;
            END IF;
        END LOOP;
        COMMIT;

        ATECH_SGR_LOG.INSERIR_LOG(p_cod_objeto => 'TB_INDICADORES'
                                , p_cod_tipo => 1
                                , p_dt_log => CAST(SYSTIMESTAMP AT TIME ZONE 'UTC' AS DATE)
                                , p_cod_usuario => NULL
                                , p_descricao => 'Removidos ' || CAST(v_contador AS VARCHAR2) || ' registros.'
                                , p_operacao => 'Limpa historico');

        -- Varre a tabela de dados do device para remover o histórico.
        v_contador := 0;
        v_conta_commit := 0;
        FOR reg IN
           (SELECT DD.DEVICE_ADDRESS, DD.DT_DEVICE_DATA 
            FROM   TB_DEVICE_DATA DD
            WHERE  DD.FL_MAIS_RECENTE = ATECH_SGR.C_FL_NAO
            AND    DD.DT_DEVICE_DATA < v_data_limite
           )LOOP
           
           DELETE FROM TB_DEVICE_DATA DD
           WHERE  DD.DEVICE_ADDRESS = reg.device_address
           AND    DD.DT_DEVICE_DATA = reg.dt_device_data;

           v_contador := v_contador + 1;
           v_conta_commit := v_conta_commit + 1;
           IF v_conta_commit = ATECH_SGR.C_CONTADOR_COMMIT THEN
               COMMIT;
               v_conta_commit := 0;
           END IF;
           
        END LOOP;
        COMMIT;

        ATECH_SGR_LOG.INSERIR_LOG(p_cod_objeto => 'TB_DEVICE_DATA'
                                , p_cod_tipo => 1
                                , p_dt_log => CAST(SYSTIMESTAMP AT TIME ZONE 'UTC' AS DATE)
                                , p_cod_usuario => NULL
                                , p_descricao => 'Removidos ' || CAST(v_contador AS VARCHAR2) || ' registros.'
                                , p_operacao => 'Limpa historico');

    END limpar_historico;

end ATECH_SGR;
/


create or replace package body ATECH_SGR_CONCENTRADOR_CONFIG is

  
PROCEDURE LISTAR_CONCENTRADORES (IO_CURSOR IN OUT T_CURSOR) AS 
     
    BEGIN   

         OPEN IO_CURSOR FOR   
             SELECT C.IP
                  , C.PORTA
                  , C.URL_GET
                  , C.URL_DELETE
                  , C.QTDMAX
                  , C.DEVICE_ADDRESS
             FROM TB_CONCENTRADOR_CONFIG C
             INNER JOIN TB_DEVICE D
             ON C.DEVICE_ADDRESS = D.DEVICE_ADDRESS
             WHERE D.ATIVO = ATECH_SGR.C_FL_ATIVO;

    END LISTAR_CONCENTRADORES;


END ATECH_SGR_CONCENTRADOR_CONFIG;
/


CREATE OR REPLACE PACKAGE BODY ATECH_SGR_DASHBOARD IS


PROCEDURE LISTAR_TOTALIZADORES (IO_CURSOR IN OUT T_CURSOR) AS 

P_ESTAVEIS NUMBER(38);
P_ATENCAO NUMBER(38);
P_CRITICOS NUMBER(38);
P_INDISPONIVEIS NUMBER(38);
T_PENDENTES NUMBER(38);

BEGIN   
             -- Retorna Pontos Estáveis                  
             select count(d.device_address) INTO P_ESTAVEIS
             from tb_device d
             inner join tb_indicadores tdd
             on tdd.device_address = d.device_address
             inner join tb_device_configuration c
             on d.device_address = c.device_address 
             left join tb_unidade_consumidora u 
             on d.num_medidor = u.num_medidor
             left join tb_device_data ddd
             on ddd.device_address = d.device_address
             where d.ativo = 'S'
             and tdd.fl_mais_recente = 'S'
             and ddd.fl_mais_recente = 'S'
             and tdd.falha = 1
             and d.tipo_device in (ATECH_SGR.C_TERMINAL,ATECH_SGR.C_REPETIDOR);
             
             -- Retorna Pontos Atenção                  
             select count(d.device_address) INTO P_ATENCAO
             from tb_device d
             inner join tb_indicadores tdd
             on tdd.device_address = d.device_address
             inner join tb_device_configuration c
             on d.device_address = c.device_address 
             left join tb_unidade_consumidora u 
             on d.num_medidor = u.num_medidor
             left join tb_device_data ddd
             on ddd.device_address = d.device_address
             where d.ativo = 'S'
             and tdd.fl_mais_recente = 'S'
             and ddd.fl_mais_recente = 'S'
             and tdd.falha = 2
             and d.tipo_device in (ATECH_SGR.C_TERMINAL,ATECH_SGR.C_REPETIDOR);
             
             
             -- Retorna Pontos Críticos                  
             select count(d.device_address) INTO P_CRITICOS
             from tb_device d
             inner join tb_indicadores tdd
             on tdd.device_address = d.device_address
             inner join tb_device_configuration c
             on d.device_address = c.device_address 
             left join tb_unidade_consumidora u 
             on d.num_medidor = u.num_medidor
             left join tb_device_data ddd
             on ddd.device_address = d.device_address
             where d.ativo = 'S'
             and tdd.fl_mais_recente = 'S'
             and ddd.fl_mais_recente = 'S'
             and tdd.falha = 3
             and d.tipo_device in (ATECH_SGR.C_TERMINAL,ATECH_SGR.C_REPETIDOR);
             
               -- Retorna Pontos Indisponiveis                  
             select count(d.device_address) INTO P_INDISPONIVEIS
             from tb_device d
             inner join tb_indicadores tdd
             on tdd.device_address = d.device_address
             inner join tb_device_configuration c
             on d.device_address = c.device_address 
             left join tb_unidade_consumidora u 
             on d.num_medidor = u.num_medidor
             left join tb_device_data ddd
             on ddd.device_address = d.device_address
             where d.ativo = 'S'
             and tdd.fl_mais_recente = 'S'
             and ddd.fl_mais_recente = 'S'
             and tdd.falha = 0
             and d.tipo_device in (ATECH_SGR.C_TERMINAL,ATECH_SGR.C_REPETIDOR);
             
             -- Retorna Pontos Devices Pendentes
             select count(de.tipo_device) INTO T_PENDENTES  from
                (select device_address, max(dt_device_data) as maxdata 
                from tb_device_data group by device_address) d
                inner join tb_device de 
                on de.device_address = d.device_address
                left join tb_device_configuration c 
                on c.device_address = d.device_address
                where c.device_address is null
                and de.tipo_device in ('T','R');
             
           OPEN IO_CURSOR FOR
             SELECT P_ESTAVEIS as P_ESTAVEIS ,P_ATENCAO as P_ATENCAO ,P_CRITICOS as P_CRITICOS,P_INDISPONIVEIS as P_INDISPONIVEIS,T_PENDENTES as T_PENDENTES FROM DUAL;
              

END LISTAR_TOTALIZADORES;

PROCEDURE LISTAR_EQUIPAMENTO_FALHA (IO_CURSOR IN OUT T_CURSOR) AS 

BEGIN  
           OPEN IO_CURSOR FOR
                
                SELECT i.falha as indicador , count(*) as total from tb_indicadores i
                 inner join tb_device d
                 on i.device_address = d.device_address
                 inner join tb_device_configuration c
                 on d.device_address = c.device_address 
                 left join tb_unidade_consumidora u 
                 on d.num_medidor = u.num_medidor
                 left join tb_device_data ddd
                 on ddd.device_address = d.device_address
                 where d.ativo = 'S'
                 and i.fl_mais_recente = 'S'
                 and ddd.fl_mais_recente = 'S'
                 and d.tipo_device in (ATECH_SGR.C_TERMINAL,ATECH_SGR.C_REPETIDOR)
                group by i.falha;
                
                
                
                 
END LISTAR_EQUIPAMENTO_FALHA;

PROCEDURE LISTAR_PACOTES_COM_ERROS (IO_CURSOR IN OUT T_CURSOR) AS 

BEGIN   

           OPEN IO_CURSOR FOR
               SELECT i.erros as indicador , count(*) as total from tb_indicadores i 
                 inner join tb_device d
                 on i.device_address = d.device_address
                 inner join tb_device_configuration c
                 on d.device_address = c.device_address 
                 left join tb_unidade_consumidora u 
                 on d.num_medidor = u.num_medidor
                 left join tb_device_data ddd
                 on ddd.device_address = d.device_address
                 where d.ativo = 'S'
                 and i.fl_mais_recente = 'S'
                 and ddd.fl_mais_recente = 'S'
                 and d.tipo_device in (ATECH_SGR.C_TERMINAL,ATECH_SGR.C_REPETIDOR)
                group by i.erros;
                    
END LISTAR_PACOTES_COM_ERROS;

PROCEDURE LISTAR_LATENCIA (IO_CURSOR IN OUT T_CURSOR) AS 

BEGIN   

           OPEN IO_CURSOR FOR
               SELECT i.latencia as indicador , count(*) as total from tb_indicadores i  
                 inner join tb_device d
                 on i.device_address = d.device_address
                 inner join tb_device_configuration c
                 on d.device_address = c.device_address 
                 left join tb_unidade_consumidora u 
                 on d.num_medidor = u.num_medidor
                 left join tb_device_data ddd
                 on ddd.device_address = d.device_address
                 where d.ativo = 'S'
                 and i.fl_mais_recente = 'S'
                 and ddd.fl_mais_recente = 'S'
                 and d.tipo_device in (ATECH_SGR.C_TERMINAL,ATECH_SGR.C_REPETIDOR)
                group by i.latencia;
                    
END LISTAR_LATENCIA;


END ATECH_SGR_DASHBOARD;
/


create or replace package body ATECH_SGR_DEVICE is

procedure listar_devices (io_cursor in out t_cursor,
                         p_tp_device varchar2,
                         p_falha varchar2,
                         p_sinal varchar2,
                         p_throughput varchar2,
                         p_latencia varchar2,
                         p_rank varchar2,
                         p_rotas varchar2,
                         p_vizinhos varchar2,
                         p_municipio varchar2,
                         p_bairro varchar2,
                         p_ipconcentrador varchar2,
                         p_gruposub varchar2,
                         p_filtrorapido varchar2
                         ) as 
 v_num_uc NUMBER;
 begin   
        begin
          v_num_uc := TO_NUMBER(p_filtrorapido);
        exception
          when others then
          v_num_uc := 0;
        end;
   
         open io_cursor for   

             select d.device_address,c.num_medidor,u.num_uc,c.logradouro,d.tipo_device, ddd.dt_device_data as maxdata, d.device_address, ddd.device_parent_address, c.latitude, c.longitude, tdd.falha, ((tdd.vl_sinal/2)-130) as vl_sinal, tdd.vl_rotas as rotas, tdd.vl_erros as erros, tdd.sinal
             from tb_device d
             inner join tb_indicadores tdd
             on tdd.device_address = d.device_address
             and tdd.fl_mais_recente = ATECH_SGR.C_FL_ATIVO
             inner join tb_device_configuration c
             on d.device_address = c.device_address 
             left join tb_unidade_consumidora u 
             on d.num_medidor = u.num_medidor
             left join tb_device_data ddd
             on ddd.device_address = d.device_address
             and ddd.fl_mais_recente = ATECH_SGR.C_FL_ATIVO
             left join tb_concentrador_config cc on
             d.device_address = cc.device_address
              where d.ativo = ATECH_SGR.C_FL_ATIVO
              and tdd.dt_calculo > (sysdate-1)
              and (p_tp_device is null or d.tipo_device in (SELECT extractvalue(column_value, '/indicador/id') "user" FROM TABLE(XMLSequence(XMLTYPE(p_tp_device).extract('/indicadores/indicador'))) t))
              and (p_falha is null or tdd.falha in (SELECT extractvalue(column_value, '/indicador/id') "user" FROM TABLE(XMLSequence(XMLTYPE(p_falha).extract('/indicadores/indicador'))) t))
              and (p_sinal is null or tdd.sinal in (SELECT extractvalue(column_value, '/indicador/id') "user" FROM TABLE(XMLSequence(XMLTYPE(p_sinal).extract('/indicadores/indicador'))) t))
              and (p_throughput is null or tdd.throughput in (SELECT extractvalue(column_value, '/indicador/id') "user" FROM TABLE(XMLSequence(XMLTYPE(p_throughput).extract('/indicadores/indicador'))) t))
              and (p_latencia is null or tdd.latencia in (SELECT extractvalue(column_value, '/indicador/id') "user" FROM TABLE(XMLSequence(XMLTYPE(p_latencia).extract('/indicadores/indicador'))) t))
              and (p_rank is null or tdd.rank in (SELECT extractvalue(column_value, '/indicador/id') "user" FROM TABLE(XMLSequence(XMLTYPE(p_rank).extract('/indicadores/indicador'))) t))
              and (p_rotas is null or tdd.rotas in (SELECT extractvalue(column_value, '/indicador/id') "user" FROM TABLE(XMLSequence(XMLTYPE(p_rotas).extract('/indicadores/indicador'))) t))
              and (p_vizinhos is null or tdd.vizinhos in (SELECT extractvalue(column_value, '/indicador/id') "user" FROM TABLE(XMLSequence(XMLTYPE(p_vizinhos).extract('/indicadores/indicador'))) t))
              and (p_municipio is null or c.municipio = p_municipio)
              and (p_bairro is null or c.bairro like  '%' || p_bairro || '%')
              and (p_ipconcentrador is null or (ddd.ip_concentrador = p_ipconcentrador or cc.ip = p_ipconcentrador))
              and (p_gruposub is null or u.grupo_subgrupo_fat = p_gruposub)
              and (p_filtrorapido is null or (d.device_address = p_filtrorapido or u.num_medidor = p_filtrorapido or u.num_uc = v_num_uc));

end listar_devices;


procedure listar_devices_pendentes (io_cursor in out t_cursor, P_TIPO_DEVICE tb_device.tipo_device%type, p_device_adrress varchar2) as 

   begin
     open io_cursor for   
            select     d.num_medidor ,d.device_address, dd.dt_device_data as maxdata, d.tipo_device
            from       tb_device d
            inner join tb_device_data dd
            on         d.device_address = dd.device_address
            and        dd.fl_mais_recente = ATECH_SGR.C_FL_ATIVO
            left join  tb_device_configuration dc
            on         d.device_address = dc.device_address
            where      d.tipo_device in (ATECH_SGR.C_TERMINAL, ATECH_SGR.C_REPETIDOR)
            and        d.ativo = ATECH_SGR.C_FL_ATIVO
            and        dc.device_address is null
            and        d.tipo_device = nvl(p_tipo_device, d.tipo_device)
            and        (p_device_adrress is null or d.device_address = p_device_adrress);

end listar_devices_pendentes; 


procedure listar_detalhes_device (io_cursor in out t_cursor, p_device_address tb_device.device_address%type) as
 begin   
   open io_cursor for   
       select d.device_address
            , d.tipo_device
            , cc.ip
            , cc.porta
            , u.num_uc
            , u.nome_completo
            , u.cod_situ
            , u.dt_situ
            , u.dt_ligacao
            , u.grupo_subgrupo_fat
            , u.cod_fase
            , u.classe_principal
            , u.cond_fat
            , u.etapa
            , u.livro
            , u.tipo_med
            , u.num_medidor
            , u.num_elementos
            , u.num_fios
            , u.imin
            , u.ano_fab_med
            , u.subtipo_equip
            , u.alimentador
            , u.transformador
            , u.cod_clas_cons
            , u.classe_consumo
            , u.num_uc
            , c.num_medidor
            , c.latitude
            , c.longitude
            , c.bairro
            , c.distrital
            , c.municipio
            , c.localidade
            , c.cod_local
            , (c.tipo_logr || ' ' || c.logradouro) as logradouro
            , c.num_imovel
            , c.complemento
            , c.device_address
            , ddd.device_parent_address
            , ((ddd.sinal_min / 2) - 130) as sinal_min
            , ((ddd.sinal_max / 2) - 130) as sinal_max
            , ((ddd.sinal_med / 2) - 130) as sinal_med
            , ddd.bytes_env
            , ddd.pack_env
            , ddd.pack_no_ack
            , ddd.ip_concentrador
            , tdd.falha
            , tdd.sinal
            , tdd.rotas
            , tdd.erros
            , tdd.latencia
            , tdd.throughput
            , tdd.vizinhos
            , tdd.rank
            , tdd.vl_throughput
            , tdd.vl_erros
            , tdd.vl_rank
            , tdd.vl_vizinhos
            , ((tdd.vl_sinal / 2 )-130) as vl_sinal
            , tdd.vl_latencia
            , tdd.vl_rotas
            , tdd.dt_keep_alive as keepalive
       from       tb_device d
       inner join tb_indicadores tdd
       on         d.device_address = tdd.device_address
       and        tdd.fl_mais_recente = ATECH_SGR.C_FL_ATIVO
       inner join tb_device_configuration c
       on         d.device_address = c.device_address
       left join  tb_unidade_consumidora u
       on         d.num_medidor = u.num_medidor
       left join  tb_device_data ddd
       on         d.device_address = ddd.device_address
       and        ddd.fl_mais_recente = ATECH_SGR.C_FL_ATIVO
       left join  tb_concentrador_config cc
       on         d.device_address = cc.device_address
       where      d.ativo = ATECH_SGR.C_FL_ATIVO
       and        d.device_address = p_device_address;              
end listar_detalhes_device; 


procedure listar_devices_calc (io_cursor in out t_cursor) as 
 begin   
         open io_cursor for   
              select device_id as DeviveId, g.tb as TempoFalha,  
                     num_medidor as NumMedidor, 
                     device_address as DeviceAdrreess, 
                     firmware_version as FirmwareVersion, 
                     d.id_grupo as IdGrupo
                     from tb_device d
                     inner join TB_GRUPO g
                     on d.id_grupo = g.id_grupo
                     where d.ativo = ATECH_SGR.C_FL_ATIVO
                     and d.tipo_device in (ATECH_SGR.C_TERMINAL, ATECH_SGR.C_REPETIDOR);
                     
end listar_devices_calc; 

procedure listar_devices_concentradores (io_cursor in out t_cursor) as 
 begin   
         open io_cursor for   
              select device_id as DeviveId, g.tb as TempoFalha,  
                     num_medidor as NumMedidor, 
                     device_address as DeviceAdrreess, 
                     firmware_version as FirmwareVersion, 
                     d.id_grupo as IdGrupo
                     from tb_device d
                     inner join TB_GRUPO g
                     on d.id_grupo = g.id_grupo
                     where d.ativo = ATECH_SGR.C_FL_ATIVO
                     and d.tipo_device in (ATECH_SGR.C_CONCENTRADOR);
                     
end listar_devices_concentradores; 

procedure listar_regras (io_cursor in out t_cursor) as 
 
 begin   
         open io_cursor for   
              select id_regra as IdRegra, id_grupo as IdGrupo, 
                     tipo_indicador as TipoIndicador, 
                     lim_inferior as LimiteInferior, 
                     lim_superior as LimiteSuperior, 
                     nivel_alerta as Nivel
                     from TB_REGRA;

end listar_regras; 

procedure listar_device_dataCalc (io_cursor in out t_cursor) as 
 
 begin   
         open io_cursor for   
                   select 
                    dm.device_address , dm.maxdata dt_device_data , dm.tipo, 
                    dados.num_rank, dados.sinal_min ,dados.sinal_med,
                    dados.sinal_max, dados.bytes_env, dados.pack_env,
                    dados.pack_no_ack,dados.latencia,dados.num_rotas,dados.num_vizinhos
                     from
                    (select dd.device_address, max(dd.dt_device_data) as maxdata , 'tb1' as tipo 
                    from tb_device_data dd
                    inner join tb_device d 
                    on dd.device_address = d.device_address
                    inner join tb_grupo g
                    on d.id_grupo = g.id_grupo 
                    group by dd.device_address) dm
                    inner join tb_device_data dados
                    on dm.device_address = dados.device_address 
                    and dm.maxdata = dados.dt_device_data 
                    UNION 
                    select 
                    distinct(dm.device_address), dm.maxdata dt_device_data , dm.tipo , 
                    dados.num_rank, dados.sinal_min ,dados.sinal_med,
                    dados.sinal_max, dados.bytes_env, dados.pack_env,
                    dados.pack_no_ack,dados.latencia,dados.num_rotas,dados.num_vizinhos
                    from
                    (SELECT dd.device_address,  min(dd.dt_device_data) as maxdata , 'tb2' as tipo
                    FROM tb_device_data dd
                    inner join tb_device d 
                    on dd.device_address = d.device_address
                    inner join tb_grupo g
                    on d.id_grupo = g.id_grupo 
                    WHERE dd.dt_device_data >= (CAST (SYSTIMESTAMP AT TIME ZONE 'UTC' AS DATE) - (g.tb)/24/60)
                    group by dd.device_address) dm
                    inner join tb_device_data dados
                    on dm.device_address = dados.device_address
                    and dm.maxdata = dados.dt_device_data;
                   
end listar_device_dataCalc;


procedure device_inserir_indicadores (xml_data clob)  as

v_count NUMBER(38) := 1;
v_xml XMLType;
v_count_commit NUMBER(10) := 0;

begin        
    v_xml:= XMLType(xml_data);      
    
    WHILE v_xml.existsNode('//indicador[' || v_count || ']') = 1 LOOP

    INSERT INTO TB_INDICADORES 
      (device_address, 
       falha, 
       erros, 
       throughput, 
       rank, 
       vizinhos, 
       sinal, 
       latencia, 
       rotas, 
       vl_erros,
       vl_throughput,
       vl_rank,
       vl_vizinhos,
       vl_sinal,
       vl_latencia,
       vl_rotas,
       dt_calculo,
       dt_keep_alive
       )
    VALUES
    (
       v_xml.extract('//indicador[' || v_count || ']/deviceaddress/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/falhas/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/erros/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/throughput/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/rank/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/vizinhos/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/sinal/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/latencia/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/rotas/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/vl_erros/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/vl_throughput/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/vl_rank/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/vl_vizinhos/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/vl_sinal/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/vl_latencia/text()').getStringVal(),
       v_xml.extract('//indicador[' || v_count || ']/vl_rotas/text()').getStringVal(),
       to_date(v_xml.extract('//indicador[' || v_count || ']/dt_calc/text()').getStringVal(), 'dd/mm/yyyy hh24:mi:ss'),
       to_date(v_xml.extract('//indicador[' || v_count || ']/dt_keep_alive/text()').getStringVal(), 'dd/mm/yyyy hh24:mi:ss')    
    ) ;

    v_count := v_count + 1;

    -- Executa COMMIT em lote
    v_count_commit := v_count_commit + 1;
    IF v_count_commit = ATECH_SGR.C_CONTADOR_COMMIT THEN
        COMMIT;
        v_count_commit := 0;
    END IF;

  END LOOP;      

  -- COMMIT final.
  COMMIT;        
 
end device_inserir_indicadores;
        
 
procedure listar_ultimo_calculado (io_cursor in out t_cursor) as

begin
    open io_cursor for
        select dados.device_address
             , dados.dt_calculo as dataUltimoCalculo
             , dados.throughput
             , dados.falha
             , dados.erros
             , dados.rank
             , dados.vizinhos
             , dados.sinal
             , dados.latencia
             , dados.rotas
             , dados.vl_throughput
             , dados.vl_erros
             , dados.vl_rank
             , dados.vl_vizinhos
             , dados.vl_sinal
             , dados.vl_latencia
             , dados.vl_rotas
             , dados.dt_keep_alive as dataKeepAlive
        from  tb_indicadores dados
        where dados.fl_mais_recente = ATECH_SGR.C_FL_ATIVO;

/*              select 
              i.device_address , i.maxdata dataUltimoCalculo, 
              dados.throughput , dados.falha, dados.erros ,dados.rank,
              dados.vizinhos,dados.sinal,dados.latencia,dados.rotas,
              dados.vl_throughput,dados.vl_erros,dados.vl_rank,
              dados.vl_vizinhos,dados.vl_sinal,dados.vl_latencia,
              dados.vl_rotas,dados.dt_keep_alive dataKeepAlive
              from
              (select device_address, max(dt_calculo) as maxdata  
              from tb_indicadores  
              group by device_address) i
              inner join tb_indicadores dados
              on i.device_address = dados.device_address 
              and i.maxdata = dados.dt_calculo;   
*/

end listar_ultimo_calculado;

procedure listar_rotas (io_cursor in out t_cursor,p_device_address tb_device.device_address%type) as
begin
    open io_cursor for

        select     de.device_address
                 , r.device_parent_address
                 , de.tipo_device
                 , c.latitude
                 , c.longitude
        from       tb_device de
        inner join tb_route r
        on         de.device_address = r.device_address
        and        r.fl_active = ATECH_SGR.C_FL_ATIVO
        left join  tb_device_configuration c
        on         de.device_address = c.device_address
        where      de.device_address = p_device_address;


end listar_rotas;

procedure listar_grupo_subgrupo (io_cursor in out t_cursor) as
  
begin
    open io_cursor for
         select tipo_subgrupo, 
                descr_subgrupo from tb_subgrupo_fat;
end listar_grupo_subgrupo;

procedure listar_municipio (io_cursor in out t_cursor) as
  
begin
    open io_cursor for
         select distinct(municipio) from tb_device_configuration; 
end listar_municipio;

procedure listar_bairro (io_cursor in out t_cursor) as
  
begin
    open io_cursor for
         select distinct(bairro) from tb_device_configuration; 
end listar_bairro;

procedure busca_device_pendente (io_cursor in out t_cursor, p_device_adrees varchar2) as
  
begin
    open io_cursor for
            select     d.num_medidor ,d.device_address, dd.dt_device_data as maxdata, d.tipo_device
            from       tb_device d
            inner join tb_device_data dd
            on         d.device_address = dd.device_address
            and        dd.fl_mais_recente = ATECH_SGR.C_FL_ATIVO
            left join  tb_device_configuration dc
            on         d.device_address = dc.device_address
            where      d.tipo_device in (ATECH_SGR.C_TERMINAL, ATECH_SGR.C_REPETIDOR)
            and        dc.device_address is null
            and        d.device_address = p_device_adrees;

/*         select de.num_medidor ,d.device_address, d.maxdata, de.tipo_device from
                (select device_address, max(dt_device_data) as maxdata 
                from tb_device_data group by device_address) d
                inner join tb_device de 
                on de.device_address = d.device_address
                left join tb_device_configuration c 
                on c.device_address = d.device_address
                where c.device_address is null
                and de.tipo_device in (ATECH_SGR.C_TERMINAL, ATECH_SGR.C_REPETIDOR)
                and de.device_address = p_device_adrees;
*/
end busca_device_pendente;


end ATECH_SGR_DEVICE;
/


create or replace package body ATECH_SGR_DEVICE_CONFIGURATION is

  procedure insert_device_configuration(p_latitude       decimal,
                                         p_longitude      decimal,
                                         p_bairro         nvarchar2,
                                         p_distrital      nvarchar2,
                                         p_municipio      nvarchar2,
                                         p_tipo_logr      nvarchar2,
                                         p_logradouro     nvarchar2,
                                         p_num_imovel     nvarchar2,
                                         p_complemento    nvarchar2,
                                         p_device_address nvarchar2,
                                         p_tipo_device    nvarchar2,
                                         p_ip             nvarchar2,
                                         p_porta          nvarchar2,
                                         p_qtdMax         number,
                                         p_ativo          tb_device.ativo%type,
                                         p_Api_get        nvarchar2,
                                         p_Api_delete     nvarchar2) is
  begin  
    insert into tb_device
      (device_address, tipo_device, ativo)
    values
      (p_device_address, p_tipo_device, p_ativo);
    
    insert into tb_device_configuration
      (latitude,
       longitude,
       bairro,
       distrital,
       municipio,
       tipo_logr,
       logradouro,
       num_imovel,
       complemento,
       device_address,
       tipo_device)
    values
      (p_latitude,
       p_longitude,
       p_bairro,
       p_distrital,
       p_municipio,
       p_tipo_logr,
       p_logradouro,
       p_num_imovel,
       p_complemento,
       p_device_address,
       p_tipo_device);
  
    if (p_tipo_device = ATECH_SGR.C_CONCENTRADOR) then
      insert into tb_concentrador_config
        (ip, porta, qtdmax, device_address, url_get, url_delete)
      values
        (p_ip,
         p_porta,
         p_qtdmax,
         p_device_address,
         p_Api_get,
         p_Api_delete);
    end if;  
    commit;
    
  end insert_device_configuration;

  procedure update_device_configuration(p_latitude       decimal,
                                        p_longitude      decimal,
                                        p_bairro         nvarchar2,
                                        p_distrital      nvarchar2,
                                        p_municipio      nvarchar2,
                                        p_tipo_logr      nvarchar2,
                                        p_logradouro     nvarchar2,
                                        p_num_imovel     nvarchar2,
                                        p_complemento    nvarchar2,
                                        p_device_address nvarchar2,
                                        p_tipo_device    nvarchar2,
                                        p_ip             nvarchar2,
                                        p_porta          nvarchar2,
                                        p_qtdMax         number,
                                        p_ativo          tb_device.ativo%type,
                                        p_Api_get        nvarchar2,
                                        p_Api_delete     nvarchar2) is
                                        
    l_count number;                                    
                                        
  begin
    
  
  
    update tb_device
       set ativo = p_ativo
     where device_address = p_device_address;
  
  select count (*) into l_count from tb_device_configuration dc where dc.device_address = p_device_address;
  
  if l_count > 0 then 
    update tb_device_configuration
       set latitude    = p_latitude,
           longitude   = p_longitude,
           bairro      = p_bairro,
           distrital   = p_distrital,
           municipio   = p_municipio,
           tipo_logr   = p_tipo_logr,
           logradouro  = p_logradouro,
           num_imovel  = p_num_imovel,
           complemento = p_complemento
     where device_address = p_device_address;
else
    insert into tb_device_configuration
      (latitude,
       longitude,
       bairro,
       distrital,
       municipio,
       tipo_logr,
       logradouro,
       num_imovel,
       complemento,
       device_address,
       tipo_device)
    values
      (p_latitude,
       p_longitude,
       p_bairro,
       p_distrital,
       p_municipio,
       p_tipo_logr,
       p_logradouro,
       p_num_imovel,
       p_complemento,
       p_device_address,
       p_tipo_device);
 end if;
       
    if (p_tipo_device = ATECH_SGR.C_CONCENTRADOR) then
      update tb_concentrador_config
         set ip         = p_ip,
             porta      = p_porta,
             qtdmax     = p_qtdmax,            
             url_get    = p_Api_get,
             url_delete = p_Api_delete
       where device_address = p_device_address;
    end if;
  
    commit;
  end update_device_configuration;
  
  
procedure select_device_configuration (io_cursor in out t_cursor, p_device_address tb_device.device_address%type) as
 begin   
   open io_cursor for   
       select 
         d.device_address, d.tipo_device, d.ativo,
         dc.latitude, dc.longitude, dc.bairro, dc.distrital, dc.municipio, dc.tipo_logr, dc.logradouro,
         dc.num_imovel, dc.complemento,
         cc.ip, cc.porta, cc.qtdmax, cc.url_get,cc.url_delete  
         from tb_device d
         left join tb_concentrador_config cc
         on d.device_address = cc.device_address
         left join tb_device_configuration dc
         on d.device_address = dc.device_address
         where 
         d.device_address = p_device_address
         and (d.tipo_device = 'R' or  d.tipo_device = 'C') ;
end select_device_configuration; 




  PROCEDURE listar_devices_configuration(IO_CURSOR IN OUT T_CURSOR) AS
  BEGIN
    OPEN IO_CURSOR FOR
      SELECT num_medidor,
             latitude,
             longitude,
             bairro,
             distrital,
             municipio,
             localidade,
             cod_local,
             tipo_logr,
             logradouro,
             num_imovel,
             complemento,
             device_address,
             tipo_device
        FROM tb_device_configuration
       WHERE tipo_device in (ATECH_SGR.C_REPETIDOR, ATECH_SGR.C_CONCENTRADOR);
  END listar_devices_configuration;
   
  procedure excluir_device_configuration(p_device_address nvarchar2) is
  begin
    delete from tb_device_configuration
     where device_address = p_device_address;
    commit;
  end excluir_device_configuration;

end ATECH_SGR_DEVICE_CONFIGURATION;
/


create or replace package body ATECH_SGR_LOG is

procedure inserir_log(p_cod_objeto nvarchar2, p_cod_tipo nvarchar2, p_dt_log date, p_cod_usuario nvarchar2, p_descricao nvarchar2, p_operacao nvarchar2) is
  PRAGMA AUTONOMOUS_TRANSACTION;
begin    
    
   insert into tb_log
   (     
      id,    
      cod_objeto, 
      cod_tipo, 
      dt_log, 
      cod_usuario, 
      descricao, 
      operacao    
   )
   values
   (    
      TB_LOG_SEQ.NEXTVAL, 
      p_cod_objeto, 
      p_cod_tipo, 
      CAST(SYSTIMESTAMP AT TIME ZONE 'UTC' AS DATE), -- Registra sempre da data do log ao inves da fornecida.
      p_cod_usuario, 
      p_descricao, 
      p_operacao  
   );
   commit;
end inserir_log;   
end ATECH_SGR_LOG;
/


CREATE OR REPLACE PACKAGE BODY ATECH_SGR_MENU IS

  -- Author  : RGRACCIA
  -- Created : 21/02/2018 15:44:57
  -- Purpose : 
  
PROCEDURE LISTAR_MENU (IO_CURSOR IN OUT T_CURSOR, P_COD_PERFIL NUMBER) AS 

BEGIN   
          OPEN IO_CURSOR FOR
             SELECT M.COD_MENU AS CodigoMenu, NOME AS Nome ,  CAMINHO_PAGINA AS CaminhoPagina, COD_MENU_PAI AS CodigoMenuPai, ICONE_PAGINA AS IconePagina, EXIBIR_MENU as Exibir_menu  
             FROM TB_MENU M
              INNER JOIN TB_PERFIL_MENU PM 
               ON M.COD_MENU = PM.COD_MENU
                WHERE PM.COD_PERFIL = P_COD_PERFIL             
                ORDER BY ORDEM_MENU ASC; 
   
END LISTAR_MENU; 

END ATECH_SGR_MENU;
/


create or replace package body ATECH_SGR_PERMISSAO is

PROCEDURE LISTA_PERMISSAO_PAGINA (IO_CURSOR IN OUT T_CURSOR,P_CAMINHO_PAGINA TB_MENU.CAMINHO_PAGINA%type) AS 

BEGIN   
          OPEN IO_CURSOR FOR
              SELECT 
              MO.cod_operacao as CodigoOperacao, 
              MO.permitido as Permitido , 
              O.nome_operacao as NomeOperacao,
              O.descricao_operacao as DescricaoOperacao
              FROM TB_MENU_OPERACAO MO
              INNER JOIN TB_OPERACAO O
              ON MO.COD_OPERACAO = O.COD_OPERACAO
              INNER JOIN TB_MENU M
              ON M.COD_MENU = MO.COD_MENU
              WHERE M.CAMINHO_PAGINA = P_CAMINHO_PAGINA;
                
 END LISTA_PERMISSAO_PAGINA;

end ATECH_SGR_PERMISSAO;
/

create or replace package body atech_sgr_route is

 procedure inserir_upd_route(p_device_address tb_route.device_address%type,p_device_parent_address tb_route.device_parent_address%type,p_fl_active tb_route.fl_active%type,p_dt_route nvarchar2) is
 
  v_num number;
  v_pai tb_route.device_parent_address%type;
 
 begin

  -- Verifica se existe alguma rota ativa para esse device

 select count(*)
   into v_num
   from tb_route t
  where t.device_address = p_device_address  
    and t.fl_active = ATECH_SGR.C_FL_ATIVO;
  
 if v_num > 0 then
         -- Tem uma rota ativa. Então verifica quem é o pai dela.
         -- PS: Pela lógica não pode ter mais de uma rota ativa.
         -- Se tiver, essa procedure vai dar erro aqui.
         -- Se, por algum infortúnio do destino, começar a dar erro,
         -- faça o tratamento aqui.

         select t.device_parent_address
           into v_pai
           from tb_route t
          where t.device_address = p_device_address  
            and t.fl_active = ATECH_SGR.C_FL_ATIVO; 

          -- Verifica se mudou o pai. Só atualiza a tabela de rotas se ocorreu mudança de rota.
          if v_pai <> p_device_parent_address then
               -- Elimina todas as possíveis rotas ativas para esse device.
               update tb_route t
                  set t.fl_active = ATECH_SGR.C_FL_INATIVO
                where t.device_address = p_device_address;

               -- Insere a nova rota ativa.
               insert into tb_route
               (
                device_address, 
                device_parent_address, 
                fl_active, 
                dt_route   
               )
               values
               (
                p_device_address,
                p_device_parent_address,
                ATECH_SGR.C_FL_ATIVO,
                cast(to_timestamp(p_dt_route, 'yyyy-mm-dd"T"hh24:mi:ss.ff3"Z"') as date)
               );
          end if;
 else
          -- Aqui não achou nenhuma rota ativa. Então insere uma nova.
          insert into tb_route
          (
           device_address, 
           device_parent_address, 
           fl_active, 
           dt_route   
          )
          values
          (
           p_device_address,
           p_device_parent_address,
           ATECH_SGR.C_FL_ATIVO,
           cast(to_timestamp(p_dt_route, 'yyyy-mm-dd"T"hh24:mi:ss.ff3"Z"') as date)
          );
 end if;
 commit;
end inserir_upd_route;   
 
procedure inserir_upd_device(p_device_id tb_device.device_id%type,p_num_medidor tb_device.num_medidor%type,p_device_address tb_device.device_address%type,p_firmware_version tb_device.firmware_version%type) is

v_num number;
v_tipo_device tb_device.tipo_device%type;

begin

select count(*) into v_num from tb_device 
where device_address = p_device_address;

if v_num = 0
  then
       -- Se não tem número de medidor, então é repetidor. Senão é terminal.
       if p_num_medidor is null then
         v_tipo_device := ATECH_SGR.C_REPETIDOR;
       else
         v_tipo_device := ATECH_SGR.C_TERMINAL;
       end if;
       
       insert into tb_device
       (
          device_id, 
          num_medidor, 
          device_address, 
          firmware_version,
          id_grupo,
          ativo,
          tipo_device
       )
       values
       (
          p_device_id, 
          p_num_medidor, 
          p_device_address, 
          p_firmware_version,
          ATECH_SGR.C_GRUPO_DEFAULT,
          ATECH_SGR.C_FL_ATIVO,
          v_tipo_device
       );
       
       if  p_num_medidor is not null
         then
           /* Atualização relação device com numero de medidor */ 
           update tb_device_configuration 
           set device_address = p_device_address
           where num_medidor = p_num_medidor; 
         end if;
  else
      update tb_device set
      device_id = p_device_id, 
      num_medidor = p_num_medidor,
      firmware_version = p_firmware_version 
      where device_address = p_device_address; 
     
     if p_num_medidor is not null
       then 
          /* Retira o device address de qualquer medidor que esteja associado a este device address */ 
         update tb_device_configuration 
         set device_address = null
         where device_address = p_device_address;
           
         /* Atualização de device address relação a número medidor*/ 
         update tb_device_configuration 
         set device_address = p_device_address
         where num_medidor = p_num_medidor;
     end if;
     
 end if;
 commit;   
end inserir_upd_device; 

 
procedure inserir_devicedata(p_num_id number, p_device_address nvarchar2, p_device_parent_address nvarchar2, p_sinal_min number, p_sinal_max number, p_sinal_med number, p_bytes_env number, p_pack_env number, p_pack_no_ack number, p_num_rank number, p_latencia number, p_num_rotas number, p_num_vizinhos number, p_dt_device_data nvarchar2,p_ip_concentrador nvarchar2 ) is

v_num number;

begin  
  
select count(*) into v_num from tb_device_data d
where d.device_address = p_device_address and d.dt_device_data = cast(to_timestamp(p_dt_device_data, 'yyyy-mm-dd"T"hh24:mi:ss.ff3"Z"') as date);

if v_num = 0
  then     
   insert into tb_device_data
   (
      num_id, 
      device_address, 
      device_parent_address, 
      sinal_min, 
      sinal_max, 
      sinal_med, 
      bytes_env, 
      pack_env, 
      pack_no_ack, 
      num_rank, 
      latencia, 
      num_rotas, 
      num_vizinhos, 
      dt_device_data,
      ip_concentrador
   )
   values
   (
      p_num_id, 
      p_device_address, 
      p_device_parent_address, 
      p_sinal_min, 
      p_sinal_max, 
      p_sinal_med, 
      p_bytes_env, 
      p_pack_env, 
      p_pack_no_ack, 
      p_num_rank, 
      p_latencia, 
      p_num_rotas, 
      p_num_vizinhos, 
      cast(to_timestamp(p_dt_device_data, 'yyyy-mm-dd"T"hh24:mi:ss.ff3"Z"') as date),
      p_ip_concentrador
   );
   end if;
   commit;
   
 end inserir_devicedata;                        

end atech_sgr_route;
/


CREATE OR REPLACE PACKAGE BODY ATECH_SGR_USUARIO IS

PROCEDURE AUTENTICAR_ADMINISTRADOR (IO_CURSOR IN OUT T_CURSOR, P_COD_USUARIO VARCHAR2,P_SENHA VARCHAR2,P_TIPO CHAR) AS 

BEGIN   
  
           IF (P_TIPO = '1')
             THEN 
                OPEN IO_CURSOR FOR
                   SELECT COD_USUARIO, ATIVO FROM TB_USUARIO U
                   WHERE COD_USUARIO = P_COD_USUARIO
                   AND SENHA = P_SENHA
                   AND U.ATIVO = ATECH_SGR.C_FL_ATIVO;
             ELSE

             OPEN IO_CURSOR FOR
                 SELECT U.COD_USUARIO, U.ATIVO FROM TB_USUARIO U
                 INNER JOIN TB_USUARIO_PERFIL UP
                 ON UP.COD_USUARIO = U.COD_USUARIO
                 WHERE U.COD_USUARIO = P_COD_USUARIO
                 AND U.ATIVO = ATECH_SGR.C_FL_ATIVO; 
              END IF;   
   
 END AUTENTICAR_ADMINISTRADOR;
 
PROCEDURE USUARIO_EXISTE (IO_CURSOR IN OUT T_CURSOR, P_COD_USUARIO VARCHAR2) AS 

BEGIN   
          OPEN IO_CURSOR FOR
             SELECT COD_USUARIO AS NOME, COD_PERFIL AS DESCRICAOPERFIL FROM TB_USUARIO_PERFIL UP
             WHERE UP.COD_USUARIO = P_COD_USUARIO;
   
 END USUARIO_EXISTE; 
 
PROCEDURE LISTAR_USUARIOS_COM_PERFIL (IO_CURSOR IN OUT T_CURSOR) AS 

BEGIN   
          OPEN IO_CURSOR FOR
              SELECT U.COD_USUARIO AS Nome, P.NOME AS DescricaoPerfil ,UP.DATA AS DataCadastro, UP.ID as Id_Usuario_Perfil ,U.ATIVO FROM TB_USUARIO U
              INNER JOIN TB_USUARIO_PERFIL UP
              ON U.COD_USUARIO = UP.COD_USUARIO
              INNER JOIN TB_PERFIL P
              ON UP.COD_PERFIL = P.COD_PERFIL;
 END LISTAR_USUARIOS_COM_PERFIL;


PROCEDURE INSERIR_PERFIL_USUARIO (P_COD_USUARIO TB_USUARIO.COD_USUARIO%type,P_COD_PERFIL TB_USUARIO_PERFIL.COD_PERFIL%type) AS 
    BEGIN   
             DELETE FROM TB_USUARIO_PERFIL WHERE COD_USUARIO = P_COD_USUARIO;
             DELETE FROM TB_USUARIO WHERE COD_USUARIO = P_COD_USUARIO;
             
             INSERT INTO TB_USUARIO
             (COD_USUARIO, ATIVO)
             VALUES(P_COD_USUARIO,ATECH_SGR.C_FL_ATIVO);
             
             INSERT INTO TB_USUARIO_PERFIL
             (COD_USUARIO,COD_PERFIL,DATA,ID)
             VALUES(P_COD_USUARIO,P_COD_PERFIL,(CAST (SYSTIMESTAMP AT TIME ZONE 'UTC' AS DATE)),TB_USUARIO_PERFIL_SEQ.NEXTVAL);
             COMMIT;
             
    END INSERIR_PERFIL_USUARIO;
    
PROCEDURE EXCLUIR_PERFIL_USUARIO (P_ID Number) AS 
    BEGIN   
      
             DELETE FROM TB_USUARIO_PERFIL UP WHERE UP.ID = P_ID;
             DELETE FROM TB_USUARIO U WHERE U.COD_USUARIO = (SELECT COD_USUARIO FROM TB_USUARIO_PERFIL UP WHERE UP.ID = P_ID);

             COMMIT;
                  
END EXCLUIR_PERFIL_USUARIO;


PROCEDURE UPDATE_USUARIO_ATIVO (P_ID Number,P_ATIVO CHAR) AS 
   
 BEGIN   
             UPDATE TB_USUARIO U  
             SET U.ATIVO = P_ATIVO 
             WHERE U.COD_USUARIO = (SELECT COD_USUARIO FROM TB_USUARIO_PERFIL UP WHERE UP.ID = P_ID);                              
                        
             COMMIT;  
                  
END UPDATE_USUARIO_ATIVO;

PROCEDURE LISTAR_PERFIL (IO_CURSOR IN OUT T_CURSOR) AS 

BEGIN   
          OPEN IO_CURSOR FOR
              SELECT COD_PERFIL as CodigoPerfil, NOME, DESCRICAO FROM TB_PERFIL;
            
 END LISTAR_PERFIL;
 
PROCEDURE BUSCA_PERFIL_USUARIO (IO_CURSOR IN OUT T_CURSOR,P_COD_USUARIO TB_USUARIO.COD_USUARIO%type) AS 

BEGIN   
          OPEN IO_CURSOR FOR
              SELECT P.COD_PERFIL AS CodigoPerfil, P.NOME, P.DESCRICAO FROM TB_PERFIL P
                     INNER JOIN TB_USUARIO_PERFIL UP 
                     ON P.COD_PERFIL = UP.COD_PERFIL
                     WHERE UP.COD_USUARIO = P_COD_USUARIO;  
 END BUSCA_PERFIL_USUARIO;


END ATECH_SGR_USUARIO;

/

create or replace trigger TG_TB_DEVICE_DATA_INSERT
  before insert
  on tb_device_data 
  for each row
    
begin

  IF :NEW.FL_MAIS_RECENTE = 'N' THEN

    -- Nesse caso foi explicitamente solicitado inserir um registro com 'N'.
    -- Então não faz nada e mantém o mais recente do jeito que está.
    NULL;
  ELSE

    -- Remove o flag de registro mais recente anterior.
    UPDATE TB_DEVICE_DATA T
    SET    T.FL_MAIS_RECENTE = 'N'
    WHERE  T.DEVICE_ADDRESS = :NEW.DEVICE_ADDRESS
    AND    T.FL_MAIS_RECENTE = 'S';
  
    -- Registra essa nova linha como sendo o registro mais recente.
    :NEW.FL_MAIS_RECENTE := 'S';
  END IF;

end TG_TB_DEVICE_DATA_INSERT;
/

create or replace trigger TG_TB_INDICADORES_INSERT
  before insert
  on tb_indicadores 
  for each row

begin
  -- Remove o flag de registro mais recente anterior.
  UPDATE TB_INDICADORES T
  SET    T.FL_MAIS_RECENTE = 'N'
  WHERE  T.DEVICE_ADDRESS = :NEW.DEVICE_ADDRESS
  AND    T.FL_MAIS_RECENTE = 'S';

  -- Registra essa nova linha como sendo o registro mais recente.
  :NEW.FL_MAIS_RECENTE := 'S';
  
end TG_TB_INDICADORES_INSERT;














