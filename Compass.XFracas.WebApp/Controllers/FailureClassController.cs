﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.XFracas.WebApp.Controllers
{
    public class FailureClassController : Controller
    {
        // GET: FailureClass
        public ActionResult Index()
        {
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (identity.IsAuthenticated)
            {
                Session.Add("UserWindows", identity.Name);
                return View();
            }
            else
            {
                return RedirectToAction("About");
            }
        }

         
    }
}