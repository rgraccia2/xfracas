﻿using Compass.XFracas.WebGerenciadorBL;
using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.XFracas.WebApp.Controllers
{
    public class TriggerRulesController : Controller
    {
        public ActionResult Index()
        {
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (identity.IsAuthenticated)
            {
                List<Trigger> lstTrigger = new List<Trigger>();
                //Trigger t = new Trigger();
                //t.hierarquia = "TESTE";
                //t.ativo = true;
                //t.expressao = "expressão";
                //t.Id = "25";
                //t.recorrencia_dias = 36;
                //lstTrigger.Add(t);
                ViewBag.lstTrigger = lstTrigger;
                Session.Add("UserWindows", identity.Name);
                return View();
            }
            else
            {
                return RedirectToAction("About");
            }
        }




        public JsonResult ListarTrigger()
        {
            RulesBL rulesBL = new RulesBL();

            var r = rulesBL.GetRules();


            List<Trigger> lstTrigger = new List<Trigger>();

            foreach (var x in r)
            {
                Trigger t = new Trigger();
                t.hierarquia = x.hierarchy;
                t.ativo = x.enabled;
                t.expressao = x.expression;
                t.Id = "25";
                t.recorrencia_dias = int.Parse(x.recidivism.ToString());
                lstTrigger.Add(t);
            }
            return Json(lstTrigger, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GravarRegras()
        {

            return Json("OK", JsonRequestBehavior.AllowGet);
        }
    }
}