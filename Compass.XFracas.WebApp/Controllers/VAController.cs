﻿using Compass.XFracas.WebApp.Resources;
using Compass.XFracas.WebGerenciadorBL;
using Compass.XFracas.WebGerenciadorDAL;
using Compass.XFracas.WebGerenciadorInterface.Model.VisualAnalysis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.XFracas.WebApp.Controllers
{

    /// <summary>
    /// Visual Analysis
    /// </summary>
    public class VAController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("VisualAnalysis", new { lang = "pt", analysisType = "PROBLEM", analysisId = "AF-V-7404", user = "VALENET\\C0632569", problem = "TESTE XFRACAS 1" });//"?lang=pt&analysisType=PROBLEM&analysisId=AF-V-7404&user=VALENET%5cC0632569&problem=");

        }

        public ActionResult SaveFileStream( )
        {

            string imagem = Request["imagem"].Replace("data:image/png;base64,",""); 
            string analysisId = Request["analysisId"];
            string sufixo = Request["sufixo"];

            byte[] bytes = Convert.FromBase64String(imagem);


            string path = System.Configuration.ConfigurationManager.AppSettings["folderToSaveReportImages"];
 

            //var bytes = Convert.FromBase64String(resizeImageContent);
            using (var imageFile = new FileStream(path + analysisId +"_"+ sufixo +".wmf", FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            return Json("OK", JsonRequestBehavior.AllowGet);
        }


        // GET: VA
        /// <summary>
        /// Nesta tela, é possível acessar as configurações do Visual Analysis, Exportar como Imagem
        ///(Print da Tela) e adicionar itens em cada um dos 6 módulos presentes(Máquina, Meio
        ///Ambiente, Mão de Obra, Material, Medida e Métodos). 
        /// </summary>
        /// <returns></returns>
        public ActionResult VisualAnalysis()
        {

            //?lang=pt&analysisType=PROBLEM&analysisId=AF-V-7404&user=VALENET%5cC0632569&problem=TESTE+XFRACAS+1

            string lang = Request["lang"];
            string analysisType = Request["analysisType"];
            string analysisId = Request["analysisId"];
            string user = Request["user"];
            string problem = Request["problem"];

            if (lang == "en") { 
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            }
            if (analysisId == null) return View("Error");

            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (identity.IsAuthenticated)
            {


                Session.Add("UserWindows", identity.Name);

                var ret = new AnalysisDAL().CreateOrRetrieveInOracle(new Analysis()
                {
                    TypeCode = analysisType,
                    ExternalCode = analysisId,
                    Problem = problem

                }, user);



                Session.Add("problem", Request["problem"]);
                Session.Add("analysisCode", ret.Code);
                ViewBag.analysis = ret;
                ViewBag.problem = problem;


                //    //Caso a Análise Visual do mesmo problema esteja sendo alterada por outro usuário no
                //    //momento, o usuário deve receber uma mensagem de validação informando que ele fará
                //    //visualização de apenas leitura e informando qual o usuário que está fazendo a análise.

                ////////////TEMPP ///////////////
                UnlockAnalysis();
                ////////////TEMPP ///////////////
                ///
                return View();
            }
            else
            {

                return RedirectToAction("About", "Home");
            }


        }



        public JsonResult GetFiveWhy()
        {
            int analysisCode = int.Parse(Session["analysisCode"].ToString());

            return Json(new FiveWhyDAL().List(analysisCode), JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveFiveWhy()
        {

            List<string> respostas = new List<string>();
            List<Why> whies = new List<Why>();

            int i = 0;
            bool x = true;
            while (x)
            {
                if (Request[$"Porque_resposta{i}"] != null)
                {
                    Why ParentQuestion = null;
                    string Question = string.Empty;

                    if (i > 0)
                    {
                        ParentQuestion = new Why() { Order = (i - 1) };
                        Question = $"{ResourceXFracas.Porque_} {Request[$"Porque_resposta{i - 1}"]} ";
                    }
                    else
                        Question = $"{ResourceXFracas.Porque_} {Session["problem"].ToString()}  ";

                    whies.Add(new Why() { Answer = Request[$"Porque_resposta{i}"], Code = i, Order = i, Question = $"{Question}", ParentQuestion = ParentQuestion });
                    //  respostas.Add(Request[$"Porque_resposta{i}"]);
                }

                else
                    x = false;

                i++;

            }

            int analysisCode = int.Parse(Session["analysisCode"].ToString());
            var ret = new FiveWhyDAL().SaveWithAnalysisInOracle(analysisCode, whies);


            return Json($"{ResourceXFracas.Dadossalvoscomsucesso}");
        }

        public JsonResult UnlockAnalysis()
        {
            int analysisCode = int.Parse(Session["analysisCode"].ToString());
            return Json(new AnalysisDAL().UnlockAnalysis(analysisCode), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetSource()
        {
            return Json(new SourceDAL().List(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSeverity()
        {
            return Json(new SeverityDAL().List(), JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetFrequency()
        {
            return Json(new FrequencyDAL().List(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEvidenceType()
        {
            return Json(new EvidenceDAL().ListTypes(), JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        ///A tela Configurações abre por padrão na aba Fonte. Os procedimentos abaixo se aplicam
        ///para as abas FONTE e TIPOS DE EVIDÊNCIA.
        ///Nesta aba, é possível adicionar os controles de Fonte que serão adicionados no sistema.O
        ///ícone faz a remoção de um item criado.É obrigatório digitar algo para conclusão da criação
        ///do item, caso contrário o sistema não deve aceitar a criação
        /// </summary>
        /// <returns></returns>
        public ActionResult Configuracao()
        {
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (identity.IsAuthenticated)
            {


                Session.Add("UserWindows", identity.Name);

                VABL us = new VABL();
                us.TestDB();


                return View();
            }
            else
            {
                return RedirectToAction("About", "Home");
            }


        }

        public JsonResult GetConfiguracoes()
        {
            Configuration c = new Configuration()
            {

            };

            return Json("OK");

        }


        public JsonResult AddConfiguracaoFonte(int code, string name)
        {
            try
            {
                SourceDAL sourceDAL = new SourceDAL();



                // Para criar um item da aba Fonte clique no botão.E digite alguma descrição.
                //O sistema não deve aceitar a criação de um item sem descrição(criar janela com a mensagem
                //Descrição Obrigatória)

                //O sistema não deve aceitar dois itens com o mesmo nome. Caso o usuário digite uma descrição
                //já existente, o sistema deve mostrar a seguinte mensagem de validação

                var source = sourceDAL.List();

                if (source.Where(o => o.Name == name).Count() > 0)
                {
                    return Json($"{ResourceXFracas.JáexisteoutraFontecommesmonome}", JsonRequestBehavior.AllowGet);
                }

                sourceDAL.SaveInOracle(new Person() { Name = name, Code = code });

                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult AlteraConfiguracaoFonte(int code, string name)
        {
            try
            {
                SourceDAL sourceDAL = new SourceDAL();



                // Para criar um item da aba Fonte clique no botão.E digite alguma descrição.
                //O sistema não deve aceitar a criação de um item sem descrição(criar janela com a mensagem
                //Descrição Obrigatória)

                //O sistema não deve aceitar dois itens com o mesmo nome. Caso o usuário digite uma descrição
                //já existente, o sistema deve mostrar a seguinte mensagem de validação

                var source = sourceDAL.List();

                if (source.Where(o => o.Name == name).Count() > 0)
                {
                    return Json($"{ResourceXFracas.JáexisteoutraFontecommesmonome}", JsonRequestBehavior.AllowGet);
                }

                sourceDAL.AlterInOracle(new Person() { Name = name, Code = code });

                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult RemoveConfiguracaoFonte(int code)
        {
            try
            {
                SourceDAL sourceDAL = new SourceDAL();




                // Caso o usuário tente remover um item já em uso, o sistema não deve permitir, e deve mostrar a
                //seguinte mensagem de validação:

                var valid = sourceDAL.VerifySourceDeletionInOracle(new Person() { Code = code });

                if (!valid)
                {
                    return Json($"{ResourceXFracas.Naopodeserremovido}", JsonRequestBehavior.AllowGet);
                }


                sourceDAL.DeleteInOracle(new Person() { Code = code });


                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult AddConfiguracaoSeveridade(int weight, string name)
        {
            try
            {
                SeverityDAL severityDAL = new SeverityDAL();


                // Para criar um item da aba Fonte clique no botão.E digite alguma descrição.
                //O sistema não deve aceitar a criação de um item sem descrição(criar janela com a mensagem
                //Descrição Obrigatória)

                //O sistema não deve aceitar dois itens com o mesmo nome. Caso o usuário digite uma descrição
                //já existente, o sistema deve mostrar a seguinte mensagem de validação
                var source = new FrequencyDAL().List();

                if (source.Where(o => o.Description == name).Count() > 0)
                {
                    return Json($"{ResourceXFracas.SeveridadeMesmoNomealert}", JsonRequestBehavior.AllowGet);
                }

                severityDAL.SaveInOracle(new Severity() { Description = name, Weight = weight });


                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AlteraConfiguracaoSeveridade(int weight, string name)
        {
            try
            {
                SeverityDAL severityDAL = new SeverityDAL();


                // Para criar um item da aba Fonte clique no botão.E digite alguma descrição.
                //O sistema não deve aceitar a criação de um item sem descrição(criar janela com a mensagem
                //Descrição Obrigatória)

                //O sistema não deve aceitar dois itens com o mesmo nome. Caso o usuário digite uma descrição
                //já existente, o sistema deve mostrar a seguinte mensagem de validação
                var source = new FrequencyDAL().List();

                if (source.Where(o => o.Description == name).Count() > 0)
                {
                    return Json($"{ResourceXFracas.SeveridadeMesmoNomealert}", JsonRequestBehavior.AllowGet);
                }

                severityDAL.AlterInOracle(new Severity() { Description = name, Weight = weight });


                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult RemoveConfiguracaoSeveridade(int code)
        {
            try
            {
                SeverityDAL severityDAL = new SeverityDAL();
                // Caso o usuário tente remover um item já em uso, o sistema não deve permitir, e deve mostrar a
                //seguinte mensagem de validação:

                var valid = severityDAL.VerifySeverityDeletionInOracle(new Severity() { Code = code });

                if (!valid)
                {
                    return Json($"{ResourceXFracas.NaoPodeserRemovidoUtilizado}" , JsonRequestBehavior.AllowGet);
                }


                severityDAL.DeleteInOracle(new Severity() { Code = code });


                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }


        public JsonResult AddConfiguracaoFrequencia(int weight, string name)
        {
            try
            {
                FrequencyDAL frequencyDAL = new FrequencyDAL();

                // Para criar um item da aba Fonte clique no botão.E digite alguma descrição.
                //O sistema não deve aceitar a criação de um item sem descrição(criar janela com a mensagem
                //Descrição Obrigatória)

                //O sistema não deve aceitar dois itens com o mesmo nome. Caso o usuário digite uma descrição
                //já existente, o sistema deve mostrar a seguinte mensagem de validação

                var source = new FrequencyDAL().List();

                if (source.Where(o => o.Description == name).Count() > 0)
                {
                    return Json($"{ResourceXFracas.AlertFrequesnciaMesmoNome}" , JsonRequestBehavior.AllowGet);
                }

                frequencyDAL.SaveInOracle(new Frequency() { Description = name, Weight = weight });


                return Json("OK", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }


        public JsonResult AlteraConfiguracaoFrequencia(int weight, string name)
        {
            try
            {
                FrequencyDAL frequencyDAL = new FrequencyDAL();

                // Para criar um item da aba Fonte clique no botão.E digite alguma descrição.
                //O sistema não deve aceitar a criação de um item sem descrição(criar janela com a mensagem
                //Descrição Obrigatória)

                //O sistema não deve aceitar dois itens com o mesmo nome. Caso o usuário digite uma descrição
                //já existente, o sistema deve mostrar a seguinte mensagem de validação

                var source = new FrequencyDAL().List();

                if (source.Where(o => o.Description == name).Count() > 0)
                {
                    return Json($"{ResourceXFracas.AlertFrequesnciaMesmoNome}" , JsonRequestBehavior.AllowGet);
                }

                frequencyDAL.AlterInOracle(new Frequency() { Description = name, Weight = weight });


                return Json("OK", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult RemoveConfiguracaoFrequencia(int code)
        {
            try
            {
                FrequencyDAL frequencyDAL = new FrequencyDAL();

                // Caso o usuário tente remover um item já em uso, o sistema não deve permitir, e deve mostrar a
                //seguinte mensagem de validação:

                var valid = new FrequencyDAL().VerifyFrequencyDeletionInOracle(new Frequency() { Code = code });

                if (!valid)
                {
                    return Json($"{ResourceXFracas.NaoPodeserRemovidoUtilizado}", JsonRequestBehavior.AllowGet);
                }


                frequencyDAL.DeleteInOracle(new Frequency() { Code = code });



                return Json("OK", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult ConfigisReadOnly(string externalCode)
        {

            return Json(new ConfigDAL().isReadOnly(externalCode), JsonRequestBehavior.AllowGet);
        }


        #region Brainstorm

        public JsonResult GetListBrainstorm()
        {
            int analysisCode = int.Parse(Session["analysisCode"].ToString());

            return Json(new BrainstormDAL().ListSubjects(analysisCode, null), JsonRequestBehavior.AllowGet);

        }

        #endregion



    }
}