﻿using Compass.XFracas.WebGerenciadorBL;
using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.XFracas.WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (identity.IsAuthenticated)
            {
          
                return View();
            }
            else
            {
                return RedirectToAction("About");
            }
            
        }


        public ActionResult About()
        {
            ViewBag.Message = Resources.ResourceXFracas.Acesso_negado;
            return View();
        }

       
 

       


    }
}