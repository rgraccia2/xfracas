﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compass.XFracas.WebApp.Controllers
{
    public class CreateMaintenanceNoteController : Controller
    {
        // GET: CreateMaintenanceNote
        public ActionResult Index()
        {
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (identity.IsAuthenticated)
            {
                Session.Add("UserWindows", identity.Name);
                return View();
            }
            else
            {
                return RedirectToAction("About");
            }
        }


        public class createMaintenanceNoteResponse
        {
            public bool isError { get; set; }
            public string exceptionMessage { get; set; }
            public string successMaintenanceNoteCreated { get; set; }
        }

        public createMaintenanceNoteResponse createMaintenanceNote(
            string Location, string Plant, string Equip, string Priority,
            string ShortDescription, string LongDescription, int Action_id,
            string userNameWithDomain, string GlobalID
         )
        {
            return new createMaintenanceNoteResponse();


            //try
            //{

            //    // Parametros do Web.config
            //    // Dim EntityID As String = ConfigurationManager.AppSettings("EntityID")
            //    string EntityID = GlobalID;
            //    string maintenanceNoteNumber1param = ConfigurationManager.AppSettings("maintenanceNoteNumber1");
            //    string maintenanceNoteStatus1param = ConfigurationManager.AppSettings("maintenanceNoteStatus1");

            //    string[] comboInformation = Priority.Split(";");

            //    // Resgata as informações da nota gravada no xfracas.
            //    RecXFracas.Layers.XFracas.MaintenanceNoteModel maintenanceNote = RecXFracas.Layers.XFracas.MaintenanceNoteController.getNoteStatusFromActionId(Action_id, EntityID, maintenanceNoteNumber1param, maintenanceNoteStatus1param);

            //    int maintenanceNoteToBeCreated = 0;

            //    // Verifica se ja existe a nota 1
            //    if (maintenanceNote == null || maintenanceNote.maintenanceNoteCode == null || maintenanceNote.maintenanceNoteCode.Trim == "")
            //        // Não existe nota 1
            //        // cria a nota 1
            //        maintenanceNoteToBeCreated = 1;
            //    else
            //        throw new Exception("Já existe nota de manutenção para esta ação.");

            //    string PlantPlant = "";
            //    string MainWorkCenterPlant = "";
            //    int strDate;
            //    string strTime = "";
            //    // Remove o dominio se houver
            //    string[] x = Plant.Split("|");
            //    if (x.Count() > 1)
            //    {
            //        PlantPlant = x[0];
            //        MainWorkCenterPlant = x[1];
            //        strDate = x[2];
            //        strTime = x[3];
            //    }
            //    // Dim EntityID As String = globalIdPlant

            //    // Busca parametros no XFracas para acesso ao webservice

            //    // Popula as variaveis de Request do webservice
            //    MaintenanceNoteRequest req = new MaintenanceNoteRequest();
            //    req.Body = new MaintenanceNoteRequestBody();
            //    req.Header = new MaintenanceNoteRequestHeader();
            //    // Parametros Dinâmicos
            //    req.Body.ShortText = ShortDescription;
            //    req.Body.LongText = LongDescription;
            //    req.Body.MaintenancePlanningPlant = Plant;
            //    req.Body.EquipmentNumber = Equip;
            //    req.Body.FunctionalLocation = Location;
            //    req.Body.Priority = comboInformation[0];  // Priority
            //    req.Body.NotificationType = comboInformation[1];
            //    string usuarioWS = userNameWithDomain;
            //    // Remove o dominio se houver
            //    string[] v = usuarioWS.Split(@"\");
            //    if (v.Count() > 1)
            //        usuarioWS = v[1];

            //    req.Body.ReportedBy = "XFR_" + usuarioWS;
            //    // Parametros Fixos
            //    req.Body.BreakdownIndicator = "";
            //    req.Body.DateTimeEndMalfunction = DateTime.Now;
            //    req.Body.DateTimeEndMalfunctionSpecified = false;
            //    req.Body.DateTimeStartMalfunction = DateTime.Now;
            //    req.Body.DateTimeStartMalfunctionSpecified = false;
            //    req.Body.RequiredEndDate = strDate;
            //    req.Body.RequestedEndTime = strTime;
            //    req.Body.EndPoint = "";
            //    req.Body.Language = "";
            //    req.Body.MainWorkCenter = MainWorkCenterPlant;
            //    // req.Body.NotificationType = "YN"
            //    req.Body.PlannerGroup = "";

            //    req.Body.StartPoint = "";
            //    req.Body.UnitMeasurementLinear = "";

            //    req.Header.MessageID = "";
            //    req.Header.GlobalID = ConfigurationManager.AppSettings("CreateMaintenanceNoteService_GlobalID"); // "MI20120005_01X"



            //    SERV_MaintenanceNoteserviceagent serviceCreateMaintenanceNote = new SERV_MaintenanceNoteserviceagent();
            //    MaintenanceNoteResponse resp = null/* TODO Change to default(_) if this is not a reference type */;

            //    // Acessa o webservice
            //    try
            //    {
            //        resp = serviceCreateMaintenanceNote.OPER_MaintenanceNoteService(req);
            //    }
            //    catch (Exception ex)
            //    {
            //        throw new Exception("Erro ao criar a nota de manutenção no SAP." + Constants.vbCrLf + Constants.vbCrLf + "Detalhes do erro retornado pelo SAP:" + Constants.vbCrLf + " - " + ex.Message + Constants.vbCrLf + Constants.vbCrLf + Constants.vbCrLf + "Por favor, entre em contato com a área responsável pelos serviços do SAP.");
            //    }
            //    // Verifica se a resposta teve sucesso

            //    if (resp.Success == null)
            //        return new createMaintenanceNoteResponse() { isError = true, exceptionMessage = resp.Exception.ExceptionMessage };
            //    else
            //    {
            //        string MaintenanceNoteNumberCreated = resp.Success.MaintenanceNoteNumber;

            //        // Cria a nota de manutenção 1
            //        RecXFracas.Layers.XFracas.MaintenanceNoteController.updateStatusMaintenanceNote(Action_id, maintenanceNoteStatus1param, maintenanceNoteNumber1param, EntityID, "MSPN", MaintenanceNoteNumberCreated, userNameWithDomain);

            //        return new createMaintenanceNoteResponse() { isError = false, successMaintenanceNoteCreated = MaintenanceNoteNumberCreated };
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return new createMaintenanceNoteResponse() { isError = true, exceptionMessage = ex.Message };
            //}

        }

        public void verificaStatusNotaManutencao()
        {
            //    SERV_RetrieveMaintenanceNoteserviceagent serviceRetrievtMaintenanceNote = new SERV_RetrieveMaintenanceNoteserviceagent();
            //    RetrieveMaintenanceNoteRequest request = new RetrieveMaintenanceNoteRequest();

            //    {
            //        var withBlock = request;
            //        withBlock.NotificationNo = "201300001086";

            //        withBlock.FromNotificationDate = DateTime.Now.AddMonths(-1);
            //        withBlock.ToNotificationDate = DateTime.Now;

            //        withBlock.MessageID = "";
            //        withBlock.GlobalID = "";
            //    }


            //    RetrieveMaintenanceNoteResponse response = serviceRetrievtMaintenanceNote.OPER_MaintenanceNote(request);

            //    if (response.Success == null)
            //        MsgBox("Erro:" + response.Exception.ExceptionMessage, Title: response.Status);
            //    else
            //        MsgBox("" + response.Success.Length, Title: response.Status);
            //}
        }
    }
}