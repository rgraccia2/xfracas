﻿

jQuery(function($){
var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
    var container1 = demo1.bootstrapDualListbox('getContainer');
    container1.find('.btn').addClass('btn-white btn-info btn-bold');

				/**var setRatingColors = function() {
        $(this).find('.star-on-png,.star-half-png').addClass('orange2').removeClass('grey');
    $(this).find('.star-off-png').removeClass('orange2').addClass('grey');
}*/
				$('.rating').raty({
        'cancel' : true,
'half': true,
'starType' : 'i'
/**,

					'click': function() {
        setRatingColors.call(this);
    },
					'mouseover': function() {
        setRatingColors.call(this);
    },
					'mouseout': function() {
        setRatingColors.call(this);
    }*/
})//.find('i:not(.star-raty)').addClass('grey');



//////////////////
//select2
				$('.select2').css('width','200px').select2({allowClear: true})
				$('#select2-multiple-style .btn').on('click', function(e){
					var target = $(this).find('input[type=radio]');
var which = parseInt(target.val());
if(which == 2) $('.select2').addClass('tag-input-style');
 else $('.select2').removeClass('tag-input-style');
});



});



var strDadosAnalise;
var loadtype = 3;
var distributionbestval;


    $("#cmbItemAnalise").change(function () {

        loadType(loadtype);

    });


    document.getElementById("ckcAutoScaleX").checked = true;
    document.getElementById("ckcAutoScaleY").checked = true;

        async  function popularReportPlot() {

        StarProgress();

    await sleep(1000);

    carregarImagemPLOTReport(1, "#imgPlot1");

    carregarImagemPLOTReport(2, "#imgPlot2");

    carregarImagemPLOTReport(3, "#imgPlot3");

    carregarImagemPLOTReport(4, "#imgPlot4");

    carregarImagemPLOTReport(5, "#imgPlot5");

    carregarImagemPLOTReport(6, "#imgPlot6");


   

    EndProgress();

}


    function LiberarItem() {
        $("#cmbItemAnalise").prop('disabled', false);
    }

    function SalvarValores() {

        var dataExcel = $('#my').jexcel('getData');
    var json = JSON.stringify(dataExcel);


    $.ajax(
            {
        url: '@Url.Action("SalvarValores", "LDA")',
    dataType: 'json',
    async: false,
                data: {
        "id_item": $("#cmbItemAnalise").val(),
    "id_datatype": loadtype,
    "dadosjson": json
},
type: 'POST'
            }).success(function (data) {
                if (data == "OK") {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sucess');
    }

            }).fail(function () {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
    });
}

        function atualizarPlot() {

            if ($('#ckcAutoScaleX').is(":checked")) {
        popularGrafico();
    }

            if ($('#ckcAutoScaleY').is(":checked")) {
        popularGrafico();
    }

}

    function LiberarEscala(eixo) {

        if (eixo == "X") {

            if ($('#ckcAutoScaleX').is(":checked")) {

        $("#txtXaxisStart").prop('disabled', true);
    $("#txtXaxisEnd").prop('disabled', true);
}
            else {
        $("#txtXaxisStart").prop('disabled', false);
    $("#txtXaxisEnd").prop('disabled', false);
}

}
        else {
            if ($('#ckcAutoScaleY').is(":checked")) {

        $("#txtYaxisStart").prop('disabled', true);
    $("#txtYaxisEnd").prop('disabled', true);

}
            else {
        $("#txtYaxisStart").prop('disabled', false);
    $("#txtYaxisEnd").prop('disabled', false);
}
}


}


    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
}

    async function popularPlot() {

        plotMLETwoParameters();

    StarProgress();

    await sleep(1000);

    CarregarComboPlotType();

    carregarImagemPLOT();

    document.getElementById("detalhePlot").innerHTML = document.getElementById("conteudoListaAnalysis").innerHTML;

}

    async function popularGrafico() {

        var plotType = $("#cmbPlotType option:selected").val();


        if (ValidarScala(plotType)) {

        StarProgress();
    await sleep(1000);
    carregarImagemPLOT();

    LiberarEscala('X');
    LiberarEscala('Y');
}



}

    function ValidarScala(plot) {

        if ($("#txtXaxisStart").val() == "") {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_eixo_x');
    return false;
}
        if ($("#txtXaxisEnd").val() == "") {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_eixo_x_end');
    return false;
}

        if ($("#txtYaxisStart").val() == "") {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_eixo_y');
    return false;
}
        if ($("#txtYaxisEnd").val() == "") {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_eixo_y_end');
    return false;
}

        if (plot == 1) { // Probabilily

            if (parseFloat($("#txtXaxisStart").val()) > parseFloat($("#txtXaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_start');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) < 0) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_end');
    return false;
}

            if (parseFloat($("#txtYaxisEnd").val()) > 100) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_maior100Y');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) > parseFloat($("#txtYaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_mininoY');
    return false;
}

return true;


}
        else if (plot == 2 || plot == 3) { // Reliability - Probabilily

            if (parseFloat($("#txtXaxisStart").val()) > parseFloat($("#txtXaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_start');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) < 0) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_end');
    return false;
}

            if (parseFloat($("#txtYaxisEnd").val()) > 100) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_maior100Y');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) > parseFloat($("#txtYaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_mininoY');
    return false;
}

return true;

}
        else if (plot == 4 || plot == 6) { // PDF / HISTOGRAM

            if (parseFloat($("#txtXaxisStart").val()) > parseFloat($("#txtXaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_start');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) < 0) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_end');
    return false;
}

            if (parseFloat($("#txtYaxisEnd").val()) > 100) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_maior100Y');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) > parseFloat($("#txtYaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_mininoY');
    return false;
}

return true;
}
        else if (plot == 5) { // Failure Rate

            if (parseFloat($("#txtXaxisStart").val()) > parseFloat($("#txtXaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_start');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) < 0) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_end');
    return false;
}

            if (parseFloat($("#txtYaxisEnd").val()) > 100) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_maior100Y');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) > parseFloat($("#txtYaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_mininoY');
    return false;
}

return true;
}
else if (plot == 7) // Countor Plot
        {

            if (parseFloat($("#txtXaxisStart").val()) > parseFloat($("#txtXaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_start');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) < 0) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_end');
    return false;
}

            if (parseFloat($("#txtYaxisEnd").val()) > 100) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_maior100Y');
    return false;
}

            if (parseFloat($("#txtYaxisStart").val()) > parseFloat($("#txtYaxisEnd").val())) {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_vl_mininoY');
    return false;
}

return true;
}
}

    function Base64DecodeUrl(str) {
        str = (str + '===').slice(0, str.length + (str.length % 4));
    return str.replace(/-/g, '+').replace(/_/g, '/');
}


var FailureRank = [];
var dataResults;
var typeDistribution = "";

var count;
var totals;
var totalf;
var DataBestFit = [];
var DataBestFitLDA = [];

var MinimumEixoX;
var MaximumEixoX;
var MajorStepEixoX;
var MinorStepEixoX;

var MinimumEixoY;
var MaximumEixoY;
var MajorStepEixoY;
var MinorStepEixoY;

    function teto(numero, multiplo, ArredondarPara) {
        if (!ArredondarPara) ArredondarPara = multiplo;
    return Math.ceil(numero / ArredondarPara) * ArredondarPara;
}


    function retMultiploReliability(length) {

        switch (length) {
            case 1:
        return 1;
        break;
    case 2:
        return 10;
        break;
    case 3:
        return 100;
        break;
    case 4:
        return 1000;
        break;
    case 5:
        return 10000;
        break;
    case 6:
        return 10000;
        break;
    case 7:
        return 100000;
        break;
    case 8:
        return 1000000;
        break;
    case 9:
        return 1000000;
        break;
    case 10:
        return 10000000;
        break;
}
}

    function retMultiploProbabylity(length) {

        switch (length) {
            case 1:
        return 5;
        break;
    case 2:
        return 5;
        break;
    case 3:
        return 50;
        break;
    case 4:
        return 500;
        break;
    case 5:
        return 5000;
        break;
    case 6:
        return 50000;
        break;
    case 7:
        return 500000;
        break;
    case 8:
        return 5000000;
        break;
    case 9:
        return 5000000;
        break;
    case 10:
        return 50000000;
        break;
}
}


    function CalcularScala(plotType) {

        var strJson = JSON.stringify(dataResults);
    var urlScalaMinor;
    var urlScalaMinimoY;
    var valorRMinor;
    var urlScalaMax;
    var valorRMax;

        if (plotType == 1) {

            if ($('#ckcAutoScaleX').is(":checked")) {

        valorRMinor = 0.999;
    urlScalaMinor = "http://reliabilityapi2.azurewebsites.net/LifeData/Functions/TimeAtReliability?reliability=" + valorRMinor + "&distribution=" + typeDistribution + "";

    valorRMax = 0.001;
    urlScalaMax = "http://reliabilityapi2.azurewebsites.net/LifeData/Functions/TimeAtReliability?reliability=" + valorRMax + "&distribution=" + typeDistribution + "";

    MajorStepEixoX = 0;
    MinorStepEixoX = 0;

    $.ajax(
                    {
        headers: {
        Accept: "application/json; charset=utf-8",
    "Content-Type": "application/json; charset=utf-8"
},
url: urlScalaMinor,
dataType: 'json',
async: false,
data: strJson,
type: 'POST'
                    }).success(function (data) {

                        var num = Math.round(data);
    var strNum = num.toString();
    var len = strNum.length;
    var multiplo = retMultiploReliability(len);
    MinimumEixoX = teto(num, multiplo);

                    }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

$.ajax(
                    {
        headers: {
        Accept: "application/json; charset=utf-8",
    "Content-Type": "application/json; charset=utf-8"
},
url: urlScalaMax,
dataType: 'json',
async: false,
data: strJson,
type: 'POST'
                    }).success(function (data) {

                        var num = Math.round(data);

    var strNum = num.toString();
    var len = strNum.length;

    var multiplo = retMultiploReliability(len);

    MaximumEixoX = teto(num, multiplo);

                    }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});
}
            else {

        MinimumEixoX = $("#txtXaxisStart").val();
    MaximumEixoX = $("#txtXaxisEnd").val();
    MajorStepEixoX = 0;
    MinorStepEixoX = 0;

}

            if ($('#ckcAutoScaleY').is(":checked")) {

        urlScalaMinimoY = "http://reliabilityapi2.azurewebsites.net/LifeData/Functions/Unreliability?time=" + MinimumEixoX + "&distribution=" + typeDistribution + "";

    $.ajax(
                    {
        headers: {
        Accept: "application/json; charset=utf-8",
    "Content-Type": "application/json; charset=utf-8"
},
url: urlScalaMinimoY,
dataType: 'json',
async: false,
data: strJson,
type: 'POST'
                    }).success(function (data) {

        MinimumEixoY = (data * 100);
    MaximumEixoY = 100;
    MajorStepEixoY = 0;
    MinorStepEixoY = 0;

                    }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

}
            else {

        MinimumEixoY = $("#txtYaxisStart").val();
    MaximumEixoY = $("#txtYaxisEnd").val();
    MajorStepEixoY = 0;
    MinorStepEixoY = 0;

}

}
        else if (plotType == 2 || plotType == 3) {

            if ($('#ckcAutoScaleX').is(":checked")) {

        MinimumEixoX = 0;
    valorRMax = 0.001;
    urlScalaMax = "http://reliabilityapi2.azurewebsites.net/LifeData/Functions/TimeAtReliability?reliability=" + valorRMax + "&distribution=" + typeDistribution + "";
    url = "";
    $.ajax(
                    {
        headers: {
        Accept: "application/json; charset=utf-8",
    "Content-Type": "application/json; charset=utf-8"
},
url: urlScalaMax,
dataType: 'json',
async: false,
data: strJson,
type: 'POST'
                    }).success(function (data) {
                        var num = Math.round(data);
    var strNum = num.toString();
    var len = strNum.length;
    var multiplo = retMultiploProbabylity(len);
    MaximumEixoX = teto(num, multiplo);

                    }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

MajorStepEixoX = (MaximumEixoX / 5);
MinorStepEixoX = (MajorStepEixoX / 5);
}
else
            {
        MinimumEixoX = $("#txtXaxisStart").val();
    MaximumEixoX = $("#txtXaxisEnd").val();
    MajorStepEixoX = (MaximumEixoX / 5);
    MinorStepEixoX = (MajorStepEixoX / 5);
}

            if ($('#ckcAutoScaleY').is(":checked")) {
        MaximumEixoY = 100;
    MinimumEixoY = 0;
    MajorStepEixoY = 20;
    MinorStepEixoY = 5;
}
else
            {
        MinimumEixoY = $("#txtYaxisStart").val();
    MaximumEixoY = $("#txtYaxisEnd").val();
    MajorStepEixoY = 20;
    MinorStepEixoY = 5;

}

}
        else if (plotType == 4 || plotType == 6) { // Pdf , Histogram

            if ($('#ckcAutoScaleX').is(":checked")) {
        MinimumEixoX = 0;
    valorRMax = 0.001;
    urlScalaMax = "http://reliabilityapi2.azurewebsites.net/LifeData/Functions/TimeAtReliability?reliability=" + valorRMax + "&distribution=" + typeDistribution + "";
    url = "";
    $.ajax(
                    {
        headers: {
        Accept: "application/json; charset=utf-8",
    "Content-Type": "application/json; charset=utf-8"
},
url: urlScalaMax,
dataType: 'json',
async: false,
data: strJson,
type: 'POST'
                    }).success(function (data) {
                        var num = Math.round(data);
    var strNum = num.toString();
    var len = strNum.length;
    var multiplo = retMultiploProbabylity(len);
    MaximumEixoX = teto(num, multiplo);

                    }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

MajorStepEixoX = (MaximumEixoX / 5);
MinorStepEixoX = (MajorStepEixoX / 5);
}
else
            {
        MinimumEixoX = $("#txtXaxisStart").val();
    MaximumEixoX = $("#txtXaxisEnd").val();
    MajorStepEixoX = (MaximumEixoX / 5);
    MinorStepEixoX = (MajorStepEixoX / 5);
}

            if ($('#ckcAutoScaleY').is(":checked")) {

        MinimumEixoY = 0;

    var lstValoresPossiveis = [];
    var valorX = (MaximumEixoX / 30);
    var valorBase = valorX


                while (valorX < MaximumEixoX) {

        valorX += valorBase;

    urlScalaPDF = "http://reliabilityapi2.azurewebsites.net/LifeData/Functions/PDF?time=" + valorX + "&distribution=" + typeDistribution + "";
    url = "";
    $.ajax(
                        {
        headers: {
        Accept: "application/json; charset=utf-8",
    "Content-Type": "application/json; charset=utf-8"
},
url: urlScalaPDF,
dataType: 'json',
async: false,
data: strJson,
type: 'POST'
                        }).success(function (data) {

                            var numf = parseFloat(data);
    lstValoresPossiveis.push(numf);
    console.log(numf);

                        }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

}

$.ajax(
                    {
        url: '@Url.Action("EcontreMaior", "LDA")',
    dataType: 'json',
    async: false,
                        data: {
        "arrayvalores": JSON.stringify(lstValoresPossiveis)
},
type: 'POST'
                    }).success(function (data) {

                        var valorOrigin = parseFloat(data);
    MaximumEixoY = (valorOrigin * parseFloat(1.10));
    MajorStepEixoY = (valorOrigin / 5);
    MinorStepEixoY = (MajorStepEixoY / 5);

                    }).fail(function () {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
    });
}
            else {

        MinimumEixoY = $("#txtYaxisStart").val();
    MaximumEixoY = $("#txtYaxisEnd").val();
    MajorStepEixoY = (parseFloat(MaximumEixoY) / 5);
    MinorStepEixoY = (MajorStepEixoY / 5);
}

}
        else if (plotType == 5) { // Failure Rate

            if ($('#ckcAutoScaleX').is(":checked")) {
        MinimumEixoX = 0;

    valorRMax = 0.001;
    urlScalaMax = "http://reliabilityapi2.azurewebsites.net/LifeData/Functions/TimeAtReliability?reliability=" + valorRMax + "&distribution=" + typeDistribution + "";
    url = "";
    $.ajax(
                    {
        headers: {
        Accept: "application/json; charset=utf-8",
    "Content-Type": "application/json; charset=utf-8"
},
url: urlScalaMax,
dataType: 'json',
async: false,
data: strJson,
type: 'POST'
                    }).success(function (data) {
                        var num = Math.round(data);
    var strNum = num.toString();
    var len = strNum.length;
    var multiplo = retMultiploProbabylity(len);
    MaximumEixoX = teto(num, multiplo);

                    }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

MajorStepEixoX = (MaximumEixoX / 5);
MinorStepEixoX = (MajorStepEixoX / 5);
}
            else {

        MinimumEixoX = $("#txtXaxisStart").val();
    MaximumEixoX = $("#txtXaxisEnd").val();
    MajorStepEixoX = (MaximumEixoX / 5);
    MinorStepEixoX = (MajorStepEixoX / 5);

}

            if ($('#ckcAutoScaleY').is(":checked")) {

        MinimumEixoY = 0;

    urlScalaMaxY = "http://reliabilityapi2.azurewebsites.net/LifeData/Functions/FailureRate?time=" + MaximumEixoX + "&distribution=" + typeDistribution + "";
    url = "";
    $.ajax(
                    {
        headers: {
        Accept: "application/json; charset=utf-8",
    "Content-Type": "application/json; charset=utf-8"
},
url: urlScalaMaxY,
dataType: 'json',
async: false,
data: strJson,
type: 'POST'
                    }).success(function (data) {

        MaximumEixoY = data

    }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

MajorStepEixoY = (MaximumEixoX / 5);
MinorStepEixoY = (MajorStepEixoX / 5);

}
else
            {
        MinimumEixoY = $("#txtYaxisStart").val();
    MaximumEixoY = $("#txtYaxisEnd").val();
    MajorStepEixoY = (parseFloat(MaximumEixoY) / 5);
    MinorStepEixoY = (MajorStepEixoY / 5);
}

}
        else if (plotType == 7) {  // Contour

    }

    $("#txtXaxisStart").val(MinimumEixoX);
    $("#txtXaxisEnd").val(MaximumEixoX);

    $("#txtYaxisStart").val(MinimumEixoY);
    $("#txtYaxisEnd").val(MaximumEixoY);

    $("#txtXaxisStart").prop('disabled', true);
    $("#txtXaxisEnd").prop('disabled', true);
    $("#txtYaxisStart").prop('disabled', true);
    $("#txtYaxisEnd").prop('disabled', true);
}



    function CarregarComboPlotType() {

        $('#cmbPlotType').find('option').remove().end();
    var options = '';
        options += '<option value="1">Prob of Failure vs. Time (' + $("#cmbDistribution option:selected").text() + ')</option>';
        options += '<option value="2">Reliability vs. Time</option>';
        options += '<option value="3">Prob of Failure vs. Time</option>';
        options += '<option value="4">Pdf vs. Time</option>';
        options += '<option value="5">Failure Rate vs. Time</option>';
        options += '<option value="6">Historigran</option>';
        options += '<option value="7">Reliability - Contour Plot</option>';
    $('#cmbPlotType').append(options);

}

    function plotMLETwoParameters() {

        StarProgress();

    plotFailureRank();

    plotMLETwoParametersFinal($("#cmbDistribution option:selected").val());
    //carregarImagemPLOT();

    EndProgress();


}


    function popularBestFit() {


        //code before the pause
        plotMLETwoParametersFinalBestFit(1);

    //code before the pause
    plotMLETwoParametersFinalBestFit(2);

    //code before the pause
    plotMLETwoParametersFinalBestFit(3);

    //code before the pause
    plotMLETwoParametersFinalBestFit(4);


}



    function clearArray(array) {
        while (array.length) {
        array.pop();
    }
}

        function plotMLETwoParametersFinalBestFit(distribution) {

        StarProgress();

    var DataRow = [];
    var dataExcel = $('#my').jexcel('getData');
    var json = JSON.stringify(dataExcel);
    var arrayJson = JSON.parse(json);
    count = 0;
    totals = 0;
    totalf = 0;
    var datatype;

        if (loadtype == 3) {

        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var itemlimpo = arrayItem[0].replace("[", "");
    var num = parseInt(itemlimpo.replace('"', ''));

                if (isNaN(num) == false) {

                    var item2Limpo = arrayItem[1].replace(']', '');
    var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                    var obj = {
        FSstatus: 0,
    FailureMode: item3Limpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 1) {
        arrayJson.forEach(function (item) {

            var itemstr = JSON.stringify(item)
            var arrayItem = [];
            arrayItem = itemstr.split(",");

            var itemlimpo = arrayItem[0].replace("[", "");
            var num = parseInt(itemlimpo.replace('"', ''));

            if (isNaN(num) == false) {

                var item2Limpo = arrayItem[1].replace(']', '');
                var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                var obj = {
                    FSstatus: 0,
                    FailureMode: item3Limpo,
                    FailureSeverity: "string",
                    LastInspectTime: 0,
                    NumofForS: 1,
                    Time: num,
                }
                count++;

                DataRow.push(obj);
            }
        });
    }
        else if (loadtype == 2) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var time = arrayItem[0].replace("[", "");
    var num = parseInt(time.replace('"', ''));

                if (isNaN(num) == false) {

                    var failure = arrayItem[1].replace(']', '');
    var failureLimpo = failure.replace('"', '').replace('"', '');

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var tipoFS;
                    if (failureLimpo == 'F') {
        tipoFS = 0
                        totalf++
}
                    else {
        tipoFS = 1;
    totals++
}

                    var obj = {
        FSstatus: tipoFS,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 4) {

        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));

                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    totals = 0;
    count += qtdlimpo;
    totalf = count;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 5) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));


                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var typefs = arrayItem[2].replace(']', '');
    var typefsLimpo = typefs.replace('"', '').replace('"', '');

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var status;

    count += qtdlimpo;

                    if (typefsLimpo == 'F') {
        totalf += qtdlimpo;
    status = 0;
}
                    else {
        totals += qtdlimpo;
    status = 1;
}


                    var obj = {
        FSstatus: status,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 6) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var end = arrayItem[1].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

        count++;
        totalf = count;
        totals = 0;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 7) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var status = arrayItem[1].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count++;
    var strStatus;
                    if (statusLimpo == 'F') {
        totalf++;
    strStatus = 0;
}
                    else {
        totals++;
    strStatus = 1;
}

                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 8) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count += qtdelimpo;
    totals = 0;
    totalf += qtdelimpo;


                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 9) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var status = arrayItem[2].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[3].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[4].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var strStatus;
                    if (statusLimpo == 'F') {
        totalf += qtdelimpo;
    strStatus = 0;
}
                    else {
        totals += qtdelimpo;
    strStatus = 1;
}

count += qtdelimpo;



                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: qtdelimpo,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}

var typeAnalisy = $('a#c_button').html();


var strurl = "";
        if (distribution == 1) {
        strurl = 'https://reliabilityapi2.azurewebsites.net/LifeData/Weibull/TwoParameter/' + typeAnalisy + '';
    typeDistribution = "Weibull";
}
        else if (distribution == 2) {
        strurl = 'https://reliabilityapi2.azurewebsites.net/LifeData/Exponential/OneParameter/' + typeAnalisy + '';
    typeDistribution = "Exponential";
}
        else if (distribution == 3) {
        strurl = 'https://reliabilityapi2.azurewebsites.net/LifeData/Normal/TwoParameter/' + typeAnalisy + '';
    typeDistribution = "Normal";
}
        else if (distribution == 4) {
        strurl = 'https://reliabilityapi2.azurewebsites.net/LifeData/Lognormal/TwoParameter/' + typeAnalisy + '';
    typeDistribution = "Lognormal";
}

$.ajax(
            {
        url: strurl,
    dataType: 'json',
    async: false,
                data: {
        "DataRow": DataRow,
    "DataType": datatype,
    "EndTime": 0,
    "FailureRank": FailureRank,
    "TotalFailureRows": 0,
    "TotalNumFailureandSuspensions": count,
    "TotalNumFailures": totalf,
    "TotalNumSuspensions": totals,
    "TotalNumofRows": 0,
    "TotalSuspensionRows": 0
},
type: 'POST'
            }).success(function (data) {

        dataResults = data;

    if (distribution == 1) {

                    var objbest = {
        distribution: "Weibull-2P",
    lkv: data.LKV,
    value: 1
}
DataBestFit.push(objbest);

}
                else if (distribution == 2) {

                    var objbest2 = {
        distribution: "Exponential",
    lkv: data.LKV,
    value: 2
}
DataBestFit.push(objbest2);

}
                else if (distribution == 3) {

                    var objbest3 = {
        distribution: "Normal",
    lkv: data.LKV,
    value: 3
}
DataBestFit.push(objbest3);
}
                else if (distribution == 4) {

                    var objbest4 = {
        distribution: "Lognormal",
    lkv: data.LKV,
    value: 4
}
DataBestFit.push(objbest4);
}

EndProgress();

            }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});
}

    function plotMLETwoParametersFinal(distribution) {

        StarProgress();

    var DataRow = [];
    var dataExcel = $('#my').jexcel('getData');
    var json = JSON.stringify(dataExcel);
    var arrayJson = JSON.parse(json);
    count = 0;
    totals = 0;
    totalf = 0;
    var datatype;

        if (loadtype == 3) {

        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var itemlimpo = arrayItem[0].replace("[", "");
    var num = parseInt(itemlimpo.replace('"', ''));

                if (isNaN(num) == false) {

                    var item2Limpo = arrayItem[1].replace(']', '');
    var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                    var obj = {
        FSstatus: 0,
    FailureMode: item3Limpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 1) {
        arrayJson.forEach(function (item) {

            var itemstr = JSON.stringify(item)
            var arrayItem = [];
            arrayItem = itemstr.split(",");

            var itemlimpo = arrayItem[0].replace("[", "");
            var num = parseInt(itemlimpo.replace('"', ''));

            if (isNaN(num) == false) {

                var item2Limpo = arrayItem[1].replace(']', '');
                var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                var obj = {
                    FSstatus: 0,
                    FailureMode: item3Limpo,
                    FailureSeverity: "string",
                    LastInspectTime: 0,
                    NumofForS: 1,
                    Time: num,
                }
                count++;

                DataRow.push(obj);
            }
        });
    }
        else if (loadtype == 2) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var time = arrayItem[0].replace("[", "");
    var num = parseInt(time.replace('"', ''));

                if (isNaN(num) == false) {

                    var failure = arrayItem[1].replace(']', '');
    var failureLimpo = failure.replace('"', '').replace('"', '');

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var tipoFS;
                    if (failureLimpo == 'F') {
        tipoFS = 0
                        totalf++
}
                    else {
        tipoFS = 1;
    totals++
}

                    var obj = {
        FSstatus: tipoFS,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 4) {

        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));

                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    totals = 0;
    count += qtdlimpo;
    totalf = count;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 5) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));


                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var typefs = arrayItem[2].replace(']', '');
    var typefsLimpo = typefs.replace('"', '').replace('"', '');

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var status;

    count += qtdlimpo;

                    if (typefsLimpo == 'F') {
        totalf += qtdlimpo;
    status = 0;
}
                    else {
        totals += qtdlimpo;
    status = 1;
}


                    var obj = {
        FSstatus: status,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 6) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var end = arrayItem[1].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

        count++;
        totalf = count;
        totals = 0;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 7) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var status = arrayItem[1].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count++;
    var strStatus;
                    if (statusLimpo == 'F') {
        totalf++;
    strStatus = 0;
}
                    else {
        totals++;
    strStatus = 1;
}

                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 8) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count += qtdelimpo;
    totals = 0;
    totalf += qtdelimpo;


                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 9) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var status = arrayItem[2].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[3].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[4].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var strStatus;
                    if (statusLimpo == 'F') {
        totalf += qtdelimpo;
    strStatus = 0;
}
                    else {
        totals += qtdelimpo;
    strStatus = 1;
}

count += qtdelimpo;



                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: qtdelimpo,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}

var typeAnalisy = $('a#c_button').html();


var strurl = "";
        if (distribution == 1) {
        strurl = 'https://reliabilityapi2.azurewebsites.net/LifeData/Weibull/TwoParameter/' + typeAnalisy + '';
    typeDistribution = "Weibull";
}
        else if (distribution == 2) {
        strurl = 'https://reliabilityapi2.azurewebsites.net/LifeData/Exponential/OneParameter/' + typeAnalisy + '';
    typeDistribution = "Exponential";
}
        else if (distribution == 3) {
        strurl = 'https://reliabilityapi2.azurewebsites.net/LifeData/Normal/TwoParameter/' + typeAnalisy + '';
    typeDistribution = "Normal";
}
        else if (distribution == 4) {
        strurl = 'https://reliabilityapi2.azurewebsites.net/LifeData/Lognormal/TwoParameter/' + typeAnalisy + '';
    typeDistribution = "Lognormal";
}

        if (DataRow.length > 1) {

        $.ajax(
            {
                url: strurl,
                dataType: 'json',
                async: false,
                data: {
                    "DataRow": DataRow,
                    "DataType": datatype,
                    "EndTime": 0,
                    "FailureRank": FailureRank,
                    "TotalFailureRows": 0,
                    "TotalNumFailureandSuspensions": count,
                    "TotalNumFailures": totalf,
                    "TotalNumSuspensions": totals,
                    "TotalNumofRows": 0,
                    "TotalSuspensionRows": 0
                },
                type: 'POST'
            }).success(function (data) {

                dataResults = data;

                if (distribution == 1) {
                    document.getElementById("lblBeta").innerHTML = data.Beta;
                    document.getElementById("lblEta").innerHTML = data.Eta;
                    document.getElementById("lblRho").innerHTML = data.Pho;
                    document.getElementById("lblLKV").innerHTML = data.LKV;
                    document.getElementById("lblFailures").innerHTML = totalf;
                    document.getElementById("lblSuspensions").innerHTML = totals;

                    //document.getElementById("lblBetaR").innerHTML = data.Beta;
                    //document.getElementById("lblEtaR").innerHTML = data.Eta;
                    //document.getElementById("lblRhoR").innerHTML = data.Pho;
                    //document.getElementById("lblLKVR").innerHTML = data.LKV;
                    //document.getElementById("lblFailuresR").innerHTML = totalf;
                    //document.getElementById("lblSuspensionsR").innerHTML = totals;

                }
                else if (distribution == 2) {

                    document.getElementById("lblLambda").innerHTML = data.Lambda;
                    document.getElementById("lblRho").innerHTML = data.Pho;
                    document.getElementById("lblLKV").innerHTML = data.LKV;
                    document.getElementById("lblFailures").innerHTML = totalf;
                    document.getElementById("lblSuspensions").innerHTML = totals;


                }
                else if (distribution == 3) {
                    document.getElementById("lblMu").innerHTML = data.Mu;
                    document.getElementById("lblSigma").innerHTML = data.Sigma;
                    document.getElementById("lblRho").innerHTML = data.Pho;
                    document.getElementById("lblLKV").innerHTML = data.LKV;
                    document.getElementById("lblFailures").innerHTML = totalf;
                    document.getElementById("lblSuspensions").innerHTML = totals;

                }
                else if (distribution == 4) {

                    document.getElementById("lblLogMu").innerHTML = data.LogMu;
                    document.getElementById("lblLogSigma").innerHTML = data.LogSigma;
                    document.getElementById("lblRho").innerHTML = data.Pho;
                    document.getElementById("lblLKV").innerHTML = data.LKV;
                    document.getElementById("lblFailures").innerHTML = totalf;
                    document.getElementById("lblSuspensions").innerHTML = totals;

                }

                EndProgress();

            }).fail(function () {
                EndProgress();
                MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
            });

    }
        else {
        MessageShow('Informe no minímo dois valores para efetuar está análise.');
    }
  }



   function carregarImagemPLOTReport(plot,idImg) {

        $("#cmbItemAnalise").prop('disabled', true);

    var plotType = plot;

     var DataRow = [];
     var dataExcel = $('#my').jexcel('getData');
     var json = JSON.stringify(dataExcel);
     var arrayJson = JSON.parse(json);

     count = 0;
     totals = 0;
     totalf = 0;
     var datatype;

     var unidade = $("#cmbUnit option:selected").text();
     var TitleProbability = $("#cmbPlotType option:selected").text();

     var TitleY;
     var TitleX;
     var TitleGeral;


     CalcularScala(plotType);

       if (plotType == 1) { //Probability

        TitleY = "Probability of Failure, F(t)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = TitleProbability;
}
       else if (plotType == 2) { //Reliability

        TitleY = "Reliability, R(t)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;

}
       else if (plotType == 3) { //Unreliability

        TitleY = "Probability of Failure (%)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;
}
       else if (plotType == 4) { //Pdf

        TitleY = "pdf, f(t)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;

}
       else if (plotType == 5) { //FailureRate

        TitleY = "Failure Rate";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;
}
       else if (plotType == 6) { //Histogram
        TitleY = "pdf, f(t)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;
}
       else if (plotType == 7) { // Contour

        TitleY = null
           TitleX = null
    TitleGeral = null
}

        if (loadtype == 3) {

        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var itemlimpo = arrayItem[0].replace("[", "");
    var num = parseInt(itemlimpo.replace('"', ''));

                if (isNaN(num) == false) {

                    var item2Limpo = arrayItem[1].replace(']', '');
    var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                    var obj = {
        FSstatus: 0,
    FailureMode: item3Limpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 1) {
        arrayJson.forEach(function (item) {

            var itemstr = JSON.stringify(item)
            var arrayItem = [];
            arrayItem = itemstr.split(",");

            var itemlimpo = arrayItem[0].replace("[", "");
            var num = parseInt(itemlimpo.replace('"', ''));

            if (isNaN(num) == false) {

                var item2Limpo = arrayItem[1].replace(']', '');
                var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                var obj = {
                    FSstatus: 0,
                    FailureMode: item3Limpo,
                    FailureSeverity: "string",
                    LastInspectTime: 0,
                    NumofForS: 1,
                    Time: num,
                }
                count++;

                DataRow.push(obj);
            }
        });
    }
        else if (loadtype == 2) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var time = arrayItem[0].replace("[", "");
    var num = parseInt(time.replace('"', ''));

                if (isNaN(num) == false) {

                    var failure = arrayItem[1].replace(']', '');
    var failureLimpo = failure.replace('"', '').replace('"', '');

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var tipoFS;
                    if (failureLimpo == 'F') {
        tipoFS = 0
                        totalf++
}
                    else {
        tipoFS = 1;
    totals++
}

                    var obj = {
        FSstatus: tipoFS,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 4) {

        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));

                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    totals = 0;
    count += qtdlimpo;
    totalf = count;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 5) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));


                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var typefs = arrayItem[2].replace(']', '');
    var typefsLimpo = typefs.replace('"', '').replace('"', '');

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var status;

    count += qtdlimpo;

                    if (typefsLimpo == 'F') {
        totalf += qtdlimpo;
    status = 0;
}
                    else {
        totals += qtdlimpo;
    status = 1;
}


                    var obj = {
        FSstatus: status,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 6) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var end = arrayItem[1].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count++;
    totalf = count;
    totals = 0;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 7) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var status = arrayItem[1].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count++;
    var strStatus;
                    if (statusLimpo == 'F') {
        totalf++;
    strStatus = 0;
}
                    else {
        totals++;
    strStatus = 1;
}

                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 8) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count += qtdelimpo;
    totals = 0;
    totalf += qtdelimpo;


                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 9) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var status = arrayItem[2].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[3].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[4].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var strStatus;
                    if (statusLimpo == 'F') {
        totalf += qtdelimpo;
    strStatus = 0;
}
                    else {
        totals += qtdelimpo;
    strStatus = 1;
}

count += qtdelimpo;



                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: qtdelimpo,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}


var metodo;
var eixo;
var bounds;
var level = '90';


var setOn = 0;
        if ($('#rdnxaxis').is(":checked")) {
        setOn = 0;
    eixo = "X- axis";
}
        else {
        setOn = 1;
    eixo = "Y- axis";
}


var setMethod;
        if ($('#rdnfishermatrix').is(":checked")) {
        setMethod = 0;
    metodo = "CB FM";
}
        else {
        setMethod = 1;
    metodo = "CB LR";
}


var setSided = 3;
        if ($('#rdnNone').is(":checked")) {
        setSided = 0;
    bounds = "--";
}
        else if ($('#rdntwosided').is(":checked")) {
        setSided = 3;
    bounds = "2S";
}
        else if ($('#rdnonsided').is(":checked")) {
        setSided = 2;
    bounds = "T-1S";
}
        else if ($('#rdnbottomone').is(":checked")) {
        setSided = 1;
    bounds = "B-1S";
}

level = $("#txtConfidenceLevel").val();

var ConfidenceLevelVal = (parseFloat(level) / 100);

document.getElementById("btnCBFM").innerHTML = metodo + " / " + level + "%  / " + bounds + " / " + eixo;

var autoscaleY = false;
var autoscaleX = false;


        var strJson = {
        Results: dataResults,
                Data: {
        DataRow: DataRow,
    EndTime: 0,
    TotalNumFailures: totalf,
    TotalNumSuspensions: totals,
    TotalNumFailureandSuspensions: count,
    TotalFailureRows: 0,
    TotalSuspensionRows: 0,
    TotalNumofRows: 0,
    DataType: datatype,
    FailureRank: FailureRank
},
                Settings: {
        Bounds: {
        ConfidenceLevel: ConfidenceLevelVal,
    Method: setMethod,
    On: setOn,
    Sided: setSided
},
                    XAxis: {
        Title: {
        Name: TitleX
},
AutoScale: autoscaleX,
Minimum: MinimumEixoX,
Maximum: MaximumEixoX,
MajorStep: MajorStepEixoX,
MinorStep: MinorStepEixoX
},
                    YAxis: {
        Title: {
        Name: TitleY
},
AutoScale: autoscaleY,
Minimum: MinimumEixoY,
Maximum: MaximumEixoY,
MajorStep: MajorStepEixoY,
MinorStep: MinorStepEixoY
},
                    Title: {
        Name: TitleGeral
}
}
}


$("#cmbDistribution option:selected").val()

var dataOutput = JSON.stringify(strJson);
var sjson = '' + dataOutput + '';

var strUrl = "";
        if (plotType == 1) {

        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Probability";
    }
        else if (plotType == 2) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Reliability";
    }
        else if (plotType == 3) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Unreliability";
    }
        else if (plotType == 4) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Pdf";
    }
        else if (plotType == 5) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/FailureRate";
    }
        else if (plotType == 6) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Histogram";
    }
        else if (plotType == 7) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Contour";
    }


        var settings = {
        "async": true,
    "crossDomain": true,
    "url": strUrl,
    "method": "POST",
            "headers": {
        "Content-Type": "application/json;charset=UTF-8",
    "Accept": "text/html",
    "cache-control": "no-cache",
    "Access-Control-Allow-Origin": "*",
    "Cache-Control": "no-cache",
    "Content-Encoding": "gzip",
     "Vary": "Accept-Encoding"
},
"data": sjson
}

        $.ajax(settings).success(function (data) {


        $(idImg).attr('src', "data:image/png;base64," + Base64DecodeUrl(data) + "");



    }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

}

    function carregarImagemPLOT() {

        $("#cmbItemAnalise").prop('disabled', true);

    StarProgress();

    var plotType = $("#cmbPlotType option:selected").val();

    var DataRow = [];
    var dataExcel = $('#my').jexcel('getData');
    var json = JSON.stringify(dataExcel);
    var arrayJson = JSON.parse(json);

    count = 0;
    totals = 0;
    totalf = 0;
    var datatype;

    var unidade = $("#cmbUnit option:selected").text();
    var TitleProbability = $("#cmbPlotType option:selected").text();

    var TitleY;
    var TitleX;
    var TitleGeral;


    CalcularScala(plotType);

        if (plotType == 1) { //Probability

        TitleY = "Probability of Failure, F(t)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = TitleProbability;
}
        else if (plotType == 2) { //Reliability

        TitleY = "Reliability, R(t)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;

}
        else if (plotType == 3) { //Unreliability

        TitleY = "Probability of Failure (%)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;
}
        else if (plotType == 4) { //Pdf

        TitleY = "pdf, f(t)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;

}
        else if (plotType == 5) { //FailureRate

        TitleY = "Failure Rate";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;
}
        else if (plotType == 6) { //Histogram
        TitleY = "pdf, f(t)";
    TitleX = "Time (" + unidade + ")";
    TitleGeral = null;
}
        else if (plotType == 7) { // Contour

        TitleY = null
            TitleX = null
    TitleGeral = null
}

        if (loadtype == 3) {

        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var itemlimpo = arrayItem[0].replace("[", "");
    var num = parseInt(itemlimpo.replace('"', ''));

                if (isNaN(num) == false) {

                    var item2Limpo = arrayItem[1].replace(']', '');
    var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                    var obj = {
        FSstatus: 0,
    FailureMode: item3Limpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 1) {
        arrayJson.forEach(function (item) {

            var itemstr = JSON.stringify(item)
            var arrayItem = [];
            arrayItem = itemstr.split(",");

            var itemlimpo = arrayItem[0].replace("[", "");
            var num = parseInt(itemlimpo.replace('"', ''));

            if (isNaN(num) == false) {

                var item2Limpo = arrayItem[1].replace(']', '');
                var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                var obj = {
                    FSstatus: 0,
                    FailureMode: item3Limpo,
                    FailureSeverity: "string",
                    LastInspectTime: 0,
                    NumofForS: 1,
                    Time: num,
                }
                count++;

                DataRow.push(obj);
            }
        });
    }
        else if (loadtype == 2) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var time = arrayItem[0].replace("[", "");
    var num = parseInt(time.replace('"', ''));

                if (isNaN(num) == false) {

                    var failure = arrayItem[1].replace(']', '');
    var failureLimpo = failure.replace('"', '').replace('"', '');

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var tipoFS;
                    if (failureLimpo == 'F') {
        tipoFS = 0
                        totalf++
}
                    else {
        tipoFS = 1;
    totals++
}

                    var obj = {
        FSstatus: tipoFS,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 4) {

        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));

                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    totals = 0;
    count += qtdlimpo;
    totalf = count;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 5) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));


                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var typefs = arrayItem[2].replace(']', '');
    var typefsLimpo = typefs.replace('"', '').replace('"', '');

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var status;

    count += qtdlimpo;

                    if (typefsLimpo == 'F') {
        totalf += qtdlimpo;
    status = 0;
}
                    else {
        totals += qtdlimpo;
    status = 1;
}


                    var obj = {
        FSstatus: status,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 6) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var end = arrayItem[1].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count++;
    totalf = count;
    totals = 0;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 7) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var status = arrayItem[1].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count++;
    var strStatus;
                    if (statusLimpo == 'F') {
        totalf++;
    strStatus = 0;
}
                    else {
        totals++;
    strStatus = 1;
}

                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 8) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count += qtdelimpo;
    totals = 0;
    totalf += qtdelimpo;


                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 9) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var status = arrayItem[2].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[3].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[4].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var strStatus;
                    if (statusLimpo == 'F') {
        totalf += qtdelimpo;
    strStatus = 0;
}
                    else {
        totals += qtdelimpo;
    strStatus = 1;
}

count += qtdelimpo;



                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: qtdelimpo,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}


var metodo;
var eixo;
var bounds;
var level = '90';


var setOn = 0;
        if ($('#rdnxaxis').is(":checked")) {
        setOn = 0;
    eixo = "X- axis";
}
        else {
        setOn = 1;
    eixo = "Y- axis";
}


var setMethod;
        if ($('#rdnfishermatrix').is(":checked")) {
        setMethod = 0;
    metodo = "CB FM";
}
        else {
        setMethod = 1;
    metodo = "CB LR";
}


var setSided = 3;
        if ($('#rdnNone').is(":checked")) {
        setSided = 0;
    bounds = "--";
}
        else if ($('#rdntwosided').is(":checked")) {
        setSided = 3;
    bounds = "2S";
}
        else if ($('#rdnonsided').is(":checked")) {
        setSided = 2;
    bounds = "T-1S";
}
        else if ($('#rdnbottomone').is(":checked")) {
        setSided = 1;
    bounds = "B-1S";
}

level = $("#txtConfidenceLevel").val();

var ConfidenceLevelVal = (parseFloat(level) / 100);

document.getElementById("btnCBFM").innerHTML = metodo + " / " + level + "%  / " + bounds + " / " + eixo;

var autoscaleY = false;
var autoscaleX = false;


        var strJson = {
        Results: dataResults,
                Data: {
        DataRow: DataRow,
    EndTime: 0,
    TotalNumFailures: totalf,
    TotalNumSuspensions: totals,
    TotalNumFailureandSuspensions: count,
    TotalFailureRows: 0,
    TotalSuspensionRows: 0,
    TotalNumofRows: 0,
    DataType: datatype,
    FailureRank: FailureRank
},
                Settings: {
        Bounds: {
        ConfidenceLevel: ConfidenceLevelVal,
    Method: setMethod,
    On: setOn,
    Sided: setSided
},
                    XAxis: {
        Title: {
        Name: TitleX
},
AutoScale: autoscaleX,
Minimum: MinimumEixoX,
Maximum: MaximumEixoX,
MajorStep: MajorStepEixoX,
MinorStep: MinorStepEixoX
},
                    YAxis: {
        Title: {
        Name: TitleY
},
AutoScale: autoscaleY,
Minimum: MinimumEixoY,
Maximum: MaximumEixoY,
MajorStep: MajorStepEixoY,
MinorStep: MinorStepEixoY
},
                    Title: {
        Name: TitleGeral
}
}
}


$("#cmbDistribution option:selected").val()

var dataOutput = JSON.stringify(strJson);
var sjson = '' + dataOutput + '';

var strUrl = "";
        if (plotType == 1) {

        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Probability";
    }
        else if (plotType == 2) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Reliability";
    }
        else if (plotType == 3) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Unreliability";
    }
        else if (plotType == 4) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Pdf";
    }
        else if (plotType == 5) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/FailureRate";
    }
        else if (plotType == 6) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Histogram";
    }
        else if (plotType == 7) {
        strUrl = "https://reliabilityapi2.azurewebsites.net/LifeData/Plot/" + typeDistribution + "/Contour";
    }


        var settings = {
        "async": true,
    "crossDomain": true,
    "url": strUrl,
    "method": "POST",
            "headers": {
        "Content-Type": "application/json;charset=UTF-8",
    "Accept": "text/html",
    "cache-control": "no-cache",
    "Access-Control-Allow-Origin": "*",
    "Cache-Control": "no-cache",
    "Content-Encoding": "gzip",
     "Vary": "Accept-Encoding"
},
"data": sjson
}

        $.ajax(settings).success(function (data) {

        $("#img").attr('src', "data:image/png;base64," + Base64DecodeUrl(data) + "");

    EndProgress();

        }).fail(function () {
        EndProgress();
    MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
});

}


    function plotFailureRank() {

        var DataRow = [];


    var dataExcel = $('#my').jexcel('getData');



    var json = JSON.stringify(dataExcel);


    var arrayJson = JSON.parse(json);
    var count = 0;
    var totals = 0;
    var totalf = 0;

        if (loadtype == 3) {
        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var itemlimpo = arrayItem[0].replace("[", "");
    var num = parseInt(itemlimpo.replace('"', ''));

                if (isNaN(num) == false) {

                    var item2Limpo = arrayItem[1].replace(']', '');
    var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                    var obj = {
        FSstatus: 0,
    FailureMode: item3Limpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 1) {
        arrayJson.forEach(function (item) {

            var itemstr = JSON.stringify(item)
            var arrayItem = [];
            arrayItem = itemstr.split(",");

            var itemlimpo = arrayItem[0].replace("[", "");
            var num = parseInt(itemlimpo.replace('"', ''));

            if (isNaN(num) == false) {

                var item2Limpo = arrayItem[1].replace(']', '');
                var item3Limpo = item2Limpo.replace('"', '').replace('"', '');

                var obj = {
                    FSstatus: 0,
                    FailureMode: item3Limpo,
                    FailureSeverity: "string",
                    LastInspectTime: 0,
                    NumofForS: 1,
                    Time: num,
                }
                count++;

                DataRow.push(obj);
            }
        });
    }
        else if (loadtype == 2) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var time = arrayItem[0].replace("[", "");
    var num = parseInt(time.replace('"', ''));

                if (isNaN(num) == false) {

                    var failure = arrayItem[1].replace(']', '');
    var failureLimpo = failure.replace('"', '').replace('"', '');

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var tipoFS;
                    if (failureLimpo == 'F') {
        tipoFS = 0
                        totalf++
}
                    else {
        tipoFS = 1;
    totals++
}

                    var obj = {
        FSstatus: tipoFS,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: 1,
    Time: num,
}
count++;

DataRow.push(obj);
}
});
}
        else if (loadtype == 4) {


        datatype = 0;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));


                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    totals = 0;
    count += qtdlimpo;
    totalf = count;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 5) {

        datatype = 2;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtd = arrayItem[0].replace("[", "");
    var qtdlimpo = parseInt(qtd.replace('"', ''));


                if (isNaN(qtdlimpo) == false) {

                    var num = arrayItem[1].replace(']', '');
    var numLimpo = parseInt(num.replace('"', '').replace('"', ''));

    var typefs = arrayItem[2].replace(']', '');
    var typefsLimpo = typefs.replace('"', '').replace('"', '');

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var status;

    count += qtdlimpo;

                    if (typefsLimpo == 'F') {
        totalf += qtdlimpo;
    status = 0;
}
                    else {
        totals += qtdlimpo;
    status = 1;
}


                    var obj = {
        FSstatus: status,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: 0,
    NumofForS: qtdlimpo,
    Time: numLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 6) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var end = arrayItem[1].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[2].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count++;
    totalf = count;
    totals = 0;

                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 7) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var start = arrayItem[0].replace("[", "");
    var startlimpo = parseInt(start.replace('"', ''));


                if (isNaN(startlimpo) == false) {

                    var status = arrayItem[1].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count++;
    var strStatus;
                    if (statusLimpo == 'F') {
        totalf++;
    strStatus = 0;
}
                    else {
        totals++;
    strStatus = 1;
}

                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startlimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}
        else if (loadtype == 8) {

        datatype = 1;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var end = arrayItem[2].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[3].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    count += qtdelimpo;
    totals = 0;
    totalf += qtdelimpo;


                    var obj = {
        FSstatus: 0,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: 1,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}

        else if (loadtype == 9) {

        datatype = 3;
    arrayJson.forEach(function (item) {

                var itemstr = JSON.stringify(item)
    var arrayItem = [];
    arrayItem = itemstr.split(",");

    var qtde = arrayItem[0].replace("[", "");
    var qtdelimpo = parseInt(qtde.replace('"', ''));


                if (isNaN(qtdelimpo) == false) {

                    var start = arrayItem[1].replace(']', '');
    var startLimpo = parseInt(start.replace('"', '').replace('"', ''));

    var status = arrayItem[2].replace(']', '');
    var statusLimpo = status.replace('"', '').replace('"', '');

    var end = arrayItem[3].replace(']', '');
    var endLimpo = parseInt(end.replace('"', '').replace('"', ''));

    var comments = arrayItem[4].replace(']', '');
    var commentsLimpo = comments.replace('"', '').replace('"', '');

    var strStatus;
                    if (statusLimpo == 'F') {
        totalf += qtdelimpo;
    strStatus = 0;
}
                    else {
        totals += qtdelimpo;
    strStatus = 1;
}

count += qtdelimpo;



                    var obj = {
        FSstatus: strStatus,
    FailureMode: commentsLimpo,
    FailureSeverity: "string",
    LastInspectTime: startLimpo,
    NumofForS: qtdelimpo,
    Time: endLimpo,
}


DataRow.push(obj);
}
});
}


        if (DataRow.length > 1) {


        $.ajax(
            {
                url: 'https://reliabilityapi2.azurewebsites.net/LifeData/FailureRank',
                dataType: 'json',
                async: true,
                data: {
                    "DataRow": DataRow,
                    "DataType": datatype,
                    "EndTime": 0,
                    "FailureRank": "",
                    "TotalFailureRows": 0,
                    "TotalNumFailureandSuspensions": count,
                    "TotalNumFailures": totalf,
                    "TotalNumSuspensions": totals,
                    "TotalNumofRows": 0,
                    "TotalSuspensionRows": 0
                },
                type: 'POST'
            }).success(function (data) {

                FailureRank = data;

            }).fail(function () {
                MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
            });
    }
        else {
        MessageShow('Informe dois valores para efetuar a análise.');
    }

}


popuparAnalisysList(1);


    function ListarDados(id_datatype) {

        $.ajax(
            {
                url: '@Url.Action("ListarValores", "LDA")',
                dataType: 'json',
                data: {
                    "id_item": $("#cmbItemAnalise").val(),
                    "id_datatype": id_datatype
                },
                type: 'POST'
            }).success(function (data) {

                if (data != "NOVAL") {
                    var dataparse = JSON.parse('' + data + '');

                    $('#my').jexcel('setData', dataparse, false);
                }

            }).fail(function () {
                MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
            });
    }

    function popuparAnalisysList(type) {

        var strList = "";
    strDadosAnalise = "";

        strList += "<div class='widget-box'>";
        strList += "<div class='widget-header'>";
        strList += "<h4 class='widget-title'>";
        strList += "<i class='ace-icon fa fa-align-justify'></i>";
                strList += "Analysis Results";
        strList +="</h4>"
        strList += "</div >";
        strList += "<div class='widget-body'>";
        strList +="<div class='widget-main'>";
    
        if (type == 1) {

                    strList += "<div class='row'><div class='col-xs-5'>Beta:</div><div id='lblBeta' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
                strList += "<div class='space-6'></div></hr>" ;
            strList += "<div class='row'><div class='col-xs-5'>Eta:</div><div id='lblEta' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
            strList += "<div class='space-6'></div></hr>";
            strList += "<div class='row'><div class='col-xs-5'>Rho:</div><div id='lblRho' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
            strList += "<div class='space-6'></div></hr>";
            strList += "<div class='row'><div class='col-xs-5'>LKV:</div><div id='lblLKV' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
            strList += "<div class='space-6'></div></hr>";
strList += "<div class='row'><div class='col-xs-5'>Failures:</div><div id='lblFailures' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
strList += "<div class='space-6'></div></hr>";
strList += "<div class='row'><div class='col-xs-5'>Suspensions: </div><div id='lblSuspensions' class='col-xs-7' style='padding-left:1px!important;padding-right:1px;!important'></div></div>";


            //strDadosAnalise += "<b>Beta:&nbsp;</b><div id='lblBetaR'></div>";
            //strDadosAnalise += "<b>Eta:&nbsp;</b><div id='lblEtaR'></div>";
            ////strDadosAnalise += "<div class'col-xs-1'><b>Rho:&nbsp;</b></div><div id='lblRhoR' class='col-xs-1'></div>";
            ////strDadosAnalise += "<div class'col-xs-1'><b>LKV:&nbsp;</b></div><div id='lblLKVR' class='col-xs-1'></div>";
            ////strDadosAnalise += "<div class'col-xs-1'><b>Failures:&nbsp;</b></div><div id='lblFailuresR' class='col-xs-1'></div>";
            ////strDadosAnalise += "<div class'col-xs-1'><b>Suspensions:&nbsp;</b></div><div id='lblSuspensionsR' class='col-xs-1'></div>";


        }
        else if (type == 2) {
    strList += "<div class='row'><div class='col-xs-5'>Lambda:</div><div id='lblLambda' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Rho:</div><div id='lblRho' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>LKV:</div><div id='lblLKV' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Failures:</div><div id='lblFailures' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Suspensions</div><div id='lblSuspensions' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
}
else if (type == 3) {
    strList += "<div class='row'><div class='col-xs-5'>Mu:</div><div id='lblMu' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Sigma:</div><div id='lblSigma' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Rho:</div><div id='lblRho' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>LKV:</div><div id='lblLKV' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Failures:</div><div id='lblFailures' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Suspensions</div><div id='lblSuspensions' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
}
else if (type == 4) {
    strList += "<div class='row'><div class='col-xs-5'>LogMu:</div><div id='lblLogMu' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>LogSigma:</div><div id='lblLogSigma' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Rho:</div><div id='lblRho' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>LKV:</div><div id='lblLKV' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Failures:</div><div id='lblFailures' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
    strList += "<div class='space-6'></div></hr>";
    strList += "<div class='row'><div class='col-xs-5'>Suspensions</div><div id='lblSuspensions' class='col-xs-7' style='padding-left:0px!important;padding-right:0px;!important'></div></div>";
}

strList += "</div>";
strList += "</div>";
strList += "</div>";


document.getElementById("conteudoListaAnalysis").innerHTML = strList;


        //document.getElementById("reportPlot").innerHTML = strDadosAnalise;


    }

function VeficarRadions() {
    if (document.getElementById('rdnprobability').checked) {
        $('#rdnsuspension').attr('disabled', true)
        $('#rdngrouped').attr('disabled', true)
        $('#intervalTime').attr('disabled', true)
        $('#exactTime').attr('disabled', true)
    }
    else {
        $('#rdnsuspension').attr('disabled', false)
        $('#rdngrouped').attr('disabled', false)
        $('#intervalTime').attr('disabled', false)
        $('#exactTime').attr('disabled', false)
    }
}


function getData(numcolunas) {

    var data = [];
    if (numcolunas === 2) {
        for (i = 0; i < 100; i++) {
            data.push(['', '']);
        }
    }
    else if (numcolunas === 3) {
        for (i = 0; i < 100; i++) {
            data.push(['', '', '']);
        }
    }
    else if (numcolunas === 4) {
        for (i = 0; i < 100; i++) {
            data.push(['', '', '', '']);
        }
    }
    else if (numcolunas === 5) {
        for (i = 0; i < 100; i++) {
            data.push(['', '', '', '', '']);
        }
    }
    return data;
}

loadType(3);
document.getElementById('exactTime').checked = true;

function loadType(type) {
    if (type == 1) {
        $('#my').jexcel({
            data: getData(2),
            colHeaders: ['X - axis (hr)', 'Y - axis (hr)', 'Comments'],
            colWidths: ['25%', '25%', '50%'],
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            tableWidth: '100%',
            columns: [
                { type: 'number' },
                { type: 'number' },
                { type: 'text' },
            ]
        });

        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0 || col == 1) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }
            }
        });

        ListarDados(1);
    }
    else if (type == 2) {
        var update = function (obj, cel, val) {

        }
        $('#my').jexcel({
            data: getData(3),
            colHeaders: ['Time Failure Suspension (hr)', 'Status (F/S)', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Comments'],
            colWidths: ['30%', '20%', '50%'],
            onchange: update,
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            columns: [
                { type: 'number' },
                { type: 'text' },
                { type: 'text' },
            ]
        });

        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }
                if (col == 1) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if ($(cell).text() === 'F' || $(cell).text() === 'S' || $(cell).text() === 's' || $(cell).text() === 'f' || $(cell).text() === '') {
                        if ($(cell).text() === 'f') {
                            $(cell).html('F');
                        }
                        else if ($(cell).text() === 's') {
                            $(cell).html('S');
                        }
                        else {
                            $(cell).html($(cell).text());
                        }
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_somenteFS');
                    }

                }
            }
        });

        ListarDados(2);
    }
    else if (type == 3) {
        $('#my').jexcel({
            data: getData(2),
            colHeaders: ['Time Failed (hr)', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Comments'],
            colWidths: ['30%', '70%'],
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            columns: [
                { type: 'number' },
                { type: 'text' },
            ]
        });

        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }
            }
        });


        ListarDados(3);


    }
    else if (type == 4) {
        $('#my').jexcel({
            data: getData(3),
            colHeaders: ['@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Qtd', 'Time Failed (hr)', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Comments'],
            colWidths: ['10%', '30%', '60%'],
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            columns: [
                { type: 'number' },
                { type: 'number' },
                { type: 'text' },
            ]
        });

        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0 || col == 1) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }
            }
        });

        ListarDados(4);
    }
    else if (type == 5) {
        $('#my').jexcel({
            data: getData(4),
            colHeaders: ['@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Qtd', 'Time Failure/Suspension (hr)', 'Status', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Comments'],
            colWidths: ['10%', '20%', '20%', '50%'],
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            columns: [
                { type: 'number' },
                { type: 'number' },
                { type: 'text' },
                { type: 'text' },
            ]
        });

        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0 || col == 1) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }
                if (col == 2) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if ($(cell).text() === 'F' || $(cell).text() === 'S' || $(cell).text() === 's' || $(cell).text() === 'f' || $(cell).text() === '') {
                        if ($(cell).text() === 'f') {
                            $(cell).html('F');
                        }
                        else if ($(cell).text() === 's') {
                            $(cell).html('S');
                        }
                        else {
                            $(cell).html($(cell).text());
                        }
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_somenteFS');
                    }

                }
            }
        });

        ListarDados(5);
    }
    else if (type == 6) {
        $('#my').jexcel({
            data: getData(3),
            colHeaders: ['@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Start', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_End', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Comments'],
            colWidths: ['15%', '15%', '70%'],
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            columns: [
                { type: 'number' },
                { type: 'number' },
                { type: 'text' },
            ]
        });

        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0 || col == 1) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }

            }
        });

        ListarDados(6);
    }
    else if (type == 7) {
        $('#my').jexcel({
            data: getData(4),
            colHeaders: ['@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Start', 'Status', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_End', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Comments'],
            colWidths: ['15%', '10%', '15%', '60%'],
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            columns: [
                { type: 'number' },
                { type: 'text' },
                { type: 'number' },
                { type: 'text' },
            ]
        });
        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0 || col == 2) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }
                if (col == 1) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if ($(cell).text() === 'F' || $(cell).text() === 'S' || $(cell).text() === 's' || $(cell).text() === 'f' || $(cell).text() === '') {
                        if ($(cell).text() === 'f') {
                            $(cell).html('F');
                        }
                        else if ($(cell).text() === 's') {
                            $(cell).html('S');
                        }
                        else {
                            $(cell).html($(cell).text());
                        }
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_somenteFS');
                    }

                }

            }
        });

        ListarDados(7);
    }
    else if (type == 8) {
        $('#my').jexcel({
            data: getData(4),
            colHeaders: ['@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Qtd', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Start', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_End', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Comments'],
            colWidths: ['10%', '15%', '15%', '60%'],
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            columns: [
                { type: 'number' },
                { type: 'number' },
                { type: 'number' },
                { type: 'text' },
            ]
        });

        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0 || col == 2 || col == 3) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }

            }
        });

        ListarDados(8);
    }
    else if (type == 9) {
        $('#my').jexcel({
            data: getData(5),
            colHeaders: ['@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Qtd', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Start', 'Status', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_End', '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_title_Comments'],
            colWidths: ['10%', '10%', '15%', '15%', '50%'],
            csvHeaders: true,
            allowInsertColumn: false,
            columnResize: false,
            allowDeleteColumn: false,
            tableOverflow: true,
            tableHeight: '500px',
            columns: [
                { type: 'number' },
                { type: 'number' },
                { type: 'text' },
                { type: 'number' },
                { type: 'text' },
            ]
        });

        $('#my').jexcel('updateSettings', {
            table: function (instance, cell, col, row, val, id) {
                if (col == 0 || col == 1 || col == 3) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if (isnumber($(cell).text())) {
                        $(cell).html($(cell).text());
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_sonumeros');
                    }

                }
                if (col == 2) {
                    // Get text
                    txt = $(cell).text();
                    // Format text
                    if ($(cell).text() === 'F' || $(cell).text() === 'S' || $(cell).text() === 's' || $(cell).text() === 'f' || $(cell).text() === '') {
                        if ($(cell).text() === 'f') {
                            $(cell).html('F');
                        }
                        else if ($(cell).text() === 's') {
                            $(cell).html('S');
                        }
                        else {
                            $(cell).html($(cell).text());
                        }
                    }
                    else {
                        $(cell).html('');
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_somenteFS');
                    }

                }

            }
        });

        ListarDados(9);
    }
}



function VeficarRadions() {
    if (document.getElementById('rdnprobability').checked) {
        $('#rdnsuspension').attr('disabled', true)
        $('#rdngrouped').attr('disabled', true)
        $('#intervalTime').attr('disabled', true)
        $('#exactTime').attr('disabled', true)
    }
    else {
        $('#rdnsuspension').attr('disabled', false)
        $('#rdngrouped').attr('disabled', false)
        $('#intervalTime').attr('disabled', false)
        $('#exactTime').attr('disabled', false)
    }
}


function isnumber(valor) {
    if (isNaN(valor)) {
        return false;
    } else {
        return true;
    }
}

$('#download').on('click', function () {
    $('#my').jexcel('download');
});


$('#upload').on('click', function () {
    var data = $('#my').jexcel('getData');
    $('#txt').val($.csv.fromArrays(data));
});



$(document).ready(function () {
    $('a#c_button').click(function () {
        $(this).toggleClass("on");

        if ($('a#c_button').html() === "MLE") {
            $('a#c_button').html("RRX");
        }
        else if ($('a#c_button').html() === "RRX") {
            $('a#c_button').html("RRY");
        }
        else if ($('a#c_button').html() === "RRY") {
            $('a#c_button').html("MLE");
        }

    });
});

//jquery tabs
$("#tabs").tabs();


function EditarItem() {
    $("#txtItemAnalise").val("");
    $("#txtItemAnalise").val($("#cmbItemAnalise option:selected").text());

    $("#itemlda").removeClass('hide').dialog({
        resizable: false,
        width: '85%',
        modal: true,
        title: 'Editar Item',
        title_html: true,
        buttons: [
            {
                html: "&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_btn_SalvarDados",
                "class": "btn btn-primary",
                click: function () {
                    editarItem()
                    $(this).dialog("close");
                }
            },
            {
                html: "&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_btn_cancelar",
                "class": "btn btn-red",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

}



function OpenOptions() {
    $("#optionsDataType").removeClass('hide').dialog({
        resizable: false,
        width: '85%',
        heigth: '400',
        modal: true,
        title: 'Data Type - Reliability',
        title_html: true,
        buttons: [
            {
                html: "&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_pop_btnAplicar",
                "class": "btn btn-primary",
                click: function () {
                    $('#my').jexcel('destroy');
                    if (document.getElementById('rdnprobability').checked) {
                        loadType(1);
                        loadtype = 1;
                        document.getElementById('btnDataType').innerHTML = " X vs. Y";
                    }
                    else {
                        if (document.getElementById('exactTime').checked && !document.getElementById('rdnsuspension').checked && !document.getElementById('rdngrouped').checked) {
                            loadType(3);
                            loadtype = 3;
                            document.getElementById('btnDataType').innerHTML = " Exact Time - F";
                        }
                        else if (document.getElementById('exactTime').checked && document.getElementById('rdnsuspension').checked && !document.getElementById('rdngrouped').checked) {
                            loadType(2);
                            loadtype = 2;
                            document.getElementById('btnDataType').innerHTML = " Exact Time - F/S";
                        }
                        else if (document.getElementById('exactTime').checked && !document.getElementById('rdnsuspension').checked && document.getElementById('rdngrouped').checked) {
                            loadType(4);
                            loadtype = 4;
                            document.getElementById('btnDataType').innerHTML = " Exact Time - F/G";
                        }
                        else if (document.getElementById('exactTime').checked && document.getElementById('rdnsuspension').checked && document.getElementById('rdngrouped').checked) {
                            loadType(5);
                            loadtype = 5;
                            document.getElementById('btnDataType').innerHTML = " Exact Time - F/S/G";
                        }
                        else if (document.getElementById('intervalTime').checked && !document.getElementById('rdnsuspension').checked && !document.getElementById('rdngrouped').checked) {
                            loadType(6);
                            loadtype = 6;
                            document.getElementById('btnDataType').innerHTML = " Interval Time - F";
                        }
                        else if (document.getElementById('intervalTime').checked && document.getElementById('rdnsuspension').checked && !document.getElementById('rdngrouped').checked) {
                            loadType(7);
                            loadtype = 7;
                            document.getElementById('btnDataType').innerHTML = " Interval Time - F/S";
                        }
                        else if (document.getElementById('intervalTime').checked && !document.getElementById('rdnsuspension').checked && document.getElementById('rdngrouped').checked) {
                            loadType(8);
                            loadtype = 8;
                            document.getElementById('btnDataType').innerHTML = " Interval Time - F/G";
                        }
                        else if (document.getElementById('intervalTime').checked && document.getElementById('rdnsuspension').checked && document.getElementById('rdngrouped').checked) {
                            loadType(9);
                            loadtype = 9;
                            document.getElementById('btnDataType').innerHTML = " Interval Time - F/S/G";
                        }

                    }
                    $(this).dialog("close");
                }
            },
            {
                html: "&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_btn_cancelar",
                "class": "btn btn-red",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function NovoItem() {

    $("#txtItemAnalise").val("");

    $("#itemlda").removeClass('hide').dialog({
        resizable: false,
        width: '85%',
        modal: true,
        title: 'Criar Item',
        title_html: true,
        buttons: [
            {
                html: "&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_btn_SalvarDados",
                "class": "btn btn-primary",
                click: function () {
                    salvarItem();
                    $(this).dialog("close");
                }
            },
            {
                html: "&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_btn_cancelar",
                "class": "btn btn-red",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function salvarItem() {

    $.ajax(
        {
            url: '@Url.Action("SalvarItem", "LDA")',
            dataType: 'json',
            data: {
                "nm_item": $("#txtItemAnalise").val()
            },
            type: 'POST'
        }).success(function (data) {
            if (data != "OK") {
                MessageShow(data);
            }
            atualizarComboItem();
        }).fail(function () {
            MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
        });
}



function excluir() {
    if ($("#cmbItemAnalise option:selected").text() != "") {
        document.getElementById("msgexcluir").innerHTML = '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_excluir_item :  ' + $("#cmbItemAnalise option:selected").text() + ' ';
        $("#dialog-confirm").removeClass('hide').dialog({
            resizable: false,
            width: '320',
            modal: true,
            title: 'Excluir',
            title_html: true,
            buttons: [
                {
                    html: "<i class='ace-icon fa fa-trash-o bigger-110'></i>&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.Usuario_btn_excluir",
                    "class": "btn btn-danger btn-minier",
                    click: function () {
                        ExcluirItem()
                        $(this).dialog("close");
                    }
                }
                ,
                {
                    html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.Usuario_btn_cancelar",
                    "class": "btn btn-minier",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });
    }
    else {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_selecione_excluir');
    }
}




function OpenConfidenceBounds() {
    $("#popConfidenceBounds").removeClass('hide').dialog({
        resizable: false,
        width: '85%',
        height: '500',
        modal: true,
        title: 'Confidence Bounds',
        title_html: true,
        buttons: [
            {
                html: "&nbsp; OK",
                "class": "btn btn-primary btn-minier",
                click: function () {
                    carregarImagemPLOT();
                    $(this).dialog("close");
                }
            }
            ,
            {
                html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.Usuario_btn_cancelar",
                "class": "btn btn-minier",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}




function OpenCalculadora() {
    $("#popCalculadora").removeClass('hide').dialog({
        resizable: false,
        width: '90%',
        height: '570',
        modal: true,
        title: 'Calculator',
        title_html: true,
        buttons: [
            {
                html: "&nbsp; OK",
                "class": "btn btn-primary btn-minier",
                click: function () {
                    $(this).dialog("close");
                }
            }
            ,
            {
                html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.Usuario_btn_cancelar",
                "class": "btn btn-minier",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}


async function OpenBestFit() {


    StarProgress();

    await sleep(1000);

    document.getElementById("divBestFit").innerHTML = "";

    setTimeout(popularBestFit(), 5000);

    var strBest = "<table width='100%'>";
    strBest += "<th style='border:solid'><center><b>Distribution</b></center></th>";
    strBest += "<th style='border:solid'><center><b> LKV Value </b></center> </th>";
    strBest += "<th style='border:solid'><center><b> Rank Value </b></center> </th>";


    DataBestFit.sort((a, b) => (a.lkv < b.lkv) ? 1 : ((b.lkv < a.lkv) ? -1 : 0));
    var index = 1;
    DataBestFit.forEach(function (item) {
        if (index == 1) {
            strBest += "<tr style='border:solid'><td style='border:solid;border-size:1px;background-color:#5fba7d'> " + item.distribution + " </td><td style='border:solid;border-size:1px;background-color:#5fba7d'>" + item.lkv + "</td><td style='border:solid;border-size:1px;background-color:#5fba7d'><center> " + index + " </center></td></tr>";
            distributionbestval = item.value;
        }
        else {
            strBest += "<tr style='border:solid'><td style='border:solid';border-size:1px> " + item.distribution + " </td><td>" + item.lkv + "</td><td style='border:solid;border-size:1px'><center> " + index + " </center></td></tr>";
        }
        index++;
    });
    strBest += "</table>";

    document.getElementById("divBestFit").innerHTML = strBest;

    $("#popBestFit").removeClass('hide').dialog({
        resizable: false,
        width: '85%',
        height: '350',
        modal: true,
        title: '@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_pop_DistribuitionBestFit',
        title_html: true,
        buttons: [
            {
                html: "&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_pop_btnAplicar",
                "class": "btn btn-primary btn-minier",
                click: function () {

                    $("#cmbDistribution").val(distributionbestval);

                    popuparAnalisysList(distributionbestval);

                    plotMLETwoParameters();

                    $(this).dialog("close");
                }
            }
            ,
            {
                html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; @Compass.Reliability.WebApp.Resources.ResourceForAll.Usuario_btn_cancelar",
                "class": "btn btn-minier",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

    EndProgress();


}

function ExcluirItem() {

    if ($('#cmbItemAnalise').val() != undefined || $('#cmbItemAnalise').val() != "" || $('#cmbItemAnalise').val() != "0") {
        $.ajax(
            {
                url: '@Url.Action("ExcluirItem", "LDA")',
                dataType: 'json',
                data: {
                    "id": $("#cmbItemAnalise").val()
                },
                type: 'POST'
            }).success(function (data) {
                if (data == "OK") {
                    MessageShow("@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_item_excluido");
                }
                else {
                    MessageShow(data);
                }
                atualizarComboItem();
            }).fail(function () {
                MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
            });

    }
    else {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_selecione_excluir');
    }
}

function editarItem() {

    if ($('#cmbItemAnalise').val() != undefined || $('#cmbItemAnalise').val() != "" || $('#cmbItemAnalise').val() != "0") {

        $.ajax(
            {
                url: '@Url.Action("EditarItem", "LDA")',
                dataType: 'json',
                data: {
                    "nm_item": $("#txtItemAnalise").val(),
                    "id": $("#cmbItemAnalise").val()
                },
                type: 'POST'
            }).success(function (data) {
                if (data != "OK") {
                    MessageShow(data);
                }
                atualizarComboItem();
            }).fail(function () {
                MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
            });

    }
    else {
        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.LDACE_msg_item_editar');
    }
}

function atualizarComboItem() {

    $('#cmbItemAnalise').find('option').remove().end();

    $.ajax(
        {
            url: '@Url.Action("AtualizarComboAnalise", "LDA")',
            dataType: 'json',
            type: 'POST'
        }).success(function (data) {

            var options = '';
            data.forEach(function (item) {
                options += '<option value=' + item.id + '>' + item.nome + '</option>';
            });

            $('#cmbItemAnalise').append(options);

        }).fail(function () {
            MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
        });
}


jQuery(function ($) {

    //editables on first profile page
    $.fn.editable.defaults.mode = 'inline';
    $.fn.editableform.loading = "<div class='editableform-loading'><i class='ace-icon fa fa-spinner fa-spin fa-2x light-blue'></i></div>";
    $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="ace-icon fa fa-check"></i></button>' +
        '<button type="button" class="btn editable-cancel"><i class="ace-icon fa fa-times"></i></button>';

    //editables

    //text editable

    $('#ldaname')
        .editable({
            type: 'text',
            name: 'ldaname',
            validate: function (value) {
                $.ajax(
                    {
                        url: '@Url.Action("AlterarLDA", "LDA")',
                        dataType: 'json',
                        data: {
                            "nomelda": value
                        },
                        type: 'POST'
                    }).success(function (data) {
                        if (data != "OK") {
                            MessageShow(data);
                        }
                    }).fail(function () {
                        MessageShow('@Compass.Reliability.WebApp.Resources.ResourceForAll.Login_msg_erro')
                    });
            }
        });


    ////////////////////
    //change profile
    $('[data-toggle="buttons"] .btn').on('click', function (e) {
        var target = $(this).find('input[type=radio]');
        var which = parseInt(target.val());
        $('.user-profile').parent().addClass('hide');
        $('#user-profile-' + which).parent().removeClass('hide');
    });



    /////////////////////////////////////
    $(document).one('ajaxloadstart.page', function (e) {
        //in ajax mode, remove remaining elements before leaving page
        try {
            $('.editable').editable('destroy');
        } catch (e) { }
        $('[class*=select2]').remove();
    });
});
