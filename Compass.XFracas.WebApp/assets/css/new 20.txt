	.navbar-default .navbar-nav>li>a {
    color: #000!important;
}.navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover {
    color: #FFF !important;
    background-color: #333!important;
}.navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
    color: #FFF!important;
    
}
.navbar-default .navbar-nav .open .dropdown-menu>li>a {
    color: #FFF!important;
}

.navbar .navbar-nav .dropdown-menu {
    float: none;
    position: relative;
    background-color: #333;
    box-shadow: none;
    border-width: 0;
}