﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class ReflectionPopulator<T>
    {
        public virtual List<T> CreateList(IDataReader reader)
        {
            var results = new List<T>();
            var properties = typeof(T).GetProperties();


            while (reader.Read())
            {
                var item = Activator.CreateInstance<T>();
                foreach (var property in typeof(T).GetProperties())
                {
                    try
                    {
                        reader[property.Name].ToString();
                        if (!reader.IsDBNull(reader.GetOrdinal(property.Name)))
                        {
                            Type convertTo = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                            property.SetValue(item, Convert.ChangeType(reader[property.Name], convertTo), null);
                        }
                    }
                    catch { }
                    
                }
                results.Add(item);
            }
            return results;
        }
    }
}
