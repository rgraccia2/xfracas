﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Transactions;


namespace Compass.XFracas.WebGerenciadorDAL  
{
    public class LogDAL : DataWorker
    {

        public List<LogModel> GetLog()
        {
            try
            {
                var listReturn = new List<LogModel>();
                string SQLstring = "select * from TB_Log";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<LogModel>().CreateList(reader);
                        }
                    }
                }
                return listReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertLog(LogModel Log)
        {
            try
            {
                string SQLstring;
                SQLstring = "Insert into TB_Log ( \"DATE\", GUIID, APPLICATION_CODE, \"TYPE\", MESSAGE, \"METHOD\", CHECK_POINT )";
                SQLstring += " VALUES (";
                SQLstring += "SYSDATE, ";
                SQLstring += "'" + Log.GuiID + "', ";
                SQLstring += "'" + Log.Application_Code + "', ";
                SQLstring += "'" + Log.Type + "', ";
                SQLstring += "'" + Log.Message + "', ";
                SQLstring += "'" + Log.Method + "', ";
                SQLstring += Log.Check_Point + ") ";


                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand())
                    {
                        try
                        {
                            using (TransactionScope scope = new TransactionScope())
                            {
                                using (IDbCommand commandText = database.CreateCommand(SQLstring, connection))
                                {
                                    commandText.ExecuteNonQuery();
                                }
                                scope.Complete();
                            }

                        }
                        catch (Exception ex)
                        {
                            LogError.Log(ex.Message, "Erro ao registrar logs" + "///" + command.CommandText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
