﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class PartTreeDAL: DataWorker
    {

        public List<PartTreeModel> getPartTreeFromNode(int entity_id, int id = -1)
        {
            var result = new List<PartTreeModel>();

            // nivel 1 com nivel 2 dummy
            string SQLstring;
            if (id == -1)
            {
                // nível raíz
                SQLstring = "SELECT '/' || p.part_id  as PATH," + 
                    " p.part_number || ':' || part_name  as PART_NUMBER," + 
                    " P.PART_ID AS ID," + " NULL as PARENT_ID," + 
                    " p.part_version as part_version" + " from part p" + 
                    " inner join parthierarchy ph on ph.part_id = p.part_id" + 
                    " where ph.entity_id = " + entity_id + " and ph.lvl = 1" + 
                    " and p.retired_dt is null" + " ORDER BY p.part_number";
            }
            else
            {
                // outros níveis
                SQLstring = "SELECT '/' || PHPARENT.PART_ID || '/' || P.PART_ID  AS PATH," + 
                    " p.part_number || ':' || part_name  as PART_NUMBER," + 
                    " P.PART_ID AS ID," + " PHPARENT.PART_ID as PARENT_ID," + 
                    " p.part_version as part_version" + " from part p" + 
                    " inner join parthierarchy ph on ph.part_id = p.part_id" + 
                    " inner join parthierarchy phparent on phparent.hid = ph.parent_hid" + 
                    " where ph.entity_id = " + entity_id + " and phparent.part_id = " + id + " and ph.lvl > 1" + 
                    " and p.retired_dt is null" + 
                    " ORDER BY p.part_number";
            }

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        result = new ReflectionPopulator<PartTreeModel>().CreateList(reader);
                    }
                }
            }
            return result;
        }

        public List<PartTreeModel> searchPartTree(string searchString, int entity_id)
        {
            var v = searchString.Split(' ');
            searchString = string.Join("%", v);
            var result = new List<PartTreeModel>();

            string SQLstring = "select p.part_id || '/' as PATH,        " + 
                "               p.part_number || ':' || part_name  as PART_NUMBER, " + 
                "               p.part_id                          as ID,          " + 
                "               p.part_id                    as PARENT_ID,   " + 
                "               p.part_version               as part_version " + 
                "          from part p,                                            " + 
                "               parthierarchy ph                                   " + 
                "         where P.PART_ID = PH.PART_ID                   " + 
                "         and ph.entity_id = " + entity_id + "                   " + 
                "  and (part_number like upper('%" + searchString + "%')" + 
                "  or part_name like upper('%" + searchString + "%'))" + 
                "  and p.retired_dt is null";


            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        result = new ReflectionPopulator<PartTreeModel>().CreateList(reader);
                    }
                }
            }
            return result;
        }

        public List<PartTreeModel> searchPartTree(string partName, string partVersion, string partNumber)
        {
            var result = new List<PartTreeModel>();
            string SQLstring;
            SQLstring = "SELECT * FROM PART " + " WHERE  UPPER(part_name) = '" + partName.ToUpper() + "'" + 
                "   AND (UPPER(part_version)= '" + partVersion.ToUpper() + "' OR part_version IS NULL)" + 
                "   AND (UPPER(part_number) = '" + partNumber.ToUpper() + "' OR part_number IS NULL)" + 
                "   AND retired_dt is null";


            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        result = new ReflectionPopulator<PartTreeModel>().CreateList(reader);
                    }
                }
            }
            return result;
        }

    }
}
