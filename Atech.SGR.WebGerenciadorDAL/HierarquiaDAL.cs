﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class HierarquiaDAL : DataWorker
    {

        public List<HierarquiaModel> GetPrimeiroNivel(DateTime dataInicial, string limitSystemHierarchyTo)
        {

            List<HierarquiaModel> listReturn = new List<HierarquiaModel>();
            // incluida condicao para ler hierarquia independente de data caso seja escolhido uma hieraquia
            if (limitSystemHierarchyTo != "")
                {
                    dataInicial = Convert.ToDateTime("2010-01-01");
                }

                string SQLstring = " SELECT CASE substr(COD_LOCALINSTALACAO,1,4) " + "   WHEN 'EFVM' THEN '3'" + "   WHEN 'EFCJ' THEN '3'" + "   WHEN 'EFNS' THEN '3'" + "   WHEN 'PAG-' THEN '3'" + "   WHEN 'EVM-' THEN '3'" + "   WHEN 'VMVO' THEN '3'" + "   WHEN 'SDC1' THEN '4'" + "   WHEN 'SPP1' THEN '4'" + "   WHEN 'TRMT' THEN '5'" + "   ELSE '2'" + "   END AS COD_ENTIDADE, " + " NVL(SUBSTR(COD_LOCALINSTALACAO, 1, INSTR(COD_LOCALINSTALACAO,'-')-1),COD_LOCALINSTALACAO) as COD_LOCALINSTALACAO " + "   FROM RDS_N0AXFRAV " + " WHERE " + "   (   DATA_ULTIMAATUALIZACAO > to_date('" + dataInicial.ToString("dd/MM/yyyy HH:mm:ss") + "','dd/mm/rrrr hh24:mi:ss') " + "  OR DATA_CRIACAO > to_date('" + dataInicial.ToString("dd/MM/yyyy HH:mm:ss") + "','dd/mm/rrrr hh24:mi:ss') " + ")  @limitSystemHierarchyTo@" + "  GROUP BY CASE substr(COD_LOCALINSTALACAO,1,4) " + "   WHEN 'EFVM' THEN '3'" + "   WHEN 'EFCJ' THEN '3'" + "   WHEN 'EFNS' THEN '3'" + "   WHEN 'PAG-' THEN '3'" + "   WHEN 'EVM-' THEN '3'" + "   WHEN 'VMVO' THEN '3'" + "   WHEN 'SDC1' THEN '4'" + "   WHEN 'SPP1' THEN '4'" + "   WHEN 'TRMT' THEN '5'" + "   ELSE '2'" + "   END, NVL(SUBSTR(COD_LOCALINSTALACAO, 1, INSTR(COD_LOCALINSTALACAO,'-')-1),COD_LOCALINSTALACAO)" + "  ORDER BY 1, 2";
                string[] arrHierarchy;
                if (limitSystemHierarchyTo == "")
                {
                    limitSystemHierarchyTo = "'EFVM','FECJ','DMSL','AABO','CCMT','NARJ','CCPX','COSB','COSG','EFCJ','EFNS','PAG-','ERVM','EVM-','VMVO','FEAG','FEAP','FEBR','FEIT','FCJA','FECG','FECL','FEFB','FEFJ','FEGA','FEGS','FEMN','GMEP','JJGD','MMAC','MMAZ','NIOP','MMUT','NAES','POBS','PECG','PPIC','POIG','POMA','POPD','POPM','POSA','POTU','PEVG','PEVT','TTAM','TTFA','TTOD','VVGR','FEAG','FEAP','FEBR','FEIT','FEGS','FEMN','POBS','POIG','POSA','FEKS','SDC1','SPP1','TRMT'";
                    arrHierarchy = limitSystemHierarchyTo.Split(',');
                    for (int i = 0, loopTo = arrHierarchy.Count() - 1; i <= loopTo; i++)
                    {
                        arrHierarchy[i] = arrHierarchy[i].Trim().ToUpper();
                        arrHierarchy[i] = "'" + arrHierarchy[i] + "'";
                    }
                }
                else
                {
                    arrHierarchy = limitSystemHierarchyTo.Split(',');
                    for (int i = 0, loopTo1 = arrHierarchy.Count() - 1; i <= loopTo1; i++)
                    {
                        arrHierarchy[i] = arrHierarchy[i].Trim().ToUpper();
                        arrHierarchy[i] = "'" + arrHierarchy[i] + "'";
                    }
                }

                SQLstring = SQLstring.Replace("@limitSystemHierarchyTo@", " AND NVL(SUBSTR(COD_LOCALINSTALACAO, 1, INSTR(COD_LOCALINSTALACAO,'-')-1),'') in (" + string.Join(",", arrHierarchy) + ") ");

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<HierarquiaModel>().CreateList(reader);
                        }
                    }
                }
            return listReturn;

        }

        public List<HierarquiaModel> GetPrimeiroNivelItem(string partNumber)
        {
            try
            {
                var listReturn = new List<HierarquiaModel>();
                string SQLstring = " SELECT COD_ENTIDADE,NVL(SUBSTR(COD_LOCALINSTALACAO, 1, INSTR(COD_LOCALINSTALACAO,'-')-1),COD_LOCALINSTALACAO) as COD_LOCALINSTALACAO " + "  FROM RDS_N0AXFRAV " + " WHERE COD_LOCALINSTALACAO = '" + partNumber.Substring(1, partNumber.IndexOf("-") - 1) + "'";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<HierarquiaModel>().CreateList(reader);
                        }
                    }
                }
                return listReturn;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<HierarquiaModel> GetHierarquia(string entidade, string cod_local)
        {
            try
            {
                var listReturn = new List<HierarquiaModel>();
                string SQLstring = "SELECT DISTINCT" + " CASE substr(COD_LOCALINSTALACAO,1,4) " + "   WHEN 'EFVM' THEN '3'" + "   WHEN 'EFCJ' THEN '3'" + "   WHEN 'EFNS' THEN '3'" + "   WHEN 'PAG-' THEN '3'" + "   WHEN 'EVM-' THEN '3'" + "   WHEN 'VMVO' THEN '3'" + "   WHEN 'SDC1' THEN '4'" + "   WHEN 'SPP1' THEN '4'" + "   WHEN 'TRMT' THEN '5'" + "   ELSE '2'" + "   END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4)" + "   WHEN 'EFVM' THEN 'FERROVIA'" + "   WHEN 'EFCJ' THEN 'FERROVIA'" + "   WHEN 'EFNS' THEN 'FERROVIA'" + "   WHEN 'PAG-' THEN 'FERROVIA'" + "   WHEN 'EVM-' THEN 'FERROVIA'" + "   WHEN 'VMVO' THEN 'FERROVIA'" + "   WHEN 'SDC1' THEN 'OMAN'" + "   WHEN 'SPP1' THEN 'OMAN'" + "   WHEN 'TRMT' THEN 'MALAYSIA'" + "   ELSE 'VALE'" + "   END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP," + " COD_CENTROPLANEJAMENTO," + " COD_LOCALINSTALACAO," + " DESC_LOCALINSTALACAO," + " COD_EQUIPAMENTO," + " DESC_EQUIPAMENTO," + " DATA_ULTIMAATUALIZACAO," + " DATA_CRIACAO" + " FROM RDS_N0AXFRAV" + " WHERE SUBSTR(COD_LOCALINSTALACAO,1,4)='" + cod_local + "' " + " ORDER BY 1, COD_LOCALINSTALACAO, COD_EQUIPAMENTO";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<HierarquiaModel>().CreateList(reader);
                        }
                    }
                }
                return listReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<HierarquiaModel> GetHierarquiaItem(bool temRDS, string entidade, string localInstalacao, string cod_item, string desc_item, string cod_versao)
        {
            try
            {
                var listReturn = new List<HierarquiaModel>();
                string SQLstring;
                if (cod_versao == "E")
                {
                    SQLstring = "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,1)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,2)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,3)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,4)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,5)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,6)-1) UNION ";
                    // COD_ENTIDADE='" & entidade & "' AND
                    if (temRDS)
                    {
                        SQLstring += "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_EQUIPAMENTO='" + cod_item + "' AND UPPER(DESC_EQUIPAMENTO)='" + desc_item.ToUpper().Replace("'", "''") + "' AND COD_IDENTIFLOCALEQUIP='" + cod_versao + "' ";
                    }
                    // COD_ENTIDADE='" & entidade & "' AND
                    else
                    {
                        SQLstring += "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + "'" + cod_versao + "' AS COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, '" + cod_item + "' AS COD_EQUIPAMENTO, '" + desc_item + "' AS DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,1)-1) ";
                        // COD_ENTIDADE='" & entidade & "' AND
                    }

                    SQLstring += "ORDER BY 3 DESC, 5 ASC";
                }
                else
                {
                    SQLstring = "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,1)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,2)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,3)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,4)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,5)-1) UNION " + "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + localInstalacao + "',1,INSTR('" + localInstalacao + "','-',1,6)-1) UNION ";
                    // COD_ENTIDADE='" & entidade & "' AND
                    if (temRDS)
                    {
                        SQLstring += "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, COD_LOCALINSTALACAO, DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO " + " FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_ENTIDADE='" + entidade + "' AND COD_LOCALINSTALACAO='" + cod_item + "' AND UPPER(DESC_LOCALINSTALACAO)='" + desc_item.ToUpper().Replace("'", "''") + "' AND COD_IDENTIFLOCALEQUIP='" + cod_versao + "' ";
                    }
                    // COD_ENTIDADE='" & entidade & "' AND
                    else
                    {
                        SQLstring += "SELECT CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN '3' WHEN 'EFCJ' THEN '3' WHEN 'EFNS' THEN '3' WHEN 'PAG-' THEN '3' WHEN 'EVM-' THEN '3' WHEN 'VMVO' THEN '3' WHEN 'SDC1' THEN '4' WHEN 'SPP1' THEN '4' WHEN 'TRMT' THEN '5' ELSE '2' END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4) WHEN 'EFVM' THEN 'FERROVIA' WHEN 'EFCJ' THEN 'FERROVIA' WHEN 'EFNS' THEN 'FERROVIA' WHEN 'PAG-' THEN 'FERROVIA' WHEN 'EVM-' THEN 'FERROVIA' WHEN 'VMVO' THEN 'FERROVIA' WHEN 'SDC1' THEN 'OMAN' WHEN 'SPP1' THEN 'OMAN' WHEN 'TRMT' THEN 'MALAYSIA' ELSE 'VALE' END AS DESC_ENTIDADE," + "'" + cod_versao + "' AS COD_IDENTIFLOCALEQUIP, COD_CENTROPLANEJAMENTO, '" + cod_item + "' AS COD_LOCALINSTALACAO, '" + desc_item + "' AS DESC_LOCALINSTALACAO, COD_EQUIPAMENTO, DESC_EQUIPAMENTO, DATA_ULTIMAATUALIZACAO, DATA_CRIACAO FROM RDS_N0AXFRAV WHERE ROWNUM=1 AND COD_LOCALINSTALACAO=SUBSTR('" + cod_item + "',1,INSTR('" + cod_item + "','-',1,1)-1) ";
                        // COD_ENTIDADE='" & entidade & "' AND
                    }

                    SQLstring += "ORDER BY 3 DESC, 5 ASC";
                }

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<HierarquiaModel>().CreateList(reader);
                        }
                    }
                }
                return listReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<HierarquiaModel> GetHierarquiaQuebra(string entidade, string cod_local, string nomeQuebra)
        {
            try
            {
                var listReturn = new List<HierarquiaModel>();
                string SQLstring;
                if (string.IsNullOrEmpty(nomeQuebra))
                {
                    SQLstring = "SELECT DISTINCT" + " CASE substr(COD_LOCALINSTALACAO,1,4) " + "   WHEN 'EFVM' THEN '3'" + "   WHEN 'EFCJ' THEN '3'" + "   WHEN 'EFNS' THEN '3'" + "   WHEN 'PAG-' THEN '3'" + "   WHEN 'EVM-' THEN '3'" + "   WHEN 'VMVO' THEN '3'" + "   WHEN 'SDC1' THEN '4'" + "   WHEN 'SPP1' THEN '4'" + "   WHEN 'TRMT' THEN '5'" + "   ELSE '2'" + "   END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4)" + "   WHEN 'EFVM' THEN 'FERROVIA'" + "   WHEN 'EFCJ' THEN 'FERROVIA'" + "   WHEN 'EFNS' THEN 'FERROVIA'" + "   WHEN 'PAG-' THEN 'FERROVIA'" + "   WHEN 'EVM-' THEN 'FERROVIA'" + "   WHEN 'VMVO' THEN 'FERROVIA'" + "   WHEN 'SDC1' THEN 'OMAN'" + "   WHEN 'SPP1' THEN 'OMAN'" + "   WHEN 'TRMT' THEN 'MALAYSIA'" + "   ELSE 'VALE'" + "   END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP," + " COD_CENTROPLANEJAMENTO," + " COD_LOCALINSTALACAO," + " DESC_LOCALINSTALACAO," + " COD_EQUIPAMENTO," + " DESC_EQUIPAMENTO," + " DATA_ULTIMAATUALIZACAO," + " DATA_CRIACAO" + " FROM RDS_N0AXFRAV" + " WHERE SUBSTR(COD_LOCALINSTALACAO,1,4)='" + cod_local + "' " + " ORDER BY 1, COD_LOCALINSTALACAO, COD_EQUIPAMENTO";
                }
                else
                {
                    SQLstring = "SELECT DISTINCT" + " CASE substr(COD_LOCALINSTALACAO,1,4) " + "   WHEN 'EFVM' THEN '3'" + "   WHEN 'EFCJ' THEN '3'" + "   WHEN 'EFNS' THEN '3'" + "   WHEN 'PAG-' THEN '3'" + "   WHEN 'EVM-' THEN '3'" + "   WHEN 'VMVO' THEN '3'" + "   WHEN 'SDC1' THEN '4'" + "   WHEN 'SPP1' THEN '4'" + "   WHEN 'TRMT' THEN '5'" + "   ELSE '2'" + "   END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4)" + "   WHEN 'EFVM' THEN 'FERROVIA'" + "   WHEN 'EFCJ' THEN 'FERROVIA'" + "   WHEN 'EFNS' THEN 'FERROVIA'" + "   WHEN 'PAG-' THEN 'FERROVIA'" + "   WHEN 'EVM-' THEN 'FERROVIA'" + "   WHEN 'VMVO' THEN 'FERROVIA'" + "   WHEN 'SDC1' THEN 'OMAN'" + "   WHEN 'SPP1' THEN 'OMAN'" + "   WHEN 'TRMT' THEN 'MALAYSIA'" + "   ELSE 'VALE'" + "   END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP," + " COD_CENTROPLANEJAMENTO," + " COD_LOCALINSTALACAO," + " DESC_LOCALINSTALACAO," + " COD_EQUIPAMENTO," + " DESC_EQUIPAMENTO," + " DATA_ULTIMAATUALIZACAO," + " DATA_CRIACAO" + " FROM RDS_N0AXFRAV" + " WHERE COD_LOCALINSTALACAO='" + cod_local + "' ";
                    SQLstring += " UNION ALL";
                    SQLstring += " SELECT DISTINCT" + " CASE substr(COD_LOCALINSTALACAO,1,4) " + "   WHEN 'EFVM' THEN '3'" + "   WHEN 'EFCJ' THEN '3'" + "   WHEN 'EFNS' THEN '3'" + "   WHEN 'PAG-' THEN '3'" + "   WHEN 'EVM-' THEN '3'" + "   WHEN 'VMVO' THEN '3'" + "   WHEN 'SDC1' THEN '4'" + "   WHEN 'SPP1' THEN '4'" + "   WHEN 'TRMT' THEN '5'" + "   ELSE '2'" + "   END AS COD_ENTIDADE, " + " CASE substr(COD_LOCALINSTALACAO,1,4)" + "   WHEN 'EFVM' THEN 'FERROVIA'" + "   WHEN 'EFCJ' THEN 'FERROVIA'" + "   WHEN 'EFNS' THEN 'FERROVIA'" + "   WHEN 'PAG-' THEN 'FERROVIA'" + "   WHEN 'EVM-' THEN 'FERROVIA'" + "   WHEN 'VMVO' THEN 'FERROVIA'" + "   WHEN 'SDC1' THEN 'OMAN'" + "   WHEN 'SPP1' THEN 'OMAN'" + "   WHEN 'TRMT' THEN 'MALAYSIA'" + "   ELSE 'VALE'" + "   END AS DESC_ENTIDADE," + " COD_IDENTIFLOCALEQUIP," + " COD_CENTROPLANEJAMENTO," + " COD_LOCALINSTALACAO," + " DESC_LOCALINSTALACAO," + " COD_EQUIPAMENTO," + " DESC_EQUIPAMENTO," + " DATA_ULTIMAATUALIZACAO," + " DATA_CRIACAO" + " FROM RDS_N0AXFRAV" + " WHERE SUBSTR(COD_LOCALINSTALACAO,1,8)='" + nomeQuebra + "' ";
                    SQLstring += " ORDER BY 1, COD_LOCALINSTALACAO, COD_EQUIPAMENTO";
                }

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<HierarquiaModel>().CreateList(reader);
                        }
                    }
                }
                return listReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
