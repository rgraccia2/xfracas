﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class RuleDAO : DataWorker
    {
        public   List<RuleModel> GetRules()
        {
            List<RuleModel> result = new List<RuleModel>();

            string SQLstring = "select HIERARCHY, " +
                "ENABLED, EXPRESSION, " +
                "CASE WHEN RECIDIVISM > 999999 THEN 999999 " +
                "  ELSE RECIDIVISM END AS RECIDIVISM " +
                "from tb_rule order by upper(hierarchy)";

       

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {
                   // OracleParameter param = new OracleParameter();
                    //param.OracleType = OracleType.Cursor;
                   // param.ParameterName = "io_cursor";
                   // param.Direction = ParameterDirection.Output;
                    //command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        result = new ReflectionPopulator<RuleModel>().CreateList(reader);
                    }
                }
            }

            return result;
        }

        public static void deleteAllRules()
        {
            string SQLstring = "delete from tb_rule where 1=1;commit;";
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {

                    command.ExecuteNonQuery();
                }
            } 
        }

        public static void saveRule(RuleModel rule)
        {
            if (rule.hierarchy == null)
                rule.hierarchy = "";
            if (rule.expression == null)
                rule.expression = "";
            string SQLstring = "insert into tb_rule(code,hierarchy,enabled,expression,recidivism) values ( seq_pk_tb_rule.nextval ,'" + rule.hierarchy + "','" + System.Convert.ToInt32(rule.enabled) + "','" + rule.expression.Replace("'", "''").Replace("\"", "''") + "','" + rule.recidivism + "'); commit;";
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {

                    command.ExecuteNonQuery();
                }
            }

            
        }

        public static bool checkRecurrence(int days, string part, string CodePerfilCatalogoIni, string CodeSistemIni, string CodeConjuntoIni, string CodeItemIni, string CodeProblemaIni, string CodeSolucaoIni, string PerfilCatalogoIni, string SistemIni, string ConjuntoIni, string ItemIni, string ProblemaIni, string SolucaoIni)
        {
            string SQLstring = "SELECT 1                                                                                              " + "  FROM INCIDENT                                                                                       " + " INNER JOIN INCIDENTRESPREVITEMUSER INCRESPPART                                                       " + "           ON  INCIDENT.INCIDENT_ID = INCRESPPART.INCIDENT_ID AND INCRESPPART.BCURRENT = 1            " + " INNER JOIN PART RESPPART   ON INCRESPPART.RESP_PART_ID = RESPPART.PART_ID                            " + " INNER JOIN DETAILALPHA DPE ON INCIDENT.INCIDENT_ID     = DPE.INCIDENT_ID AND DPE.DETAIL_TYPE_ID = " + CodePerfilCatalogoIni + " INNER JOIN DETAILALPHA DSI ON INCIDENT.INCIDENT_ID     = DSI.INCIDENT_ID AND DSI.DETAIL_TYPE_ID = " + CodeSistemIni + " INNER JOIN DETAILALPHA DCJ ON INCIDENT.INCIDENT_ID     = DCJ.INCIDENT_ID AND DCJ.DETAIL_TYPE_ID = " + CodeConjuntoIni + " INNER JOIN DETAILALPHA DIT ON INCIDENT.INCIDENT_ID     = DIT.INCIDENT_ID AND DIT.DETAIL_TYPE_ID = " + CodeItemIni + " INNER JOIN DETAILALPHA DPR ON INCIDENT.INCIDENT_ID     = DPR.INCIDENT_ID AND DPR.DETAIL_TYPE_ID = " + CodeProblemaIni + " INNER JOIN DETAILALPHA DSO ON INCIDENT.INCIDENT_ID     = DSO.INCIDENT_ID AND DSO.DETAIL_TYPE_ID = " + CodeSolucaoIni + " where INCIDENT.OCCURRENCE_DT > sysdate - " + days + "   and RESPPART.PART_ID  = '" + part + "'" + "   and DPE.detail_value1 = '" + PerfilCatalogoIni + "'" + "   and DSI.detail_value1 = '" + SistemIni + "'" + "   and DCJ.detail_value1 = '" + ConjuntoIni + "'" + "   and DIT.detail_value1 = '" + ItemIni + "'" + "   and DPR.detail_value1 = '" + ProblemaIni + "'" + "   and DSO.detail_value1 = '" + SolucaoIni + "'";

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {

                    int c = command.ExecuteNonQuery();

                    if (c > 0)
                        return true;
                    else
                        return false;
                }
            }

          
          
        }

        public static int checkRecurrenceMalaysia(int days, string part, string CodePerfilCatalogoIni, string CodeSistemIni, string CodeConjuntoIni, string CodeItemIni, string CodeProblemaIni, string CodeSolucaoIni, string PerfilCatalogoIni, string SistemIni, string ConjuntoIni, string ItemIni, string ProblemaIni, string SolucaoIni, DateTime DtOcorrencia)
        {
            string SQLstring = "SELECT COUNT(*) AS QTREC, SUM(NVL(Detail1296.detail_nr,0)) AS VLREC                                   " + "  FROM INCIDENT                                                                                       " + " INNER JOIN INCIDENTRESPREVITEMUSER INCRESPPART                                                       " + "           ON  INCIDENT.INCIDENT_ID = INCRESPPART.INCIDENT_ID AND INCRESPPART.BCURRENT = 1            " + " LEFT OUTER JOIN DetailNumeric Detail1296 ON Incident.incident_id = Detail1296.incident_id AND Detail1296.detail_type_id = 1296 " + " INNER JOIN PART RESPPART   ON INCRESPPART.RESP_PART_ID = RESPPART.PART_ID                            " + " INNER JOIN DETAILALPHA DPE ON INCIDENT.INCIDENT_ID     = DPE.INCIDENT_ID AND DPE.DETAIL_TYPE_ID = " + CodePerfilCatalogoIni + " INNER JOIN DETAILALPHA DSI ON INCIDENT.INCIDENT_ID     = DSI.INCIDENT_ID AND DSI.DETAIL_TYPE_ID = " + CodeSistemIni + " INNER JOIN DETAILALPHA DCJ ON INCIDENT.INCIDENT_ID     = DCJ.INCIDENT_ID AND DCJ.DETAIL_TYPE_ID = " + CodeConjuntoIni + " INNER JOIN DETAILALPHA DIT ON INCIDENT.INCIDENT_ID     = DIT.INCIDENT_ID AND DIT.DETAIL_TYPE_ID = " + CodeItemIni + " INNER JOIN DETAILALPHA DPR ON INCIDENT.INCIDENT_ID     = DPR.INCIDENT_ID AND DPR.DETAIL_TYPE_ID = " + CodeProblemaIni + " INNER JOIN DETAILALPHA DSO ON INCIDENT.INCIDENT_ID     = DSO.INCIDENT_ID AND DSO.DETAIL_TYPE_ID = " + CodeSolucaoIni + " where INCIDENT.OCCURRENCE_DT >= trunc(INCIDENT.OCCURRENCE_DT)-(to_number(to_char(INCIDENT.OCCURRENCE_DT,'DD')) - 1)" + "   and INCIDENT.OCCURRENCE_DT <= TO_DATE('" + DtOcorrencia.ToString("yyyy-MM-dd hh:mm:ss") + "', 'rrrr/mm/dd hh:mi:ss')" + "   and RESPPART.PART_ID  = '" + part + "'" + "   and DPE.detail_value1 = '" + PerfilCatalogoIni + "'" + "   and DSI.detail_value1 = '" + SistemIni + "'" + "   and DCJ.detail_value1 = '" + ConjuntoIni + "'" + "   and DIT.detail_value1 = '" + ItemIni + "'" + "   and DPR.detail_value1 = '" + ProblemaIni + "'" + "   and DSO.detail_value1 = '" + SolucaoIni + "'";

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {

                    int c = command.ExecuteNonQuery();

                    if (c > 0)
                        return c;
                    else
                        return 0;
                }
            }

         
        }

        public static void performRecurrenceMalaysia(string CodePerfilCatalogoIni, string CodeSistemIni, string CodeConjuntoIni, string CodeItemIni, string CodeProblemaIni, string CodeSolucaoIni, string CodeDuracao, string CodeInicioAvaria, string CodeIssueDetail1M, string CodeIssueYes1M, string CodeRecurrenceNumber1M, string CodeRecurrenceValue1M, string CodeIssueDetail6M, string CodeIssueYes6M, string CodeRecurrenceNumber6M, string CodeRecurrenceValue6M)
        {
            string SQLupdate;
            string SQLstring;
            string SQLSUBstring;

            // ULTIMO MES
            SQLstring = "SELECT RESPPART.PART_ID, DPE.detail_value1 AS NM_PROFILE, DSI.detail_value1 AS NM_SYSTEM, DCJ.detail_value1 AS NM_SET, DIT.detail_value1 AS NM_ITEM, DPR.detail_value1 AS NM_PROBLEM, DSO.detail_value1 AS NM_SOLUTION, COUNT(*) AS QTY, SUM(NVL(Detail1215.detail_nr,0)) AS DURATION" + " FROM INCIDENT" + " INNER JOIN INCIDENTRESPREVITEMUSER INCRESPPART ON INCIDENT.INCIDENT_ID = INCRESPPART.INCIDENT_ID AND INCRESPPART.BCURRENT = 1" + " LEFT OUTER JOIN DetailNumeric Detail1215 ON Incident.incident_id = Detail1215.incident_id AND Detail1215.detail_type_id = " + CodeDuracao + " INNER JOIN PART RESPPART   ON INCRESPPART.RESP_PART_ID = RESPPART.PART_ID" + " INNER JOIN DETAILALPHA DPE ON INCIDENT.INCIDENT_ID     = DPE.INCIDENT_ID AND DPE.DETAIL_TYPE_ID = " + CodePerfilCatalogoIni + " INNER JOIN DETAILALPHA DSI ON INCIDENT.INCIDENT_ID     = DSI.INCIDENT_ID AND DSI.DETAIL_TYPE_ID = " + CodeSistemIni + " INNER JOIN DETAILALPHA DCJ ON INCIDENT.INCIDENT_ID     = DCJ.INCIDENT_ID AND DCJ.DETAIL_TYPE_ID = " + CodeConjuntoIni + " INNER JOIN DETAILALPHA DIT ON INCIDENT.INCIDENT_ID     = DIT.INCIDENT_ID AND DIT.DETAIL_TYPE_ID = " + CodeItemIni + " INNER JOIN DETAILALPHA DPR ON INCIDENT.INCIDENT_ID     = DPR.INCIDENT_ID AND DPR.DETAIL_TYPE_ID = " + CodeProblemaIni + " INNER JOIN DETAILALPHA DSO ON INCIDENT.INCIDENT_ID     = DSO.INCIDENT_ID AND DSO.DETAIL_TYPE_ID = " + CodeSolucaoIni + " INNER JOIN DetailDate Detail789 ON Incident.incident_id = Detail789.incident_id AND Detail789.detail_type_id = " + CodeInicioAvaria + " WHERE Incident.entity_id = 5" + " AND Detail789.detail_dt+(-3/24) >= TRUNC(SYSDATE - TO_CHAR(SYSDATE, 'DD') +1)" + " AND Detail789.detail_dt+(-3/24) < ADD_MONTHS(TRUNC(SYSDATE - TO_CHAR(SYSDATE, 'DD') +1), 1)" + " GROUP BY RESPPART.PART_ID, DPE.detail_value1, DSI.detail_value1, DCJ.detail_value1, DIT.detail_value1, DPR.detail_value1, DSO.detail_value1" + " HAVING COUNT(*) > 1";

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {

                    //DataTable dt = command.ExecuteReader();
                    //DataTable dt = DAO_Base.executeQuery(SQLstring);
                    //if (dt.Rows().Count > 0)
                    //{
                    //    foreach (var drSUB in dt.Rows)
                    //    {
                    //        SQLSUBstring = "SELECT DISTINCT INCIDENT.incident_id" + " FROM INCIDENT" + " INNER JOIN INCIDENTRESPREVITEMUSER INCRESPPART ON INCIDENT.INCIDENT_ID = INCRESPPART.INCIDENT_ID AND INCRESPPART.BCURRENT = 1" + " INNER JOIN PART RESPPART   ON INCRESPPART.RESP_PART_ID = RESPPART.PART_ID" + " INNER JOIN DETAILALPHA DPE ON INCIDENT.INCIDENT_ID     = DPE.INCIDENT_ID AND DPE.DETAIL_TYPE_ID = " + CodePerfilCatalogoIni + " INNER JOIN DETAILALPHA DSI ON INCIDENT.INCIDENT_ID     = DSI.INCIDENT_ID AND DSI.DETAIL_TYPE_ID = " + CodeSistemIni + " INNER JOIN DETAILALPHA DCJ ON INCIDENT.INCIDENT_ID     = DCJ.INCIDENT_ID AND DCJ.DETAIL_TYPE_ID = " + CodeConjuntoIni + " INNER JOIN DETAILALPHA DIT ON INCIDENT.INCIDENT_ID     = DIT.INCIDENT_ID AND DIT.DETAIL_TYPE_ID = " + CodeItemIni + " INNER JOIN DETAILALPHA DPR ON INCIDENT.INCIDENT_ID     = DPR.INCIDENT_ID AND DPR.DETAIL_TYPE_ID = " + CodeProblemaIni + " INNER JOIN DETAILALPHA DSO ON INCIDENT.INCIDENT_ID     = DSO.INCIDENT_ID AND DSO.DETAIL_TYPE_ID = " + CodeSolucaoIni + " INNER JOIN DetailDate Detail789 ON Incident.incident_id = Detail789.incident_id AND Detail789.detail_type_id = " + CodeInicioAvaria + " WHERE Incident.entity_id = 5" + " AND Detail789.detail_dt+(-3/24) >= TRUNC(SYSDATE - TO_CHAR(SYSDATE, 'DD') +1)" + " AND Detail789.detail_dt+(-3/24) < ADD_MONTHS(TRUNC(SYSDATE - TO_CHAR(SYSDATE, 'DD') +1), 1)" + " AND RESPPART.PART_ID  = '" + drSUB("PART_ID") + "'" + " AND DPE.detail_value1 = '" + drSUB("NM_PROFILE") + "'" + " AND DSI.detail_value1 = '" + drSUB("NM_SYSTEM") + "'" + " AND DCJ.detail_value1 = '" + drSUB("NM_SET") + "'" + " AND DIT.detail_value1 = '" + drSUB("NM_ITEM") + "'" + " AND DPR.detail_value1 = '" + drSUB("NM_PROBLEM") + "'" + " AND DSO.detail_value1 = '" + drSUB("NM_SOLUTION") + "'";
                    //        string InIncident = "(";
                    //        DataTable dtSUB = DAO_Base.executeQuery(SQLSUBstring);
                    //        foreach (var drSUBINCIDENT in dtSUB.Rows)
                    //            InIncident += drSUBINCIDENT("incident_id") + ",";
                    //        SQLupdate = "UPDATE DETAILNUMERIC SET DETAIL_NR=" + drSUB("QTY") + " WHERE INCIDENT_ID IN " + InIncident + " AND detail_type_id=" + CodeRecurrenceNumber1M;
                    //        DAO_Base.executeQuery(SQLupdate);
                    //        SQLupdate = "UPDATE DETAILNUMERIC SET DETAIL_NR=" + drSUB("DURATION").ToString().Replace(",", ".") + " WHERE INCIDENT_ID IN " + InIncident + " AND detail_type_id=" + CodeRecurrenceValue1M;
                    //        DAO_Base.executeQuery(SQLupdate);
                    //        SQLupdate = "UPDATE DETAILISSUE SET ISSUE_ID=" + CodeIssueYes1M + " WHERE INCIDENT_ID IN " + InIncident + " AND detail_type_id=" + CodeIssueDetail1M;
                    //        DAO_Base.executeQuery(SQLupdate);
                    //    }
                    //    DAO_Base.executeQuery("commit");
                    //}

                    //// ULTIMOS 6 MESES
                    //SQLstring = "SELECT RESPPART.PART_ID, DPE.detail_value1 AS NM_PROFILE, DSI.detail_value1 AS NM_SYSTEM, DCJ.detail_value1 AS NM_SET, DIT.detail_value1 AS NM_ITEM, DPR.detail_value1 AS NM_PROBLEM, DSO.detail_value1 AS NM_SOLUTION, COUNT(*) AS QTY, SUM(NVL(Detail1215.detail_nr,0)) AS DURATION" + " FROM INCIDENT" + " INNER JOIN INCIDENTRESPREVITEMUSER INCRESPPART ON INCIDENT.INCIDENT_ID = INCRESPPART.INCIDENT_ID AND INCRESPPART.BCURRENT = 1" + " LEFT OUTER JOIN DetailNumeric Detail1215 ON Incident.incident_id = Detail1215.incident_id AND Detail1215.detail_type_id = " + CodeDuracao + " INNER JOIN PART RESPPART   ON INCRESPPART.RESP_PART_ID = RESPPART.PART_ID" + " INNER JOIN DETAILALPHA DPE ON INCIDENT.INCIDENT_ID     = DPE.INCIDENT_ID AND DPE.DETAIL_TYPE_ID = " + CodePerfilCatalogoIni + " INNER JOIN DETAILALPHA DSI ON INCIDENT.INCIDENT_ID     = DSI.INCIDENT_ID AND DSI.DETAIL_TYPE_ID = " + CodeSistemIni + " INNER JOIN DETAILALPHA DCJ ON INCIDENT.INCIDENT_ID     = DCJ.INCIDENT_ID AND DCJ.DETAIL_TYPE_ID = " + CodeConjuntoIni + " INNER JOIN DETAILALPHA DIT ON INCIDENT.INCIDENT_ID     = DIT.INCIDENT_ID AND DIT.DETAIL_TYPE_ID = " + CodeItemIni + " INNER JOIN DETAILALPHA DPR ON INCIDENT.INCIDENT_ID     = DPR.INCIDENT_ID AND DPR.DETAIL_TYPE_ID = " + CodeProblemaIni + " INNER JOIN DETAILALPHA DSO ON INCIDENT.INCIDENT_ID     = DSO.INCIDENT_ID AND DSO.DETAIL_TYPE_ID = " + CodeSolucaoIni + " INNER JOIN DetailDate Detail789 ON Incident.incident_id = Detail789.incident_id AND Detail789.detail_type_id = " + CodeInicioAvaria + " WHERE Incident.entity_id = 5" + " AND Detail789.detail_dt+(-3/24) >= TO_DATE('" + Strings.Format(DateTime.DateAdd(DateInterval.Day, -180, DateTime.Now), "yyyy-MM-01") + "', 'rrrr/mm/dd hh:mi:ss')" + " AND Detail789.detail_dt+(-3/24) <= TO_DATE('" + Strings.Format(DateTime.Now, "yyyy-MM-dd 23:59:59") + "', 'rrrr/mm/dd hh24:mi:ss')" + " GROUP BY RESPPART.PART_ID, DPE.detail_value1, DSI.detail_value1, DCJ.detail_value1, DIT.detail_value1, DPR.detail_value1, DSO.detail_value1" + " HAVING COUNT(*) > 1";
                    //DataTable dt2 = DAO_Base.executeQuery(SQLstring);
                    //if (dt2.Rows().Count > 0)
                    //{
                    //    foreach (var drSUB2 in dt2.Rows)
                    //    {
                    //        SQLSUBstring = "SELECT DISTINCT INCIDENT.incident_id" + " FROM INCIDENT" + " INNER JOIN INCIDENTRESPREVITEMUSER INCRESPPART ON INCIDENT.INCIDENT_ID = INCRESPPART.INCIDENT_ID AND INCRESPPART.BCURRENT = 1" + " INNER JOIN PART RESPPART   ON INCRESPPART.RESP_PART_ID = RESPPART.PART_ID" + " INNER JOIN DETAILALPHA DPE ON INCIDENT.INCIDENT_ID     = DPE.INCIDENT_ID AND DPE.DETAIL_TYPE_ID = " + CodePerfilCatalogoIni + " INNER JOIN DETAILALPHA DSI ON INCIDENT.INCIDENT_ID     = DSI.INCIDENT_ID AND DSI.DETAIL_TYPE_ID = " + CodeSistemIni + " INNER JOIN DETAILALPHA DCJ ON INCIDENT.INCIDENT_ID     = DCJ.INCIDENT_ID AND DCJ.DETAIL_TYPE_ID = " + CodeConjuntoIni + " INNER JOIN DETAILALPHA DIT ON INCIDENT.INCIDENT_ID     = DIT.INCIDENT_ID AND DIT.DETAIL_TYPE_ID = " + CodeItemIni + " INNER JOIN DETAILALPHA DPR ON INCIDENT.INCIDENT_ID     = DPR.INCIDENT_ID AND DPR.DETAIL_TYPE_ID = " + CodeProblemaIni + " INNER JOIN DETAILALPHA DSO ON INCIDENT.INCIDENT_ID     = DSO.INCIDENT_ID AND DSO.DETAIL_TYPE_ID = " + CodeSolucaoIni + " INNER JOIN DetailDate Detail789 ON Incident.incident_id = Detail789.incident_id AND Detail789.detail_type_id = " + CodeInicioAvaria + " WHERE Incident.entity_id = 5" + " AND Detail789.detail_dt+(-3/24) >= TO_DATE('" + Strings.Format(DateTime.DateAdd(DateInterval.Day, -180, DateTime.Now), "yyyy-MM-01") + "', 'rrrr/mm/dd hh:mi:ss')" + " AND Detail789.detail_dt+(-3/24) <= TO_DATE('" + Strings.Format(DateTime.Now, "yyyy-MM-dd 23:59:59") + "', 'rrrr/mm/dd hh24:mi:ss')" + " AND RESPPART.PART_ID  = '" + drSUB2("PART_ID") + "'" + " AND DPE.detail_value1 = '" + drSUB2("NM_PROFILE") + "'" + " AND DSI.detail_value1 = '" + drSUB2("NM_SYSTEM") + "'" + " AND DCJ.detail_value1 = '" + drSUB2("NM_SET") + "'" + " AND DIT.detail_value1 = '" + drSUB2("NM_ITEM") + "'" + " AND DPR.detail_value1 = '" + drSUB2("NM_PROBLEM") + "'" + " AND DSO.detail_value1 = '" + drSUB2("NM_SOLUTION") + "'";
                    //        string InIncident = "(";
                    //        DataTable dtSUB2 = DAO_Base.executeQuery(SQLSUBstring);
                    //        foreach (var drSUBINCIDENT2 in dtSUB2.Rows)
                    //            InIncident += drSUBINCIDENT2("incident_id") + ",";
                    //        InIncident = Strings.Left(InIncident, InIncident.Length() - 1) + ")";
                    //        SQLupdate = "UPDATE DETAILNUMERIC SET DETAIL_NR=" + drSUB2("QTY") + " WHERE INCIDENT_ID IN " + InIncident + " AND detail_type_id=" + CodeRecurrenceNumber6M;
                    //        DAO_Base.executeQuery(SQLupdate);
                    //        SQLupdate = "UPDATE DETAILNUMERIC SET DETAIL_NR=" + drSUB2("DURATION").ToString().Replace(",", ".") + " WHERE INCIDENT_ID IN " + InIncident + " AND detail_type_id=" + CodeRecurrenceValue6M;
                    //        DAO_Base.executeQuery(SQLupdate);
                    //        SQLupdate = "UPDATE DETAILISSUE SET ISSUE_ID=" + CodeIssueYes6M + " WHERE INCIDENT_ID IN " + InIncident + " AND detail_type_id=" + CodeIssueDetail6M;
                    //        DAO_Base.executeQuery(SQLupdate);
                    //    }
                    //    DAO_Base.executeQuery("commit");
                    //}
                }
            }
          
        }
    }

}

