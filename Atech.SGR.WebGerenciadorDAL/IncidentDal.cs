﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class IncidentDal : DataWorker
    {
        public static object Utils { get; private set; }

        public static List<IncidentModel> GetIncident()
        {
            try
            {
                var result = new List<IncidentModel>();
                string SQLstring;
                SQLstring = "SELECT * FROM RDS_N0FXFRAV";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            result = new ReflectionPopulator<IncidentModel>().CreateList(reader);
                        }
                    }
                }
                return result;
    
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<IncidentModel> GetIncident(DateTime dataInicial, string LimitIncidentTo, string LimitIncidentStartDate, string LimitIncidentEndDate)
        {
            try
            {
                var result = new List<IncidentModel>();
                string[] arrayIncident;
                string SQLstring = "SELECT  DISTINCT" + 
                    "        A.COD_NOTA," + 
                    "        A.TIPO_NOTA," + 
                    "        NVL(B.COD_LOCALINSTALACAO,A.COD_LOCALINSTALACAO) AS COD_LOCALINSTALACAO, " + 
                    "        TRIM(NVL(B.DESC_LOCALINSTALACAO,A.DESC_LOCALINSTALACAO)) AS DESC_LOCALINSTALACAO, " + 
                    "        A.COD_EQUIPAMENTO," + 
                    "        TRIM(A.DESC_EQUIPAMENTO) AS DESC_EQUIPAMENTO," + 
                    "        A.DATA_ABERTURANOTA," + 
                    "        A.COD_PLANTACENTROTRABALHO," + 
                    "        A.COD_CENTROTRABALHO," + 
                    "        NVL(A.COD_PLANTAGRUPOPLANEJAMENTO,'NI') AS COD_PLANTAGRUPOPLANEJAMENTO," + 
                    "        A.COD_GRUPOPLANEJAMENTO," + 
                    "        A.COD_PERFILCATALOGO," + 
                    "        A.DESC_PERFILCATALOGO," + 
                    "        A.DESC_SISTEMA," + 
                    "        A.DESC_CONJUNTO," + 
                    "        A.DESC_ITEM," + 
                    "        A.DESC_PROBLEMA," + 
                    "        A.DESC_SOLUCAO," + 
                    "        A.DESC_PRIORIDADE," + 
                    "        A.DATA_INICIOPARADA," + 
                    "        A.DATA_FIMPARADA," + 
                    "        ROUND(A.TEMPO_DURACAOPARADA,4) AS TEMPO_DURACAOPARADA," + 
                    "        ROUND(A.KM_INICIO,2) AS KM_INICIO," + 
                    "        ROUND(A.KM_FIM,2) AS KM_FIM," + 
                    "        A.DESC_CURTA," + 
                    "        SUBSTR(NVL(A.DESC_LONGA,'-'), 1, 2000) as DESC_LONGA," + 
                    "        A.DATA_ULTIMAATUALIZACAO," + 
                    "        A.NUMERO_ORDEM," + 
                    "        ROUND(A.INDICADOR_PARADA,1) AS INDICADOR_PARADA," + 
                    "        A.DATA_INICIOGARANTIAFABRICANTE," + 
                    "        A.DATA_FIMGARANTIAFABRICANTE," + 
                    "        A.COD_INDICADORABC," + 
                    "        NVL(A.COD_PLANTA,'NI') AS COD_PLANTA," + 
                    "        A.TIPO_ORDEM," + 
                    "        ROUND(A.VALOR_CUSTOPLANEJADO,2) AS VALOR_CUSTOPLANEJADO," + 
                    "        ROUND(A.VALOR_CUSTOREAL,2) AS VALOR_CUSTOREAL," + 
                    "        A.DATA_FECHAMENTONOTA," + 
                    "        A.COD_STATUSORDEM," + 
                    "        A.COD_IDENTIFLOCALEQUIP," +
                    "        A.COD_UNIDADEPARADA," + 
                    "        CASE A.COD_PLANTA" + 
                    "          WHEN '4056' THEN '3'" + 
                    "          WHEN '4057' THEN '3'" + 
                    "          WHEN '4059' THEN '3'" + 
                    "          WHEN '4069' THEN '3'" + 
                    "          WHEN '4065' THEN '3'" + 
                    "          WHEN '1159' THEN '4'" + 
                    "          WHEN '1161' THEN '4'" + 
                    "          WHEN '3030' THEN '5'" + 
                    "          ELSE '2'" + 
                    "        END AS COD_ENTIDADE," + 
                    "        CASE A.COD_PLANTA" + 
                    "          WHEN '4056' THEN 'FERROVIA'" + 
                    "          WHEN '4057' THEN 'FERROVIA'" + 
                    "          WHEN '4059' THEN 'FERROVIA'" + 
                    "          WHEN '4065' THEN 'FERROVIA'" + 
                    "          WHEN '4069' THEN 'FERROVIA'" + 
                    "          WHEN '1159' THEN 'OMAN'" + 
                    "          WHEN '1161' THEN 'OMAN'" + 
                    "          WHEN '3030' THEN 'MALAYSIA'" + 
                    "          ELSE 'VALE'" + 
                    "        END AS DESC_ENTIDADE," + 
                    "        A.DATA_ULTIMAALTERACAO" + 
                    "  FROM" + 
                    "        RDS_N0FXFRAV A " + 
                    " LEFT JOIN RDS_N0AXFRAV B ON A.COD_PLANTA=B.COD_CENTROPLANEJAMENTO AND A.COD_EQUIPAMENTO=B.COD_EQUIPAMENTO ";

                // " LEFT JOIN RDS_N0AXFRAV B ON A.COD_ENTIDADE=B.COD_ENTIDADE AND A.COD_EQUIPAMENTO=B.COD_EQUIPAMENTO "
                // SQLstring += " WHERE A.N0CCDPLA IN ('1089', '4181', '1090', '4270', '4143', '1111', '1095', '1106', '1103', " *** DEPENDE DA VALIDACAO DO JAQUES ***

                SQLstring += " WHERE A.COD_PLANTA IN ('1089', '4181', '1090', '4270', '4143', '1111', '1095', '1106', '1103', ";
                SQLstring += "'1096', '1119', '4059', '4069', '4057', '4056', '1081', '1077', '4199', '1083', '1086', '1070', '1072', ";
                SQLstring += "'4222', '4221', '1074', '4131', '1123', '1133', '4271', '1058', '4224', '4065', '4223', '4018', '4127', ";
                SQLstring += "'4010', '4040', '4074', '1068', '4050', ";
                SQLstring += "'1159', '1161', '3030')"; // OMAN/MALAYSIA
                SQLstring += " AND A.TIPO_NOTA in ('YN','YE')";
                SQLstring += " AND A.COD_STATUSORDEM IN ('ENTE','ENCE')";

                if (!string.IsNullOrEmpty(LimitIncidentTo))
                {
                    // check BETWEEN or IN
                    if (LimitIncidentTo.IndexOf("-") > 0)
                    {
                        arrayIncident = LimitIncidentTo.Split('-');
                        SQLstring += " AND COD_NOTA BETWEEN '" + arrayIncident[0] + "' AND '" + arrayIncident[1] + "'";
                    }
                    else
                    {
                        arrayIncident = LimitIncidentTo.Split(',');
                        string LimitIncidentToWhere = "";
                        foreach (string Incident in arrayIncident)
                        {
                            LimitIncidentToWhere += "'" + Incident + "',";


                            LimitIncidentToWhere = UtilDAL.Left(LimitIncidentToWhere, LimitIncidentToWhere.Length - 1);
                            SQLstring += " AND COD_NOTA IN (" + LimitIncidentToWhere.ToString() + ")";
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(LimitIncidentStartDate))
                {
                    // check DATE LIMITS
                    SQLstring += " AND DATA_ABERTURANOTA >= TO_DATE('" + LimitIncidentStartDate + " 00:00:00','rrrr/mm/dd hh24:mi:ss')";
                    SQLstring += " AND DATA_ABERTURANOTA <= TO_DATE('" + LimitIncidentEndDate + " 23:59:59','rrrr/mm/dd hh24:mi:ss')";
                }
                else
                {
                    // BATCH
                    SQLstring += " AND A.DATA_ULTIMAALTERACAORDS > TO_DATE('" + dataInicial.ToString("dd/MM/yyyy HH:mm:ss") + "','dd/mm/rrrr hh24:mi:ss') ";
                }
                // SQLstring += " AND COD_PLANTAGRUPOPLANEJAMENTO IS NOT NULL" 'INCLUI ESTA CONDICAO POIS UM REGISTRO ESTAVA BLOQUEANDO A CRAGA DE TODOS
                SQLstring += " ORDER BY COD_NOTA";


                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            result = new ReflectionPopulator<IncidentModel>().CreateList(reader);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
