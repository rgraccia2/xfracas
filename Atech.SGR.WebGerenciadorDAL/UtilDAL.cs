﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class UtilDAL: DataWorker
    {

        public static int getIdValue(string tablenm, int entityId)
        {
 
            int code = 0;
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand())
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (IDbCommand commandproc = database.CreateStoredProcCommand("RS_GETIDVALUE", connection))
                            {

                                IDataParameter P_tablenm = database.CreateParameter("TABLENM", tablenm);
                                commandproc.Parameters.Add(P_tablenm);

                                IDataParameter P_ENTITYID = database.CreateParameter("ENTITYID", DBNull.Value);
                                commandproc.Parameters.Add(P_ENTITYID);


                                OracleParameter paramCode = new OracleParameter();
                                paramCode.OracleType = OracleType.Cursor;
                                paramCode.ParameterName = "NEXTID";
                                paramCode.Direction = ParameterDirection.Output;
                                commandproc.Parameters.Add(paramCode);

                                commandproc.ExecuteNonQuery();

                                code = Convert.ToInt32(paramCode.Value);

                            }
                            scope.Complete();
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.Log(ex.Message, "Erro ao getIdValue" + "///" + command.CommandText);
                    }
                }
            }
            return code;

        }

        public static string Left(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            maxLength = Math.Abs(maxLength);

            return (value.Length <= maxLength
                   ? value
                   : value.Substring(0, maxLength)
                   );
        }

    }
}
