﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class FailureItemDAL : DataWorker
    {

        public List<FailureItemModel> GetFailureItem()
        {
            try
            {
                var listReturn = new List<FailureItemModel>();
                string SQLstring = "select * from TB_FAILUREITEM";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<FailureItemModel>().CreateList(reader);
                        }
                    }
                }
                return listReturn;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Estrutura GetFailureItemTree()
        {
            string SQLstring = "select CODE, " + " substr(SYS_CONNECT_BY_PATH(NAME, '(|)'),4,9999) Path," + " NAME, " + "       LEVEL AS TYPE, " + "       NVL(ANCESTOR ,-1) as ANCESTOR" + "  from tb_failureitem " + " connect by prior code  = Ancestor" + " start with ancestor is null or ancestor = 0";

            DataTable dt = new DataTable();


            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }


            int code;
            string path;
            string name;
            int type;
            int ancestor;
            var estrutura = new Estrutura();
            int cnt = 0;
            foreach (DataRow dr in dt.Rows)
            {
                cnt += 1;
                code = Convert.ToInt32(dr[0]);
                path = Convert.ToString(dr[1]);
                name = Convert.ToString(dr[2]);
                type = Convert.ToInt32(dr[3]);
                ancestor = Convert.ToInt32(dr[4]);
                var v = path.ToUpper().Split('|');
                try
                {
                    switch (type)
                    {
                        case 1:
                            {
                                estrutura.Perfis.Add(v[0], new FailureItemModel(code, name, type, ancestor));
                                break;
                            }

                        case 2:
                            {

                                //estrutura.Perfis(v[0]).dic.Add(v[1], new FailureItemModel(code, name, type, ancestor));
                                break;
                            }

                        case 3:
                            {
                                //estrutura.Perfis(v[0]).dic(v[1]).dic.Add(v[2], new FailureItemModel(code, name, type, ancestor));
                                break;
                            }

                        case 4:
                            {
                                //estrutura.Perfis(v[0]).dic(v[1]).dic(v[2]).dic.Add(v[3], new FailureItemModel(code, name, type, ancestor));
                                break;
                            }

                        case 5:
                            {
                                //estrutura.Perfis(v[0]).dic(v[1]).dic(v[2]).dic(v[3]).dic.Add(v[4], new FailureItemModel(code, name, type, ancestor));
                                break;
                            }

                        case 6:
                            {
                                //estrutura.Perfis(v[0]).dic(v[1]).dic(v[2]).dic(v[3]).dic(v[4]).dic.Add(v[5], new FailureItemModel(code, name, type, ancestor));
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    int a = 0;
                }
            }

            return estrutura;
        }

        public int GetFailureItemByNameType(string name, int type)
        {
            var listReturn = new List<Item>();
            try
            {
                string SQLstring = "select CODE from TB_FAILUREITEM WHERE NAME = '" + name + "' AND TYPE = " + type + " AND ANCESTOR IS NULL";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<Item>().CreateList(reader);
                        }
                    }
                }
                return listReturn.FirstOrDefault().CODE;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetFailureItemByNameType(string name, int type, int parent)
        {
            try
            {
                var listReturn = new List<Item>();
                string SQLstring = "select CODE from TB_FAILUREITEM WHERE NAME = '" + name + "' AND TYPE = " + type + " AND ANCESTOR = " + parent;

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            listReturn = new ReflectionPopulator<Item>().CreateList(reader);
                        }
                    }
                }
                return listReturn.FirstOrDefault().CODE;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertFailureItemFull(string P_DESC_LANGUAGE, string P_DESC_PERFILCATALOGO, string P_DESC_SISTEMA, string P_DESC_CONJUNTO, string P_DESC_ITEM, string P_DESC_PROBLEMA, string P_DESC_SOLUCAO)
        {

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand())
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (IDbCommand commandproc = database.CreateStoredProcCommand("SPFA_INSERT_CLASSE_FALHA_FULL", connection))
                            {
                                IDataParameter DESC_LANGUAGE = database.CreateParameter("P_DESC_LANGUAGE", P_DESC_LANGUAGE);
                                commandproc.Parameters.Add(DESC_LANGUAGE);

                                IDataParameter DESC_PERFILCATALOGO = database.CreateParameter("P_DESC_PERFILCATALOGO", P_DESC_PERFILCATALOGO);
                                commandproc.Parameters.Add(DESC_PERFILCATALOGO);

                                IDataParameter DESC_SISTEMA = database.CreateParameter("P_DESC_SISTEMA", P_DESC_SISTEMA);
                                commandproc.Parameters.Add(DESC_SISTEMA);

                                IDataParameter DESC_CONJUNTO = database.CreateParameter("P_DESC_CONJUNTO", P_DESC_CONJUNTO);
                                commandproc.Parameters.Add(DESC_CONJUNTO);

                                IDataParameter DESC_ITEM = database.CreateParameter("P_DESC_ITEM", P_DESC_ITEM);
                                commandproc.Parameters.Add(DESC_ITEM);

                                IDataParameter DESC_PROBLEMA = database.CreateParameter("P_DESC_PROBLEMA", P_DESC_PROBLEMA);
                                commandproc.Parameters.Add(DESC_PROBLEMA);

                                IDataParameter DESC_SOLUCAO = database.CreateParameter("P_DESC_SOLUCAO", P_DESC_SOLUCAO);
                                commandproc.Parameters.Add(DESC_SOLUCAO);

                                commandproc.ExecuteNonQuery();
                            }
                            scope.Complete();
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.Log(ex.Message, "Erro ao InsertFailureItem" + "///" + command.CommandText);
                    }
                }
            }


        }

        public int InsertFailureItem(FailureItemModel FailureItem)
        {
            int code = 0;
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand())
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (IDbCommand commandproc = database.CreateStoredProcCommand("SPFA_INSERT_CLASSE_FALHA", connection))
                            {

                                IDataParameter P_NAME = database.CreateParameter("NAME", FailureItem.NAME);
                                commandproc.Parameters.Add(P_NAME);

                                IDataParameter P_TYPE = database.CreateParameter("TYPE", FailureItem.TYPE);
                                commandproc.Parameters.Add(P_TYPE);

                                IDataParameter P_ANCESTOR = database.CreateParameter("ANCESTOR", FailureItem.ANCESTOR);
                                commandproc.Parameters.Add(P_ANCESTOR);

                                OracleParameter paramCode = new OracleParameter();
                                paramCode.OracleType = OracleType.Cursor;
                                paramCode.ParameterName = "CODE";
                                paramCode.Direction = ParameterDirection.Output;
                                commandproc.Parameters.Add(paramCode);

                                commandproc.ExecuteNonQuery();

                                code = Convert.ToInt32(paramCode.Value);

                            }
                            scope.Complete();

                            
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.Log(ex.Message, "Erro ao InsertFailureItem" + "///" + command.CommandText);
                    }
                }
            }
            return code;
        }


    }
}
