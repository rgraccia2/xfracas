﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class MaintenanceNoteDAL : DataWorker
    {
        public void insertStatusMaintenanceNote(int action_id, int detailFieldCode, int entity_id, string value, string userNameWithDomain)
        {

            int nextId = UtilDAL.getIdValue("DETAILALPHA", entity_id);

            string insertString = "insert into detailalpha " +
                "        (" + " DETAIL_ID," +
                "         DETAIL_TYPE_ID," +
                "         ACTION_ID," +
                "         DETAIL_VALUE1," +
                "         ENTITY_ID," +
                "         LAST_UPDATED_UID," +
                "         LAST_UPDATED," +
                "         LAST_UPDATED_BY" + " )" +
                " values (" + "" + nextId + "," +
                "         " + detailFieldCode + "," +
                "         " + action_id + "," +
                "         '" + value + "'," + "         " +
                "        " + entity_id + "," +
                "         (select userinfo_id" +
                "  from userinfo " +
                "  where upper(user_nm) = upper('" + userNameWithDomain + "')" + ")," +
                "  SYSDATE," + "'XFRACAS'" + " )";


            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand())
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (IDbCommand commandText = database.CreateCommand(insertString, connection))
                            {
                                commandText.ExecuteNonQuery();
                            }
                            scope.Complete();
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.Log(ex.Message, "Erro ao insertStatusMaintenanceNote" + "///" + command.CommandText);
                    }
                }
            }
        }

        public int getMaintenanceNoteDetailID(int action_id, int detailFieldCode, int entity_id)
        {

            var listReturn = new List<ItemMaintenanceNote>();
            string SQLstring = "select detail_id  " + "  from detailalpha" + " where DETAIL_TYPE_ID = " + detailFieldCode + "   and entity_id      = " + entity_id + "   and action_id    = " + action_id;

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        listReturn = new ReflectionPopulator<ItemMaintenanceNote>().CreateList(reader);
                    }
                }
            }
            return listReturn.FirstOrDefault().detail_id;
        }

        public void updateStatusMaintenanceNote(int detail_id, string newStatus, string userNameWithDomain)
        {
            string SQLstring;
            if (!string.IsNullOrEmpty(userNameWithDomain))
            {
                SQLstring = "update  detailalpha  set DETAIL_VALUE1   = '" + newStatus + "' ," + " LAST_UPDATED_UID = (select userinfo_id  " + "  from userinfo  " + " where upper(user_nm) = upper('" + userNameWithDomain + "')" + "  )" + " where DETAIL_ID  =  " + detail_id;
            }
            else
            {
                SQLstring = "update  detailalpha   set DETAIL_VALUE1 = '" + newStatus + "' " + " where DETAIL_ID =  " + detail_id;
            }

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand())
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (IDbCommand commandText = database.CreateCommand(SQLstring, connection))
                            {
                                commandText.ExecuteNonQuery();
                            }
                            scope.Complete();
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.Log(ex.Message, "Erro ao updateStatusMaintenanceNote" + "///" + command.CommandText);
                    }
                }
            }
        }

        public MaintenanceNoteModel getNoteStatusFromActionId(string action_id, string entity_id, string maintenanceNoteCode1, string maintenanceNoteStatus1)
        {

            List<MaintenanceNoteModel> lstReturn = new List<MaintenanceNoteModel>();
            string SQLstring = "select action_id, (select detail_value1 from detailalpha DD where DD.DETAIL_TYPE_ID=" + maintenanceNoteCode1 + "   and dd.entity_id = " + entity_id + " and DD.action_id = parameter.action_id) maintenanceNoteCode, (select detail_value1 from detailalpha DD where DD.DETAIL_TYPE_ID=" + maintenanceNoteStatus1 + " and dd.entity_id = " + entity_id + " and DD.action_id = parameter.action_id) maintenanceNoteStatus" + "  from " + " (select " + action_id + " as action_id" + "  from dual) parameter ";

            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        lstReturn = new ReflectionPopulator<MaintenanceNoteModel>().CreateList(reader);
                    }
                }
            }
            return lstReturn.FirstOrDefault();

        }

        public List<MaintenanceNoteStatusModel> getNotesToUpdateStatus(int detail_type_id_num_nota, int detail_type_id_sts_nota)
        {

            var lstReturn = new List<MaintenanceNoteStatusModel>();
            string SQLstring = "select detail_id as detail_id_status_nota, detail_value1 as status_nota, (select max(detail_value1) from detailalpha  where entity_id = da.entity_id  and action_id  = da.action_id and detail_type_id = " + detail_type_id_num_nota + " ) as numero_nota from detailalpha da  where da.detail_type_id = " + detail_type_id_sts_nota + "  and ( da.detail_value1 like '%MSPN%'  or da.detail_value1 like '%MSPR%') ";
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        lstReturn = new ReflectionPopulator<MaintenanceNoteStatusModel>().CreateList(reader);
                    }
                }
            }
            return lstReturn;
        }

    }
}
