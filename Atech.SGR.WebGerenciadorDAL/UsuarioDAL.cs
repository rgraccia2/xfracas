﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class UsuarioDAL: DataWorker
    {
        public void AtribuirPerfilUsuario(string CodUsuario,int CodPerfil)
        {
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand())
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (IDbCommand commandproc = database.CreateStoredProcCommand("ATECH_SGR_USUARIO.INSERIR_PERFIL_USUARIO", connection))
                            {
                                IDataParameter p_codUsuario = database.CreateParameter("P_COD_USUARIO", CodUsuario);
                                commandproc.Parameters.Add(p_codUsuario);

                                IDataParameter p_cod_perfil = database.CreateParameter("P_COD_PERFIL", CodPerfil);
                                commandproc.Parameters.Add(p_cod_perfil);

                                commandproc.ExecuteNonQuery();
                            }
                            scope.Complete();
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.Log(ex.Message, "Erro ao atribuir perfil ao usuário" + "///" + command.CommandText);
                    }
                }
            }
        }

        public void ExcluirPerfilUsuario(int Id_Usuario_Perfil)
        {
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand())
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (IDbCommand commandproc = database.CreateStoredProcCommand("ATECH_SGR_USUARIO.EXCLUIR_PERFIL_USUARIO", connection))
                            {
                                IDataParameter p_codUsuario = database.CreateParameter("P_ID", Id_Usuario_Perfil);
                                commandproc.Parameters.Add(p_codUsuario);


                                commandproc.ExecuteNonQuery();
                            }
                            scope.Complete();
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.Log(ex.Message, "Erro ao excluir perfil ao usuário" + "///" + command.CommandText);
                    }
                }
            }
        }

        public bool Authenticar(string CodUsuario,string Tipo,string Senha)
        {
            bool retorno = false;
            List<Usuario> lstUsuario = new List<Usuario>();
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateStoredProcCommand("ATECH_SGR_USUARIO.AUTENTICAR_ADMINISTRADOR", connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    command.Parameters.Add(new OracleParameter("P_COD_USUARIO", CodUsuario));
                    command.Parameters.Add(new OracleParameter("P_TIPO", Tipo));
                    command.Parameters.Add(new OracleParameter("P_SENHA", Senha));
                    
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        lstUsuario = new ReflectionPopulator<Usuario>().CreateList(reader);
                    }
                }
            }
            if(lstUsuario.Count > 0)
            {
                retorno = true;
            }
            return retorno;
        }


        public bool UsuarioExiste(string CodUsuario)
        {
            bool retorno = false;
            List<Usuario> lstUsuario = new List<Usuario>();
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateStoredProcCommand("ATECH_SGR_USUARIO.USUARIO_EXISTE", connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    command.Parameters.Add(new OracleParameter("P_COD_USUARIO", CodUsuario));

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        lstUsuario = new ReflectionPopulator<Usuario>().CreateList(reader);
                    }
                }
            }
            if (lstUsuario.Count > 0)
            {
                retorno = true;
            }
            return retorno;
        }

        public List<Usuario> ListarUsuariosComPerfil()
        {
            List<Usuario> lstUsuario = new List<Usuario>();
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateStoredProcCommand("ATECH_SGR_USUARIO.LISTAR_USUARIOS_COM_PERFIL", connection))
                {
                    OracleParameter param = new OracleParameter();
                    param.OracleType = OracleType.Cursor;
                    param.ParameterName = "io_cursor";
                    param.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        lstUsuario = new ReflectionPopulator<Usuario>().CreateList(reader);
                    }
                }
            }
            return lstUsuario;
        }


        public void UpdateUsuarioAtivo(int Id_Usuario_Perfil, string ativo )
        {
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand())
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (IDbCommand commandproc = database.CreateStoredProcCommand("ATECH_SGR_USUARIO.UPDATE_USUARIO_ATIVO", connection))
                            {
                                IDataParameter p_codUsuario = database.CreateParameter("P_ID", Id_Usuario_Perfil);
                                commandproc.Parameters.Add(p_codUsuario);

                                IDataParameter p_ativo = database.CreateParameter("P_ATIVO", ativo);
                                commandproc.Parameters.Add(p_ativo);

                                commandproc.ExecuteNonQuery();
                            }
                            scope.Complete();
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.Log(ex.Message, "Erro ao excluir perfil ao usuário" + "///" + command.CommandText);
                    }
                }
            }
        }
    }
}
