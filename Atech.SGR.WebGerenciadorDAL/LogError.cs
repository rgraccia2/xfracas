﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public static class LogError
    {
        public static void Log(string logMessage, string TipoLog)
        {
            StreamWriter Arq;

            string vArquivo = ConfigurationManager.AppSettings["CaminhoLog"].ToString();

            Arq = File.AppendText(vArquivo.Insert(vArquivo.IndexOf("."), "_" + string.Format("{0:yyyy_MM_dd}", DateTime.Now)));

            Arq.Write("\r\n" + TipoLog);

            Arq.Write("\r\n" + new string('-', 200) + "\r\nData: " + DateTime.Now.ToString() + ": " + logMessage);

            Arq.Close();

            Arq.Dispose();
        }
    }
}
