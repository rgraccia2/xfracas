﻿using Compass.XFracas.WebGerenciadorInterface.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Compass.XFracas.WebGerenciadorDAL
{
    public class ClasseFalhaDAL : DataWorker
    {

        public List<ClasseFalhaModel> GetClasseFalha()
        {
            try
            {
                var result = new List<ClasseFalhaModel>();
                DataTable dt = new DataTable();

                string SQLstring = "with incident as  (select * from rds_N0FXFRAV), classeFalha as (select * from RDS_N0XXFRAV) select *  from incident  i, classeFalha c where i.COD_PERFILCATALOGO = c.DESC_PERFILCATALOGO  and i.DESC_SISTEMA  = c.DESC_SISTEMA   and i.DESC_CONJUNTO  = c.DESC_CONJUNTO and i.DESC_ITEM  = c.DESC_ITEM   and i.DESC_PROBLEMA  = c.DESC_PROBLEMA  and i.DESC_SOLUCAO  = c.DESC_SOLUCAO";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            result = new ReflectionPopulator<ClasseFalhaModel>().CreateList(reader);
                            dt.Load(reader);
                        }
                    }
                }

                var f = new StreamWriter(@"c:\ViewClasseDeFalhas.txt", false);

                foreach (DataColumn col in dt.Columns)
                    f.Write(col.ColumnName + '\t');
                f.Write(System.Environment.NewLine);
                foreach (DataRow linha in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        f.Write(linha[col.ColumnName].ToString() + '\t');
                        f.Write(System.Environment.NewLine);
                    }
                }

                f.Close();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClasseFalhaModel> GetClasseFalha(DateTime dataRefInicial)
        {
            try
            {
                var result = new List<ClasseFalhaModel>();
                string SQLstring = "with incident as    (select * from rds_N0FXFRAV)," + 
                    "     classeFalha as (select * from RDS_N0XXFRAV)" + 
                    "select * " + "  from incident    i," + 
                    "       classeFalha c" + 
                    " where i.COD_PERFILCATALOGO = c.DESC_PERFILCATALOGO  " + 
                    "   and i.DESC_SISTEMA        = c.DESC_SISTEMA   " + 
                    "   and i.DESC_CONJUNTO       = c.DESC_CONJUNTO  " + 
                    "   and i.DESC_ITEM           = c.DESC_ITEM      " + 
                    "   and i.DESC_PROBLEMA       = c.DESC_PROBLEMA  " + 
                    "   and i.DESC_SOLUCAO        = c.DESC_SOLUCAO   " + 
                    "union all                                       " + 
                    " select * from RDS_N0XXFRAV where rownum < 80   ";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            result = new ReflectionPopulator<ClasseFalhaModel>().CreateList(reader);
                        }
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ClasseFalhaModel> GetClasseFalha_ORACLE()
        {
            try
            {

                DataTable dt = new DataTable();
                var result = new List<ClasseFalhaModel>();
                string SQLstring = "select * from ClasseFalha";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            result = new ReflectionPopulator<ClasseFalhaModel>().CreateList(reader);
                            dt.Load(reader);
                        }
                    }
                }

                var f = new StreamWriter(@"c:\ViewClasseDeFalhas.txt", false);
                foreach (DataColumn col in dt.Columns)
                    f.Write(col.ColumnName + '\t');
                f.Write(System.Environment.NewLine);
                foreach (DataRow linha in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                        f.Write(linha[col.ColumnName].ToString() + '\t');
                    f.Write(System.Environment.NewLine);
                }
                f.Close();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ClasseFalhaModel> GetClasseFalha_ORACLE(DateTime dataRefInicial)
        {
            try
            {
                var result = new List<ClasseFalhaModel>();
                string SQLstring = "with classeFalha as (select * from RDS_N0XXFRAV where idioma IN ('PT,'EN'))" + "select * " + "  from classeFalha c" + " where (DATA_ULTIMAATUALIZACAO > to_date('" + dataRefInicial.ToString("dd/MM/yyyy HH:mm:ss") + "','dd/mm/yyyy hh24:mi:ss') " + " or DATA_CRIACAO > to_date('" + dataRefInicial.ToString("dd/MM/yyyy HH:mm:ss") + "','dd/mm/yyyy hh24:mi:ss') ) ";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            result = new ReflectionPopulator<ClasseFalhaModel>().CreateList(reader);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ClasseFalhaModel> GetClasseFalha_SAP()
        {
            try
            {
                var result = new List<ClasseFalhaModel>();
                DataTable dt = new DataTable();
                string SQLstring = "select * from \"_SYS_BIC\".\"HIW_PRD.E.SISTEMAS.XFRACAS/XFRACAS_CV_001_CF\";";


                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            result = new ReflectionPopulator<ClasseFalhaModel>().CreateList(reader);
                            dt.Load(reader);
                        }
                    }
                }


                var f = new StreamWriter(@"c:\ViewClasseDeFalhas.txt", false);
                foreach (DataColumn col in dt.Columns)
                    f.Write(col.ColumnName + '\t');
                f.Write(System.Environment.NewLine);
                foreach (DataRow linha in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        f.Write(linha[col.ColumnName].ToString() + '\t');
                        f.Write(System.Environment.NewLine);
                    }
                }

                f.Close();

                return result;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClasseFalhaModel> GetClasseFalha_SAP(DateTime dataRefInicial)
        {

            try
            {
                var result = new List<ClasseFalhaModel>();

                string SQLstring;
                SQLstring = "   select top 10 * from  _SYS_BIC .HIW_PRD.E.SISTEMAS.XFRACAS/XFRACAS_CV_001_CF";
                SQLstring += "  where idioma In ('PT','EN')";
                SQLstring += "  and ( DATA_ULTIMAATUALIZACAO > '" + dataRefInicial.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                SQLstring += "  or DATA_CRIACAO > '" + dataRefInicial.ToString("yyyy-MM-dd HH:mm:ss") + "' )";

                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    using (IDbCommand command = database.CreateCommand(SQLstring, connection))
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleType = OracleType.Cursor;
                        param.ParameterName = "io_cursor";
                        param.Direction = ParameterDirection.Output;
                        command.Parameters.Add(param);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            result = new ReflectionPopulator<ClasseFalhaModel>().CreateList(reader);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            
        }

    }
}
